<?php

namespace App\Console\Commands;

use App\Helpers\BioIdHashHelper;
use App\Http\Services\HttpService;
use App\Model\CompanyModel;
use Exception;
use GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Illuminate\Console\Command;

class BioIdCompaniesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:companies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create companies in BIOID application';

    /**
     * variable for create csv error
     *
     * @var array
     */
    private $csv = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param CompanyModel $model
     * @return mixed
     */
    public function handle(CompanyModel $model)
    {
        $companies = $model->getCompanies();
        foreach ($companies as $company) {
            $func = 'alert';
            $str = sprintf('Company %s not created.', $company->name);

            $return = $this->createCompanyInBioId($company);
            if ($return) {
                if ($model->setReferenceBioId($return)) {
                    $func = 'info';
                    $str = sprintf('Company %s for reference bioid %s created successfully.'
                        , $return->name
                        , $return->reference
                    );
                }
            }

            $this->$func($str);
        }

        $this->csv($this->csv);
    }

    private function createCompanyInBioId($company)
    {
        $url = sprintf('%s%s', env('BIOID_API_URI'), '/api/v1/companies');
        $authorization = BioIdHashHelper::generate($url, 'POST');

        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => "{$authorization}"
        ];

        $fields = [
            "company" => [
                "identifier_value" => "{$company->register}",
                "identifier_kind" => "cnpj",
                "social_name" => "{$company->name}",
                "commercial_name" => "{$company->name}",
                "kind" => "buyer"
            ],
            "plan_uid" => "plannexo_small_business"
        ];

        $client = new Client(['base_uri' => env('BIOID_API_URI')]);

        try {
            $response = (new HttpService($client))
                ->method('POST')
                ->setHeaders($headers)
                ->setPath($url)
                ->setData($fields)
                ->send();

            if ($response->getStatusCode() !== 200) {
                throw new Exception('Company create failed', 400);
            }

            $body = $response->getContents()->decode();
            $company->reference = (int) $body['company']['id'];

            return $company;
        } catch (Exception | ClientErrorResponseException $exp) {
            array_push($this->csv, [$company->id, $company->register, trim($exp->getMessage())]);
            return false;
        }
    }

    private function csv($contents)
    {
        if (count($contents) < 1) {
            return false;
        }

        $path = sprintf('../resources/csv/companies_%s_errors.csv', date('Ymd'));

        $filename = app_path($path);
        $header = ['company_id', 'cnpj', 'error'];

        $output = fopen($filename, 'w');
        fputcsv($output, $header, ';');

        foreach ($contents as $content) {
            fputcsv($output, $content, ';');
        }

        fclose($output);
    }
}
