<?php

namespace App\Console\Commands;

use App\Domain\Auth\BioAssigner\BioAssignerAuthToken;
use App\Http\Services\HttpService;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Console\Command;

class BioIdPersonaCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bioid:persona
            {user : UserId for BIOID.}
            {company : CompanyId references to BIOID in the Plannexo.}
            {--act|action="create" : The action of persona (optional);}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cria persona via api na aplicação BIOID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = trim($this->argument('user'));
        if (!$user or is_null($user) or empty($user)) {
            $this->error('Not enough arguments (missing: "user").');
            return false;
        }

        $company = trim($this->argument('company'));
        if (!$company or is_null($company) or empty($company)) {
            $this->error('Not enough arguments (missing: "company").');
            return false;
        }

        $url = sprintf('%s%s', env('BIOID_API_URI') , '/api/v1/personas');
        $token = $this->createToken($url, 'POST');

        $payload = [
            'persona' => [
                'user_id' => (int) $user,
//                'company_id' => (int) $company,
            ],
            'acl_profile_uid' => 'plannexo'
        ];

        try {
            $client = new Client(['base_uri' => env('BIOID_API_URI')]);
            $response = (new HttpService($client))
                ->method('POST')
                ->setHeaders($this->headers($token))
                ->setPath($url)
                ->setData($payload)
                ->send();

            if ($response->getStatusCode() !== 200) {
                throw new Exception('Persona create failed', 422);
            }

            $body = $response->getContents()->decode();
            $this->info(json_encode([
                'success' => true,
                'user_id_reference' => $body['id'],
                'company_id_reference' => $body['company']['id'],
            ]));
        } catch (Exception | ClientException | ClientErrorResponseException $exp) {
            $this->info(json_encode([
                'success' => false,
                'message' => $exp->getMessage(),
                'user' => $user,
                'company' => $company,
            ]));
        }
    }

    private function createToken($url, $method = 'POST')
    {
        $bioAssignerAuthToken = new BioAssignerAuthToken();
        $bioAssignerAuthToken->setMethod($method);
        $bioAssignerAuthToken->setUrl($url);

        return $bioAssignerAuthToken->generateToken()->token();
    }

    private function headers(string $token)
    {
        return [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => "{$token}"
        ];
    }
}
