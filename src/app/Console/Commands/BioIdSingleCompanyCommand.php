<?php

namespace App\Console\Commands;

use App\Helpers\BioIdHashHelper;
use App\Http\Services\HttpService;
use App\Model\CompanyModel;
use Exception;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class BioIdSingleCompanyCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:company {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create company by id in BIOID application';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param CompanyModel $model
     * @return mixed
     */
    public function handle(CompanyModel $model)
    {
        $company_id = trim($this->argument('id'));
        if (!$company_id or is_null($company_id) or empty($company_id)) {
            $this->error('Not enough arguments (missing: "company id").');
            return false;
        }

        $company = $model->getCompany($company_id);

        $func = 'alert';
        $str = sprintf('Company %s not created.', $company->name);

        $return = $this->createCompanyInBioId($company);
        if ($return) {
            if ($model->setReferenceBioId($return)) {
                $func = 'info';
                $str = sprintf('Company %s for reference bioid %s created successfully.'
                    , $return->name
                    , $return->reference
                );
            }
        }

        $this->$func($str);
    }

    private function createCompanyInBioId($company)
    {
        $csv = [];
        $url = sprintf('%s%s', env('BIOID_API_URI'), '/api/v1/companies');
        $authorization = BioIdHashHelper::generate($url, 'POST');

        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => "{$authorization}"
        ];

        $fields = [
            "company" => [
                "identifier_value" => "{$company->register}",
                "identifier_kind" => "cnpj",
                "social_name" => "{$company->name}",
                "commercial_name" => "{$company->name}",
                "kind" => "buyer",
                "status" => 'active'
            ],
            "plan_uid" => "plannexo_small_business",
        ];

        $client = new Client(['base_uri' => env('BIOID_API_URI')]);
        try {
            $response = (new HttpService($client))
                ->method('POST')
                ->setHeaders($headers)
                ->setPath($url)
                ->setData($fields)
                ->send();

            if ($response->getStatusCode() !== 200) {
                throw new Exception('Company create failed', 400);
            }

            $body = $response->getContents()->decode();
            $company->reference = (int) $body['company']['id'];

            return $company;
        } catch (Exception | ClientErrorResponseException $exp) {
            #$this->error($exp->getMessage());
            array_push($csv, [$company->id, $company->register, trim($exp->getMessage())]);
            $this->csv($csv);
            return false;
        }
    }

    private function csv($contents)
    {
        if (count($contents) < 1) {
            return false;
        }

        $path = sprintf('../resources/csv/company_%s_errors.csv', date('Ymd'));
        $header = ['company_id', 'cnpj', 'error'];
        $filename = app_path($path);

        if (!file_exists($filename)) {
            $output = fopen($filename, 'a+');
            fputcsv($output, $header, ';');
        }

        $output = fopen($filename, 'a+');
        foreach ($contents as $content) {
            fputcsv($output, $content, ';');
        }

        fclose($output);
    }
}
