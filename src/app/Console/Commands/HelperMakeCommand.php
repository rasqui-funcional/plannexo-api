<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class HelperMakeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:helper {helper_name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a helper Class';

    /**
     * The path service
     *
     * @var string
     */
    private $path = 'Helpers';

    /**
     * Create a new command instance.
     *
     * @param Filesystem $files
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();
        $this->files = $files;
    }

    protected function getStub()
    {
        return __DIR__ . '/stubs/helper.stub';
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $helper = trim($this->argument('helper_name'));

        if (!$helper or is_null($helper) or empty($helper)) {
            $this->error('Not enough arguments (missing: "helper_name").');
            return false;
        }

        if (!file_exists(app_path($this->path)))
            mkdir(app_path($this->path), 0777);

        $this->createService($helper);
    }

    private function createService($helper_name)
    {
        $helper_name = $this->checkServiceName($helper_name);

        $file = sprintf('%s.php', $helper_name);

        if ($this->files->exists(app_path("{$this->path}/" . $file))) {
            $this->error('Helper already exists!');
            return false;
        }

        $original = $this->files->get($this->getStub());
        $original = str_replace('NameHelper', ucfirst($helper_name), $original);

        $this->files->put(app_path("{$this->path}/" . sprintf('%s.php', $helper_name)), $original);

        $this->info('Helper created successfully.');
        return true;
    }

    private function checkServiceName($name)
    {
        if (strpos($name, 'Helper'))
            return ucfirst($name);
        else if (strpos($name, 'helper'))
            return ucfirst(str_replace('helper', 'Helper', $name));
        else
            return ucfirst($name) . 'Helper';
    }
}
