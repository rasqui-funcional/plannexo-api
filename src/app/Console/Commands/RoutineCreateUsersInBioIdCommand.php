<?php

namespace App\Console\Commands;

use App\Domain\Auth\BioAssigner\BioAssignerAuthToken;
use App\Helpers\PasswordHashHelper;
use App\Http\Services\HttpService;
use App\Model\UserModel;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Console\Command;
use stdClass;

class RoutineCreateUsersInBioIdCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // protected $signature = 'users:migrate {--uid|userId=0}, {--cid|companyId=0}';
    protected $signature = 'users:migrate';
    // protected $signature = 'users:migrate {companyId}';
    // protected $signature = 'users:migrate {companyId? : Company Id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cria os usuarios apartir ou não da company';

    /**
     * Password default
     *
     * @var string
     */
    protected string $password = 's6PCtYKbtW8YFe4t';

    /**
     * @var int
     */
    protected int $success = 0;

    /**
     * @var int
     */
    protected int $errors = 0;

    /**
     * @var array
     */
    protected array $csv_error = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param UserModel $model
     * @return mixed
     */
    public function handle(UserModel $model)
    {
//        $decode = PasswordHashHelper::decode('VQNwZSNUZ1NaVWrqDurK');
//        $nencode = hash('sha256', $decode);
//        dd($decode, $nencode);

        // dd(PasswordHashHelper::encode('s6PCtYKbtW8YFe4t'));
        // $options = $this->option(); dd($options);
        // dd($this->argument('companyId')); // argumnto opcional

        $company_id = false;
        if ($this->confirm('Migração de usuários por company?', true)) {
            $company_id = $this->ask('Informe o ID da Company?');
        }

        $csv = [];
        $users = $model->getUsersByCompanyId($company_id);
        //dd($users);
        foreach ($users as $user) {
            $user->pass = ($user->id == 1) ? $this->password : $this->decodePassword($user->pass);

            $payload = $this->getFirstAndLastName($user->name, $user->login);
            $payload->user = $user;

            $create = $this->createUserBioId($payload);
            if (!$create) {
                $this->errors++;
                continue;
            }

            array_push($csv, [$create['user_id'], $user->pass]); // para o bioid

            $update = $model->updateBioIdReference($create, $user);
            if (!$update) {
                $this->errors++;
                continue;
            }

            $this->success++;
        }

        $this->csv($csv);
        $this->csv_error($this->csv_error);

        $this->info("Users migrates for success: {$this->success}");
        $this->info("Users migrates for errors: {$this->errors}");
        return;
    }

    private function getFirstAndLastName(string $name, string $default = null)
    {
        $parties = explode(' ', $name);

        $obj = new stdClass();
        $obj->first_name = array_shift($parties);
        $obj->last_name = array_pop($parties);

        $obj->first_name ??= $default;
        $obj->last_name ??= $default;
        return $obj;
    }

    private function decodePassword(?string $password)
    {
        if (is_null($password)) {
            return rand(50000, 100000);
        }

        return PasswordHashHelper::decode($password);
    }

    /**
     * TODO: refatorar e tratar erros
     *
     * @param object $object
     * @return mixed
     */
    private function createUserBioId(object $object)
    {
        $client = new Client(['base_uri' => env('BIOID_API_URI')]);

        /** Fluxo de cadastro de usuario **/
        $user_uri = sprintf('%s%s', env('BIOID_API_URI') , '/api/v1/users'); //dd($user_uri);
        #$this->info(sprintf('url - %s', $user_uri));

        $token = $this->createToken($user_uri, 'POST');
        #$this->info(sprintf('authorization - %s', $token));

        $user_fields = [
            'user' => [
                'first_name' => $object->first_name,
                'last_name' => $object->last_name,
                'email' => $object->user->email,
            ],
        ];

        if (strlen($object->user->pass) > 7) {
            $user_fields['user']['activated'] ="true";
            $user_fields['user']['password'] = $object->user->pass;
            $user_fields['user']['skip_confirmation'] ="true";
        }
        #$this->info(sprintf('payload - %s', json_encode($user_fields)));

        try {
            $process = 'cad_user';
            $user_response = (new HttpService($client))
                ->method('POST')
                ->setHeaders($this->headers($token))
                ->setPath($user_uri)
                ->setData($user_fields)
                ->send();

            if ($user_response->getStatusCode() !== 200) {
                throw new Exception('User create failed', 422);
            }

            $u_body = $user_response->getContents()->decode();

            /** Fluxo de cadastro da persona **/
            $persona_uri = sprintf('%s%s', env('BIOID_API_URI') , '/api/v1/personas');
            $token2 = $this->createToken($persona_uri, 'POST');

            $persona_fields = [
                'persona' => [
                    'user_id' => (int) $u_body['id'],
                    'company_id' => (int) $object->user->company_bioid_reference,
                ],
                'acl_profile_uid' => 'plannexo'
            ];

            $process = "cad_persona_[{$u_body['id']}]";
            $persona_response = (new HttpService($client))
                ->method('POST')
                ->setHeaders($this->headers($token2))
                ->setPath($persona_uri)
                ->setData($persona_fields)
                ->send();

            if ($persona_response->getStatusCode() !== 200) {
                throw new Exception('Persona create failed', 422);
            }

            return $persona_response->getContents()->decode();
        } catch (Exception | ClientException $exp) {
            array_push($this->csv_error, [
                $object->user->id, $object->user->id_company_fk, trim($exp->getMessage()), $process
            ]);

            $this->info(sprintf('response - %s', trim($exp->getMessage())));
            return false;
        }
    }

    private function createToken($url, $method = 'POST')
    {
        $bioAssignerAuthToken = new BioAssignerAuthToken();
        $bioAssignerAuthToken->setMethod($method);
        $bioAssignerAuthToken->setUrl($url);

        return $bioAssignerAuthToken->generateToken()->token();
    }

    private function headers(string $token)
    {
        return [
            'Content-Type' => 'application/json',
            'Content-Length' => 180,
            'Accept' => 'application/json',
            'Authorization' => "{$token}"
        ];
    }

    private function csv($contents)
    {
        if (count($contents) < 1) {
            return false;
        }

        $path = sprintf('../resources/csv/users_%s.csv', date('Ymd'));
        $header = ['user_id', 'password'];
        $filename = app_path($path);

        if (!file_exists($filename)) {
            $output = fopen($filename, 'a+');
            fputcsv($output, $header, ';');
        }

        $output = fopen($filename, 'a+');
        foreach ($contents as $content) {
            fputcsv($output, $content, ';');
        }

        fclose($output);
        return true;
    }

    private function csv_error($contents)
    {
        if (count($contents) < 1) {
            return false;
        }

        $path = sprintf('../resources/csv/users_%s_errors.csv', date('Ymd'));
        $header = ['user_id', 'pl_company_id', 'message_error', 'process'];
        $filename = app_path($path);

        if (!file_exists($filename)) {
            $output = fopen($filename, 'a+');
            fputcsv($output, $header, ';');
        }

        $output = fopen($filename, 'a+');
        foreach ($contents as $content) {
            fputcsv($output, $content, ';');
        }

        fclose($output);
        return true;
    }
}
