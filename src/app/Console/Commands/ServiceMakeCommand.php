<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class ServiceMakeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:service {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Service';

    /**
     * The path service
     *
     * @var string
     */
    private $path = 'Http/Services';

    /**
     * Create a new command instance.
     *
     * @param Filesystem $files
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();
        $this->files = $files;
    }

    protected function getStub()
    {
        return __DIR__ . '/stubs/service.stub';
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = trim($this->argument('name'));

        if (!$name or is_null($name) or empty($name)) {
            $this->error('Not enough arguments (missing: "name").');
            return false;
        }

        if (!file_exists(app_path($this->path)))
            mkdir(app_path($this->path), 0777);

        $this->createService($name);
    }

    private function createService($name)
    {
        $name = $this->checkServiceName($name);

        $file = sprintf('%s.php', $name);

        if ($this->files->exists(app_path("{$this->path}/" . $file))) {
            $this->error('Service already exists!');
            return false;
        }

        $original = $this->files->get($this->getStub());
        $original = str_replace('NameService', ucfirst($name), $original);

        $this->files->put(app_path("{$this->path}/" . sprintf('%s.php', $name)), $original);

        $this->info('Service created successfully.');
        return true;
    }

    private function checkServiceName($name)
    {
        if (strpos($name, 'Service'))
            return ucfirst($name);
        else if (strpos($name, 'service'))
            return ucfirst(str_replace('service', 'Service', $name));
        else
            return ucfirst($name) . 'Service';
    }
}
