<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $method = 'GET';
        $command = 'python';
        $path = app_path('../crypto/generate_hash.py');
        $url = 'https://bioid.qa.cloud.bionexo.com.br/api/v1/companies';

        var_dump(sprintf('%s %s %s %s', $command, $path, $url, $method));

        echo shell_exec(sprintf('%s %s %s %s', $command, $path, $url, $method));
    }
}
