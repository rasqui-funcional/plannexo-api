<?php
declare(strict_types=1);

namespace App\Domain\AF;

/**
 * Interface CompanySuppliersInterface
 */
interface CompanySuppliersInterface
{
    /**
     * @param int $id_company_fk
     * @return mixed
     */
    public function createCompanySuppliers(int $id_company_fk = 0);
}
