<?php
declare(strict_types=1);
namespace App\Domain\Auth;

/**
 * Interface AuthToken
 * @package App\Domain\Auth
 */
interface AuthToken
{


    /**
     * @return $this
     */
    public function generateToken(): self;

    /**
     * @return mixed
     */
    public function token();

}
