<?php
declare(strict_types=1);
namespace App\Domain\Auth;
/**
 * Interface Authenticate
 * @package App\Domain\Auth
 */
interface Authenticate
{

    /**
     * @return bool
     */
    public function authenticate(): bool;

}
