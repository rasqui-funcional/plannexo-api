<?php


namespace App\Domain\Auth\BioAssigner;

use App\Domain\Auth\AuthToken;
use App\Helpers\BioIdHashHelper;

class BioAssignerAuthToken implements AuthToken
{
    /**
     * @var
     */
    private $token;

    /**
     * @var string
     */
    private string $url;

    /**
     * @var string
     */
    private string $method;

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     */
    public function setMethod(string $method): void
    {
        $this->method = $method;
    }

    /**
     * @inheritDoc
     */
    public function generateToken(): AuthToken
    {
        $this->token = BioIdHashHelper::generate($this->url, $this->method);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function token()
    {
        return $this->token;
    }
}
