<?php
declare(strict_types=1);

namespace App\Domain\Auth\BioId;


use League\OAuth2\Client\Provider\GenericProvider;

/**
 * Class BioId
 * @package App\Domain\Auth\BioId
 */
class BioId extends GenericProvider
{

    /**
     * BioId constructor.
     */
    public function __construct()
    {
        parent::__construct([
            'clientId' => env('BIOID_CLIENT_ID'),
            'clientSecret' => env('BIOID_CLIENT_SECRET'),
            'redirectUri' => env('BIOID_REDIRECT_URI'),
            'urlAuthorize' => env('BIOID_URL_AUTHORIZE'),
            'urlAccessToken' => env('BIOID_URL_ACCESS_TOKEN'),
            'urlResourceOwnerDetails' => env('BIOID_URL_RESOURCE_OWNER_DETAILS'),
        ]);
    }

}
