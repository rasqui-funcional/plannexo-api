<?php
declare(strict_types=1);

namespace App\Domain\Auth\BioId;

use App\Domain\Auth\AuthToken;
use League\OAuth2\Client\Provider\GenericProvider;

/**
 * Class BioIdAuthToken
 * @package App\Domain\Auth\BioId
 */
class BioIdAuthToken implements AuthToken
{
    /**
     * @var
     */
    private $token;
    /**
     * @var string
     */
    private $code;
    /**
     * @var BioId
     */
    private $auth;

    /**
     * BioIdAuthToken constructor.
     * @param BioId $auth
     */
    public function __construct(BioId $auth)
    {
        $this->auth = $auth ?: new BioId();
    }

    /**
     * @inheritDoc
     * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
     */
    public function generateToken(): AuthToken
    {
        $token = $this->auth->getAccessToken('authorization_code', ['code' => $this->code]);
        $this->token = $token;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function token()
    {
        return $this->token;
    }

    /**
     * @param string $code
     * @return BioIdAuthToken
     */
    public function setCode(string $code): BioIdAuthToken
    {
        $this->code = $code;
        return $this;
    }

}
