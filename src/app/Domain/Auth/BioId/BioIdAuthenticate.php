<?php
declare(strict_types=1);

namespace App\Domain\Auth\BioId;

use App\Domain\Auth\Authenticate;
use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Token\AccessTokenInterface;

/**
 * Class BioIdAuthenticate
 * @package App\Domain\Auth\BioId
 */
class BioIdAuthenticate implements Authenticate
{


    /**
     * @var AccessTokenInterface
     */
    private $token;
    /**
     * @var GenericProvider
     */
    public $auth;

    /**
     * BioIdAuthenticate constructor.
     * @param AccessTokenInterface $token
     */
    public function __construct(AccessTokenInterface $token = null)
    {
        $this->auth = new GenericProvider([
            'clientId' => getenv('BIOID_CLIENT_ID'),
            'clientSecret' => getenv('BIOID_CLIENT_SECRET'),
            'redirectUri' => getenv('BIOID_REDIRECT_URI'),
            'urlAuthorize' => getenv('BIOID_URL_AUTHORIZE'),
            'urlAccessToken' => getenv('BIOID_URL_ACCESS_TOKEN'),
            'urlResourceOwnerDetails' => env('BIOID_URL_RESOURCE_OWNER_DETAILS'),
        ]);
        $this->token = $token;
    }

    /**
     * @inheritDoc
     */
    public function authenticate(): bool
    {
        return !$this->token->hasExpired();
    }

    /**
     * @return string
     */
    public function getAuthorizationUrl(){

        return $this->auth->getAuthorizationUrl();
    }
}
