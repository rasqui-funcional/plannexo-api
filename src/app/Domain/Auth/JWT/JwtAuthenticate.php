<?php
declare(strict_types=1);

namespace App\Domain\Auth\JWT;

use App\Domain\Auth\Authenticate;
use App\Domain\Company\Entity\Company;
use App\Domain\User\Entity\User;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\ValidationData;

/**
 * Class JwtAutenticate
 * @package App\Domain\Auth\JWT
 */
class JwtAuthenticate implements Authenticate
{

    /**
     * @var \Lcobucci\JWT\Token
     */
    public $token;
    /**
     * @var ValidationData
     */
    private $validate;
    /**
     * @var Sha256
     */
    private $signer;


    /**
     * JwtAutenticate constructor.
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->token = $token && $token != 'null' ? (new Parser())->parse($token) : new Parser();
        $this->validate = new ValidationData();
        $this->signer = new Sha256();
    }

    /**
     * @return bool
     */
    public function authenticate(): bool
    {

        $this->validate->setIssuer($this->token->getClaim('iss'));
        $this->validate->setAudience($this->token->getClaim('aud'));
        $this->validate->setId($this->token->getClaim('jti'));
        $this->validate->has($this->token->getClaim('uid'));
        return $this->token->validate($this->validate) && $this->token->verify($this->signer, getenv('JWT_SECRET'));
    }

    public function getCompany()
    {
        $company_id_fk = 0;
        $company_name = '';
        if (method_exists($this->token, 'getClaim')) {
            $company_id_fk = $this->token->getClaim('jti');
            $company_name = $this->token->getClaim('company_name');
        }

        return new Company([
            'id_company_pk' => (int)$company_id_fk,
            'company_name' => $company_name
        ]);
    }

    public function getUser()
    {
        $id_user_pk = 0;
        $id_company_fk = 0;
        $company_name = '';
        $user_name = '';
        if (method_exists($this->token, 'getClaim')) {
            $id_user_pk = $this->token->getClaim('uid', $id_user_pk);
            $id_company_fk = $this->token->getClaim('jti', $id_company_fk);
            $company_name = $this->token->getClaim('company_name', $company_name);
            $user_name = $this->token->getClaim('user_name', $user_name);
        }

        return new User([
            'id_user_pk' => (int)$id_user_pk,
            'id_company_fk' => (int)$id_company_fk,
            'id_company_pk' => (int)$id_company_fk,
            'company_name' => $company_name,
            'name' => $user_name
        ]);
    }

}
