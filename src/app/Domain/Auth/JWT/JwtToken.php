<?php
declare(strict_types=1);

namespace App\Domain\Auth\JWT;

use App\Domain\Auth\AuthToken;
use App\Domain\User\Entity\User;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Hmac;


/**
 * Class JwtToken
 * @package App\Domain\Auth\JWT
 */
class JwtToken implements AuthToken
{

    /**
     * The JWT Token generator.
     *
     * @var Builder
     */

    private $auth;
    /**
     * @var
     */
    private $token;

    /**
     * @var User
     */
    private User $user;

    /**
     *
     * @param Builder $auth
     *
     * @return void
     */
    public function __construct(User $user = null)
    {
        $this->auth = new Builder();
        $this->user = $user ?? new User();
    }

    /**
     * @return mixed
     */
    public function token()
    {
        return (string)$this->token;
    }

    /**
     * @return AuthToken
     */
    public function generateToken(): AuthToken
    {
        $time = time();
        $token = $this->auth->issuedBy(getenv('APP_URL'))
            ->permittedFor(getenv('MAIN_DOMAIN'))
            ->identifiedBy($this->user->getIdCompanyFk())
            ->withClaim('uid', $this->user->getIdUserPk())
            ->withClaim('company_name', $this->user->getCompany()->getCompanyName())
            ->withClaim('user_name', $this->user->getName())
            ->issuedAt($time)
            ->canOnlyBeUsedAfter($time)
            ->expiresAt($time + (3600 * 24)) # 24 hours
            ->getToken((new Hmac\Sha256()), new Key(getenv('JWT_SECRET')));

        $this->token = $token;

        return $this;
    }
}
