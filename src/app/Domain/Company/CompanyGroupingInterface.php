<?php
declare(strict_types=1);

namespace App\Domain\Company;

/**
 * Interface CompanyGroupingInterface
 * @package App\Domain\Company
 */
interface CompanyGroupingInterface
{
    /**
     * @return array
     */
    public function getCompanyGrouping(): array;

    /**
     * @param string $id
     * @return array
     */
    public function getCompanyGroupingById(string $id): array;
}
