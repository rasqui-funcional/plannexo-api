<?php
declare(strict_types=1);

namespace App\Domain\Company;

use App\Domain\Company\Entity\Company;

/**
 * Interface CompanyGroupsInterface
 * @package App\Domain\Company
 */
interface CompanyGroupsInterface
{

    /**
     * CompanyGroupsInterface constructor.
     * @param Company $company
     */
    public function __construct(Company $company);

    /**
     * @return array
     */
    public function getCompanyGroups(): array;

    /**
     * @param string $id
     * @return array
     */
    public function getCompanyGroup(string $id): array;

    /**
     * @param string $group_id
     * @return array
     */
    public function getCompanyGroupData(string $group_id): array;
}
