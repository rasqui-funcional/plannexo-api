<?php


namespace App\Domain\Company;


use App\Domain\Company\Entity\Company;

interface CompanyInterface
{
    /**
     * CompanyInterface constructor.
     * @param Company $company
     */
    public function __construct(Company $company);

    /**
     * @return mixed
     */
    public function getCompanies();

    /**
     * @param int $id
     * @return mixed
     */
    public function getCompany(int $id);

    /**
     * @return mixed
     */
    public function createCompany();

    /**
     * @param int $id
     * @return mixed
     */
    public function updateCompany(int $id);

    /**
     * @param int $id
     * @return mixed
     */
    public function deleteCompany(int $id);
}
