<?php
declare(strict_types=1);

namespace App\Domain\Company;

use App\Domain\Company\Entity\Company;

/**
 * Interface UserPoliciesInterface
 * @package App\Domain\Company
 */
interface CompanyPoliciesInterface
{

    /**
     * CompanyPoliciesInterface constructor.
     * @param Company $company
     */
    public function __construct(Company $company);

    /**
     * @return array
     */
    public function getCompanyPolicies(): array;
}
