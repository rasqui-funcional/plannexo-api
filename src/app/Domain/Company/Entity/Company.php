<?php

namespace App\Domain\Company\Entity;

class Company
{
    /**
     * @var int|null
     */
    private ?int $id_company_pk;

    /**
     * @var string
     */
    private string $company_name;

    /**
     * @var string
     */
    private string $company_reg;

    /**
     * @var string
     */
    private string $key;

    /**
     * @var string
     */
    private string $dtu;

    /**
     * @var int|null
     */
    private ?int $maintenance_mode;

    /**
     * @var string|null
     */
    private ?string $app_link;

    /**
     * @var string|null
     */
    private ?string $company_logo;

    /**
     * @var string|null
     */
    private ?string $auto_update;

    /**
     * @var string
     */
    private string $smtp_server; // mail.bionexo.com.br

    /**
     * @var string
     */
    private string $smtp_user; // infra@bionexo.com

    /**
     * @var string
     */
    private string $smtp_pass; // VQXGZSX7Z1BeVWBODurKDkXe

    /**
     * @var string
     */
    private string $smtp_email_address; // infra@bionexo.com

    /**
     * @var int
     */
    private int $smtp_port; // 25

    /**
     * @var int
     */
    private int $smtp_ssl; // 0

    /**
     * @var int
     */
    private int $base_test; // 1

    /**
     * @var int
     */
    private int $id_language_pk; // 1

    /**
     * @var int
     */
    private ?int $bioid_reference;

    public function __construct(array $data = [])
    {
        $this->setIdCompanyPk($data['id_company_pk'] ??= 0);
        $this->setCompanyName($data['company_name'] ??= '');
        $this->setCompanyReg($data['company_reg'] ??= '');
        $this->setKey($data['key'] ??= '');
        $this->setDtu($data['dtu'] ??= '');
        $this->setMaintenanceMode($data['maintenance_mode'] ??= null);
        $this->setAppLink($data['app_link'] ??= null);
        $this->setCompanyLogo($data['company_logo'] ??= null);
        $this->setAutoUpdate($data['auto_update'] ??= null);
        $this->setSmtpServer($data['smtp_server'] ??= '');
        $this->setSmtpUser($data['smtp_user'] ??= '');
        $this->setSmtpPass($data['smtp_pass'] ??= '');
        $this->setSmtpEmailAddress($data['smtp_email_address'] ??= '');
        $this->setSmtpPort($data['smtp_port'] ??= 25);
        $this->setSmtpSsl($data['smtp_ssl'] ??= 0);
        $this->setBaseTest($data['base_test'] ??= 1);
        $this->setIdLanguagePk($data['id_language_pk'] ??= 1);
        $this->setBioidReference($data['bioid_reference'] ??= null);
    }

    /**
     * @return int
     */
    public function getIdCompanyPk(): int
    {
        return $this->id_company_pk;
    }

    /**
     * @param int $id_company_pk
     */
    public function setIdCompanyPk(int $id_company_pk): void
    {
        $this->id_company_pk = $id_company_pk;
    }

    /**
     * @return string
     */
    public function getCompanyName(): string
    {
        return $this->company_name;
    }

    /**
     * @param string $company_name
     */
    public function setCompanyName(string $company_name): void
    {
        $this->company_name = $company_name;
    }

    /**
     * @return string
     */
    public function getCompanyReg(): string
    {
        return $this->company_reg;
    }

    /**
     * @param string $company_reg
     */
    public function setCompanyReg(string $company_reg): void
    {
        $this->company_reg = $company_reg;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey(string $key): void
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getDtu(): string
    {
        return $this->dtu;
    }

    /**
     * @param string $dtu
     */
    public function setDtu(string $dtu): void
    {
        $this->dtu = $dtu;
    }

    /**
     * @return int|null
     */
    public function getMaintenanceMode(): ?int
    {
        return $this->maintenance_mode;
    }

    /**
     * @param int|null $maintenance_mode
     */
    public function setMaintenanceMode(?int $maintenance_mode): void
    {
        $this->maintenance_mode = $maintenance_mode;
    }

    /**
     * @return string|null
     */
    public function getAppLink(): ?string
    {
        return $this->app_link;
    }

    /**
     * @param string|null $app_link
     */
    public function setAppLink(?string $app_link): void
    {
        $this->app_link = $app_link;
    }

    /**
     * @return string|null
     */
    public function getCompanyLogo(): ?string
    {
        return $this->company_logo;
    }

    /**
     * @param string|null $company_logo
     */
    public function setCompanyLogo(?string $company_logo): void
    {
        $this->company_logo = $company_logo;
    }

    /**
     * @return string|null
     */
    public function getAutoUpdate(): ?string
    {
        return $this->auto_update;
    }

    /**
     * @param string|null $auto_update
     */
    public function setAutoUpdate(?string $auto_update): void
    {
        $this->auto_update = $auto_update;
    }

    /**
     * @return string
     */
    public function getSmtpServer(): string
    {
        return $this->smtp_server;
    }

    /**
     * @param string $smtp_server
     */
    public function setSmtpServer(string $smtp_server): void
    {
        $this->smtp_server = $smtp_server;
    }

    /**
     * @return string
     */
    public function getSmtpUser(): string
    {
        return $this->smtp_user;
    }

    /**
     * @param string $smtp_user
     */
    public function setSmtpUser(string $smtp_user): void
    {
        $this->smtp_user = $smtp_user;
    }

    /**
     * @return string
     */
    public function getSmtpPass(): string
    {
        return $this->smtp_pass;
    }

    /**
     * @param string $smtp_pass
     */
    public function setSmtpPass(string $smtp_pass): void
    {
        $this->smtp_pass = $smtp_pass;
    }

    /**
     * @return string
     */
    public function getSmtpEmailAddress(): string
    {
        return $this->smtp_email_address;
    }

    /**
     * @param string $smtp_email_address
     */
    public function setSmtpEmailAddress(string $smtp_email_address): void
    {
        $this->smtp_email_address = $smtp_email_address;
    }

    /**
     * @return int
     */
    public function getSmtpPort(): int
    {
        return $this->smtp_port;
    }

    /**
     * @param int $smtp_port
     */
    public function setSmtpPort(int $smtp_port): void
    {
        $this->smtp_port = $smtp_port;
    }

    /**
     * @return int
     */
    public function getSmtpSsl(): int
    {
        return $this->smtp_ssl;
    }

    /**
     * @param int $smtp_ssl
     */
    public function setSmtpSsl(int $smtp_ssl): void
    {
        $this->smtp_ssl = $smtp_ssl;
    }

    /**
     * @return int
     */
    public function getBaseTest(): int
    {
        return $this->base_test;
    }

    /**
     * @param int $base_test
     */
    public function setBaseTest(int $base_test): void
    {
        $this->base_test = $base_test;
    }

    /**
     * @return int
     */
    public function getIdLanguagePk(): int
    {
        return $this->id_language_pk;
    }

    /**
     * @param int $id_language_pk
     */
    public function setIdLanguagePk(int $id_language_pk): void
    {
        $this->id_language_pk = $id_language_pk;
    }

    /**
     * @return int|null
     */
    public function getBioidReference(): ?int
    {
        return $this->bioid_reference;
    }

    /**
     * @param int|null $bioid_reference
     */
    public function setBioidReference(?int $bioid_reference): void
    {
        $this->bioid_reference = $bioid_reference;
    }
}
