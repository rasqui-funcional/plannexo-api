<?php
declare(strict_types=1);

namespace App\Domain\Company\Groups;

use App\Domain\Company\CompanyGroupingInterface;
use App\Domain\Company\CompanyGroupsInterface;
use App\Domain\Company\Traits\CompanyGroupsTrait;

/**
 * Class CompanyGrouping
 * @package App\Http\Repository
 */
class CompanyGrouping implements CompanyGroupingInterface
{

    use CompanyGroupsTrait {
        CompanyGroupsTrait::__construct as __CompanyGroupsTraitConstruct;
    }

    /**
     * @var array
     */
    private array  $groups = [];
    /**
     * @var array
     */
    private array  $groups_data = [];

    /**
     * CompanyGrouping constructor.
     * @param CompanyGroupsInterface $company_groups
     */
    public function __construct(CompanyGroupsInterface $company_groups)
    {
        $this->__CompanyGroupsTraitConstruct($company_groups);
    }


    /**
     * @return array
     * @throws \Exception
     */
    public function getCompanyGrouping(): array
    {
        try {
            $this->groups = $this->getCompanyGroups();
            return $this->buildGrouping();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param string $id
     * @return array
     * @throws \Exception
     */
    public function getCompanyGroupingById(string $id): array
    {
        try {
            $this->groups = $this->getCompanyGroup($id);
            return $this->buildGrouping();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @return array
     */
    public function buildGrouping(): array
    {
        return array_reduce($this->groups, array($this, 'formatData'), []);
    }


    /**
     * @param $groups
     * @param $group
     * @return array
     */
    private function formatData($groups, $group): array
    {
        $groups[$group->key] = [
            'title' => $group->title,
            'values' => $this->getCompanyGroupData($group->id)
        ];

        return $groups;
    }
}
