<?php
declare(strict_types=1);

namespace App\Domain\Company\Traits;

use App\Domain\Company\CompanyGroupingInterface;

/**
 * Trait CompanyGroupingTrait
 * @package App\Domain\Company\Traits
 */
trait CompanyGroupingTrait
{

    /**
     * @var CompanyGroupingInterface
     */
    private $company_grouping;

    /**
     * CompanyGroupingTrait constructor.
     * @param CompanyGroupingInterface $company_grouping
     */
    public function __construct(CompanyGroupingInterface $company_grouping)
    {
        $this->company_grouping = $company_grouping;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function getCompanyGrouping(): array
    {
        return $this->company_grouping->getCompanyGrouping();
    }
}
