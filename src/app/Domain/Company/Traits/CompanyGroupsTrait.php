<?php
declare(strict_types=1);

namespace App\Domain\Company\Traits;

use App\Domain\Company\CompanyGroupsInterface;
use App\Domain\Company\Entity\Company;

/**
 * Trait CompanyGroupsTrait
 * @package App\Domain\Company\Traits
 */
trait CompanyGroupsTrait
{


    /**
     * @var CompanyGroupsInterface
     */
    private $company_groups;

    /**
     * CompanyGroupsTrait constructor.
     * @param CompanyGroupsInterface $company_groups
     */
    public function __construct(CompanyGroupsInterface $company_groups)
    {
        $this->company_groups = $company_groups;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function getCompanyGroups(): array
    {
        return $this->company_groups->getCompanyGroups();
    }

    /**
     * @inheritDoc
     */
    public function getCompanyGroup(string $id): array
    {
        return $this->company_groups->getCompanyGroup($id);
    }

    /**
     * @inheritDoc
     */
    public function getCompanyGroupData(string $id): array
    {
        return $this->company_groups->getCompanyGroupData($id);
    }
}
