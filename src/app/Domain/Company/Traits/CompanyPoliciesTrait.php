<?php
declare(strict_types=1);

namespace App\Domain\Company\Traits;

use App\Domain\Company\CompanyPoliciesInterface;
use App\Domain\Company\Entity\Company;
use Illuminate\Support\Facades\DB;


/**
 * Trait CompanyPoliciesTrait
 * @package App\Domain\Company\Traits
 */
trait CompanyPoliciesTrait
{

    /**
     * @var Company
     */
    private $policies;

    /**
     * UserManagers constructor.
     * @param Company $company
     */
    public function __construct(CompanyPoliciesInterface $policies)
    {
        $this->policies = $policies;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function getCompanyPolicies(): array
    {
        return $this->policies->getCompanyPolicies();
    }
}
