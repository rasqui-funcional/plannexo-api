<?php


namespace App\Domain\Email;


interface EmailInterface
{
    /**
     * @param string $endpoint
     * @return string
     */
    public function buildEndpoint(string $endpoint): string;

    /**
     * @return array
     */
    public function getHeaders(): array;

    /**
     * @return array
     */
    public function getDataReceive();
}
