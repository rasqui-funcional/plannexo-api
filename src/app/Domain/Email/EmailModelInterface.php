<?php


namespace App\Domain\Email;


use App\Domain\Email\Entity\EmailModel;

interface EmailModelInterface
{
    /**
     * @return mixed
     */
    public function getEmailModels();

    /**
     * @param int $id
     * @return mixed
     */
    public function getEmailModel(int $id);

    /**
     * @return mixed
     */
    public function createEmailModel();

    /**
     * @param int $id
     * @return mixed
     */
    public function updateEmailModel(int $id);

    /**
     * @param int $id
     * @return mixed
     */
    public function deleteEmailModel(int $id);
}
