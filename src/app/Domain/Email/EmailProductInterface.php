<?php

namespace App\Domain\Email;

use App\Domain\Email\Entity\EmailProduct;

interface EmailProductInterface
{
    /**
     * @return mixed
     */
    public function getEmailProducts();
    
    /**
     * @return mixed
     */
    public function getEmailProduct();

}
