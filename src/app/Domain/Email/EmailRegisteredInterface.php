<?php

namespace App\Domain\Email;

use App\Domain\Email\Entity\EmailRegistered;

interface EmailRegisteredInterface
{
    /**
     * @return mixed
     */
    public function getEmailRegistered();

    /**
     * @return mixed
     */
    public function createEmailRegistered();

    /**
     * @param int $id
     * @return mixed
     */
    public function updateEmailRegistered(int $id);

    /**
     * @param int $id
     * @return mixed
     */
    public function deleteEmailRegistered(int $id);
}
