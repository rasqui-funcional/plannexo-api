<?php

namespace App\Domain\Email;

use App\Domain\Email\Entity\EmailType;

interface EmailTypeInterface
{
    /**
     * @return mixed
     */
    public function getEmailTypes();
    
    /**
     * @return mixed
     */
    public function getEmailType(int $id);

}
