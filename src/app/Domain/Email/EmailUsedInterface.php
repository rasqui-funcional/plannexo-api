<?php

namespace App\Domain\Email;

use App\Domain\Email\Entity\EmailUsed;

interface EmailUsedInterface
{
    /**
     * @return mixed
     */
    public function getEmailUsed();

    /**
     * @return mixed
     */
    public function createEmailUsed();

    /**
     * @param int $id
     * @return mixed
     */
    public function updateEmailUsed(int $id);

    /**
     * @param int $id
     * @return mixed
     */
    public function deleteEmailUsed(int $id);
}
