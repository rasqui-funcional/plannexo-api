<?php

namespace App\Domain\Email\Entity;

use DateTime;
use Exception;

class EmailModel
{
    /**
     * @var int|null
     */
    private ?int $id_company_fk;

    /**
     * @var int|null
     */
    private ?int $id;

    /**
     * @var string|null
     */
    private ?string $title;

    /**
     * @var string|null
     */
    private ?string $subject;

    /**
     * @var string|null
     */
    private ?string $body;

    /**
     * @var string
     */
    private string $create_at;

    /**
     * @var string
     */
    private string $update_at;

    /**
     * @array
     */
    private array $used_in;

    public function __construct(array $data = [])
    {
        $this->setIdCompanyFk($data['id_company_fk'] ??= 0);
        $this->setIdEmailModelPk($data['id'] ??= 0);
        $this->setTitle($data['title'] ??= "");
        $this->setSubject($data['subject'] ??= "");
        $this->setBody($data['body'] ??= "");
        $this->setCreateAt($data['create_at'] ??= $this->dateFormat());
        $this->setUpdateAt($data['update_at'] ??= $this->dateFormat());
        
        if(!isset($data['used_in'])) {
            $data['used_in'] = [];
        }
        
        $used_in = [];
        foreach ($data['used_in'] as $used) {
            $used_in[] = new EmailUsed($used);
        }
        $this->setEmailUsed($used_in);        
    }

    /**
     * @return int
     */
    public function getIdCompanyFk(): int
    {
        return $this->id_company_fk;
    }

    /**
     * @param int $id_company_fk
     */
    public function setIdCompanyFk(int $id_company_fk): void
    {
        $this->id_company_fk = $id_company_fk;
    }

    /**
     * @return int
     */
    public function getIdEmailModelPk(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setIdEmailModelPk(int $id_email_model_pk): void
    {
        $this->id = $id_email_model_pk;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    /**DateTime
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody(string $body): void
    {
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getCreateAt(): string
    {
        return $this->create_at;
    }

    /**
     * @param string $create_at
     */
    public function setCreateAt(string $create_at): void
    {
        $this->create_at = $create_at;
    }

    /**
     * @return string
     */
    public function getUpdateAt(): string
    {
        return $this->update_at;
    }

    /**
     * @param string $update_at
     */
    public function setUpdateAt(string $update_at): void
    {
        $this->update_at = $update_at;
    }

    /**
     * @return array $used_in
     */
    public function getEmailUsed(): array
    {
        return $this->used_in;
    }

    /**
     * @param array $used_in
     */
    public function setEmailUsed(array $used_in): void
    {
        $this->used_in = $used_in;
    }

    private function dateFormat($date = null)
    {
        if (is_null($date)) {
            return (new DateTime())->format('Y-m-d H:i:s');
        }

        return (new DateTime($date))->format('Y-m-d H:i:s');
    }

    public function exchangeArray()
    {
        return [
            'name' => "{$this->title}",
            'subject' => "{$this->subject}",
            'description' => "{$this->body}",
            'id_company_fk' => $this->id_company_fk,
            'created_at' => "{$this->dateFormat($this->create_at)}",
            'update_at' => "{$this->dateFormat($this->update_at)}"
        ];
    }
}