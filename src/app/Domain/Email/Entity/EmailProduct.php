<?php


namespace App\Domain\Email\Entity;


class EmailProduct
{
    /**
     * @var string
     */
    private string $module;

    /**
     * @var string
     */
    private string $label;
    
    /**
     * @var string
     */
    private string $product;
    
    public function __construct(array $data = [])
    {
        $this->setModule($data['module'] ??= '');
        $this->setLabel($data['label'] ??= '');
        $this->setProduct($data['product'] ??= '');
    }

    /**
     * @return string
     */
    public function getModule(): string
    {
        return $this->module;
    }

    /**
     * @param string $module
     */
    public function setModule(string $module): void
    {
        $this->module = $module;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getProduct(): string
    {
        return $this->product;
    }

    /**
     * @param string $product
     */
    
     public function setProduct(string $product): void
    {
        $this->product = $product;
    }

    public function exchangeArray()
    {
        return [
            'module' => $this->module,
            'label' => $this->label,
            'product' => $this->product,
        ];
    }
}
