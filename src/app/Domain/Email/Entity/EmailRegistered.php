<?php

namespace App\Domain\Email\Entity;

class EmailRegistered
{
    /**
     * @var int|null
     */
    private ?int $id_model_fk;
    
    /**
     * @var int|null
     */
    private ?int $id_module_fk;

    /**
     * @var int|null
     */
    private ?int $id_type_fk;

    /**
     * @var string|null
     */
    private ?string $module_name;

    /**
     * @var string|null
     */
    private ?string $label;

    /**
     * @var string|null
     */
    private ?string $type;

    public function __construct(array $data = [])
    {
        $this->setIdModel($data['id_model_fk'] ??= 0);
        $this->setIdModule($data['id_module_fk'] ??= 0);
        $this->setIdType($data['id_type_fk'] ??= 0);
        $this->setModuleName($data['module_name'] ??= '');
        $this->setLabel($data['label'] ??= '');
        $this->setType($data['type'] ??= '');
    }

    /**
     * @return int
     */
    public function getIdModel(): int
    {
        return $this->id_model_fk;
    }

    /**
     * @param int $id_model_fk
     */
    public function setIdModel(int $id_model_fk): void
    {
        $this->id_model_fk = $id_model_fk;
    }

    /**
     * @return int
     */
    public function getIdModule(): int
    {
        return $this->id_module_fk;
    }

    /**
     * @param int $id_module_fk
     */
    public function setIdModule(int $id_module_fk): void
    {
        $this->id_module_fk = $id_module_fk;
    }


    /**
     * @return int
     */
    public function getIdType(): int
    {
        return $this->id_type_fk;
    }

    /**
     * @param int $id_type_fk
     */
    public function setIdType(int $id_type_fk): void
    {
        $this->id_type_fk = $id_type_fk;
    }

    /**
     * @return string
     */
    public function getModuleName(): string
    {
        return $this->module_name;
    }

    /**
     * @param string $module_name
     */
    public function setModuleName(string $module_name): void
    {
        $this->module_name = $module_name;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function exchangeArray()
    {
        return [
            'id_model_fk' => "{$this->id_model_fk}",
            'id_module_fk' => "{$this->id_module_fk}",
            'id_type_fk' => "{$this->id_type_fk}",
        ];
    }
}