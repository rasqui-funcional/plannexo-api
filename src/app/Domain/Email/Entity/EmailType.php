<?php


namespace App\Domain\Email\Entity;


class EmailType
{
    /**
     * @var int
     */
    private string $id_module_fk;

    /**
     * @var string
     */
    private string $label;
    
    /**
     * @var string
     */
    private string $type;
    
    public function __construct(array $data = [])
    {
        $this->setIdModule($data['id_module_fk'] ??= 0);
        $this->setLabel($data['label'] ??= '');
        $this->setType($data['type'] ??= '');
    }

    /**
     * @return int
     */
    public function getIdModule(): int
    {
        return $this->id_module_fk;
    }

    /**
     * @param int $id_module_fk
     */
    public function setIdModule(int $id_module_fk): void
    {
        $this->id_module_fk = $id_module_fk;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    
     public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function exchangeArray()
    {
        return [
            'id_module_fk' => $this->id_module_fk,
            'label' => $this->label,
            'type' => $this->type,
        ];
    }
}
