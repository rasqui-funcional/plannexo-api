<?php
declare(strict_types=1);

namespace App\Domain\Email\Services;


use App\Http\Client\HttpServiceInterface;

abstract class EmailService
{
    /**
     * @var string|null
     */
    private ?string $sender;

    /**
     * @var array|null
     */
    private ?array $receivers;

    /**
     * @var array|null
     */
    private ?array $cc;

    /**
     * @var array|null
     */
    private ?array $bcc;

    /**
     * @var string|null
     */
    private ?string $subject;

    /**
     * @var string|null
     */
    private ?string $htmlBody;

    /**
     * @var string|null
     */
    private ?string $textBody;

    /**
     * @var string|null
     */
    private ?string $encoding;

    /**
     * @var HttpServiceInterface
     */
    protected HttpServiceInterface $service;

    /**
     * @return HttpServiceInterface
     */
    public function getClient(): HttpServiceInterface
    {
        return $this->service;
    }

    public function exchangeArray(array $data = []): void
    {
        $this->sender = (isset($data['sender'])) ? $data['sender'] : env('EMAIL_SENDER');
        $this->receivers = (isset($data['receivers'])) ? $data['receivers'] : [];
        $this->cc = (isset($data['cc'])) ? $data['cc'] : [];
        $this->bcc = (isset($data['bcc'])) ? $data['bcc'] : [];
        $this->subject = (isset($data['subject'])) ? $data['subject'] : null;
        $this->htmlBody = (isset($data['htmlBody'])) ? $data['htmlBody'] : null;
        $this->textBody = (isset($data['textBody'])) ? $data['textBody'] : null;
        $this->encoding = (isset($data['encoding'])) ? $data['encoding'] : null;
    }

    /**
     * @return string|null
     */
    public function getSender(): ?string
    {
        return $this->sender;
    }

    /**
     * @param string|null $sender
     */
    public function setSender(?string $sender): void
    {
        $this->sender = $sender;
    }

    /**
     * @return array|null
     */
    public function getReceivers(): ?array
    {
        return $this->receivers;
    }

    /**
     * @param array|null $receivers
     */
    public function setReceivers(?array $receivers): void
    {
        $this->receivers = $receivers;
    }

    /**
     * @return array|null
     */
    public function getCc(): ?array
    {
        return $this->cc;
    }

    /**
     * @param array|null $cc
     */
    public function setCc(?array $cc): void
    {
        $this->cc = $cc;
    }

    /**
     * @return array|null
     */
    public function getBcc(): ?array
    {
        return $this->bcc;
    }

    /**
     * @param array|null $bcc
     */
    public function setBcc(?array $bcc): void
    {
        $this->bcc = $bcc;
    }

    /**
     * @return string|null
     */
    public function getSubject(): ?string
    {
        return $this->subject;
    }

    /**
     * @param string|null $subject
     */
    public function setSubject(?string $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return string|null
     */
    public function getHtmlBody(): ?string
    {
        return $this->htmlBody;
    }

    /**
     * @param string|null $htmlBody
     */
    public function setHtmlBody(?string $htmlBody): void
    {
        $this->htmlBody = $htmlBody;
    }

    /**
     * @return string|null
     */
    public function getTextBody(): ?string
    {
        return $this->textBody;
    }

    /**
     * @param string|null $textBody
     */
    public function setTextBody(?string $textBody): void
    {
        $this->textBody = $textBody;
    }

    /**
     * @return string|null
     */
    public function getEncoding(): ?string
    {
        return $this->encoding;
    }

    /**
     * @param string|null $encoding
     */
    public function setEncoding(?string $encoding): void
    {
        $this->encoding = $encoding;
    }
}
