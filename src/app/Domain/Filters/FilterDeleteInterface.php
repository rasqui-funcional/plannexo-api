<?php
declare(strict_types=1);

namespace App\Domain\Filters;

/**
 * Interface FilterDeleteInterface
 * @package App\Domain\Filters
 */
interface FilterDeleteInterface
{

    /**
     * @return array
     */
    public function deleteFilter(int $id): bool;

}
