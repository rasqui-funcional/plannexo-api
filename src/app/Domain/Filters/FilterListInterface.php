<?php
declare(strict_types=1);

namespace App\Domain\Filters;

/**
 * Interface FilterListInterface
 * @package App\Domain\Filters
 */
interface FilterListInterface
{

    /**
     * @return array
     */
    public function getFilters(): array ;

    public function getFilter(int $id): array;

}
