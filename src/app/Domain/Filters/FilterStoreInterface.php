<?php
declare(strict_types=1);

namespace App\Domain\Filters;

/**
 * Interface FilterStoreInterface
 * @package App\Domain\Filters
 */
interface FilterStoreInterface
{

    /**
     * @return array
     */
    public function saveFilter(): array;

    public function setData(array $data): self;

}
