<?php
declare(strict_types=1);

namespace App\Domain\Filters;

/**
 * Interface FilterUpdateInterface
 * @package App\Domain\Filters
 */
interface FilterUpdateInterface
{

    /**
     * @return array
     */
    public function updateFilter(int $id): array;

    public function setData(array $data): self;

}
