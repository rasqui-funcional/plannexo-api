<?php

namespace App\Domain\FollowUp\Filters;

use App\Domain\Company\CompanyGroupingInterface;
use App\Domain\Company\CompanyPoliciesInterface;
use App\Domain\Company\Traits\CompanyGroupingTrait;
use App\Domain\Company\Traits\CompanyPoliciesTrait;
use App\Domain\FollowUp\FollowUpFiltersInterface;
use App\Domain\User\Traits\UserManagersTrait;
use App\Domain\User\Traits\UserStandardDepositTrait;
use App\Domain\User\UserEstablishmentsInterface;
use App\Domain\User\UserManagersInterface;
use App\Domain\User\UserStandardDepositInterface;
use App\Model\UserEstablishmentsModel;

/**
 * Class FollowUpFiltersRepository
 * @package App\Http\Repository
 */
class FollowUpFilters implements FollowUpFiltersInterface
{

    /**
     * @var UserEstablishmentsModel
     */
    private UserEstablishmentsInterface $user_establishment;

    use CompanyPoliciesTrait {
        CompanyPoliciesTrait::__construct as __CompanyPoliciesTraitConstruct;
    }

    use UserManagersTrait {
        UserManagersTrait::__construct as __UserManagersTraitConstruct;
    }

    use UserStandardDepositTrait {
        UserStandardDepositTrait::__construct as __UserStandardDepositConstruct;
    }

    use CompanyGroupingTrait {
        CompanyGroupingTrait::__construct as __CompanyGroupingTraitConstruct;
    }


    public function __construct(
        UserEstablishmentsInterface $user_establishment,
        UserManagersInterface $user_managers,
        UserStandardDepositInterface $user_standard_deposit,
        CompanyPoliciesInterface $policies,
        CompanyGroupingInterface $company_grouping
    )
    {

        $this->user_establishment = $user_establishment;

        $this->__CompanyPoliciesTraitConstruct($policies);
        $this->__UserManagersTraitConstruct($user_managers);
        $this->__UserStandardDepositConstruct($user_standard_deposit);
        $this->__CompanyGroupingTraitConstruct($company_grouping);

    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function getFollowUpFilters(): array
    {
        return array_merge(
            [
                'estabs' => $this->getEstablishments()
            ],
            [
                'managers' => $this->getUserManagers()
            ],
            [
                'policies' => $this->getCompanyPolicies()
            ],
            [
                'standard_deposit' => $this->getStandardDeposit()
            ],
            [
                'grouping' => $this->getCompanyGrouping()
            ],
        );
    }

    /**
     * @return array|mixed
     */
    public function getEstablishments()
    {
        $estabs = $this->user_establishment->getEstablishments();

        return $estabs;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function getStandardDeposit(): array
    {
        $standard_deposit = $this->setEstabs($this->user_establishment->getEstablishmentsCodes())->getUserStandardDeposit();

        return $standard_deposit;
    }
}
