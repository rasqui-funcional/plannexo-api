<?php
declare(strict_types=1);

namespace App\Domain\FollowUp\Filters;

use App\Helpers\DateHelper;
use App\Helpers\StringHelper;

/**
 * Class FollowUpFilters
 * @package App\Domain\FollowUp\Entity
 */
class FollowUpFiltersFiltered
{

    /**
     * @var array
     */
    private array $special_filters_dates = ['specialFilters' =>
        [
            'dateRequestOrder',
            'datePurchaseOrder',
            'dateExpected',
        ]
    ];

    private array $sku_attr = ['sku' =>
        [
            'suppliersId',
            'itemCode',
            'requestOrderIds',
            'purchaseOrderIds',
        ]
    ];

    /**
     * @var string
     */
    private string $date;

    /**
     * @var array
     */
    private array  $filters;

    /**
     * FollowUpFilters constructor.
     * @param array $filters
     */
    public function __construct($filters = [])
    {
        $this->filters = $filters;
        $this->prepareFilters();
    }

    /**
     * @return $this
     */
    private function prepareFilters(): self
    {
        $this
            ->formatDates()
            ->formartSkuData()
        ;

        return $this;
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        return $this->filters;
    }

    /**
     * @return $this
     */
    private function formatDates(): self
    {
        $special_filter_key = key($this->special_filters_dates);
        if (array_key_exists($special_filter_key, $this->filters)) {
            foreach ($this->filters[$special_filter_key] as $key => $value) {
                if (in_array($key, $this->special_filters_dates[$special_filter_key])) {

                    if (isset($this->filters[$special_filter_key][$key]['start'])) {
                        $this->filters[$special_filter_key][$key]['start'] = (string)$this->setDate($value['start'])
                            ->withStartOfTheDay()
                            ->formatDate();
                    }

                    if (isset($this->filters[$special_filter_key][$key]['end'])) {
                        $this->filters[$special_filter_key][$key]['end'] = (string) $this->setDate($value['end'])
                            ->withEndOfTheDay()
                            ->formatDate();
                    }
                }
            }
        }
        return $this;
    }

    /**
     * @param string $date
     */
    public function setDate(string $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return string
     */
    private function formatDate(): string
    {
        return DateHelper::isoFormat($this->date);
    }

    /**
     * @return $this
     */
    private function withStartOfTheDay(): self
    {
        $this->date .= date(' H:i:s ', strtotime("today"));

        return $this;
    }

    /**
     * @return $this
     */
    private function withEndOfTheDay(): self
    {
        $this->date .= (date(' H:i:s', strtotime("today") + (60 * 1440) - 1));

        return $this;
    }

    private function formartSkuData()
    {
        $sku_key = key($this->sku_attr);
        if (array_key_exists($sku_key, $this->filters)) {
            foreach ($this->filters[$sku_key] as $key => $value) {
                if (in_array($key, $this->sku_attr[$sku_key])) {
                    $this->filters[$sku_key][$key] = StringHelper::toArray((string) $this->filters[$sku_key][$key]);
                }
            }
        }
        return $this;
    }
}
