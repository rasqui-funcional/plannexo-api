<?php
declare(strict_types=1);

namespace App\Domain\FollowUp;

use App\Domain\Company\CompanyGroupingInterface;
use App\Domain\Company\CompanyPoliciesInterface;
use App\Domain\User\UserEstablishmentsInterface;
use App\Domain\User\UserManagersInterface;
use App\Domain\User\UserStandardDepositInterface;

/**
 * Interface FollowUpFiltersInterface
 * @package App\Domain\FollowUp
 */
interface FollowUpFiltersInterface
{

    /**
     * FollowUpFiltersInterface constructor.
     * @param UserEstablishmentsInterface $user_establishment
     * @param UserManagersInterface $user_managers
     * @param UserStandardDepositInterface $user_standard_deposit
     * @param CompanyPoliciesInterface $policies
     * @param CompanyGroupingInterface $company_grouping
     */
    public function __construct(
        UserEstablishmentsInterface $user_establishment,
        UserManagersInterface $user_managers,
        UserStandardDepositInterface $user_standard_deposit,
        CompanyPoliciesInterface $policies,
        CompanyGroupingInterface $company_grouping
    );

    /**
     * @return array
     */
    public function getFollowUpFilters(): array;

    /**
     * @return array
     */
    public function getEstablishments();

    /**
     * @return array
     */
    public function getUserManagers(): array;

    /**
     * @return array
     */
    public function getCompanyPolicies(): array;

    /**
     * @return array
     */
    public function getStandardDeposit(): array;

    /**
     * @return array
     */
    public function getCompanyGrouping(): array;

}
