<?php
declare(strict_types=1);

namespace App\Domain\FollowUp;

/**
 * Interface FollowUpHistoryInterface
 * @package App\Domain\FollowUp
 */
interface FollowUpHistoryInterface
{

    /**
     * @return array
     */
    public function getHistory(): array;

}
