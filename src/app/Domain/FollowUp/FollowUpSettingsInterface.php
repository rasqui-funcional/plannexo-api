<?php
declare(strict_types=1);

namespace App\Domain\FollowUp;

/**
 * Interface FollowUpSettingsInterface
 * @package App\Domain\FollowUp
 */
interface FollowUpSettingsInterface
{

    /**
     * @return array
     */
    public function updateSettings(): array;

    public function setData(array $data): self;

    public function getSettings(): array;

}
