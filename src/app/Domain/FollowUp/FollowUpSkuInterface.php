<?php
declare(strict_types=1);

namespace App\Domain\FollowUp;

/**
 * Interface FollowUpSkuInterface
 * @package App\Domain\FollowUp
 */
interface FollowUpSkuInterface
{

    /**
     * @return array
     */
    public function getPendingSkus(): array;

    /**
     * @return array
     */
    public function getPendingSkusFiltered(): array;

}
