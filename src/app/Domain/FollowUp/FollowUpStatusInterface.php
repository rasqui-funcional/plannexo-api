<?php
declare(strict_types=1);

namespace App\Domain\FollowUp;

/**
 * Interface FollowUpStatusInterface
 * @package App\Domain\FollowUp
 */
interface FollowUpStatusInterface
{

    /**
     * @return array
     */
    public function getFollowUpStatus(): array;

}
