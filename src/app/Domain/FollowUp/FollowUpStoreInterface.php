<?php
declare(strict_types=1);

namespace App\Domain\FollowUp;

/**
 * Interface FollowUpStoreInterface
 * @package App\Domain\FollowUp
 */
interface FollowUpStoreInterface
{

    /**
     * @return array
     */
    public function saveFollowUp(): bool;

    public function setData(array $data): self;

}
