<?php
declare(strict_types=1);

namespace App\Domain\FollowUp;


/**
 * Interface FollowUpSummaryInterface
 * @package App\Domain\FollowUp
 */
interface FollowUpSupplierInterface
{
    /**
     * @return array
     */
    public function getSummary(): array;

    /**
     * @return array
     */
    public function getInfo(): array;

    /**
     * @param array $data
     * @return array
     */
    public function getQuerySummary(): array;
}
