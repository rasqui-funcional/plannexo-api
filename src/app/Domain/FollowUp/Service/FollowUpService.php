<?php
declare(strict_types=1);

namespace App\Domain\FollowUp\Service;

use App\Domain\FollowUp\FollowUpSkuInterface;
use App\Http\Client\HttpServiceInterface;

/**
 * Class FollowUpService
 * @package App\Domain\FollowUp\Service
 */
abstract class FollowUpService
{
    /**
     * @var HttpServiceInterface
     */
    protected $service;

    /**
     * @var int
     */
    protected int $id_company_fk;

    /**
     * @var string
     */
    protected string $id_supplier_fk;

    private int $per_page = 1000;

    /**
     * @return HttpServiceInterface
     */
    public function getClient(): HttpServiceInterface
    {
        return $this->service;
    }

    /**
     * @param int $id_company_fk
     * @return FollowUpService
     */
    public function setCompanyId(int $id_company_fk): self
    {
        $this->id_company_fk = $id_company_fk;

        return $this;
    }

    /**
     * @param string $id_supplier_fk
     * @return FollowUpService
     */
    public function setSupplierId(string $id_supplier_fk): self
    {
        $this->id_supplier_fk = $id_supplier_fk;

        return $this;
    }

    /**
     * @param string $endpoint
     * @return string
     */
    public function buildEndpoint(string $endpoint): string
    {
        return sprintf('%s%s?limit=%s', env('FOLLOWUP_API_URI'), $endpoint, $this->per_page);
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'companyId' => $this->id_company_fk,
        ];
    }

}
