<?php
declare(strict_types=1);

namespace App\Domain\Tag;


/**
 * Interface TagInterface
 * @package App\Domain\Tag
 */
interface TagInterface
{

    /**
     * @return bool
     */

    public static function hasTag(): bool;

    public static function applyTag(string $tag);

    public static function removeTag(string $tag);

}
