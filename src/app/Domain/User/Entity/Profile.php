<?php
declare(strict_types = 1);

namespace App\Domain\User\Entity;


class Profile
{
    /**
     * @var int|null
     */
    private ?int $id_company_fk;

    /**
     * @var int|null
     */
    private ?int $id_profile_pk;

    /**
     * @var string|null
     */
    private ?string $desc_group;

    public function __construct(array $data = [])
    {
        $this->id_company_fk = $data['id_company_fk'] ?? 0;
        $this->id_profile_pk = $data['id_profile_pk'] ?? 0;
        $this->desc_group = $data['desc_group'] ?? '';
    }

    /**
     * @return int
     */
    public function getIdCompanyFk(): int
    {
        return $this->id_company_fk;
    }

    /**
     * @param int $id_company_fk
     */
    public function setIdCompanyFk(int $id_company_fk): void
    {
        $this->id_company_fk = $id_company_fk;
    }

    /**
     * @return int
     */
    public function getIdProfilePk(): int
    {
        return $this->id_profile_pk;
    }

    /**
     * @param int $id_profile_pk
     */
    public function setIdProfilePk(int $id_profile_pk): void
    {
        $this->id_profile_pk = $id_profile_pk;
    }

    /**
     * @return string
     */
    public function getDescGroup(): string
    {
        return $this->desc_group;
    }

    /**
     * @param string $desc_group
     */
    public function setDescGroup(string $desc_group): void
    {
        $this->desc_group = $desc_group;
    }
}
