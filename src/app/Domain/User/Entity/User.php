<?php
declare(strict_types = 1);

namespace App\Domain\User\Entity;

use App\Domain\Company\Entity\Company;
use DateTime;
use Exception;

class User
{
    /**
     * @var int|null
     */
    private ?int $id_user_pk;

    /**
     * @var string|null
     */
    private ?string $name;

    /**
     * @var string|null
     */
    private ?string $login;

    /**
     * @var int
     */
    private int $type;

    /**
     * @var string
     */
    private string $mail_login;

    /**
     * @var string|null
     */
    private ?string $cod_user_erp;

    /**
     * @var string|null
     */
    private ?string $tel;

    /**
     * @var string|null
     */
    private ?string $ddd;

    /**
     * @var DateTime
     */
    private DateTime $date_added;

    /**
     * @var DateTime
     */
    private DateTime $date_updated;

    /**
     * @var DateTime|null
     */
    private ?DateTime $date_last_access = null;

    /**
     * @var int
     */
    private int $active = 1;

    /**
     * @var int
     */
    private int $id_company_fk;

    /**
     * @var string|null
     */
    private ?string $password;

    /**
     * @var int|null
     */
    private ?int $bioid_reference;

    /**
     * @var array
     */
    private array $companies;

    /**
     * @var Profile
     */
    private Profile $profile;

    /**
     * @var UserEstablishments
     */
    private UserEstablishments $establishments;

    /**
     * @var int
     */
    private int $ref_company_bioid;

    /**
     * @var Company
     */
    private Company $company;

    /**
     * User constructor.
     * @param array $data
     * @throws Exception
     */
    public function __construct(array $data = [])
    {
        $data['profile'] ??= 0;
        $data['establishments'] ??= [];
        $data['ref_company_bioid'] ??= 0;

        $this->setIdUserPk((int)$data['id_user_pk'] ??= null);
        $this->setName($data['name'] ??= null);
        $this->setLogin($data['login'] ??= null);
        $this->setType((int)$data['type'] ??= 1);
        $this->setMailLogin($data['email'] ??= '');
        $this->setCodUserErp($data['erpcode'] ??= null);
        $this->setDdd($data['ddd'] ??= null);
        $this->setTel($data['phone'] ??= null);
        $this->setDateAdded($data['date_added'] ??= new DateTime());
        $this->setDateUpdated($data['date_updated'] ??= new DateTime());
        $this->setDateLastAccess($data['date_last_access'] ??= new DateTime());
        $this->setActive((int)$data['status'] ??= 1);
        $this->setPassword($data['password'] ??= '');
        $this->setBioidReference((int)$data['bioid_reference'] ??= null);
        $this->setCompanies($data['companies'] ??= []);
        $this->setIdCompanyFk((int)$data['id_company_fk'] ??= 0);
        $this->setProfile(new Profile(['id_profile_pk' => (int)$data['profile']]));
        $this->setEstablishments(new UserEstablishments([
            'establishments' => $data['establishments'],
            'id_company_fk' => $this->getIdCompanyFk(),
            'id_user_fk' => $this->getIdUserPk(),
        ]));
        $this->setRefCompanyBioid((int)$data['ref_company_bioid']);
        $this->setCompany($data);
    }

    /**
     * @return int
     */
    public function getIdUserPk(): ?int
    {
        return $this->id_user_pk;
    }

    /**
     * @param int $id_user_pk
     */
    public function setIdUserPk(?int $id_user_pk): void
    {
        $this->id_user_pk = $id_user_pk;
    }

    /**
     * @return int
     */
    public function getIdCompanyFk(): int
    {
        return $this->id_company_fk;
    }

    /**
     * @param int $id_company_fk
     */
    public function setIdCompanyFk(int $id_company_fk): void
    {
        $this->id_company_fk = $id_company_fk;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getLogin(): ?string
    {
        return $this->login;
    }

    /**
     * @param string|null $login
     */
    public function setLogin(?string $login): void
    {
        $this->login = $login;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getMailLogin(): string
    {
        return $this->mail_login;
    }

    /**
     * @param string $mail_login
     */
    public function setMailLogin(string $mail_login): void
    {
        $this->mail_login = $mail_login;
    }

    /**
     * @return string|null
     */
    public function getCodUserErp(): ?string
    {
        return $this->cod_user_erp;
    }

    /**
     * @param string|null $cod_user_erp
     */
    public function setCodUserErp(?string $cod_user_erp): void
    {
        $this->cod_user_erp = $cod_user_erp;
    }

    /**
     * @return string|null
     */
    public function getTel(): ?string
    {
        return $this->tel;
    }

    /**
     * @param string|null $tel
     */
    public function setTel(?string $tel): void
    {
        $this->tel = $tel;
    }

    /**
     * @return string|null
     */
    public function getDdd(): ?string
    {
        return $this->ddd;
    }

    /**
     * @param string|null $ddd
     */
    public function setDdd(?string $ddd): void
    {
        $this->ddd = $ddd;
    }

    /**
     * @return DateTime
     */
    public function getDateAdded(): DateTime
    {
        return $this->date_added;
    }

    /**
     * @param DateTime $date_added
     */
    public function setDateAdded(DateTime $date_added): void
    {
        $this->date_added = $date_added;
    }

    /**
     * @return DateTime
     */
    public function getDateLastAccess(): DateTime
    {
        return $this->date_last_access;
    }

    /**
     * @param DateTime $date_last_access
     */
    public function setDateLastAccess(?DateTime $date_last_access): void
    {
        $this->date_last_access = $date_last_access;
    }

    /**
     * @return int
     */
    public function getActive(): int
    {
        return $this->active;
    }

    /**
     * @param int $active
     */
    public function setActive(int $active): void
    {
        $this->active = $active;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     */
    public function setPassword(?string $password): void
    {
        $this->password = hash('sha256', $password);
    }

    /**
     * @return int|null
     */
    public function getBioidReference(): ?int
    {
        return $this->bioid_reference;
    }

    /**
     * @param int|null $bioid_reference
     */
    public function setBioidReference(?int $bioid_reference): void
    {
        $this->bioid_reference = $bioid_reference;
    }

    /**
     * @return array
     */
    public function getCompanies(): array
    {
        return $this->companies;
    }

    /**
     * @param array $companies
     */
    public function setCompanies(array $companies): void
    {
        $this->companies = $companies;
    }

    /**
     * @return DateTime
     */
    public function getDateUpdated(): DateTime
    {
        return $this->date_updated;
    }

    /**
     * @param DateTime $date_updated
     */
    public function setDateUpdated(DateTime $date_updated): void
    {
        $this->date_updated = $date_updated;
    }

    /**
     * @return Profile
     */
    public function getProfile(): Profile
    {
        return $this->profile;
    }

    /**
     * @param Profile $profile
     */
    public function setProfile(Profile $profile): void
    {
        $this->profile = $profile;
    }

    /**
     * @return UserEstablishments $establishments
     */
    public function getEstablishments(): UserEstablishments
    {
        return $this->establishments;
    }

    /**
     * @param UserEstablishments $establishments
     */
    public function setEstablishments(UserEstablishments $establishments): void
    {
        $this->establishments = $establishments;
    }

    /**
     * @return int
     */
    public function getRefCompanyBioid(): int
    {
        return $this->ref_company_bioid;
    }

    /**
     * @param int $ref_company_bioid
     */
    public function setRefCompanyBioid(int $ref_company_bioid): void
    {
        $this->ref_company_bioid = $ref_company_bioid;
    }

    public function exchangeArray()
    {
        return [
            'id_user_pk' => $this->id_user_pk,
            'pass' => "NULL",
            'mail' => "NULL",
            'pass_warning' => 0,
            'date_expire' => 0,
            'name' => "{$this->name}",
            'login' => "{$this->login}",
            'mail_login' => "{$this->mail_login}",
            'cod_user_erp' => "{$this->cod_user_erp}",
            'ddd' => "{$this->ddd}",
            'tel' => "{$this->tel}",
            'type' => $this->type,
            'password' => "{$this->password}",
            'bioid_reference' => $this->bioid_reference,
            'id_company_fk' => $this->id_company_fk,
            'active' => $this->active,
            'date_added' => "{$this->date_added->format('Y-m-d H:i:s')}",
            'date_updated' => "{$this->date_updated->format('Y-m-d H:i:s')}"
        ];
    }

    public function toArray()
    {
        $lastActivity = null;
        if (!is_null($this->date_last_access)) {
            $lastActivity = $this->date_last_access->format('Y-m-d H:i:s');
        }

        return [
            'id' => $this->id_user_pk,
            'id_company' => $this->id_company_fk,
            'name' => $this->name,
            'login' => $this->login,
            'email' => $this->mail_login,
            'erpcode' => $this->cod_user_erp,
            'ddd' => $this->ddd,
            'phone' => $this->tel,
            'type' => $this->type,
            'status' => $this->active,
            'createat' => $this->date_added->format('Y-m-d H:i:s'),
            'updateat' => $this->date_updated->format('Y-m-d H:i:s'),
            'lastactivity' => $lastActivity,
            'bioid_reference' => $this->bioid_reference
        ];
    }

    public function findInitialsName($length = 2)
    {
        $letters = explode(' ', trim($this->name));
        if (count($letters) == 1) {
            return substr(strtoupper($letters[0]), 0, $length);
        }

        return strtoupper($letters[0][0] . end($letters)[0]);
    }


    public function getGeneratedIdTagManager()
    {
        $generate_id = str_pad(
            (string)$this->id_company_fk,
            4,
            '0',
            STR_PAD_LEFT
        );

        $generate_id .= str_pad(
            (string)$this->id_user_pk,
            5,
            '0',
            STR_PAD_LEFT
        );

        return $generate_id;
    }

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }


    /**
     * @param array $data
     * @return User
     */
    public function setCompany(array $data): User
    {
        $this->company = new Company($data);

        return $this;
    }


}
