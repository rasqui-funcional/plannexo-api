<?php
declare(strict_types = 1);

namespace App\Domain\User\Entity;


class UserEstablishments
{
    /**
     * @var int
     */
    private int $id_company_pk;

    /**
     * @var array
     */
    private $establishments;

    /**
     * @var int
     */
    private ?int $id_user_pk;

    public function __construct(array $data = [])
    {
        $this->setIdCompanyFk($data['id_company_fk'] ??= 0);
        $this->setCodeEstablishments($data['establishments'] ??= []);
        $this->setIdUserFk($data['id_user_fk'] ??= 0);
    }

    /**
     * @return int
     */
    public function getIdCompanyFk(): int
    {
        return $this->id_company_pk;
    }

    /**
     * @param int $id_company_pk
     */
    public function setIdCompanyFk(int $id_company_pk): void
    {
        $this->id_company_pk = $id_company_pk;
    }

    /**
     * @return array
     */
    public function getCodeEstablishments()
    {
        return $this->establishments;
    }

    /**
     * @param array $establishments
     */
    public function setCodeEstablishments(array $establishments): void
    {
        $this->establishments = $establishments;
    }

    /**
     * @return int
     */
    public function getIdUserFk(): ?int
    {
        return $this->id_user_pk;
    }

    /**
     * @param int $id_user_pk
     */
    public function setIdUserFk(?int $id_user_pk): void
    {
        $this->id_user_pk = $id_user_pk;
    }

    public function exchangeArray()
    {
        $estabs = [];
        foreach ($this->establishments as $establishment) {
            $estabs[] = [
                'id_company_pk' => $this->id_company_pk,
                'id_user_pk' => $this->id_user_pk,
                'cod_estab_pk' => $establishment,
            ];
        }

        return $estabs;
    }
}
