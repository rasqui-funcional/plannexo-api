<?php


namespace App\Domain\User\Entity;


class UserProfile
{
    /**
     * @var int
     */
    private int $id_company_fk;

    /**
     * @var int
     */
    private int $id_profile_fk;

    /**
     * @var int
     */
    private int $id_user_fk;

    public function __construct(array $data = [])
    {
        $this->setIdCompanyFk($data['id_company_fk'] ??= 0);
        $this->setIdProfileFk($data['id_profile_fk'] ??= 0);
        $this->setIdUserFk($data['id_user_fk'] ??= 0);
    }

    /**
     * @return int
     */
    public function getIdCompanyFk(): int
    {
        return $this->id_company_fk;
    }

    /**
     * @param int $id_company_fk
     */
    public function setIdCompanyFk(int $id_company_fk): void
    {
        $this->id_company_fk = $id_company_fk;
    }

    /**
     * @return int
     */
    public function getIdProfileFk(): int
    {
        return $this->id_profile_fk;
    }

    /**
     * @param int $id_profile_fk
     */
    public function setIdProfileFk(int $id_profile_fk): void
    {
        $this->id_profile_fk = $id_profile_fk;
    }

    /**
     * @return int
     */
    public function getIdUserFk(): int
    {
        return $this->id_user_fk;
    }

    /**
     * @param int $id_user_fk
     */
    public function setIdUserFk(int $id_user_fk): void
    {
        $this->id_user_fk = $id_user_fk;
    }

    public function exchangeArray()
    {
        return [
            'id_company_fk' => $this->id_company_fk,
            'id_profile_fk' => $this->id_profile_fk,
            'id_user_fk' => $this->id_user_fk,
        ];
    }
}
