<?php


namespace App\Domain\User;


use App\Domain\User\Entity\Profile;

interface ProfileInterface
{
    /**
     * ProfileInterface constructor.
     * @param Profile $profile
     */
    public function __construct(Profile $profile);

    /**
     * @return mixed
     */
    public function getProfiles();

    /**
     * @param int $id
     * @return mixed
     */
    public function getProfile(int $id);

    /**
     * @return mixed
     */
    public function createProfile();

    /**
     * @param int $id
     * @return mixed
     */
    public function updateProfile(int $id);

    /**
     * @param int $id
     * @return mixed
     */
    public function deleteProfile(int $id);
}
