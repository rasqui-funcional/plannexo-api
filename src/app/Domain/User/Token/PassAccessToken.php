<?php


namespace App\Domain\User\Token;


use App\Domain\Auth\AuthToken;

class PassAccessToken implements AuthToken
{
    /**
     * @var string
     */
    private string $token;

    /**
     * @var string
     */
    private string $sault = 'R[(L';

    /**
     * @var int
     */
    private int $uuid;

    /**
     * @var int
     */
    private int $ccid;

    /**
     * @param int $uuid
     * @return PassAccessToken
     */
    public function setUuid(int $uuid): PassAccessToken
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * @param int $ccid
     * @return PassAccessToken
     */
    public function setCcid(int $ccid): PassAccessToken
    {
        $this->ccid = $ccid;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function generateToken(): AuthToken
    {
        $dateTime = new \DateTime('now', new \DateTimeZone('America/Sao_Paulo'));
        $dateTime->modify('+1 day');

        $data = sprintf('user_id=%d&company_id=%d&expired=%s&d=%s',
            $this->uuid, $this->ccid, $dateTime->getTimestamp(), $this->sault);

        $this->token = base64_encode($data);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function token()
    {
        return $this->token;
    }
}
