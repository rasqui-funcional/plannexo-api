<?php
declare(strict_types=1);

namespace App\Domain\User\Traits;

use App\Domain\User\Entity\User;
use App\Domain\User\UserManagersInterface;
use Illuminate\Support\Facades\DB;

/**
 * Trait UserManagersTrait
 * @package App\Domain\User\Traits
 */
trait UserManagersTrait
{

    /**
     * @var User
     */
    private $user_managers;

    public function __construct(UserManagersInterface $user_managers)
    {
        $this->user_managers = $user_managers;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getUserManagers(): array
    {

        return $this->user_managers->getUserManagers();

    }
}
