<?php
declare(strict_types=1);

namespace App\Domain\User\Traits;


use App\Domain\User\Entity\User;
use App\Domain\User\UserStandardDepositInterface;
use Illuminate\Support\Facades\DB;

/**
 * Trait UserStandardDepositTrait
 * @package App\Domain\User\Traits
 */
trait UserStandardDepositTrait
{
    /**
     * @var UserStandardDepositInterface
     */
    private $user_deposit;

    /**
     * UserStandardDepositTrait constructor.
     * @param UserStandardDepositInterface $user_deposit
     */
    public function __construct(UserStandardDepositInterface $user_deposit)
    {
        $this->user_deposit = $user_deposit;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getUserStandardDeposit(): array
    {

        return $this->user_deposit->getUserStandardDeposit();
    }

    /**
     * @param mixed $estabs
     */
    public function setEstabs($estabs)
    {
        $this->user_deposit->setEstabs($estabs);

        return $this;
    }
}
