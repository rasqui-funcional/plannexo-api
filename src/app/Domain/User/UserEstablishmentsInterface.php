<?php


namespace App\Domain\User;

use App\Domain\User\Entity\User;
use App\Domain\User\Entity\UserEstablishments;

/**
 * Interface UserEstablishmentsInterface
 * @package App\Domain\User
 */
interface UserEstablishmentsInterface
{
    /**
     * UserEstablishmentsInterface constructor.
     * @param UserEstablishments $entity
     */
    public function __construct(UserEstablishments $entity);

    /**
     * @return mixed
     */
    public function getEstablishments();

    /**
     * @return mixed
     */
    public function getEstablishmentsCodes();

    /**
     * @return mixed
     */
    public function createUserEstablishments();

    /**
     * @param int $id
     * @return mixed
     */
    public function deleteUserEstablishments();
}
