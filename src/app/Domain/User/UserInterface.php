<?php


namespace App\Domain\User;


use App\Domain\User\Entity\User;

/**
 * Interface UserInterface
 * @package App\Domain\User
 */
interface UserInterface
{
    /**
     * UserInterface constructor.
     * @param User $user
     */
    public function __construct(User $user);

    /**
     * @return mixed
     */
    public function getUsers();

    /**
     * @param int $id
     * @return mixed
     */
    public function getUser(int $id);

    /**
     * @return mixed
     */
    public function createUser();

    /**
     * @param int $id
     * @return mixed
     */
    public function updateUser(int $id);

    /**
     * @param int $id
     * @return mixed
     */
    public function deleteUser(int $id);
}
