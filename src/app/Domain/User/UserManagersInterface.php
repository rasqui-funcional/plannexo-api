<?php
declare(strict_types=1);

namespace App\Domain\User;

use App\Domain\User\Entity\User;

/**
 * Interface UserManagersInterface
 * @package App\Domain\User
 */
interface UserManagersInterface
{

    /**
     * UserManagersInterface constructor.
     * @param User $user
     */
    public function __construct(User $user);

    /**
     * @return array
     */
    public function getUserManagers(): array;
}
