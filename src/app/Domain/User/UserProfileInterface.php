<?php


namespace App\Domain\User;


use App\Domain\User\Entity\UserProfile;

/**
 * Interface UserInterface
 * @package App\Domain\User
 */
interface UserProfileInterface
{
    /**
     * UserInterface constructor.
     * @param UserProfile $user
     */
    public function __construct(UserProfile $user);

    /**
     * @return mixed
     */
    public function getUserProfiles();

    /**
     * @param int $id
     * @return mixed
     */
    public function getUserProfile(int $id);

    /**
     * @return mixed
     */
    public function createUserProfile();

    /**
     * @param int $id
     * @return mixed
     */
    public function updateUserProfile(int $id);

    /**
     * @param int $id
     * @return mixed
     */
    public function deleteUserProfile(int $id);

    /**
     * @return mixed
     */
    public function getUserProfileByUserId();
}
