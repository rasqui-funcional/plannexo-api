<?php
declare(strict_types=1);

namespace App\Domain\User;

use App\Domain\User\Entity\User;

/**
 * Interface UserStandardDeposit
 * @package App\Domain\User
 */
interface UserStandardDepositInterface
{

    /**
     * UserManagersModel constructor.
     * @param User $user
     */
    public function __construct(User $user);
    /**
     * @return array
     */
    public function getUserStandardDeposit(): array;

    /**
     * @param mixed $estabs
     */
    public function setEstabs($estabs);
}
