<?php

namespace App\Events;

use App\Domain\User\Entity\User;
use App\Http\Services\AF\CompanySuppliersStoreCacheService;
use GuzzleHttp\Client;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CompanySuppliersStoreEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $user_login;

    /**
     * Create a new event instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    public function createCompanySuppliers()
    {
        (new CompanySuppliersStoreCacheService(resolve(Client::class)))->createCompanySuppliers($this->user->getIdCompanyFk());
    }
}
