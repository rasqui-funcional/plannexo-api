<?php


namespace App\Events;

use App\Domain\Email\EmailInterface;
use App\Domain\User\Token\PassAccessToken;
use App\Http\Services\EmailSendingService;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendEmailEvent
{
    /**
     * @var int
     */
    private int $user_id;

    /**
     * @var int
     */
    private int $company_id;

    /**
     * @var string
     */
    private string $title;

    /**
     * @var string
     */
    private string $email;

    /**
     * @var EmailInterface|EmailSendingService
     */
    private EmailSendingService $emailSendingService;

    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * SendEmailEvent constructor.
     * @param EmailInterface $emailSendingService
     */
    public function __construct(EmailInterface $emailSendingService)
    {
        $this->emailSendingService = $emailSendingService;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    /**
     * @param array $data
     * @return $this
     */
    public function exchangeArray(array $data)
    {
        $this->user_id = $data['id'];
        $this->company_id = $data['id_company'];
        $this->title = $data['name'];
        $this->email = $data['email'];
        return $this;
    }

    /**
     * @throws \Throwable
     */
    public function send(): void
    {
        $token = (new PassAccessToken())
            ->setUuid($this->user_id)
            ->setCcid($this->company_id)
            ->generateToken()
            ->token();

        $this->emailSendingService->exchangeArray([
            'receivers' => [$this->email],
            'subject' => 'Cadastro de Nova Senha',
            'htmlBody' => view('new-user', [
                'title' => $this->title, 'token' => $token, 'year' => date('Y')
            ])->render()
        ]);

        try {
            $this->emailSendingService->sendMailings();
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            // TODO: verificar tratativas para retentativa de envio
        }
    }
}
