<?php


namespace App\Events;

use App\Domain\User\Entity\UserEstablishments;
use App\Model\UserEstablishmentsModel;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserEstablishmentsEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var UserEstablishmentsModel
     */
    private UserEstablishmentsModel $user_establishments;

    /**
     * UserEstablishmentsEvent constructor.
     * @param UserEstablishments $userEstablishments
     */
    public function __construct(UserEstablishments $userEstablishments)
    {
        $this->user_establishments = new UserEstablishmentsModel($userEstablishments);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    public function createUserEstablishments()
    {
        $this->user_establishments->createUserEstablishments();
    }

    public function deleteUserEstablishments()
    {
        $this->user_establishments->deleteUserEstablishments($this->user_establishments->getEntity()->getIdUserFk());
    }
}
