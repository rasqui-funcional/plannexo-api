<?php


namespace App\Events;

use App\Domain\User\Entity\UserProfile;
use App\Model\UserProfileModel;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserProfileEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var UserProfileModel
     */
    private UserProfileModel $user_profile;

    /**
     * UserProfileEvent constructor.
     * @param UserProfile $userProfile
     */
    public function __construct(UserProfile $userProfile)
    {
        $this->user_profile = new UserProfileModel($userProfile);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    public function createUserProfile()
    {
        $this->user_profile->createUserProfile();
    }

    public function deleteUserProfile()
    {
        $this->user_profile->deleteUserProfile($this->user_profile->getEntity()->getIdUserFk());
    }
}
