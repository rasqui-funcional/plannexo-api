<?php

namespace App\Helpers;

class BioIdHashHelper
{
    private static string $command = 'python';

    public static function generate(string $url, string $method = 'GET')
    {
        if (empty($url) || is_null($url)) {
            // TODO: tratar os erros aqui com logs e retornos corretos
            return null;
        }

        $env = env('APP_ENV', 'prod');
        $path = app_path('../crypto/generate_hash.py');
        return trim(shell_exec(sprintf('%s %s %s %s %s', self::$command, $path, $url, $method, $env)));
    }
}
