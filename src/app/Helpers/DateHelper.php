<?php
declare(strict_types=1);

namespace App\Helpers;


use Carbon\Carbon;

/**
 * Class DateHelper
 * @package App\Helpers
 */
class DateHelper
{

    /**
     * @param string $date
     * @return string
     */
    public static function isoFormat(string $date): string
    {
        return Carbon::parse(self::cleanUpDate($date))->format('Y-m-d H:i:s');
    }

    private static function cleanUpDate($date)
    {
        return str_replace('/', '-', $date);
    }

}
