<?php

namespace App\Helpers;


/**
 * Class Mask
 * @package App\Helpers
 */
class MaskHelper
{
    /*
     * @param int $number
     * @return string
     */
    public static function cnpjCpf($number)
    {
        $cnpj_cpf = preg_replace("/\D/", '', $number);

        $cnpj_cpf_final = preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_cpf);

        if (strlen($cnpj_cpf) === 11) {
            $cnpj_cpf_final = preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "\$1.\$2.\$3-\$4", $cnpj_cpf);
        }

        return $cnpj_cpf_final;
    }
}
