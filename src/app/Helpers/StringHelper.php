<?php
declare(strict_types=1);

namespace App\Helpers;


/**
 * Class StringHelper
 * @package App\Helpers
 */
class StringHelper
{

    /**
     * @param string $str
     * @param string $separator
     * @return array
     */
    public static function toArray(string $str = '', $separator = ','): array
    {
        return array_filter(array_map(array((new self), 'clearString'), explode($separator, $str)));
    }


    /**
     * @param string $str
     * @return string|void
     */
    private static function clearString(string $str)
    {
        if (!empty($str))
            return trim($str);

    }

}
