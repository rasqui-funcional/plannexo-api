<?php
declare(strict_types=1);

namespace App\Http\Client;

/**
 * Client interface for sending HTTP requests.
 */
interface HttpServiceInterface
{
    /**
     * @param string $path
     * @return $this
     */
    public function setPath(string $path = ""): self;

    /**
     * @param string $method
     * @return $this
     */
    public function method(string $method = ""): self;

    /**
     * @param array $headers
     * @return $this
     */
    public function setHeaders(array $headers = []): self;

    /**
     * @param array $data
     * @return $this
     */
    public function setData(array $data = []): self;

    /**
     * @return $this
     */
    public function send(): self;

    /**
     * @return mixed
     */
    public function getStatusCode();

    /**
     * @return mixed
     */
    public function getContents();

    /**
     * @return mixed
     */
    public function decode();

}
