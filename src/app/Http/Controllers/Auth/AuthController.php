<?php

namespace App\Http\Controllers\Auth;

use App\Domain\Auth\BioId\BioIdAuthenticate;
use App\Domain\User\Entity\User;
use App\Events\CompanySuppliersStoreEvent;
use App\Events\UserLoginEvent;
use App\Http\Client\HttpServiceInterface;
use App\Http\Controllers\Controller;
use App\Http\Services\AF\CompanySuppliersStoreCacheService;
use App\Model\UserLoginModel;
use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

/**
 * Class AuthController
 * @package App\Http\Controllers\Auth
 */
class AuthController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAuthUrl()
    {
        try {
            $data['redirectUrl'] = (new BioIdAuthenticate())->getAuthorizationUrl();
            return response()->json($data, 200);
        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return response()->json(['error' => ['message' => $e->getMessage()]], 401);
        }
    }

    /**
     * @OA\Get(
     *     tags={"Auth"},
     *     summary="Authenticate",
     *     description="Authenticate",
     *     path="/api/v1/auth/callback?code={code}",
     *     @OA\Parameter(name="code",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Return list of users"
     *     ),
     *     @OA\Response(
     *          response="400",
     *          description="Bad Request"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *   * @OA\Post(
     *     tags={"Auth"},
     *     summary="Login",
     *     description="Login",
     *     path="/api/v1/auth/login",
     *     @OA\Response(
     *          response="200",
     *          description="Authorized"
     *     ),
     *     @OA\Response(
     *          response="400",
     *          description="Bad Request"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Not authorized'"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function callback(Request $request)
    {
        ini_set('display_errors', 0);
        error_reporting(0);

        try {
            event(new UserLoginEvent($request->user_login));
            event(new CompanySuppliersStoreEvent($request->user_login));

            return response()->json(['user' => array_merge(['token' => $request->token], $request->user_session->getUserData())], 200);
        } catch (\Exception $e) {
            Log::error($e->getMessage(), []);

            return response()->json(['error' => ['message' => $e->getMessage()]], 401);
        }
    }


    /**
     * @OA\Get(
     *     tags={"Auth"},
     *     summary="Verify if mail exists",
     *     path="/api/v1/auth/mail-validation/{email}",
     *     description="Verify if email exists on Plannexo",
     *     @OA\Parameter(
     *         name="email",
     *         in="path",
     *         description="Mail tht need be verified",
     *         required=true,
     *     ),
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"name"},
     *                 @OA\Property(
     *                     property="login",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *                 example={"auth": {"login": "qa@bionexo.com", "passwors": "passWord"}}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description=""
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="A user with the specified email was not found"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @param string $mail
     * @return JsonResponse
     * @throws Exception
     */
    public function mailValidation(Request $request): JsonResponse
    {

        try {
            $success = true;
            $user = new User(['email' => $request->email]);
            $user_login = new UserLoginModel($user);

            $response = $user_login->getUserByEmail();

            if (!$response->getMailLogin()) {
                $success = false;
            }

            $auth = [
                'name' => $response->getName(),
                'email' => $response->getMailLogin(),
                'success' => $success
            ];

            return response()->json(compact('auth'), 200);
        } catch (\Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }
    }
}
