<?php

namespace App\Http\Controllers\Company;

use App\Domain\Company\Entity\Company;
use App\Http\Controllers\Controller;
use App\Model\CompanyModel;
use App\Model\EstablishmentModel;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * @OA\Get(
     *     tags={"Company"},
     *     summary="Returns a list of Companys",
     *     description="Returns a object of Companys",
     *     path="/api/v1/companies",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Return list of profiles"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getList(Request $request): JsonResponse
    {
        $companyModel = new CompanyModel(new Company());
        $companies = $companyModel->getCompanies();
        return response()->json(['companies' => $companies], 200);
    }

    /**
     * @OA\Get(
     *     tags={"Company"},
     *     summary="Returns a list of Company establishments",
     *     path="/api/v1/companies/establishments",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *      @OA\Response(
     *          response="200",
     *          description="Returns a list of Company establishments",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  required={"name"},
     *                  @OA\Property(
     *                      property="id",
     *                      type="string",
     *                      property="short_name",
     *                      type="string",
     *                      property="name",
     *                      type="string"
     *                  ),
     *                  example = {"establishments": {
     *                      {"id": "1", "short_name": "", "name": "HDP" },
     *                      {"id": "2", "short_name": "", "name": "HSM" }
     *                  }}
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="A Establishment with the specified ID was not found"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getListEstablishment(Request $request): JsonResponse
    {
        $establishments = (new EstablishmentModel())->getEstablishmentsByCompanyId($request->user->getIdCompanyFk());
        return response()->json(['establishments' => $establishments], 200);
    }

    /**
     * @OA\Get(
     *     tags={"Company"},
     *     summary="Returns a object Company",
     *     path="/api/v1/companies/{cid}",
     *     @OA\Parameter(
     *         name="cid",
     *         in="path",
     *         description="ID of Company to return",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Return Company object"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="A Company with the specified ID was not found"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @param int $cid
     * @return JsonResponse
     */
    public function get(Request $request, int $cid): JsonResponse
    {
        $companyModel = new CompanyModel(new Company());
        $company = $companyModel->getCompany($cid);
        return response()->json(['company' => $company], 200);
    }

    /**
     * @OA\Post(
     *     tags={"Company"},
     *     summary="Create a new Company",
     *     path="/api/v1/companies",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"name"},
     *                 @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 example={"name": ""}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Company created",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function create(Request $request): JsonResponse
    {
        return response()->json(['success' => [
            'message' => 'Method not Allowed.'
        ]], 405);
    }

    /**
     * @OA\Put(
     *     tags={"Company"},
     *     summary="Update a Company",
     *     path="/api/v1/companies/{cid}",
     *     @OA\Parameter(
     *         name="cid",
     *         in="path",
     *         description="ID of Company to return",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"name"},
     *                 @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 example={"name": ""}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Update Company object"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="A Company with the specified ID was not found"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @param int $cid
     * @return JsonResponse
     * @throws Exception
     */
    public function update(Request $request, int $cid): JsonResponse
    {
        return response()->json(['success' => [
            'message' => 'Method not Allowed.'
        ]], 405);
    }

    /**
     * @OA\Delete(
     *     tags={"Company"},
     *     summary="Delete a Company",
     *     path="/api/v1/companies/{cid}",
     *     @OA\Parameter(
     *         name="cid",
     *         in="path",
     *         description="ID of Company to delete",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Delete Company object"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="A Company with the specified ID was not found"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param int $cid
     * @return JsonResponse
     */
    public function delete(int $cid): JsonResponse
    {
        return response()->json(['success' => [
            'message' => 'Method not Allowed.'
        ]], 405);
    }
}
