<?php

namespace App\Http\Controllers\Company;

use App\Domain\Company\CompanyGroupingInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CompanyGroupingController extends Controller
{

    private CompanyGroupingInterface $company_grouping;

    /**
     * CompanyGroupingController constructor.
     * @param CompanyGroupingInterface $company_grouping
     */
    public function __construct(CompanyGroupingInterface $company_grouping)
    {
        $this->company_grouping = $company_grouping;
    }


    /**
     * @OA\Get(
     *     tags={"Company"},
     *     summary="Returns a list of groups for the company",
     *     description="Returns a list of groups for the company",
     *     path="/api/v1/companies/grouping",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns a list of groups for the company",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *      @OA\Schema(
     *           @OA\Property(
     *              property="grouping",
     *               type="array",
     *              description="The response data",
     *              @OA\Items(
     *                @OA\Property(
     *                 property="id",
     *                 type="integer",
     *                 description="The policy ID",
     *                ),
     *                @OA\Property(
     *                         property="name",
     *                         type="string",
     *                         description="The policy name",
     *                 ),
     *               )
     *             ),
     *                 example= * {
     *     "grouping": {
     *         {
     *             "cod_group1_fk": {
     *                 "title": "Cat. Sistema",
     *                 "values": {
     *                     {"id": "DIV", "name": "DIV" },
     *                     {"id": "MAT", "name": "MAT" },
     *                     {"id": "MED", "name": "MED" }
     *                 }
     *             }
     *         },
     *         {
     *             "cod_group2_fk": {
     *                 "title": "Grupo",
     *                 "values": {
     *                      {"id": "0020", "name": "Bens de Pequeno Valor-20" },
     *                      {"id": "0012", "name": "Combustiveis, Lubrificantes e Gás-12" },
     *                      {"id": "008", "name": "Gases Medicinais-8" }
     *                 }
     *             }
     *         },
     *         {
     *             "cod_group4_fk": {
     *                 "title": "Subgrupo",
     *                 "values": {
     *                      {"id": "924", "name": " uniformes-24" },
     *                      {"id": "924",  "name": " uniformes-24" },
     *                      {"id": "16115", "name": "acessorios para uniformes-115" }
     *                 }
     *             }
     *         },
     *         {
     *             "cod_group4_fk": {
     *                 "title": "Classe",
     *                 "values": {
     *                      {"id": "16-115-348", "name": "acessorios para uniformes-348" },
     *                      {"id": "21-124-449", "name": "acessorios-449" },
     *                      {"id": "17-120-357", "name": "acessórios de higiene e limpeza-357" }
     *                 }
     *             }
     *         },
     *         {
     *             "cod_group5_fk": {
     *                 "title": "Filtros Especiais",
     *                 "values": {
     *                      {"id": "#", "name": "#" },
     *                      {"id": "1", "name": "Higiene e Limpeza" }
     *                 }
     *             }
     *         },
     *         {
     *             "cod_group6_fk": {
     *                 "title": "Contratos",
     *                 "values": {
     *                      {"id": "#", "name": "#" },
     *                      {"id": "Abbvie Farmaceutica Ltdda", "name": "Abbvie Farmaceutica Ltdda" },
     *                      {"id": "Alive Heart Mat Medico Ltda", "name": "Alive Heart Mat Medico Ltda" }
     *                 }
     *             }
     *         },
     *         {
     *             "cod_group7_fk": {
     *                 "title": "#",
     *                 "values": {
     *                      {"id": "A", "name": "ALTO" },
     *                      {"id": "B", "name": "BAIXO" },
     *                      {"id": "M", "name": "MDIO" }
     *                 }
     *             }
     *         },
     *         {
     *             "cod_group8_fk": {
     *                 "title": "Outros Agrupamentos",
     *                 "values": {
     *                      {"id": "#", "name": "#" },
     *                      {"id": "CME", "name": "CME" }
     *                 }
     *             }
     *         }
     *     }
     * }
     *              )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     *
     * )
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function getList(): JsonResponse
    {
        try {
            $grouping = $this->company_grouping->getCompanyGrouping();
            return response()->json(compact('grouping'), 200);

        } catch (\Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }

    }

    /**
     * @OA\Get(
     *     tags={"Company"},
     *     summary="Returns a of group by Id for the company",
     *     description="Returns a of group by Id for the company",
     *     path="/api/v1/companies/grouping/{id}",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns a of group by Id for the company",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *      @OA\Schema(
     *           @OA\Property(
     *              property="grouping",
     *               type="array",
     *              description="The response data",
     *              @OA\Items(
     *                @OA\Property(
     *                 property="id",
     *                 type="integer",
     *                 description="The policy ID",
     *                ),
     *                @OA\Property(
     *                         property="name",
     *                         type="string",
     *                         description="The policy name",
     *                 ),
     *               )
     *             ),
     *                 example= {
     *                       "grouping": {
     *                           "cod_group1_fk": {
     *                               "title": "Cat. Sistema",
     *                               "values": {
     *                                   {
     *                                       "id": "DIV",
     *                                       "name": "DIV"
     *                                   },
     *                                   {
     *                                       "id": "MAT",
     *                                       "name": "MAT"
     *                                   },
     *                                   {
     *                                       "id": "MED",
     *                                       "name": "MED"
     *                                   }
     *                               }
     *                           }
     *                       }
     *                   }
     *              )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     *
     * )
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */

    public function getGroup(Request $request): JsonResponse
    {
        try {
            $id = $request->id;
            $grouping = $this->company_grouping->getCompanyGroupingById($id);
            return response()->json(compact('grouping'), 200);

        } catch (\Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }

    }
}
