<?php

namespace App\Http\Controllers\Company;

use App\Domain\Company\CompanyGroupsInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CompanyGroupsController extends Controller
{

    private CompanyGroupsInterface $company_groups;

    /**
     * CompanyGroupsController constructor.
     * @param CompanyGroupsInterface $company_groups
     */
    public function __construct(CompanyGroupsInterface $company_groups)
    {
        $this->company_groups = $company_groups;
    }

    /**
     * @OA\Get(
     *     tags={"Company"},
     *     summary="Returns a list of groups for the company",
     *     description="Returns a group of company",
     *     path="/api/v1/companies/groups",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns a list of groups for the company",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *      @OA\Schema(
     *                 example= { "groups": {
     *                            {"id": "1", "title": "Cat. Sistema", "key": "cod_group1_fk"},
     *                            {"id": "2", "title": "Grupo", "key": "cod_group2_fk"},
     *                            {"id": "3", "title": "Subgrupo", "key": "cod_group3_fk"},
     *                           }
     *                      }
     *              )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     *
     * )
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function getList(Request $request): JsonResponse
    {
        try {

            $groups = $this->company_groups->getCompanyGroups();
            return response()->json(compact('groups'), 200);

        } catch (\Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }

    }

    /**
     * @OA\Get(
     *     tags={"Company"},
     *     summary="Returns a group to the company by Id",
     *     description="Returns a group to the company by Id",
     *     path="/api/v1/companies/groups/{id}",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *        name="id",
     *        in="query",
     *        required=true,
     *        description="The Group ID",
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns a group to the company by Id",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *      @OA\Schema(
     *                 example= { "groups": {
     *                            {"id": "1", "title": "Cat. Sistema", "key": "cod_group1_fk"}
     *                           }
     *                      }
     *              )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     *
     * )
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */

    public function getGroup(Request $request): JsonResponse
    {
        try {
            $id = $request->id;
            $groups = $this->company_groups->getCompanyGroup($id);
            return response()->json(compact('groups'), 200);

        } catch (\Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }

    }

    /**
     * @OA\Get(
     *     tags={"Company"},
     *     summary="Returns a group with data for the Company",
     *     description="Returns a group with data for the Company",
     *     path="/api/v1/companies/groups/data/{id}",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *        name="id",
     *        in="query",
     *        required=true,
     *        description="The Group ID",
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns a group with data for the Company",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *      @OA\Schema(
     *                 example= { "values": {
     *                              {"name": "DIV", "id": "DIV"},
     *                              {"name": "MAT", "id": "MAT"},
     *                              {"name": "MED", "id": "MED"}
     *                          }
     *                  }
     *              )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     *
     * )
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */

    public function getGroupData(Request $request): JsonResponse
    {
        try {
            $id = $request->id;
            $values = $this->company_groups->getCompanyGroupData($id);
            return response()->json(compact('values'), 200);

        } catch (\Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }
    }
}
