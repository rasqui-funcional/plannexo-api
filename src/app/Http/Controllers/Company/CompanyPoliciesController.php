<?php

namespace App\Http\Controllers\Company;

use App\Domain\Company\CompanyPoliciesInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class CompanyPoliciesController
 * @package App\Http\Controllers\Company
 */
class CompanyPoliciesController extends Controller
{

    /**
     * @var CompanyPoliciesInterface
     */
    private $policies;

    /**
     * CompanyPoliciesController constructor.
     * @param CompanyPoliciesInterface $polices
     */
    public function __construct(CompanyPoliciesInterface $polices)
    {
        $this->policies = $polices;
    }

    /**
     * @OA\Get(
     *     tags={"Company"},
     *     summary="Returns a list of Policies for the Company",
     *     description="Returns a list of Policies for the Company",
     *     path="/api/v1/companies/policies",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns a list of Policies for the Company",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *      @OA\Schema(
     *           @OA\Property(
     *              property="policies",
     *               type="array",
     *              description="The response data",
     *              @OA\Items(
     *                @OA\Property(
     *                 property="id",
     *                 type="integer",
     *                 description="The policy ID",
     *                ),
     *                @OA\Property(
     *                         property="name",
     *                         type="string",
     *                         description="The policy name",
     *                 ),
     *               )
     *             ),
     *                 example= {"policies": {
     *                                           {"id": 1,"name": "policyName"},
     *                                           {"id": 2,"name": "policyName 2"}
     *                              }
     *                          }
     *              )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     *
     * )
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function getList(): JsonResponse
    {
        try {

            $policies = $this->policies->getCompanyPolicies();
            return response()->json(compact('policies'), 200);

        } catch (\Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }

    }
}
