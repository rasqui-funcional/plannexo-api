<?php
declare(strict_types=1);

namespace App\Http\Controllers\Email;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Domain\Email\Entity\EmailModel;
use App\Model\Email\EmailModelModel;

class EmailModelController extends Controller
{
    private $email = [];

    /**
     * @OA\Get(
     *     tags={"Email"},
     *     summary="Returns a list of Email Models",
     *     description="Returns a object of EmailModel",
     *     path="/api/v1/email-models",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Return list of profiles"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getList(Request $request): JsonResponse
    {
        $this->email = [
            'id_company_fk' => $request->user->getIdCompanyFk()
        ];

        $emailModel_model = new EmailModelModel(new EmailModel($this->email));
        $email_models = $emailModel_model->getEmailModels();

        /*foreach ($email_models as $key => $model) {
        foreach ($email_models as $key => $model) {
            $model->id = (int) $model->id;
            $used_in = $model->used_in;
            $model->used_in = [];

            if($used_in != "[]") {
                $model->used_in = json_decode($used_in, true);
            }

            $model->id_company_fk = $request->company->getIdCompanyPk();
            $email_model = new EmailModel((array)$model);
        }*/

        $this->getEmailArrayCopy($email_models, $request->company->getIdCompanyPk());

        return response()->json(['email_models' => $email_models], 200);
    }

    /**
     * @OA\Get(
     *     tags={"Email"},
     *     summary="Returns a list of Email Models",
     *     description="Returns a object of EmailModel",
     *     path="/api/v1/email-models/all",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Return list of profiles"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getListWithBody(Request $request): JsonResponse
    {
        $this->email = [
            'id_company_fk' => $request->user->getIdCompanyFk()
        ];

        $emailModel_model = new EmailModelModel(new EmailModel($this->email));
        $email_models = $emailModel_model->getEmailModelsWithBody();

        /*foreach ($email_models as $key => $model) {
        foreach ($email_models as $key => $model) {
            $model->id = (int) $model->id;
            $used_in = $model->used_in;
            $model->used_in = [];

            if($used_in != "[]") {
                $model->used_in = json_decode($used_in, true);
            }

            $model->id_company_fk = $request->company->getIdCompanyPk();
            $email_model = new EmailModel((array)$model);
        }*/

        $this->getEmailArrayCopy($email_models, $request->company->getIdCompanyPk());

        return response()->json(['email_models' => $email_models], 200);
    }

    /**
     * @OA\Get(
     *     tags={"Email"},
     *     summary="Returns a object Email Model",
     *     path="/api/v1/email-models/{eid}",
     *     @OA\Parameter(
     *         name="eid",
     *         in="path",
     *         description="ID of Email Model to return",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Return Email Model object"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="A Email Model with the specified ID was not found"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @param int $eid
     * @return JsonResponse
     */
    public function get(Request $request, int $eid): JsonResponse
    {
        $this->email = [
            'id_company_fk' => $request->user->getIdCompanyFk()
        ];

        $emailModel_model = new EmailModelModel(new EmailModel($this->email));
        $email_model = $emailModel_model->getEmailModel($eid);
        foreach ($email_model as $key => $model) {
            $model->id = (int) $model->id;
            if($model->used_in == "[]") {
                $model->used_in = [];
            }
        }

        return response()->json(['email_models' => $email_model], 200);
    }

    /**
     * @OA\Post(
     *     tags={"Email"},
     *     summary="Save the Email Model",
     *     path="/api/v1/email-models/store",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     * @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="title",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="subject",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     type="string"
     *                 ),
     *                 example= {"email_models": {
     *                     {"title": "title 1", "subject": "Subject 1", "description": "Body HTML to email"}
     *                 } }
     *             )
     *         )
     *     ),
     * @OA\Response(
     *         response=201,
     *         description="Email Model saved",
     *     ),
     * @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     * @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     * @OA\Response(
     *         response=417,
     *         description="Create User Failed"
     *     ),
     * @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     * @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        try {
            $post_models = $request->post('email_models');

            foreach ($post_models as $key => $model) {
                $model["id_company_fk"] = $request->company->getIdCompanyPk();
                $emailModel_model = new EmailModelModel(new EmailModel($model));
                $email_models = $emailModel_model->createEmailModel();
            }
            return response()->json(compact('email_models'), 201);

        } catch (Exception $e) {

            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }
    }

    /**
     * @OA\Put(
     *     tags={"Email"},
     *     summary="Update a Email Model",
     *     path="/api/v1/email-models/{eid}",
     *     @OA\Parameter(
     *         name="eid",
     *         in="path",
     *         description="ID of Email Model to return",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"id"},
     *                 @OA\Property(
     *                     property="title",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="subject",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="body",
     *                     type="string"
     *                 ),
     *                 example={"title": "", "subject": "", "body": ""}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Update Email Model object"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="A Email Model with the specified ID was not found"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=417,
     *         description="Updated Email Model Failed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @param int $eid
     * @return JsonResponse
     */
    public function update(Request $request, int $eid): JsonResponse
    {
        try {
            $post_models = $request->post('email_models');

            foreach ($post_models as $key => $model) {
                $model["id_company_fk"] = $request->company->getIdCompanyPk();
                $emailModel_model = new EmailModelModel(new EmailModel($model));
                $email_models = $emailModel_model->updateEmailModel($eid);
            }
            return response()->json(compact('email_models'), 201);

        } catch (Exception $e) {

            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }
    }

    /**
     * @OA\Delete(
     *     tags={"Email"},
     *     summary="Delete a Email Model",
     *     path="/api/v1/email-models/{eid}",
     *     @OA\Parameter(
     *         name="eid",
     *         in="path",
     *         description="ID of Email Model to delete",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Delete Email Model object"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="A Email Model with the specified ID was not found"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @param int $eid
     * @return JsonResponse
     */
    public function delete(Request $request, int $eid): JsonResponse
    {
        try {
            $emailModel_model = new EmailModelModel(new EmailModel($request->all()));
            $email_models = $emailModel_model->deleteEmailModel($eid);

            return response()->json(compact('email_models'), 201);

        } catch (Exception $e) {

            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }
    }

    private function getEmailArrayCopy($email_models, $id_company_fk)
    {
        foreach ($email_models as $key => $model) {
            $model->id = (int) $model->id;
            $used_in = $model->used_in;
            $model->used_in = [];

            if($used_in != "[]") {
                $model->used_in = json_decode($used_in, true);
            }

            $model->id_company_fk = $id_company_fk;
            $email_model = new EmailModel((array)$model);
        }

        return;
    }
}
