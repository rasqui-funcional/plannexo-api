<?php
declare(strict_types=1);

namespace App\Http\Controllers\FUP;

use App\Domain\FollowUp\FollowUpFiltersInterface;
use App\Http\Controllers\Controller;
use App\Domain\FollowUp\Filters\FollowUpFilters;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FollowUpFiltersController extends Controller
{


    private FollowUpFiltersInterface $follow_up_filters;

    public function __construct(FollowUpFiltersInterface $follow_up_filters)
    {
        $this->follow_up_filters = $follow_up_filters;
    }

    /**
     * @OA\Get(
     *     tags={"Follow Up"},
     *     summary="Return list of filters to FollowUp Module",
     *     description="Return list of filters to FollowUp Module",
     *     path="/api/v1/fup/filters",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Return list of filters to FollowUp Module",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *      @OA\Schema(
     *           @OA\Property(
     *              property="fupFilters",
     *               type="array",
     *              description="The response data",
     *              @OA\Items(
     *               @OA\Property(
     *                      property="estabs",
     *                      type="array",
     *                      description="Estabs of user",
     *                          @OA\Items(
     *                              @OA\Property(
     *                                  property="id",
     *                                  type="string",
     *                                  description="Id of estab",
     *                              ),
     *                              @OA\Property(
     *                                  property="name",
     *                                  type="string",
     *                                  description="Name estab",
     *                              ),
     *                          )
     *                ),
     *               @OA\Property(
     *                      property="managers",
     *                      type="array",
     *                      description="Managers on same establishment of of user",
     *                          @OA\Items(
     *                              @OA\Property(
     *                                  property="id",
     *                                  type="integer",
     *                                  description="id of manager",
     *                              ),
     *                              @OA\Property(
     *                                  property="name",
     *                                  type="string",
     *                                  description="Name of manager",
     *                              ),
     *                          )
     *                ),
     *               @OA\Property(
     *                      property="policies",
     *                      type="array",
     *                      description="Policies on company of of user",
     *                          @OA\Items(
     *                              @OA\Property(
     *                                  property="id",
     *                                  type="string",
     *                                  description="id of policy",
     *                              ),
     *                              @OA\Property(
     *                                  property="name",
     *                                  type="integer",
     *                                  description="Name of policy",
     *                              ),
     *                          )
     *                ),
     *               @OA\Property(
     *                      property="standard_deposit",
     *                      type="array",
     *                      description="Standard Deposit on same establishment of of user",
     *                          @OA\Items(
     *                              @OA\Property(
     *                                  property="id",
     *                                  type="integer",
     *                                  description="id of standard deposit",
     *                              ),
     *                              @OA\Property(
     *                                  property="name",
     *                                  type="string",
     *                                  description="Name of standard deposit",
     *                              ),
     *                          )
     *                ),
     *               )
     *             ),
     *                 example= {"fupFilters": {
     *                                "estabs": {
     *                                              {"id": "1","name": "estabName"},
     *                                              {"id": "2","name": "estabName 2"}
     *                                          },
     *                                "managers": {
     *                                              {"id": 1,"name": "managerName"},
     *                                              {"id": 2,"name": "managersName 2"}
     *                                          },
     *                                "policies": {
     *                                              {"id": 1,"name": "policyName"},
     *                                              {"id": 2,"name": "policyName 2"}
     *                                          },
     *                                "standard_deposit": {
     *                                              {"id": 1,"name": "depositName"},
     *                                              {"id": 2,"name": "depositName 2"}
     *                                          },
     *                                  "grouping": {
     *                                      {
     *                                          "cod_group1_fk": {
     *                                              "title": "Cat. Sistema",
     *                                              "values": {
     *                                                  {"id": "DIV", "name": "DIV" },
     *                                                  {"id": "MAT", "name": "MAT" },
     *                                                  {"id": "MED", "name": "MED" }
     *                                              }
     *                                          }
     *                                      },
     *                                      {
     *                                          "cod_group2_fk": {
     *                                              "title": "Grupo",
     *                                              "values": {
     *                                                   {"id": "0020", "name": "Bens de Pequeno Valor-20" },
     *                                                   {"id": "0012", "name": "Combustiveis, Lubrificantes e Gás-12" },
     *                                                   {"id": "008", "name": "Gases Medicinais-8" }
     *                                              }
     *                                          }
     *                                      },
     *                                      {
     *                                          "cod_group4_fk": {
     *                                              "title": "Subgrupo",
     *                                              "values": {
     *                                                   {"id": "924", "name": " uniformes-24" },
     *                                                   {"id": "924",  "name": " uniformes-24" },
     *                                                   {"id": "16115", "name": "acessorios para uniformes-115" }
     *                                              }
     *                                          }
     *                                      },
     *                                      {
     *                                          "cod_group4_fk": {
     *                                              "title": "Classe",
     *                                              "values": {
     *                                                   {"id": "16-115-348", "name": "acessorios para uniformes-348" },
     *                                                   {"id": "21-124-449", "name": "acessorios-449" },
     *                                                   {"id": "17-120-357", "name": "acessórios de higiene e limpeza-357" }
     *                                              }
     *                                          }
     *                                      },
     *                                      {
     *                                          "cod_group5_fk": {
     *                                              "title": "Filtros Especiais",
     *                                              "values": {
     *                                                   {"id": "#", "name": "#" },
     *                                                   {"id": "1", "name": "Higiene e Limpeza" }
     *                                              }
     *                                          }
     *                                      },
     *                                      {
     *                                          "cod_group6_fk": {
     *                                              "title": "Contratos",
     *                                              "values": {
     *                                                   {"id": "#", "name": "#" },
     *                                                   {"id": "Abbvie Farmaceutica Ltdda", "name": "Abbvie Farmaceutica Ltdda" },
     *                                                   {"id": "Alive Heart Mat Medico Ltda", "name": "Alive Heart Mat Medico Ltda" }
     *                                              }
     *                                          }
     *                                      },
     *                                      {
     *                                          "cod_group7_fk": {
     *                                              "title": "#",
     *                                              "values": {
     *                                                   {"id": "A", "name": "ALTO" },
     *                                                   {"id": "B", "name": "BAIXO" },
     *                                                   {"id": "M", "name": "MDIO" }
     *                                              }
     *                                          }
     *                                      },
     *                                      {
     *                                          "cod_group8_fk": {
     *                                              "title": "Outros Agrupamentos",
     *                                              "values": {
     *                                                   {"id": "#", "name": "#" },
     *                                                   {"id": "CME", "name": "CME" }
     *                                              }
     *                                          }
     *                                      }
     *                                  }
     *                             }
     *                          }
     *              )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     *
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getList(): JsonResponse
    {
        try {

            $fupFilters = $this->follow_up_filters->getFollowUpFilters();
            return response()->json(compact('fupFilters'), 200);
        } catch (\Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }
    }
}
