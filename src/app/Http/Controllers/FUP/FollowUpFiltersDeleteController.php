<?php
declare(strict_types=1);

namespace App\Http\Controllers\FUP;

use App\Http\Controllers\Controller;
use App\Http\Requests\FilterStoreRequest;
use App\Http\Requests\FilterDeleteRequest;
use App\Model\FollowUp\FollowUpUserFilterDeleteModel;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FollowUpFiltersDeleteController extends Controller
{

    /**
     * @OA\Delete(
     *     tags={"Follow Up"},
     *     summary="Delete a user filter on FollowUp",
     *     path="/api/v1/fup/users/filters/{filter_id}",
     *     @OA\Parameter(
     *        name="filter_id",
     *        in="query",
     *        required=true,
     *        description="The filter ID",
     *        @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     * @OA\Response(
     *         response=200,
     *         description="Filter deleted",
     *     ),
     * @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     * @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     * @OA\Response(
     *         response=417,
     *         description="Create User Failed"
     *     ),
     * @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     * @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function delete(FilterDeleteRequest $request): JsonResponse
    {

        try {

            $fupFilters = (new FollowUpUserFilterDeleteModel($request->user))->deleteFilter((int)$request->filter_id);
            $fupFilters = ['success' => $fupFilters];

            return response()->json(compact('fupFilters'), 200);

        } catch (\Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }
    }
}
