<?php
declare(strict_types=1);

namespace App\Http\Controllers\FUP;

use App\Http\Controllers\Controller;
use App\Http\Requests\FilterUpdateRequest;
use App\Model\FollowUp\FollowUpUserFilterUpdateModel;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FollowUpFiltersUpdateController extends Controller
{

    /**
     * @OA\Put(
     *     tags={"Follow Up"},
     *     summary="Update a user filter on FollowUp",
     *     path="/api/v1/fup/users/filters/{filter_id}",
     *     @OA\Parameter(
     *        name="filter_id",
     *        in="query",
     *        required=true,
     *        description="The filter ID",
     *        @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *           @OA\Property(
     *                  property="fupFilters",
     *                  type="array",
     *                  description="The body data",
     *              @OA\Items(
     *                   @OA\Property(
     *                       property="filter_nome",
     *                       type="string",
     *                   ),
     *                   @OA\Property(
     *                       property="filter_data",
     *                       type="string",
     *                   ),
     *              ),
     *              ),
     *                 example= {
     *                           "fupFilters": {
     *                               "filter_nome": "Filter Name",
     *                               "filter_data": "'curves':{'ABC': ['A', 'B'],'XYZ': [...],'PQR': [...],'123': [...],},'estabs':['1', ...],'managers': ['1', '2', ...],'policies': ['1', '2',...],'standard_deposit': ['1', '2']"
     *                             }
     *                           }
     *             )
     *         )
     *     ),
     * @OA\Response(
     *         response=201,
     *         description="Filter updated",
     *     ),
     * @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     * @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     * @OA\Response(
     *         response=417,
     *         description="Create User Failed"
     *     ),
     * @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     * @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function update(FilterUpdateRequest $request): JsonResponse
    {

        try {
            $data = $request->post('fupFilters');
            $fupFilters = (new FollowUpUserFilterUpdateModel($request->user))->setData($data)->updateFilter((int)$request->filter_id);

            return response()->json(compact('fupFilters'), 201);

        } catch (\Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }
    }
}
