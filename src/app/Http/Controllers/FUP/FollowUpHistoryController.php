<?php

namespace App\Http\Controllers\FUP;

use App\Domain\FollowUp\FollowUpHistoryInterface;
use App\Http\Controllers\Controller;
use DateTime;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Mockery\Matcher\Any;

class FollowUpHistoryController extends Controller
{
    private FollowUpHistoryInterface $follow_up_history;
    private $paramStart;

    public function __construct(FollowUpHistoryInterface $follow_up_history)
    {
        $this->follow_up_history = $follow_up_history;
    }

    /**
     * @OA\Get(
     *     tags={"Follow Up"},
     *     summary="Returns History of FollowUp by Supplier",
     *     path="/api/v1/fup/history/{supplierId}",
     *     @OA\Parameter(
     *         name="supplierId",
     *         in="path",
     *         description="ID of Supplier",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns History of FollowUp by Supplier",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *      @OA\Schema(
     *           @OA\Property(
     *              property="Returns History FollowUp",
     *               type="array",
     *              description="The response data",
     *              @OA\Items(
     *                  @OA\Property(
     *                         property="supplier",
     *                         type="string",
     *                         description="Name of supplier",
     *                 ),
     *                @OA\Property(
     *                      property="supplierId",
     *                      type="integer",
     *                      description="The supplier ID",
     *                  ),
     *               @OA\Property(
     *                      property="contactInformation",
     *                      type="array",
     *                      description="Contact information of Supplier",
     *                          @OA\Items(
     *                              @OA\Property(
     *                                  property="name",
     *                                  type="string",
     *                                  description="Name of contact",
     *                              ),
     *                              @OA\Property(
     *                                  property="phone",
     *                                  type="string",
     *                                  description="Phone of contact",
     *                              ),
     *                              @OA\Property(
     *                                  property="email",
     *                                  type="string",
     *                                  description="Email of contact",
     *                              ),
     *                          )
     *                ),
     *                @OA\Property(
     *                         property="criticalItensQty",
     *                         type="integer",
     *                         description="Total of critical items",
     *                 ),
     *                 @OA\Property(
     *                         property="delayedItensQty",
     *                         type="integer",
     *                         description="Total of delayed items",
     *                 ),
     *                 @OA\Property(
     *                         property="dueValue",
     *                         type="decimal",
     *                         description="Amount of pending items",
     *                 ),
     *                 @OA\Property(
     *                         property="delayedDueValue",
     *                         type="decimal",
     *                         description="Amount of delayed items",
     *                 ),
     *                 @OA\Property(
     *                         property="totalItensQty",
     *                         type="integer",
     *                         description="Total of pending items",
     *                 ),
     *                 @OA\Property(
     *                         property="diffDelayedPendingQty",
     *                         type="decimal",
     *                         description="Diff between pending and delayed items",
     *                 ),
     *              @OA\Property(
     *              property="followups",
     *               type="array",
     *              description="The history sku list",
     *              @OA\Items(
     *                  @OA\Property(
     *                         property="companyId",
     *                         type="integer",
     *                         description="Company Id",
     *                 ),
     *                 @OA\Property(
     *                         property="orderId",
     *                         type="integer",
     *                         description="Id of order",
     *                 ),
     *                 @OA\Property(
     *                         property="idPdcBionexo",
     *                         type="integer",
     *                         description="Id of bionexo integration",
     *                 ),
     *                  @OA\Property(
     *                         property="statusCode",
     *                         type="integer",
     *                         description="status code of followp",
     *                 ),
     *                @OA\Property(
     *                         property="note",
     *                         type="text",
     *                         description="information about followup",
     *                 ),
     *                 @OA\Property(
     *                         property="createdAt",
     *                         type="string",
     *                         description="Date create",
     *                 ),
     *                  @OA\Property(
     *                         property="skuCode",
     *                         type="integer",
     *                         description="Sku code",
     *                 ),
     *                 @OA\Property(
     *                         property="user",
     *                         type="String",
     *                         description="name of user",
     *                 ),
     *                 @OA\Property(
     *                         property="itemDescription",
     *                         type="string",
     *                         description="Description of item",
     *                 ),
     *                  @OA\Property(
     *                         property="statusDescription",
     *                         type="string",
     *                         description="Status description",
     *                 ),
     *               )
     *             ),
     *               )
     *             ),
     *                 example= {
     *                           "fupStory": {
     *                               "supplier": "Fortpel Com de Descartaveis Lt",
     *                               "supplierId": "04907604000177",
     *                               "contactInformation": {
     *                                   "name": "Nome do contato",
     *                                   "phone": "(11) 1234-5678",
     *                                   "email": "email@email.com"
     *                               },
     *                               "criticalItensQty": 346,
     *                               "delayedItensQty": 479,
     *                               "totalItensQty": 479,
     *                               "dueValue": 19334762.48,
     *                               "delayedDueValue": 19334762.48,
     *                               "orders": null,
     *                               "diffDelayedPendingQty": 0,
     *                               "followups": {
     *                                   {
     *                                       "companyId": 2,
     *                                       "orderId": 176298,
     *                                       "idPdcBionexo": 2244,
     *                                       "statusCode": 6,
     *                                       "note": "Observação",
     *                                       "createdAt": "2020-06-04 15:44:00",
     *                                       "skuCode": 35401,
     *                                       "user": "UserName",
     *                                       "itemDescription": "sapolio radiun liquido 300ml",
     *                                       "statusDescription": "Em falta no mercado"
     *                                   },
     *                                   {
     *                                       "companyId": 2,
     *                                       "orderId": 179033,
     *                                       "idPdcBionexo": 2245,
     *                                       "statusCode": 8,
     *                                       "note": "Observação",
     *                                       "createdAt": "2020-06-02 10:00:00",
     *                                       "skuCode": 37375,
     *                                       "user": "UserName",
     *                                       "itemDescription": "saco plastico p/lixo 60 litros verde",
     *                                       "statusDescription": "Em falta no mercado"
     *                                   },
     *                                   {
     *                                       "companyId": 2,
     *                                       "orderId": 179530,
     *                                       "idPdcBionexo": 2246,
     *                                       "statusCode": 10,
     *                                       "note": "Observação",
     *                                       "createdAt": "2020-06-02 10:00:00",
     *                                       "skuCode": 51722,
     *                                       "user": "UserName",
     *                                       "itemDescription": "diabo verde desencrostante",
     *                                       "statusDescription": "Aguardando pagamento"
     *                                   }
     *                               }
     *                           }
     *                       }
     *              )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="A Supplier with the specified ID was not found"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @param int $supplierId
     * @return JsonResponse
     */
    public function get(Request $request): JsonResponse
    {
        try {

           $follow_up_historys = $this->follow_up_history
               ->setCompanyId($request->user->getIdCompanyFk())
               ->setSupplierId($request->supplierId)
               ->getHistory();

            //JOIN SUPPLIER DATA AND PENDING ITEMS
            $fupStory = array_merge($request->supplier_info['supplier'], $follow_up_historys);
            return response()->json(compact('fupStory'), 200);

        } catch (\Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }

    }

    /**
     * @OA\Post(
     *     tags={"Follow Up"},
     *     summary="Returns History of FollowUp by Supplier and date interval",
     *     path="/api/v1/fup/history/{supplierId}?token={token}",
     *     @OA\Parameter(
     *         name="supplierId",
     *         in="path",
     *         description="ID of Supplier",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns History of FollowUp by Supplier and date interval,
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *      @OA\Schema(
     *           @OA\Property(
     *              property="Returns History FollowUp",
     *               type="array",
     *              description="The response data",
     *              @OA\Items(
     *                  @OA\Property(
     *                         property="supplier",
     *                         type="string",
     *                         description="Name of supplier",
     *                 ),
     *                @OA\Property(
     *                      property="supplierId",
     *                      type="integer",
     *                      description="The supplier ID",
     *                  ),
     *               @OA\Property(
     *                      property="contactInformation",
     *                      type="array",
     *                      description="Contact information of Supplier",
     *                          @OA\Items(
     *                              @OA\Property(
     *                                  property="name",
     *                                  type="string",
     *                                  description="Name of contact",
     *                              ),
     *                              @OA\Property(
     *                                  property="phone",
     *                                  type="string",
     *                                  description="Phone of contact",
     *                              ),
     *                              @OA\Property(
     *                                  property="email",
     *                                  type="string",
     *                                  description="Email of contact",
     *                              ),
     *                          )
     *                ),
     *                @OA\Property(
     *                         property="criticalItensQty",
     *                         type="integer",
     *                         description="Total of critical items",
     *                 ),
     *                 @OA\Property(
     *                         property="delayedItensQty",
     *                         type="integer",
     *                         description="Total of delayed items",
     *                 ),
     *                 @OA\Property(
     *                         property="dueValue",
     *                         type="decimal",
     *                         description="Amount of pending items",
     *                 ),
     *                 @OA\Property(
     *                         property="delayedDueValue",
     *                         type="decimal",
     *                         description="Amount of delayed items",
     *                 ),
     *                 @OA\Property(
     *                         property="totalItensQty",
     *                         type="integer",
     *                         description="Total of pending items",
     *                 ),
     *                 @OA\Property(
     *                         property="diffDelayedPendingQty",
     *                         type="decimal",
     *                         description="Diff between pending and delayed items",
     *                 ),
     *              @OA\Property(
     *              property="followups",
     *               type="array",
     *              description="The history sku list",
     *              @OA\Items(
     *                  @OA\Property(
     *                         property="companyId",
     *                         type="integer",
     *                         description="Company Id",
     *                 ),
     *                 @OA\Property(
     *                         property="orderId",
     *                         type="integer",
     *                         description="Id of order",
     *                 ),
     *                 @OA\Property(
     *                         property="idPdcBionexo",
     *                         type="integer",
     *                         description="Id of bionexo integration",
     *                 ),
     *                  @OA\Property(
     *                         property="statusCode",
     *                         type="integer",
     *                         description="status code of followp",
     *                 ),
     *                @OA\Property(
     *                         property="note",
     *                         type="text",
     *                         description="information about followup",
     *                 ),
     *                 @OA\Property(
     *                         property="createdAt",
     *                         type="string",
     *                         description="Date create",
     *                 ),
     *                  @OA\Property(
     *                         property="skuCode",
     *                         type="integer",
     *                         description="Sku code",
     *                 ),
     *                 @OA\Property(
     *                         property="user",
     *                         type="String",
     *                         description="name of user",
     *                 ),
     *                 @OA\Property(
     *                         property="itemDescription",
     *                         type="string",
     *                         description="Description of item",
     *                 ),
     *                  @OA\Property(
     *                         property="statusDescription",
     *                         type="string",
     *                         description="Status description",
     *                 ),
     *               )
     *             ),
     *               )
     *             ),
     *                 example= {
     *                           "fupStory": {
     *                               "supplier": "Fortpel Com de Descartaveis Lt",
     *                               "supplierId": "04907604000177",
     *                               "contactInformation": {
     *                                   "name": "Nome do contato",
     *                                   "phone": "(11) 1234-5678",
     *                                   "email": "email@email.com"
     *                               },
     *                               "criticalItensQty": 346,
     *                               "delayedItensQty": 479,
     *                               "totalItensQty": 479,
     *                               "dueValue": 19334762.48,
     *                               "delayedDueValue": 19334762.48,
     *                               "orders": null,
     *                               "diffDelayedPendingQty": 0,
     *                               "followups": {
     *                                   {
     *                                       "companyId": 2,
     *                                       "orderId": 176298,
     *                                       "idPdcBionexo": 2244,
     *                                       "statusCode": 6,
     *                                       "note": "Observação",
     *                                       "createdAt": "2020-06-04 15:44:00",
     *                                       "skuCode": 35401,
     *                                       "user": "UserName",
     *                                       "itemDescription": "sapolio radiun liquido 300ml",
     *                                       "statusDescription": "Em falta no mercado"
     *                                   },
     *                                   {
     *                                       "companyId": 2,
     *                                       "orderId": 179033,
     *                                       "idPdcBionexo": 2245,
     *                                       "statusCode": 8,
     *                                       "note": "Observação",
     *                                       "createdAt": "2020-06-02 10:00:00",
     *                                       "skuCode": 37375,
     *                                       "user": "UserName",
     *                                       "itemDescription": "saco plastico p/lixo 60 litros verde",
     *                                       "statusDescription": "Em falta no mercado"
     *                                   },
     *                                   {
     *                                       "companyId": 2,
     *                                       "orderId": 179530,
     *                                       "idPdcBionexo": 2246,
     *                                       "statusCode": 10,
     *                                       "note": "Observação",
     *                                       "createdAt": "2020-06-02 10:00:00",
     *                                       "skuCode": 51722,
     *                                       "user": "UserName",
     *                                       "itemDescription": "diabo verde desencrostante",
     *                                       "statusDescription": "Aguardando pagamento"
     *                                   }
     *                               }
     *                           }
     *                       }
     *              )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="A Supplier with the specified ID was not found"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @param int $supplierId
     * @param array $period
     * @return JsonResponse
     */
    public function post(Request $request): JsonResponse
    {
        try {
            $period = $request['period'];
            $follow_up_historys = $this->follow_up_history
                ->setCompanyId($request->user->getIdCompanyFk())
                ->setSupplierId($request->supplierId)
                ->setData($period)
                ->postHistory();

             //JOIN SUPPLIER DATA AND PENDING ITEMS
             $fupStory = array_merge($request->supplier_info['supplier'], $follow_up_historys);
             return response()->json(compact('fupStory'), 200);

         } catch (\Exception $e) {
             return response()->json(['error' => ['message' => $e->getMessage()]], 500);
         }
    }
}
