<?php
declare(strict_types=1);

namespace App\Http\Controllers\FUP;


use App\Domain\Email\EmailInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\FilterMailManuallyRequest;
use App\Http\Services\EmailSendingService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Throwable;

class FollowUpMailManually extends Controller
{
    private EmailSendingService $emailSendingService;

    public function __construct(EmailInterface $emailSendingService)
    {
        $this->emailSendingService = $emailSendingService;
    }

    /**
     * @OA\Post(
     *     tags={"Follow Up"},
     *     summary="E-mail send request",
     *     description="E-mail send request",
     *     path="/api/v1/fup/email/send",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="manual_email",
     *                     type="array",
     *                     description="The body data",
     *                     @OA\Items(
     *                          @OA\Property(
     *                              property="emails",
     *                              type="array",
     *                              description="Any address e-mail",
     *                              @OA\Items(
     *                                  example={"emails": {"email1", "email2"}}
     *                              )
     *                          ),
     *                          @OA\Property(
     *                              property="subject",
     *                              type="string",
     *                              description="Title from for e-mail"
     *                          ),
     *                          @OA\Property(
     *                              property="body",
     *                              type="string",
     *                              description="Body email encoded with Base64"
     *                          ),
     *                     )
     *                 ),
     *                 example={"manual_email":{
     *                            "emails":{
     *                                "email@bionexo.com",
     *                                "email2@bionexo.com"
     *                            },
     *                            "subject":"Assunto de email",
     *                            "body":"Body",
     *                            "header":{"Estab.","Item."},
     *                            "items":{
     *                                {
     *                                    "estab_desc":"1-HDP",
     *                                    "item_name":"sapolio radiun liquido 300ml",
     *                                    "order_number":176298,
     *                                    "order_date":"2019-05-28T09:30:51",
     *                                    "expected_date":"2019-05-29T00:00:00",
     *                                    "delayed_days":365,
     *                                    "quantity":96,
     *                                    "total_value":"R$\u00a0489,60",
     *                                    "idPdcBionexo":"1751",
     *                                    "brandRequested":"Marca test"
     *                                }
     *                            },
     *                            "i18n":{
     *                                "Itens Pendentes",
     *                                "Este \u00e9 um e-mail autom\u00e1tico, por favor, n\u00e3o responda",
     *                                "Caso voc\u00ea n\u00e3o tenha conhecimento deste cadastro, desconsidere este e-mail.",
     *                                "Todos os direitos reservados"
     *                            }
     *                          }
     *                        }
     *
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successfully Sent",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unsuccessfully Sent"
     *     )
     * )
     *
     * @param FilterMailManuallyRequest $request
     * @return JsonResponse
     * @throws Throwable
     */
//    public function sendEmail(Request $request): JsonResponse
    public function sendEmail(FilterMailManuallyRequest $request): JsonResponse
    {

        // Obs.: Se "delayed_days" for maior que 0 trocar a cor para vermelho na coluna "order_date", "order_date" e "delayed_days".
        $params = $request->post('manual_email');
        $items = call_user_func([__NAMESPACE__ .'\FollowUpMailManually', 'arrayToObject'], $params['items']);

        try {
            $this->emailSendingService->exchangeArray([
                'receivers' => $params['emails'],
                'subject' => $params['subject'],
                'htmlBody' => view('fup-mail', [
                    'description' => $params['body'],
                    'i18n' => $params['i18n'],
                    'th_headers' => $params['header'],
                    'items' => $items,
                    'year' => date('Y'),
                    'company_name'=> $request->user->getCompany()->getCompanyName()
                ])->render()
            ]);

            $http_response = $this->emailSendingService->sendMailings();
            if (!$http_response['success']) throw new Exception($http_response['errors'], $http_response['status']);

            $response = response()->json(['success' => true, 'message' => 'Successfully Sent'], 200);
        } catch (Exception $exp) {
            Log::error($exp->getMessage(), ['cod_error' => $exp->getCode(), 'trace' => $exp->getTrace()]);
            $response = response()->json(['success' => false, 'message' => 'Unsuccessfully Sent'], 500);
        }

        return $response;
    }

    private function arrayToObject(array $items): array
    {
        $arr = [];
        foreach ($items as $item) {
            $item['order_date'] = (new \DateTime($item['order_date']))->format('d/m/Y');
            $item['expected_date'] = (new \DateTime($item['expected_date']))->format('d/m/Y');
            array_push($arr, (object) $item);
        }

        return $arr;
    }
}
