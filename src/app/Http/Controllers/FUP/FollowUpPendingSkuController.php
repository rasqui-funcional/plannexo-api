<?php
declare(strict_types=1);

namespace App\Http\Controllers\FUP;

use App\Domain\FollowUp\Filters\FollowUpFiltersFiltered;
use App\Domain\FollowUp\FollowUpSkuInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\FollowUpPendingSkuRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FollowUpPendingSkuController extends Controller
{
    private FollowUpSkuInterface $follow_up_sku;

    private FollowUpFiltersFiltered $follow_up_filters;

    public function __construct(FollowUpSkuInterface $follow_up_sku)
    {
        $this->follow_up_sku = $follow_up_sku;
    }

    /**
     * @OA\Get(
     *     tags={"Follow Up"},
     *     summary="Return list of items and a resume of supplier data",
     *     description="Return list of items and a resume of supplier data",
     *     path="/api/v1/fup/{supplierId}",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Return list of items and a resume of supplier data",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *      @OA\Schema(
     *           @OA\Property(
     *              property="fup",
     *               type="array",
     *              description="The response data",
     *              @OA\Items(
     *                  @OA\Property(
     *                         property="supplier",
     *                         type="string",
     *                         description="Name of supplier",
     *                 ),
     *                @OA\Property(
     *                      property="supplierId",
     *                      type="integer",
     *                      description="The supplier ID",
     *                  ),
     *               @OA\Property(
     *                      property="contactInformation",
     *                      type="array",
     *                      description="Contact information of Supplier",
     *                          @OA\Items(
     *                              @OA\Property(
     *                                  property="name",
     *                                  type="string",
     *                                  description="Name of contact",
     *                              ),
     *                              @OA\Property(
     *                                  property="phone",
     *                                  type="string",
     *                                  description="Phone of contact",
     *                              ),
     *                              @OA\Property(
     *                                  property="email",
     *                                  type="string",
     *                                  description="Email of contact",
     *                              ),
     *                          )
     *                ),
     *                @OA\Property(
     *                         property="criticalItensQty",
     *                         type="integer",
     *                         description="Total of critical items",
     *                 ),
     *                 @OA\Property(
     *                         property="delayedItensQty",
     *                         type="integer",
     *                         description="Total of delayed items",
     *                 ),
     *                 @OA\Property(
     *                         property="dueValue",
     *                         type="decimal",
     *                         description="Amount of pending items",
     *                 ),
     *                 @OA\Property(
     *                         property="delayedDueValue",
     *                         type="decimal",
     *                         description="Amount of delayed items",
     *                 ),
     *                 @OA\Property(
     *                         property="totalItensQty",
     *                         type="integer",
     *                         description="Total of pending items",
     *                 ),
     *                 @OA\Property(
     *                         property="diffDelayedPendingQty",
     *                         type="decimal",
     *                         description="Diff between pending and delayed items",
     *                 ),
     *              @OA\Property(
     *              property="items",
     *               type="array",
     *              description="The items list",
     *              @OA\Items(
     *                  @OA\Property(
     *                         property="orderId",
     *                         type="integer",
     *                         description="Id of order",
     *                 ),
     *                  @OA\Property(
     *                         property="analysed",
     *                         type="boolean",
     *                         description="If item was analysed",
     *                 ),
     *                 @OA\Property(
     *                         property="lastAnalysis",
     *                         type="string",
     *                         description="Date of sku's last analysis",
     *                 ),
     *                  @OA\Property(
     *                         property="estab_cod",
     *                         type="integer",
     *                         description="Code of estab",
     *                 ),
     *                  @OA\Property(
     *                         property="estab_desc",
     *                         type="integer",
     *                         description="Estab name",
     *                 ),
     *                 @OA\Property(
     *                         property="sku_cod",
     *                         type="integer",
     *                         description="Code of Sku",
     *                 ),
     *                 @OA\Property(
     *                         property="item_name",
     *                         type="string",
     *                         description="Description of item",
     *                 ),
     *                  @OA\Property(
     *                         property="order_number",
     *                         type="integer",
     *                         description="Order number",
     *                 ),
     *                  @OA\Property(
     *                         property="idPdcBionexo",
     *                         type="integer",
     *                         description="Order number in Bionexo",
     *                 ),
     *                 @OA\Property(
     *                         property="brandRequested",
     *                         type="string",
     *                         description="Brand item",
     *                 ),
     *                  @OA\Property(
     *                         property="order_date",
     *                         type="string",
     *                         description="Order date",
     *                 ),
     *                  @OA\Property(
     *                         property="expected_date",
     *                         type="string",
     *                         description="Expected date to receive order",
     *                 ),
     *                 @OA\Property(
     *                         property="delayed_days",
     *                         type="integer",
     *                         description="Dalayed days of order",
     *                 ),
     *                  @OA\Property(
     *                         property="quantity",
     *                         type="integer",
     *                         description="Total items on order",
     *                 ),
     *                 @OA\Property(
     *                         property="unit_value",
     *                         type="numeric",
     *                         description="Total items on order",
     *                 ),
     *                 @OA\Property(
     *                         property="total_value",
     *                         type="numeric",
     *                         description="Amount of order",
     *                 ),
     *                  @OA\Property(
     *                         property="level",
     *                         type="integer",
     *                         description="Inv level of item",
     *                 ),
     *                 @OA\Property(
     *                         property="status",
     *                         type="integer",
     *                         description="status of followp",
     *                 ),
     *                 @OA\Property(
     *                         property="note",
     *                         type="text",
     *                         description="information about followup",
     *                 ),
     *                 @OA\Property(
     *                         property="supplierId",
     *                         type="string",
     *                         description="The supplier ID",
     *                 ),
     *
     *               )
     *             ),
     *               )
     *             ),
     *                 example= {
     *                               "fup":{
     *                                   "supplier":"Fortpel Com de Descartaveis Lt",
     *                                   "supplierId":"04907604000177",
     *                                   "contactInformation":{
     *                                       "name":"Nome",
     *                                       "phone": "11 1234-5678",
     *                                       "email": "email@email.com"
     *                                   },
     *                                   "criticalItensQty":346,
     *                                   "delayedItensQty":479,
     *                                   "totalItensQty":479,
     *                                   "dueValue":19334762.48,
     *                                   "delayedDueValue":19334762.48,
     *                                   "orders":null,
     *                                   "diffDelayedPendingQty":0,
     *                                   "itens":{
     *                                       {
     *                                           "orderId":458785,
     *                                           "analysed":false,
     *                                           "lastAnalysis":"2020-03-26T18:53:00",
     *                                           "estab_cod":1,
     *                                           "estab_desc":"HDP",
     *                                           "sku_cod":35401,
     *                                           "item_name":"sapolio radiun liquido 300ml",
     *                                           "order_number":176298,
     *                                           "idPdcBionexo":0,
     *                                           "brandRequested": "Marca test",
     *                                           "order_date":"2019-05-28T09:30:51",
     *                                           "expected_date":"2019-05-29T00:00:00",
     *                                           "delayed_days":365,
     *                                           "quantity":96,
     *                                           "unit_value":5.1,
     *                                           "total_value":489.6,
     *                                           "level":1,
     *                                           "status":0,
     *                                           "note":null,
     *                                           "supplier_id":"04907604000177"
     *                                       },
     *                                       {
     *                                           "orderId":468233,
     *                                           "analysed":false,
     *                                           "lastAnalysis":"2020-03-26T18:53:00",
     *                                           "estab_cod":1,
     *                                           "estab_desc":"HDP",
     *                                           "sku_cod":37375,
     *                                           "item_name":"saco plastico p\/lixo 60 litros verde  pc=100un = 06 micras",
     *                                           "order_number":179033,
     *                                           "idPdcBionexo":0,
     *                                           "brandRequested": "Marca test",
     *                                           "order_date":"2019-07-10T13:23:18",
     *                                           "expected_date":"2019-07-11T00:00:00",
     *                                           "delayed_days":322,
     *                                           "quantity":50,
     *                                           "unit_value":7.41,
     *                                           "total_value":370.5,
     *                                           "level":1,
     *                                           "status":0,
     *                                           "note":null,
     *                                           "supplier_id":"04907604000177"
     *                                       }
     *                                   },
     *                                   "nextPage":null,
     *                                   "success":true,
     *                                   "errors":null
     *                               }
     *                           }
     *              )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     *
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getList(Request $request): JsonResponse
    {

        try {

            $follow_up_skus = $this->follow_up_sku
                ->setCompanyId($request->user->getIdCompanyFk())
                ->setSupplierId($request->supplierId)
                ->getPendingSkus();

            //JOIN SUPPLIER DATA AND PENDING ITEMS
            $fup = array_merge($request->supplier_info['supplier'], $follow_up_skus);
            return response()->json(compact('fup'), 200);

        } catch (\Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }
    }

    /**
     * @OA\Post(
     *     tags={"Follow Up"},
     *     summary="Return list of items and a resume of supplier data",
     *     description="Return list of items and a resume of supplier data",
     *     path="/api/v1/fup/{supplierId}",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 example= {
     *                          "filters": {
     *                              "curves":{
     *                                  "ABC": {"A", "B"},
     *                                  "XYZ": {"X", "Z"},
     *                                  "PQR": {"R"},
     *                                  "oneTwoThree": {"3"}
     *                              },
     *                              "estabs":{"1", "12"},
     *                              "managers": {"1", "2"},
     *                              "policies": {"3", "4"},
     *                              "standard_deposit": {"1", "2"},
     *                               "specialFilters": {
     *                                      "orders": {"operator": "2", "schedule_options": "2", "amount": "10"},
     *                                      "dateRequestOrder": {"start": "11/05/2020", "end": "22/05/2020", "type": 1},
     *                                      "datePurchaseOrder": {"start": "11/05/2020", "end": "11/05/2020", "type": 0},
     *                                      "dateExpected": {"start": "", "end": "", "type": 6}
     *                              },
     *                              "sku": {
     *                                      "suppliersId": {"12"},
     *                                      "supplierDescription": "Description",
     *                                      "itemCode": {"34", "56"},
     *                                      "requestOrderIds": {"78", "90"},
     *                                      "purchaseOrderIds": {"12", "34"},
     *                                      "lifeCycle": {1, 2, 3},
     *                                      "inventoryLevel": {0, 4, 1, 5},
     *                                      "systemStatus": {0, 1}
     *                              },
     *                          }
     *                      }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Return list of items and a resume of supplier data",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *      @OA\Schema(
     *           @OA\Property(
     *              property="fup",
     *               type="array",
     *              description="The response data",
     *              @OA\Items(
     *                  @OA\Property(
     *                         property="supplier",
     *                         type="string",
     *                         description="Name of supplier",
     *                 ),
     *                @OA\Property(
     *                      property="supplierId",
     *                      type="integer",
     *                      description="The supplier ID",
     *                  ),
     *               @OA\Property(
     *                      property="contactInformation",
     *                      type="array",
     *                      description="Contact information of Supplier",
     *                          @OA\Items(
     *                              @OA\Property(
     *                                  property="name",
     *                                  type="string",
     *                                  description="Name of contact",
     *                              ),
     *                              @OA\Property(
     *                                  property="phone",
     *                                  type="string",
     *                                  description="Phone of contact",
     *                              ),
     *                              @OA\Property(
     *                                  property="email",
     *                                  type="string",
     *                                  description="Email of contact",
     *                              ),
     *                          )
     *                ),
     *                @OA\Property(
     *                         property="criticalItensQty",
     *                         type="integer",
     *                         description="Total of critical items",
     *                 ),
     *                 @OA\Property(
     *                         property="delayedItensQty",
     *                         type="integer",
     *                         description="Total of delayed items",
     *                 ),
     *                 @OA\Property(
     *                         property="dueValue",
     *                         type="decimal",
     *                         description="Amount of pending items",
     *                 ),
     *                 @OA\Property(
     *                         property="delayedDueValue",
     *                         type="decimal",
     *                         description="Amount of delayed items",
     *                 ),
     *                 @OA\Property(
     *                         property="totalItensQty",
     *                         type="integer",
     *                         description="Total of pending items",
     *                 ),
     *                 @OA\Property(
     *                         property="diffDelayedPendingQty",
     *                         type="decimal",
     *                         description="Diff between pending and delayed items",
     *                 ),
     *              @OA\Property(
     *              property="items",
     *               type="array",
     *              description="The items list",
     *              @OA\Items(
     *                  @OA\Property(
     *                         property="orderId",
     *                         type="integer",
     *                         description="Id of order",
     *                 ),
     *                  @OA\Property(
     *                         property="analysed",
     *                         type="boolean",
     *                         description="If item was analysed",
     *                 ),
     *                 @OA\Property(
     *                         property="lastAnalysis",
     *                         type="string",
     *                         description="Date of sku's last analysis",
     *                 ),
     *                  @OA\Property(
     *                         property="estab_cod",
     *                         type="integer",
     *                         description="Code of estab",
     *                 ),
     *                  @OA\Property(
     *                         property="estab_desc",
     *                         type="integer",
     *                         description="Estab name",
     *                 ),
     *                 @OA\Property(
     *                         property="sku_cod",
     *                         type="integer",
     *                         description="Code of Sku",
     *                 ),
     *                 @OA\Property(
     *                         property="item_name",
     *                         type="string",
     *                         description="Description of item",
     *                 ),
     *                  @OA\Property(
     *                         property="order_number",
     *                         type="integer",
     *                         description="Order number",
     *                 ),
     *                  @OA\Property(
     *                         property="idPdcBionexo",
     *                         type="integer",
     *                         description="Order number in Bionexo",
     *                 ),
     *                 @OA\Property(
     *                         property="brandRequested",
     *                         type="string",
     *                         description="Brand item",
     *                 ),
     *                  @OA\Property(
     *                         property="order_date",
     *                         type="string",
     *                         description="Order date",
     *                 ),
     *                  @OA\Property(
     *                         property="expected_date",
     *                         type="string",
     *                         description="Expected date to receive order",
     *                 ),
     *                 @OA\Property(
     *                         property="delayed_days",
     *                         type="integer",
     *                         description="Dalayed days of order",
     *                 ),
     *                  @OA\Property(
     *                         property="quantity",
     *                         type="integer",
     *                         description="Total items on order",
     *                 ),
     *                 @OA\Property(
     *                         property="unit_value",
     *                         type="numeric",
     *                         description="Total items on order",
     *                 ),
     *                 @OA\Property(
     *                         property="total_value",
     *                         type="numeric",
     *                         description="Amount of order",
     *                 ),
     *                  @OA\Property(
     *                         property="level",
     *                         type="integer",
     *                         description="Inv level of item",
     *                 ),
     *                 @OA\Property(
     *                         property="status",
     *                         type="integer",
     *                         description="status of followp",
     *                 ),
     *                 @OA\Property(
     *                         property="note",
     *                         type="text",
     *                         description="information about followup",
     *                 ),
     *                 @OA\Property(
     *                         property="supplierId",
     *                         type="string",
     *                         description="The supplier ID",
     *                 ),
     *               )
     *             ),
     *               )
     *             ),
     *                 example= {
     *                               "fup":{
     *                                   "supplier":"Fortpel Com de Descartaveis Lt",
     *                                   "supplierId":"04907604000177",
     *                                   "contactInformation":{
     *                                       "name":"Nome",
     *                                       "phone": "11 1234-5678",
     *                                       "email": "email@email.com"
     *                                   },
     *                                   "criticalItensQty":346,
     *                                   "delayedItensQty":479,
     *                                   "totalItensQty":479,
     *                                   "dueValue":19334762.48,
     *                                   "delayedDueValue":19334762.48,
     *                                   "orders":null,
     *                                   "diffDelayedPendingQty":0,
     *                                   "itens":{
     *                                       {
     *                                           "orderId":458785,
     *                                           "analysed":false,
     *                                           "lastAnalysis":"2020-03-26T18:53:00",
     *                                           "estab_cod":1,
     *                                           "estab_desc":"HDP",
     *                                           "sku_cod":35401,
     *                                           "item_name":"sapolio radiun liquido 300ml",
     *                                           "order_number":176298,
     *                                           "idPdcBionexo":0,
     *                                           "brandRequested": "Marca test",
     *                                           "order_date":"2019-05-28T09:30:51",
     *                                           "expected_date":"2019-05-29T00:00:00",
     *                                           "delayed_days":365,
     *                                           "quantity":96,
     *                                           "unit_value":5.1,
     *                                           "total_value":489.6,
     *                                           "level":1,
     *                                           "status":0,
     *                                           "note":null,
     *                                           "supplier_id":"04907604000177"
     *                                       },
     *                                       {
     *                                           "orderId":468233,
     *                                           "analysed":false,
     *                                           "lastAnalysis":"2020-03-26T18:53:00",
     *                                           "estab_cod":1,
     *                                           "estab_desc":"HDP",
     *                                           "sku_cod":37375,
     *                                           "item_name":"saco plastico p\/lixo 60 litros verde  pc=100un = 06 micras",
     *                                           "order_number":179033,
     *                                           "idPdcBionexo":0,
     *                                           "brandRequested": "Marca test",
     *                                           "order_date":"2019-07-10T13:23:18",
     *                                           "expected_date":"2019-07-11T00:00:00",
     *                                           "delayed_days":322,
     *                                           "quantity":50,
     *                                           "unit_value":7.41,
     *                                           "total_value":370.5,
     *                                           "level":1,
     *                                           "status":0,
     *                                           "note":null,
     *                                           "supplier_id":"04907604000177"
     *                                       }
     *                                   },
     *                                   "nextPage":null,
     *                                   "success":true,
     *                                   "errors":null
     *                               }
     *                           }
     *              )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     *
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function postList(FollowUpPendingSkuRequest $request): JsonResponse
    {

        try {

            $this->follow_up_filters = new FollowUpFiltersFiltered($request->post('filters'));

            $follow_up_skus = $this->follow_up_sku
                ->setCompanyId($request->user->getIdCompanyFk())
                ->setSupplierId($request->supplierId)
                ->setData($this->follow_up_filters->getFilters())
                ->getPendingSkusFiltered();

            //JOIN SUPPLIER DATA AND PENDING ITEMS
            $fup = array_merge($request->supplier_info['supplier'], $follow_up_skus);
            return response()->json(compact('fup'), 200);

        } catch (\Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }
    }
}
