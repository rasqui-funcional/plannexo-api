<?php
declare(strict_types=1);

namespace App\Http\Controllers\FUP;

use App\Domain\FollowUp\FollowUpSettingsInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\FollowUpSettingsRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use App\Model\Email\EmailProductModel;
use App\Model\Email\EmailTypeModel;
use App\Model\Email\EmailRegisteredModel;
use App\Model\Email\EmailUsedModel;
use App\Domain\Email\Entity\EmailProduct;
use App\Domain\Email\Entity\EmailType;
use App\Domain\Email\Entity\EmailRegistered;
use App\Domain\Email\Entity\EmailUsed;

class FollowUpSettingsController extends Controller
{
    private FollowUpSettingsInterface $follow_up_settings;

    public function __construct(FollowUpSettingsInterface $follow_up_settings)
    {
        $this->follow_up_settings = $follow_up_settings;
    }

    /**
     * @OA\Post(
     *     tags={"Follow Up"},
     *     summary="Update the FollowUp Settings",
     *     path="/api/v1/fup/settings/update",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *           @OA\Property(
     *                  property="settings",
     *                  type="array",
     *                  description="The body data",
     *              @OA\Items(
     *               @OA\Property(
     *                      property="analyzeItems",
     *                      type="array",
     *                          @OA\Items(
     *                              @OA\Property(
     *                                  property="pendingPurchaseOrder",
     *                                  type="integer",
     *                              ),
     *                              @OA\Property(
     *                                  property="delayedPurchaseOrder",
     *                                  type="integer",
     *                              ),
     *                          )
     *                ),
     *               @OA\Property(
     *                      property="automaticEmail",
     *                      type="array",
     *                          @OA\Items(
     *                              @OA\Property(
     *                                  property="pendingItemsStatus",
     *                                  type="boolean",
     *                              ),
     *                              @OA\Property(
     *                                  property="pendingEmailModel",
     *                                  type="integer",
     *                              ),
     *                              @OA\Property(
     *                                  property="pendingDaysAlert",
     *                                  type="integer",
     *                              ),
     *                              @OA\Property(
     *                                  property="delayedItemsStatus",
     *                                  type="boolean",
     *                              ),
     *                              @OA\Property(
     *                                  property="delayedEmailModel",
     *                                  type="integer",
     *                              ),
     *                              @OA\Property(
     *                                  property="delayedDaysAlert",
     *                                  type="integer",
     *                              ),
     *                          )
     *                ),
     *               @OA\Property(
     *                      property="statusRegistration",
     *                      type="array",
     *                          @OA\Items(
     *               @OA\Property(
     *                      property="newStatus",
     *                      type="array",
     *                          @OA\Items(
     *                              @OA\Property(
     *                                  property="enabled",
     *                                  type="boolean",
     *                              ),
     *                              @OA\Property(
     *                                  property="description",
     *                                  type="string",
     *                              ),
     *                          )
     *                 ),
     *                 @OA\Property(
     *                      property="editStatus",
     *                      type="array",
     *                          @OA\Items(
     *                              @OA\Property(
     *                                  property="code",
     *                                  type="integer",
     *                              ),
     *                              @OA\Property(
     *                                  property="enabled",
     *                                  type="boolean",
     *                              ),
     *                          )
     *                 ),
     *                  )
     *                ),
     *              ),
     *              ),
     *                 example= {"settings": {
     *                          "analyzeItems": {"pendingPurchaseOrder": 7, "delayedPurchaseOrder": 7},
     *                          "automaticEmail": {"pendingItemsStatus": false, "pendingEmailModel": 7, "pendingDaysAlert": 7, "delayedItemsStatus": false, "delayedEmailModel": 7, "delayedDaysAlert": 7 },
     *                          "statusRegistration": {
     *                              "newStatus": { "enabled": false, "description": "Description" },
     *                              "editStatus": { "code": 1, "enabled": false }
     *                          }
     *                      }
     *                  }
     *             )
     *         )
     *     ),
     * @OA\Response(
     *         response=201,
     *         description="Fup saved",
     *     ),
     * @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     * @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     * @OA\Response(
     *         response=417,
     *         description="Create User Failed"
     *     ),
     * @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     * @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function updateSettings(Request $request): JsonResponse
    {

        try {
            $settings = $request->post('settings');

            $this->insertOrUpdateUsedAndRegistered($request);
            $follow_up = $this->follow_up_settings
                ->setCompanyId($request->user->getIdCompanyFk())
                ->setData($settings)
                ->updateSettings();

            return response()->json($follow_up, 201);

        } catch (\Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }
    }

    private function insertOrUpdateUsedAndRegistered($request)
    {
        $settings = $request->post('settings');
        /**
         * Caso o modulo não exista na base de dados inserir manualmente na tabela EMAIL_PRODUCT_MODULE (Ainda não temos um admin para admin).
         * Precisamos resgatar o ID do modulo usando o campo MODULE
         */
        $product = ["module" => 'fup'];
        $product_model = (new EmailProductModel(new EmailProduct($product)))->getEmailProduct();

        /**
         * Caso o type não exista na base de dados inserir manualmente na tabela EMAIL_TYPE (Ainda não temos um admin para admin).
         * Precisamos resgatar os tipo de emails usando o campo ID_MODULE_FK
         * Exemplo de cadastro no campo type: "automatico_itens_pendentes", "automatico_itens_automatico"
         */
        $types = ['id_module_fk' => $product_model->id];
        $type_model = (new EmailTypeModel(new EmailType($types)))->getEmailTypeByModuleId();

        foreach ($type_model as $key => $type) {
            $registered = [
                'id_module_fk' => "{$type->id_module_fk}",
                'id_type_fk' => "{$type->id}",
            ];

            if (!isset($settings["automaticEmail"])) {
                continue;
            }

            if ($type = "automatico_itens_pendentes") {
                $registered['id_model_fk'] = "{$settings["automaticEmail"]["pendingEmailModel"]}";
            }

            if ($type = "automatico_itens_atrasados") {
                $registered['id_model_fk'] = "{$settings["automaticEmail"]["delayedEmailModel"]}";
            }

            /**
             * Cria um novo registro caso não exista uma ligação na tabela de EmailRegistered no banco de dados
             * Caso exista, é feito um update
             */
            (new EmailRegisteredModel(new EmailRegistered($registered)))->insertOrUpdate($request->user->getIdCompanyFk());

            /**
             * Cria um novo registro caso não exista uma ligação na tabela de LIG_EMAIL_MODEL_PRODUCT no banco de dados
             */
            (new EmailUsedModel(new EmailUsed($registered)))->noExistsInsert();
        }
    }

    /**
     * @OA\Get(
     *     tags={"Follow Up"},
     *     summary="Returns a list of settings",
     *     description="Returns a object of settings",
     *     path="/api/v1/fup/settings",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Return list of settings",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *      @OA\Schema(
     *           @OA\Property(
     *              property="settings",
     *               type="array",
     *              description="The settings data",
     *              @OA\Items(
     *                @OA\Property(
     *                 property="code",
     *                 type="integer",
     *                 description="The settings ID",
     *             ),
     *                @OA\Property(
     *                         property="description",
     *                         type="string",
     *                         description="Description of settings",
     *                 ),
     *               )
     *             ),
     *                 example= {"settings": {
     *                              "settingsId": 34,
     *                              "companyId": 41,
     *                              "dateCreated": "2020-03-24T12:34:30",
     *                              "analyzeItems": {
     *                                  "pendingPurchaseOrder": 1,
     *                                  "delayedPurchaseOrder": 1
     *                              },
     *                              "automaticEmail": {
     *                                  "pendingItemsStatus": true,
     *                                  "pendingEmailModel": 1,
     *                                  "pendingDaysAlert": 1,
     *                                  "delayedItemsStatus": true,
     *                                  "delayedEmailModel": 1,
     *                                  "delayedDaysAlert": 1,
     *                              },
     *                          }
     *                  }
     *              )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     *
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getSettings(Request $request): JsonResponse
    {
        try {
            $follow_up = $this->follow_up_settings
                ->setCompanyId($request->user->getIdCompanyFk())
                ->getSettings();

            return response()->json($follow_up, 200);

        } catch (\Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }
    }
}
