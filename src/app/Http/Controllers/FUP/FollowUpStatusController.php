<?php
declare(strict_types=1);

namespace App\Http\Controllers\FUP;

use App\Domain\FollowUp\FollowUpStatusInterface;
use App\Http\Services\FollowUpStatusService;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FollowUpStatusController extends Controller
{

    private FollowUpStatusService $follow_up_status;

    public function __construct(FollowUpStatusInterface $follow_up_status)
    {
        $this->follow_up_status = $follow_up_status;
    }

    /**
     * @OA\Get(
     *     tags={"Follow Up"},
     *     summary="Returns a list of status",
     *     description="Returns a object of status",
     *     path="/api/v1/fup/status/list",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Return list of status",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *      @OA\Schema(
     *           @OA\Property(
     *              property="statuses",
     *               type="array",
     *              description="The response data",
     *              @OA\Items(
     *                @OA\Property(
     *                 property="code",
     *                 type="integer",
     *                 description="The status ID",
     *             ),
     *                @OA\Property(
     *                         property="description",
     *                         type="string",
     *                         description="Description of status",
     *                 ),
     *               )
     *             ),
     *                 example= {"statuses": {
     *                                  {"code": 1, "description": "Em falta no mercado"},
     *                                  {"code": 2, "description": "Compra Cancelada"}
     *                              }
     *                          }
     *              )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     *
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getList(Request $request): JsonResponse
    {
        try {
            $follow_up = $this->follow_up_status
                ->setCompanyId($request->user->getIdCompanyFk())
                ->getFollowUpStatus();

            return response()->json($follow_up, 200);

        } catch (\Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }
    }
}
