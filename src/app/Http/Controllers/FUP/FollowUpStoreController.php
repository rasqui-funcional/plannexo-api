<?php
declare(strict_types=1);

namespace App\Http\Controllers\FUP;

use App\Domain\FollowUp\FollowUpStoreInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\FollowUpStoreRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FollowUpStoreController extends Controller
{
    private FollowUpStoreInterface $follow_up_store;

    public function __construct(FollowUpStoreInterface $follow_up_store)
    {
        $this->follow_up_store = $follow_up_store;
    }

    /**
     * @OA\Post(
     *     tags={"Follow Up"},
     *     summary="Save the FollowUp",
     *     path="/api/v1/fup/store",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     * @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"orderNumber", "skuCode"},
     *                 @OA\Property(
     *                     property="orderNumber",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="analysed",
     *                     type="boolean"
     *                 ),
     *                 @OA\Property(
     *                     property="skuCode",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="status",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="note",
     *                     type="text"
     *                 ),
     *                 example= {"fup": {
     *                                  {"orderNumber": 123, "analysed": true, "skuCode": 302742,  "status": 3, "note": "suspendisse potenti nullam porttitor"},
     *                                  {"orderNumber": 456, "analysed": true, "skuCode": 78910,  "status": 1, "note": "suspendisse potenti nullam porttitor"},
     *                              }
     *                          }
     *             )
     *         )
     *     ),
     * @OA\Response(
     *         response=201,
     *         description="Fup saved",
     *     ),
     * @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     * @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     * @OA\Response(
     *         response=417,
     *         description="Create User Failed"
     *     ),
     * @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     * @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(FollowUpStoreRequest $request): JsonResponse
    {

        try {

            $fup = $request->post('fup');

            $follow_up = $this->follow_up_store
                ->setCompanyId($request->user->getIdCompanyFk())
                ->setSupplierId($fup[0]['supplierId'])
                ->setUserId($request->user->getIdUserPk())
                ->setData($fup)
                ->saveFollowUp();

            return response()->json(compact('fup'), 201);

        } catch (\Exception $e) {

            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }
    }
}
