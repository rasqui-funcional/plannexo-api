<?php
declare(strict_types=1);

namespace App\Http\Controllers\FUP;

use App\Domain\FollowUp\Filters\FollowUpFiltersFiltered;
use App\Domain\FollowUp\FollowUpSupplierInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FollowUpSummaryController extends Controller
{
    private FollowUpSupplierInterface $follow_up_summary;

    private FollowUpFiltersFiltered $follow_up_filters;

    public function __construct(FollowUpSupplierInterface $follow_up_summary)
    {
        $this->follow_up_summary = $follow_up_summary;
    }

    /**
     * @OA\Get(
     *     tags={"Follow Up"},
     *     summary="Returns a list of suppliers and a resume of pendent orders",
     *     description="Returns a object of suppliers",
     *     path="/api/v1/fup",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Return list of suppliers and a resume of pendent orders",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *      @OA\Schema(
     *           @OA\Property(
     *              property="fup",
     *               type="array",
     *              description="The response data",
     *              @OA\Items(
     *                @OA\Property(
     *                 property="id",
     *                 type="integer",
     *                 description="The supplier ID",
     *             ),
     *                @OA\Property(
     *                         property="supplier_name",
     *                         type="string",
     *                         description="Name of supplier",
     *                 ),
     *                @OA\Property(
     *                         property="critical_items_total",
     *                         type="integer",
     *                         description="Total of critical items",
     *                 ),
     *                 @OA\Property(
     *                         property="delayed_items_total",
     *                         type="integer",
     *                         description="Total of delayed items",
     *                 ),
     *                 @OA\Property(
     *                         property="delayed_items_amount",
     *                         type="decimal",
     *                         description="Amount of delayed items",
     *                 ),
     *                 @OA\Property(
     *                         property="pending_items_total",
     *                         type="integer",
     *                         description="Total of pending items",
     *                 ),
     *                 @OA\Property(
     *                         property="diff_pending_delayed_items",
     *                         type="decimal",
     *                         description="Diff between pending and delayed items",
     *                 ),
     *               )
     *             ),
     *                 example= { "suppliers": {
     *                            {
     *                              "supplier": "Fornecedor 1",
     *                              "supplierId": "123456789123",
     *                              "contactInformation": { "name": null, "phone": "11 1234-4567", "email": "email@emailc.om" },
     *                              "criticalItensQty": 0,
     *                              "delayedItensQty": 1,
     *                              "totalItensQty": 1,
     *                              "dueValue": 500,
     *                              "delayedDueValue": 500,
     *                              "orders": null,
     *                              "diffDelayedPendingQty": 0
     *                             },
     *                            {
     *                              "supplier": "Fornecedor 2",
     *                              "supplierId": "0123456789",
     *                              "contactInformation": { "name": null, "phone": "11 1234-4567", "email": "email@emailc.om" },
     *                              "criticalItensQty": 0,
     *                              "delayedItensQty": 1,
     *                              "totalItensQty": 1,
     *                              "dueValue": 500,
     *                              "delayedDueValue": 500,
     *                              "orders": null,
     *                              "diffDelayedPendingQty": 0
     *                             },
     *                          },
     *                          "success": true,
     *                          "errors": null
     *                          }
     *              )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     *
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getList(Request $request): JsonResponse
    {
        try {
            $follow_up = $this->follow_up_summary
                ->setCompanyId($request->user->getIdCompanyFk())
                ->getSummary();

            return response()->json($follow_up, 200);

        } catch (\Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }
    }

    /**
     * @OA\Post(
     *     tags={"Follow Up"},
     *     summary="Returns a list of suppliers and a resume of pendent orders",
     *     description="Returns a object of suppliers",
     *     path="/api/v1/fup",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 example= {
     *                          "filters": {
     *                              "curves":{
     *                                  "ABC": {"A", "B"},
     *                                  "XYZ": {"X", "Z"},
     *                                  "PQR": {"R"},
     *                                  "oneTwoThree": {"3"}
     *                              },
     *                              "estabs":{"1", "12"},
     *                              "managers": {"1", "2"},
     *                              "policies": {"3", "4"},
     *                              "standard_deposit": {"1", "2"},
     *                               "specialFilters": {
     *                                      "orders": {"operator": "2", "schedule_options": "2", "amount": "10"},
     *                                      "dateRequestOrder": {"start": "11/05/2020", "end": "22/05/2020", "type": 1},
     *                                      "datePurchaseOrder": {"start": "11/05/2020", "end": "11/05/2020", "type": 0},
     *                                      "dateExpected": {"start": "", "end": "", "type": 6}
     *                              },
     *                              "sku": {
     *                                      "suppliersId": {"12"},
     *                                      "supplierDescription": "Description",
     *                                      "itemCode": {"34", "56"},
     *                                      "requestOrderIds": {"78", "90"},
     *                                      "purchaseOrderIds": {"12", "34"},
     *                                      "lifeCycle": {1, 2, 3},
     *                                      "inventoryLevel": {0, 4, 1, 5},
     *                                      "systemStatus": {0, 1}
     *                              },
     *                          }
     *                      }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Return list of suppliers and a resume of pendent orders",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *      @OA\Schema(
     *           @OA\Property(
     *              property="fup",
     *               type="array",
     *              description="The response data",
     *              @OA\Items(
     *                @OA\Property(
     *                 property="id",
     *                 type="integer",
     *                 description="The supplier ID",
     *             ),
     *                @OA\Property(
     *                         property="supplier_name",
     *                         type="string",
     *                         description="Name of supplier",
     *                 ),
     *                @OA\Property(
     *                         property="critical_items_total",
     *                         type="integer",
     *                         description="Total of critical items",
     *                 ),
     *                 @OA\Property(
     *                         property="delayed_items_total",
     *                         type="integer",
     *                         description="Total of delayed items",
     *                 ),
     *                 @OA\Property(
     *                         property="delayed_items_amount",
     *                         type="decimal",
     *                         description="Amount of delayed items",
     *                 ),
     *                 @OA\Property(
     *                         property="pending_items_total",
     *                         type="integer",
     *                         description="Total of pending items",
     *                 ),
     *                 @OA\Property(
     *                         property="diff_pending_delayed_items",
     *                         type="decimal",
     *                         description="Diff between pending and delayed items",
     *                 ),
     *               )
     *             ),
     *                 example= { "suppliers": {
     *                              {
     *                              "supplier": "Fornecedor 1",
     *                              "supplierId": "08918717000129",
     *                              "contactInformation": { "name": null, "phone": "11 1234-4567", "email": "email@emailc.om" },
     *                              "criticalItensQty": 0,
     *                              "delayedItensQty": 1,
     *                              "totalItensQty": 1,
     *                              "dueValue": 500,
     *                              "delayedDueValue": 500,
     *                              "orders": null,
     *                              "diffDelayedPendingQty": 0
     *                             },
     *                            {
     *                              "supplier": "Fornecedor 1",
     *                              "supplierId": "08918717000129",
     *                              "contactInformation": { "name": null, "phone": "11 1234-4567", "email": "email@emailc.om" },
     *                              "criticalItensQty": 0,
     *                              "delayedItensQty": 1,
     *                              "totalItensQty": 1,
     *                              "dueValue": 500,
     *                              "delayedDueValue": 500,
     *                              "orders": null,
     *                              "diffDelayedPendingQty": 0
     *                             },
     *                          },
     *                          "success": true,
     *                          "errors": null
     *                          }
     *              )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     *
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getQueryList(Request $request): JsonResponse
    {
        $filters = $request->all();

        try {
            if (count($filters) < 1) {
                throw new \Exception("Dados dos filtros não enviados.", 422);
            }

            $this->follow_up_filters = new FollowUpFiltersFiltered($request->post('filters'));
            $follow_up = $this->follow_up_summary
                ->setCompanyId($request->user->getIdCompanyFk())
                ->setData($this->follow_up_filters->getFilters())
                ->getQuerySummary();

            return response()->json($follow_up, 200);

        } catch (\Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }
    }
}
