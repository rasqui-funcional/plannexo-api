<?php
declare(strict_types=1);

namespace App\Http\Controllers\FUP;

use App\Http\Controllers\Controller;
use App\Model\FollowUp\FollowUpUserFilterListModel;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FollowUpUserFiltersController extends Controller
{

    /**
     * @OA\Get(
     *     tags={"Follow Up"},
     *     summary="Return user's filters on FollowUp",
     *     description="Return user's filters on FollowUp",
     *     path="/api/v1/fup/user/filters",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Return user's filters on FollowUp",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *      @OA\Schema(
     *           @OA\Property(
     *              property="fupFilters",
     *               type="array",
     *              description="The response data",
     *              @OA\Items(
     *                      @OA\Property(
     *                                  property="id",
     *                                  type="integer",
     *                                  description="Id of filter",
     *                              ),
     *                              @OA\Property(
     *                                  property="filer_nome",
     *                                  type="string",
     *                                  description="Filter name",
     *                              ),
     *                              @OA\Property(
     *                                  property="filer_data",
     *                                  type="string",
     *                                  description="Filters saved",
     *                              ),
     *               )
     *             ),
     *                 example= {
     *                           "fupFilters": {
     *                              {
     *                               "filter_id": 1,
     *                               "filter_nome": "Filter Name",
     *                               "filter_data": "'curves':{'ABC': ['A', 'B'],'XYZ': [...],'PQR': [...],'123': [...],},'estabs':['1', ...],'managers': ['1', '2', ...],'policies': ['1', '2',...],'standard_deposit': ['1', '2']"
     *                              },
     *                             {
     *                               "filter_id": 1,
     *                               "filter_nome": "Filter Name",
     *                               "filter_data": "'curves':{'ABC': ['A', 'B'],'XYZ': [...],'PQR': [...],'123': [...],},'estabs':['1', ...],'managers': ['1', '2', ...],'policies': ['1', '2',...],'standard_deposit': ['1', '2']"
     *                              }
     *                             }
     *                           }
     *              )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     *
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getList(Request $request): JsonResponse
    {

        try {

            $fupFilters = (new FollowUpUserFilterListModel($request->user))->getFilters();
            return response()->json(compact('fupFilters'), 200);

        } catch (\Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }
    }

    /**
     * @OA\Get(
     *     tags={"Follow Up"},
     *     summary="Return a specific filter of user",
     *     description="Return a specific filter of user",
     *     path="/api/v1/fup/user/filters/{filter_id}",
     *      @OA\Parameter(
     *         name="filter_id",
     *         in="path",
     *         description="ID of Filter",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Return list of user filter on FollowUp",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *      @OA\Schema(
     *           @OA\Property(
     *              property="fupFilters",
     *               type="array",
     *              description="The response data",
     *              @OA\Items(
     *                      @OA\Property(
     *                                  property="id",
     *                                  type="integer",
     *                                  description="Id of filter",
     *                              ),
     *                              @OA\Property(
     *                                  property="filer_nome",
     *                                  type="string",
     *                                  description="Filter name",
     *                              ),
     *                              @OA\Property(
     *                                  property="filer_data",
     *                                  type="string",
     *                                  description="Filters saved",
     *                              ),
     *               )
     *             ),
     *                 example= {
     *                           "fupFilters": {
     *                              {
     *                               "filter_id": 1,
     *                               "filter_nome": "Filter Name",
     *                               "filter_data": "'curves':{'ABC': ['A', 'B'],'XYZ': [...],'PQR': [...],'123': [...],},'estabs':['1', ...],'managers': ['1', '2', ...],'policies': ['1', '2',...],'standard_deposit': ['1', '2']"
     *                              }
     *                             }
     *                           }
     *              )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     *
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */

    public function getFilter(Request $request): JsonResponse
    {

        try {
            $fupFilters = (new FollowUpUserFilterListModel($request->user))->getFilter((int)$request->filter_id);

            return response()->json(compact('fupFilters'), 200);

        } catch (\Exception $e) {

            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }
    }
}
