<?php

namespace App\Http\Controllers\Permission;

use App\Http\Controllers\Controller;
use App\Model\Tag;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PermissionController extends Controller
{

    /**
     * @OA\Get(
     *     tags={"Auth"},
     *     summary="Return if company has tag",
     *     description="Returns a boolean",
     *     path="/api/v1/permissions/{tag}",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="If Company has tag in a boolean return"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */

    public function hasTag(Request $request): JsonResponse
    {
        try {
            Tag::$company_id_fk = $request->user->getIdCompanyFk();
            return response()->json(['permission' => Tag::hasTag($request->tag)], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }

    }

    public function getList(): JsonResponse
    {
        $permissions = [];
        for ($i = 0; $i <= 20; $i++) {
            $permissions[$i] = [
                'id' => $i + 1,
                'name' => sprintf('Permission %d', $i + 1)
            ];
        }

        return response()->json(['permissions' => $permissions], 200);
    }

    public function get(int $pid): JsonResponse
    {
        $permission = [
            "id" => $pid,
            "name" => "Permission $pid"
        ];
        return response()->json(['permission' => $permission], 200);
    }

    public function apply(Request $request)
    {

        $data = $request->post();
        $companies = $data['companies'] ?? [];
        $tag = $data['tag'] ?? '';
        $response = [];
        foreach ($companies as $id_company_fk) {
            Tag::$company_id_fk = $id_company_fk;
            if (!Tag::hasTag($tag)) {
                Tag::applyTag($tag);
                $response[] = "Apply tag {$data['tag']} on company {$id_company_fk}";
                Log::info(current($response));
            }

        }

        return response()->json(['data' => $response], 200);
    }

    public function remove(Request $request)
    {

        $data = $request->post();
        $companies = $data['companies'] ?? [];
        $tag = $data['tag'] ?? '';
        $response = [];
        foreach ($companies as $id_company_fk) {
            Tag::$company_id_fk = $id_company_fk;
            if (Tag::hasTag($tag)) {
                Tag::removeTag($tag);
                $response[] = "Remove tag {$data['tag']} on company {$id_company_fk}";
                Log::info(current($response));
            }

        }

        return response()->json(['data' => $response], 200);
    }
}
