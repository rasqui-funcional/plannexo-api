<?php

namespace App\Http\Controllers\Profile;

use App\Domain\User\Entity\Profile;
use App\Http\Controllers\Controller;
use App\Model\ProfileModel;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    private array $company_id = [];

    /**
     * @OA\Get(
     *     tags={"Profile"},
     *     summary="Returns a list of profiles",
     *     description="Returns a object of profiles",
     *     path="/api/v1/profiles",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Return list of profiles"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getList(Request $request): JsonResponse
    {
        $this->company_id = [
            'id_company_fk' => $request->user->getIdCompanyFk()
        ];
        $profileModel = new ProfileModel(new Profile($this->company_id));
        $profiles = $profileModel->getProfiles();
        return response()->json(['profiles' => $profiles], 200);
    }

    /**
     * @OA\Get(
     *     tags={"Profile"},
     *     summary="Returns a object Profile",
     *     path="/api/v1/profiles/{pid}",
     *     @OA\Parameter(
     *         name="pid",
     *         in="path",
     *         description="ID of Profile to return",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Return profile object"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="A Profile with the specified ID was not found"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param int $pid
     * @return JsonResponse
     * @throws Exception
     */
    public function get(Request $request, int $pid): JsonResponse
    {
        $this->company_id = [
            'id_company_fk' => $request->user->getIdCompanyFk()
        ];
        $profileModel = new ProfileModel(new Profile($this->company_id));
        $profile = $profileModel->getProfile($pid);
        return response()->json(['profile' => $profile], 200);
    }

    /**
     * @OA\Post(
     *     tags={"Profile"},
     *     summary="Create a new Profile",
     *     path="/api/v1/profiles",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"name"},
     *                 @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 example={"name": ""}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Profile created",
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function create(Request $request): JsonResponse
    {
        return response()->json(['success' => [
            'message' => 'Method not Allowed.'
        ]], 405);
    }

    /**
     * @OA\Put(
     *     tags={"Profile"},
     *     summary="Update a Profile",
     *     path="/api/v1/profiles/{pid}",
     *     @OA\Parameter(
     *         name="pid",
     *         in="path",
     *         description="ID of Profile to return",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"name"},
     *                 @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 example={"name": ""}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Update Profile object"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="A Profile with the specified ID was not found"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @param int $pid
     * @return JsonResponse
     * @throws Exception
     */
    public function update(Request $request, int $pid): JsonResponse
    {
        return response()->json(['success' => [
            'message' => 'Method not Allowed.'
        ]], 405);
    }

    /**
     * @OA\Delete(
     *     tags={"Profile"},
     *     summary="Delete a Profile",
     *     path="/api/v1/profiles/{pid}",
     *     @OA\Parameter(
     *         name="pid",
     *         in="path",
     *         description="ID of Profile to delete",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Delete Profile object"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="A Profile with the specified ID was not found"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param int $pid
     * @return JsonResponse
     */
    public function delete(int $pid): JsonResponse
    {
        return response()->json(['success' => [
            'message' => 'Method not Allowed.'
        ]], 405);
    }
}
