<?php

namespace App\Http\Controllers;

use App\Domain\Email\EmailInterface;
use App\Http\Services\EmailSendingService;
use App\Http\Services\HttpService;
use GuzzleHttp\Client;

class TesteController extends Controller
{
    private EmailSendingService $emailSendingService;

    public function __construct(EmailInterface $emailSendingService)
    {
        $this->emailSendingService = $emailSendingService;
    }

    public function open()
    {
//        $dateTime = new \DateTime('now', new \DateTimeZone('America/Sao_Paulo'));
//        $dateTime->modify('+1 minutes');
//        dd($dateTime->getTimestamp());


        $data = [
            'receivers' => [
                'receiver@bio.com'
            ],
//            'cc' => [
//                'cc@bio'
//            ],
            'subject' => 'Mussum Ipsum, cacilds vidis litro abertis',
            'htmlBody' => '<html><head><title>Teste</title></head><body><h1 style="color: #5e9ca0;">You can edit <span style="color: #2b2301;">this demo</span> text!</h1>
<h2 style="color: #2e6c80;">How to use the editor:</h2>
<p>Paste your documents in the visual editor on the left or your HTML code in the source editor in the right. <br />Edit any of the two areas and see the other changing in real time.&nbsp;</p>
<p>Click the <span style="background-color: #2b2301; color: #fff; display: inline-block; padding: 3px 10px; font-weight: bold; border-radius: 5px;">Clean</span> button to clean your source code.</p>
<h2 style="color: #2e6c80;">Some useful features:</h2>
<ol style="list-style: none; font-size: 14px; line-height: 32px; font-weight: bold;">
<li style="clear: both;"><img style="float: left;" src="https://html-online.com/img/01-interactive-connection.png" alt="interactive connection" width="45" /> Interactive source editor</li>
<li style="clear: both;"><img style="float: left;" src="https://html-online.com/img/02-html-clean.png" alt="html cleaner" width="45" /> HTML Cleaning</li>
<li style="clear: both;"><img style="float: left;" src="https://html-online.com/img/03-docs-to-html.png" alt="Word to html" width="45" /> Word to HTML conversion</li>
<li style="clear: both;"><img style="float: left;" src="https://html-online.com/img/04-replace.png" alt="replace text" width="45" /> Find and Replace</li>
<li style="clear: both;"><img style="float: left;" src="https://html-online.com/img/05-gibberish.png" alt="gibberish" width="45" /> Lorem-Ipsum generator</li>
<li style="clear: both;"><img style="float: left;" src="https://html-online.com/img/6-table-div-html.png" alt="html table div" width="45" /> Table to DIV conversion</li>
</ol>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
<h2 style="color: #2e6c80;">Cleaning options:<strong style="color: #000000; font-size: 14px;">&nbsp;</strong></h2>
<p><strong>Save this link into your bookmarks and share it with your friends. It is all FREE! </strong><br /><strong>Enjoy!</strong></p>
<p><strong>&nbsp;</strong></p></body></html>',
//            'textBody' => 'Suco de cevadiss deixa as pessoas mais interessantis. Detraxit consequat et quo num tendi nada. In elementis mé pra quem é amistosis quis leo. Praesent vel viverra nisi. Mauris aliquet nunc non turpis scelerisque, eget.'
        ];

        try {
            $this->emailSendingService->exchangeArray($data);
            $response = $this->emailSendingService->sendMailings();

            dd($response);
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }
}
