<?php

namespace App\Http\Controllers\User;

use App\Domain\Email\EmailInterface;
use App\Domain\User\Entity\User;
use App\Domain\User\Entity\UserProfile;
use App\Domain\User\Entity\UserEstablishments;
use App\Events\SendEmailEvent;
use App\Domain\User\Token\PassAccessToken;
use App\Http\Controllers\Controller;
use App\Http\Services\EmailSendingService;
use App\Model\UserModel;
use App\Model\UserProfileModel;
use App\Model\UserEstablishmentsModel;
use DateTime;
use DateTimeZone;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Throwable;

/**
 * @OA\Info(
 *     title="Plannexo Api",
 *     description="Documentação das APIs da nova versão do Plannexo",
 *     version="0.1",
 *     @OA\Contact(
 *          email="cesantos@bionexo.com",
 *          name="API Support"
 *    )
 * )
 */
class UserController extends Controller
{
    private EmailSendingService $emailSendingService;

    public function __construct(EmailInterface $emailSendingService)
    {
        $this->emailSendingService = $emailSendingService;
    }

    /**
     * @var array|null
     */
    private ?array $posted_data = null;

    /**
     * @OA\Get(
     *     tags={"User"},
     *     summary="Returns a list of users",
     *     description="Returns a object of users",
     *     path="/api/v1/users",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Return list of users"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getList(Request $request): JsonResponse
    {
//        dd($request->user->getIdUserPk());
        $user = $request->user;
        $userModel = new UserModel($user);

        try {
            $users = $userModel->getUsers();
        } catch (Exception $exp) {
            Log::error($exp->getMessage(), $exp->getTrace());
            return response()->json(['errors' => [
                'message' => $exp->getMessage()
            ]], 500);
        }

        return response()->json(['users' => $users], 200);
    }

    /**
     * @OA\Get(
     *     tags={"User"},
     *     summary="Returns a object user",
     *     path="/api/v1/users/{uid}",
     *     @OA\Parameter(
     *         name="uid",
     *         in="path",
     *         description="ID of User to return",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Return user object"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="A user with the specified ID was not found"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @param int $uid
     * @return JsonResponse
     */
    public function get(Request $request, int $uid): JsonResponse
    {
        $session = $request->user;
        $userModel = new UserModel($session);
        try {
            $user = $userModel->getUser($uid);

            $userProfile = new UserProfile([
                'id_company_fk' => (int) $user->id_company,
                'id_profile_fk' => 0,
                'id_user_fk' => (int) $user->id
            ]);
            $user->profiles = (new UserProfileModel($userProfile))->getUserProfilesIdByUserId()->getUserProfilesId();

            $userEstablishments = new UserEstablishments([
                'id_company_fk' => (int) $user->id_company,
                'id_user_fk' => (int) $user->id
            ]);
            $user->establishments = (new UserEstablishmentsModel($userEstablishments))
                ->getEstablishmentsByUserIdAndCompanyId($user->id, $user->id_company);
        } catch (Exception $exp) {
            Log::error($exp->getMessage(), $exp->getTrace());
            return response()->json(['errors' => [
                'message' => $exp->getMessage()
            ]], 500);
        }

        return response()->json(['user' => $user], 200);
    }

    /**
     * @OA\Get(
     *     tags={"User"},
     *     summary="Forgot Password for User",
     *     path="/api/v1/users/forgot-password/{mail}",
     *     @OA\Parameter(
     *         name="mail",
     *         in="path",
     *         description="Email of User",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Return user object"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="A user with the specified ID was not found"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @param string $mail
     * @return JsonResponse
     * @throws Throwable
     */
    public function forgotPassword(Request $request, string $mail): JsonResponse
    {
        try {
            $entity = new User($request->all());
            $user = (new UserModel($entity))->getUserByMail($mail);
            if (!$user) {
                throw new Exception(sprintf('User not found for %s', $mail), 404);
            }

            $token = (new PassAccessToken())
                ->setUuid($user->id)
                ->setCcid($user->id_company)
                ->generateToken()
                ->token();

            $title = $user->name ?? $user->email;

            $this->emailSendingService->exchangeArray([
                'receivers' => [$user->email],
                'subject' => 'Recuperação de Senha',
                'htmlBody' => view('recover-password', [
                    'title' => $title, 'token' => $token, 'year' => date('Y')
                ])->render()
            ]);
            $this->emailSendingService->sendMailings();

            $response = response()->json(['success' => true, 'message' => 'Successfully Sent'], 200);
        } catch (Exception $exp) {
            Log::error($exp->getMessage(), ['cod_error' => $exp->getCode(), 'trace' => $exp->getTrace()]);
            $response = response()->json(['success' => false, 'message' => 'Unsuccessfully Sent'], 200);
        }

        return $response;
    }

    public function getPartialList(Request $request): JsonResponse
    {
        $user = $request->user;
        $userModel = new UserModel($user);

        try {
            $users = $userModel->getPartialUsers();
        } catch (Exception $exp) {
            Log::error($exp->getMessage(), $exp->getTrace());
            return response()->json(['errors' => [
                'message' => $exp->getMessage()
            ]], 500);
        }

        return response()->json(['users' => $users], 200);
    }

    /**
     * @OA\Post(
     *     tags={"User"},
     *     summary="Create a new user",
     *     path="/api/v1/users",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     * @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"name"},
     *                 @OA\Property(
     *                     property="login",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="erpCode",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="phone",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="profile",
     *                     type="int"
     *                 ),
     *                 example={"login": "", "name": "", "email": "", "erpCode": "", "phone": "", "profile": ""}
     *             )
     *         )
     *     ),
     * @OA\Response(
     *         response=201,
     *         description="User created",
     *     ),
     * @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     * @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     * @OA\Response(
     *         response=417,
     *         description="Create User Failed"
     *     ),
     * @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     * @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function create(Request $request): JsonResponse
    {
        $user = new User($request->all());
        try {
            $newUsers = (new UserModel($user))->createUser();
            event( (new SendEmailEvent($this->emailSendingService))->exchangeArray($newUsers)->send() );
        } catch (Exception | QueryException $exp) {
            Log::error($exp->getMessage(), $exp->getTrace());
            return response()->json(['errors' => [
                'message' => 'Create User Failed'
            ]], 417);
        }

        return response()->json(['users' => $newUsers], 201);
    }

    /**
     * @OA\Put(
     *     tags={"User"},
     *     summary="Update a user",
     *     path="/api/v1/users/{uid}",
     *     @OA\Parameter(
     *         name="uid",
     *         in="path",
     *         description="ID of User to return",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"name"},
     *                 @OA\Property(
     *                     property="login",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="erpCode",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="phone",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="profile",
     *                     type="int"
     *                 ),
     *                 example={"login": "", "name": "", "email": "", "erpCode": "", "phone": "", "profile": ""}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Update user object"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="A user with the specified ID was not found"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=417,
     *         description="Updated User Failed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @param int $uid
     * @return JsonResponse
     * @throws Exception
     */
    public function update(Request $request, int $uid): JsonResponse
    {
        $user = new User($request->all());

        try {
            $updateUsers = (new UserModel($user))->updateUser($uid);
        } catch (Exception | QueryException $exp) {
            Log::error($exp->getMessage(), $exp->getTrace());
            return response()->json(['errors' => [
                'message' => 'Updated User Failed'
            ]], 417);
        }

        return response()->json(['user' => $updateUsers], 200);
    }

    /**
     * @OA\Patch(
     *     tags={"User"},
     *     summary="Update status user",
     *     path="/api/v1/users/{uid}",
     *     @OA\Parameter(
     *         name="uid",
     *         in="path",
     *         description="ID of User to return",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"status"},
     *                 @OA\Property(
     *                     property="status",
     *                     type="int"
     *                 ),
     *                 example={"status": 1}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Update status user object"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="A user with the specified ID was not found"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=417,
     *         description="Updated Status User Failed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @param int $uid
     * @return JsonResponse
     * @throws Exception
     */
    public function statusUser(Request $request, int $uid): JsonResponse
    {
        $user = new User($request->all());
        $user->setIdCompanyFk($request->user->getIdCompanyFk());

        try {
            $statusUser = (new UserModel($user))->statusUser($uid);
        } catch (Exception | QueryException $exp) {
            Log::error($exp->getMessage(), $exp->getTrace());
            return response()->json(['errors' => [
                'message' => 'Updated Status User Failed'
            ]], 417);
        }

        return response()->json(['user' => $statusUser], 200);
    }

    /**
     * @OA\Patch(
     *     tags={"User"},
     *     summary="Change Data user",
     *     path="/api/v1/users/change-data/{uid}",
     *     @OA\Parameter(
     *         name="uid",
     *         in="path",
     *         description="ID of User to return",
     *     ),
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="login",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="user_id",
     *                     type="int"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="erpCode",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="phone",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="profile",
     *                     type="int"
     *                 ),
     *                 @OA\Property(
     *                     property="valid_token",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password_confirmed",
     *                     type="string"
     *                 ),
     *                 example={"password": "", "password_confirmed": "", "user_id": "", "valid_token": ""}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Update status user object"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="A user with the specified ID was not found"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=417,
     *         description="Updated User Failed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @param int|null $uid
     * @return JsonResponse
     * @throws Exception
     */
    public function changeData(Request $request, ?int $uid = null): JsonResponse
    {
        $valid_token = $request->input('valid_token', null);
        try {
            if (!$this->parseToken($valid_token) || $this->validExpiredToken((int) $this->posted_data['expired'])) {
                throw new Exception('Token is invalid', 400);
            }

            $user = new User($request->all());
            $user->setIdUserPk((int) $this->posted_data['user_id']);
            $user->setIdCompanyFk((int) $this->posted_data['company_id']);

            $changeUser = (new UserModel($user))->changeUser();
        } catch (Exception | QueryException $exp) {
            Log::error($exp->getMessage(), $exp->getTrace());
            return response()->json(['success' => false, 'message' => 'Updated User Failed'], 200);
        }

        return response()->json(['success' => true, 'user' => $changeUser], 200);
    }

    /**
     * @OA\Patch(
     *     tags={"User"},
     *     summary="Verify if mail exists",
     *     path="/api/v1/mail-validation/{mail}",
     *     @OA\Parameter(
     *         name="mail",
     *         in="path",
     *         description="Mail of User to return",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 required={"status"},
     *                 @OA\Property(
     *                     property="mail",
     *                     type="string"
     *                 ),
     *                 example={"mail": "qa@bionexo.com"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Update status user object"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="A user with the specified ID was not found"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param Request $request
     * @param string $mail
     * @param string $id
     * @return JsonResponse
     */
    public function mailValidation(Request $request, string $mail, string $id): JsonResponse
    {
        $session = $request->user;
        $userModel = new UserModel($session);

        $mail = ($id != "null") ? $userModel->getMailUpdateUser($mail, $id) : $userModel->getMailNewUser($mail);
        return response()->json(['mail' => $mail], 200);
    }

    /**
     * @OA\Delete(
     *     tags={"User"},
     *     summary="Delete a user",
     *     path="/api/v1/users/{uid}",
     *     @OA\Parameter(
     *         name="uid",
     *         in="path",
     *         description="ID of User to delete",
     *         required=true,
     *     ),
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Delete user object"
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="A user with the specified ID was not found"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     * )
     *
     * @param int $uid
     * @return JsonResponse
     */
    public function delete(int $uid): JsonResponse
    {
        /**
         * No caso de erros a estrutura segue abaixo:
         * return response()->json(['errors' => [
         *       'message' => "motivo do erro"
         *  ]], codigo_do_erro);
         */

        return response()->json(['success' => [
            'message' => 'Method not Allowed.'
        ]], 405);
    }

    /**
     * @param string|null $token
     * @return bool
     */
    private function parseToken(?string $token): bool
    {
        $output = false;
        if (!is_null($token)) {
            $output = true;
            $decode = base64_decode($token);
            parse_str($decode, $this->posted_data);
        }

        return $output;
    }

    /**
     * @param int|null $timestamp
     * @return bool
     * @throws Exception
     */
    private function validExpiredToken(?int $timestamp): bool
    {
        try {
            $dateCurrent = new DateTime('now', new DateTimeZone('America/Sao_Paulo'));
            $dateExpired = new DateTime('now', new DateTimeZone('America/Sao_Paulo'));
            $dateExpired->setTimestamp($timestamp);

            return ($dateExpired->getTimestamp() < $dateCurrent->getTimestamp());
        } catch (Exception $e) {
            return false;
        }
    }
}
