<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Model\UserEstablishmentsModel;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserEstablishmentsController extends Controller
{

    /**
     * @OA\Get(
     *     tags={"User"},
     *     summary="Returns a list of establishment of user",
     *     description="Returns a list of establishment of user",
     *     path="/api/v1/users/establishments",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns a list of establishment of user",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *      @OA\Schema(
     *           @OA\Property(
     *              property="estabs",
     *               type="array",
     *              description="The response data",
     *              @OA\Items(
     *                @OA\Property(
     *                 property="id",
     *                 type="string",
     *                 description="The establishment ID",
     *                ),
     *                @OA\Property(
     *                         property="name",
     *                         type="string",
     *                         description="The establishment name",
     *                 ),
     *               )
     *             ),
     *                 example= {"estabs": {
     *                                           {"id": 1,"name": "estabName"},
     *                                           {"id": 2,"name": "estabName 2"}
     *                              }
     *                          }
     *              )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     *
     * )
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function getList(Request $request): JsonResponse
    {
        try {
            $estabs = (new UserEstablishmentsModel($request->user->getEstablishments()))->getEstablishments();

            return response()->json(compact('estabs'), 200);

        } catch (\Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }
    }

}
