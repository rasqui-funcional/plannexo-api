<?php

namespace App\Http\Controllers\User;

use App\Domain\Company\CompanyPoliciesInterface;
use App\Domain\User\UserManagersInterface;
use App\Http\Controllers\Controller;
use App\Model\User\UserManagersModel;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserManagersController extends Controller
{

    private $user_manager;
    public function __construct(UserManagersInterface $user_manager)
    {
        $this->user_manager = $user_manager;
    }
    /**
     * @OA\Get(
     *     tags={"User"},
     *     summary="Returns a list of managers of company",
     *     description="Returns a list of managers of user",
     *     path="/api/v1/users/managers",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns a list of managers of company",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *      @OA\Schema(
     *           @OA\Property(
     *              property="managers",
     *               type="array",
     *              description="The response data",
     *              @OA\Items(
     *                @OA\Property(
     *                 property="id",
     *                 type="integer",
     *                 description="The managers ID",
     *                ),
     *                @OA\Property(
     *                         property="name",
     *                         type="string",
     *                         description="The managers name",
     *                 ),
     *               )
     *             ),
     *                 example= {"managers": {
     *                                           {"id": 1,"name": "managerName"},
     *                                           {"id": 2,"name": "managerName 2"}
     *                              }
     *                          }
     *              )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     *
     * )
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function getList(): JsonResponse
    {

        try {
            $managers = $this->user_manager->getUserManagers();

            return response()->json(compact('managers'), 200);

        } catch (\Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }
    }

}
