<?php

namespace App\Http\Controllers\User;

use App\Domain\User\UserStandardDepositInterface;
use App\Http\Controllers\Controller;
use App\Model\User\UserStandardDepositModel;
use App\Model\UserEstablishmentsModel;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class UserStandardDepositController
 * @package App\Http\Controllers\User
 */
class UserStandardDepositController extends Controller
{

    /**
     * @var UserStandardDepositInterface
     */
    private $user_standard_deposit;

    /**
     * UserStandardDepositController constructor.
     * @param UserStandardDepositInterface $user_standard_deposit
     */
    public function __construct(UserStandardDepositInterface $user_standard_deposit)
    {
        $this->user_standard_deposit = $user_standard_deposit;
    }
    /**
     * @OA\Get(
     *     tags={"User"},
     *     summary="Returns a list of standard deposit of user",
     *     description="Returns a list of standard deposit of user",
     *     path="/api/v1/users/standard-deposit",
     *     @OA\Parameter(
     *        name="token",
     *        in="query",
     *        required=true,
     *        @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Returns a list of standard deposit of user",
     *         content={
     *             @OA\MediaType(
     *                 mediaType="application/json",
     *      @OA\Schema(
     *           @OA\Property(
     *              property="estabs",
     *               type="array",
     *              description="The response data",
     *              @OA\Items(
     *                @OA\Property(
     *                 property="id",
     *                 type="string",
     *                 description="The standard deposit ID",
     *                ),
     *                @OA\Property(
     *                         property="name",
     *                         type="string",
     *                         description="The standard deposit name",
     *                 ),
     *               )
     *             ),
     *                 example= {"standard_deposit": {
     *                                           {"id": 1,"name": "estabName"},
     *                                           {"id": 2,"name": "estabName 2"}
     *                              }
     *                          }
     *              )
     *             )
     *         }
     *     ),
     *     @OA\Response(
     *          response="401",
     *          description="Authorization information is missing or invalid"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Method not Allowed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error"
     *     )
     *
     * )
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function getList(Request $request): JsonResponse
    {

        try {
            //TODO Need improve User to retrieve user_estabs to not be necessay invoke class UserEstablishmentsModel here
            $estabs = (new UserEstablishmentsModel($request->user->getEstablishments()))->getEstablishmentsCodes();

            $standard_deposit = $this->user_standard_deposit->setEstabs($estabs)->getUserStandardDeposit();

            return response()->json(compact('standard_deposit'), 200);

        } catch (\Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage()]], 500);
        }
    }

}
