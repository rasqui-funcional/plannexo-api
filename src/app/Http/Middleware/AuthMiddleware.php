<?php

namespace App\Http\Middleware;

use App\Domain\Auth\Authenticate;
use App\Domain\Company\Entity\Company;
use App\Domain\User\Entity\User;
use Closure;
use Exception;
use Illuminate\Http\Request;
use Lcobucci\JWT\Token;

/**
 * Class AuthMiddleware
 * @package App\Http\Middleware
 */
class AuthMiddleware
{

    /**
     * @var \App\Domain\Auth\Authenticate
     */
    private $auth;

    /**
     * AuthMiddleware constructor.
     * @param \App\Domain\Auth\Authenticate $auth
     */
    public function __construct(\App\Domain\Auth\Authenticate $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->bearerToken() && !$request->token) {
            return response()->json(['status' => 'token is required'], 401);
        }

        try {
            if ($this->auth->authenticate()) {
                return $next($this->addUserDataInRequest($request, $this->auth));
            }
        } catch (Exception $e) {
            return response()->json(['status' => $e->getMessage()], 400);
        }

        return response()->json(['status' => 'Token is invalid'], 401);
    }


    /**
     * @param Request $request
     * @param Token $token
     * @return Request
     * @throws Exception
     */
    private function addUserDataInRequest(Request $request, Authenticate $auth)
    {
        return $request->merge([
            'user' => $auth->getUser(),
            'company' => $auth->getUser()->getCompany(),
        ]);
    }
}
