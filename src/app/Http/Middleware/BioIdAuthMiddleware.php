<?php

namespace App\Http\Middleware;

use App\Domain\Auth\BioId\BioIdAuthenticate;
use App\Domain\Company\Entity\Company;
use App\Domain\User\Entity\User;
use App\Model\CompanyModel;
use App\Model\UserLoginModel;
use App\Model\UserModel;
use Closure;
use Exception;
use Illuminate\Support\Facades\Log;

class BioIdAuthMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     * @throws Exception
     */
    public function handle($request, Closure $next)
    {
        if (!$request->bioid_token) {
            return response()->json(['error' => ['message' => 'Missing Data']], 422);
        }

        $auth = new BioIdAuthenticate($request->bioid_token);

        if (!$auth->authenticate()) {
            return response()->json(['error' => ['message' => 'not authorized']], 401);
        }

        $user_data = ($auth->auth->getResourceOwner($request->bioid_token))->toArray();
        Log::info(__CLASS__, ['user_auth_data' => $user_data]);

        # id que vem no retorno do bioid faz referencia na tabela ADM_COMPANY
        $entity_company = new Company(['bioid_reference' => (int)$user_data['company']['id']]);
        $companyModel = (new CompanyModel($entity_company))->getCompanyByBioId();

        $entity_user = new User(['id_company_fk' => (int)$companyModel->id, 'bioid_reference' => $user_data['id']]);
        $userModel = (new UserModel($entity_user))->getUserByBioId();

        $user_data = [
            'email' => $user_data['email'],
            'id_company_fk' => (int)$companyModel->id,
            'id_user_pk' => (int)$userModel->id_user_pk,
            'bioid_reference' => $user_data['id'],
            'ref_company_bioid' => $companyModel->reference,
            'password' => 'Bio@1234', //TODO need remove this when auth in sigah has been done. This work only to users that have password equals Bio@1234
        ];

        $user_data = new UserLoginModel((new User($user_data)));

        Log::info(__CLASS__, ['user_data' => $user_data]);

        $request->merge(['user_data' => $user_data]);

        return $next($request);
    }
}
