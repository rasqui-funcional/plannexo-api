<?php

namespace App\Http\Middleware;

use App\Domain\Auth\BioId\BioId;
use App\Domain\Auth\BioId\BioIdAuthToken;
use App\Domain\User\Entity\User;
use Closure;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException as IdentityProviderExceptionAlias;

class BioIdAuthTokenMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
     */
    public function handle($request, Closure $next)
    {
        if (!$request->code) {
            Log::alert('Code não enviado', ['code' =>$request->code]);
            return response()->json(['error' => ['message' => 'code is required']], 400);
        }

        $token = false;
        try {
            $auth_token = (new BioIdAuthToken(new BioId()))->setCode($request->code);
            $token = $auth_token->generateToken()->token();
            if(!$token) {
                throw new Exception('Erro ao gerar o token', 422);
            }
        }
        catch (\Exception | IdentityProviderExceptionAlias $exp) {
            Log::error($exp->getMessage(), ['code' =>$request->code, 'token' => $token]);
            return response()->json(['error' => ['message' => 'Authorization information is missing or invalid']], 401);
        }

        if(!$token) {
            Log::alert('Erro ao gerar o token', ['token' => $token]);
            return response()->json(['error' => ['message' => 'Missing Data']], 422);
        }

        $request->merge(['bioid_token' => $token]);

        return $next($request);
    }


    /**
     * @param Request $request
     * @param Token $token
     * @return Request
     * @throws Exception
     */
    private function addUserDataInRequest(Request $request, Token $token)
    {
        return $request->merge(['user' => new User(['id_user_pk' => $token->getClaim('uid'), 'id_company_fk' => (int)$token->getClaim('jti')])]);
    }
}
