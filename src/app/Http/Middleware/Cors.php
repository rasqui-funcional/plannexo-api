<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        header('Access-Control-Allow-Origin: http://localhost:8080');
//        return $next($request)
//            ->header('Access-Control-Allow-Origin', "http://localhost:8080")
//            ->header('Access-Control-Allow-Methods', "GET, POST, PUT, DELETE, OPTIONS, PATCH, HEAD")
//            ->header('Access-Control-Allow-Headers', "Accept, Authorization, Content-Type")
//        ;

        header("Access-Control-Allow-Origin: *");

        $headers = [
            'Access-Control-Allow-Methods'=> 'GET, POST, PUT, DELETE, OPTIONS, PATCH, HEAD',
            'Access-Control-Allow-Headers'=> 'Accept, Content-Type, X-Auth-Token, Origin, Authorization'
        ];

        if ($request->getMethod() == "OPTIONS") {
            return response()->make('OK', 200, $headers);
        }

        $response = $next($request);
        foreach($headers as $key => $value)
            $response->header($key, $value);

        return $response;
    }
}
