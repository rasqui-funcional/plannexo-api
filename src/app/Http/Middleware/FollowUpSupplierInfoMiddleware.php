<?php

namespace App\Http\Middleware;

use App\Domain\FollowUp\FollowUpSupplierInterface;
use Closure;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException as IdentityProviderExceptionAlias;

class FollowUpSupplierInfoMiddleware
{
    private FollowUpSupplierInterface $follow_up_summary;

    public function __construct(FollowUpSupplierInterface $follow_up_summary)
    {
        $this->follow_up_summary = $follow_up_summary;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $data = $this->follow_up_summary
            ->setSupplierId($request->supplierId)
            ->setCompanyId($request->user->getIdCompanyFk())
            ->getInfo();

        $request->merge(['supplier_info' => $data]);

        return $next($request);
    }
}
