<?php

namespace App\Http\Middleware;

use App\Domain\Auth\BioId\BioId;
use App\Domain\Auth\BioId\BioIdAuthToken;
use App\Domain\Auth\JWT\JwtToken;
use App\Domain\User\Entity\User;
use Closure;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException as IdentityProviderExceptionAlias;

class JwtAuthTokenMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->user_login) {
            Log::alert('Objeto user_login não existe', ['user_login' =>$request->user_login]);
            return response()->json(['error' => ['message' => 'Missing Data']], 422);
        }

        $token = false;
        try {
            $token = (new JwtToken($request->user_login))->generateToken()->token();
            if(!$token) {
                throw new Exception('Unexpected error', 500);
            }
        }
        catch (\Exception $exp) {
            Log::error($exp->getMessage(), ['token' => $token]);
            return response()->json(['error' => ['message' => $exp->getMessage()]], 500);
        }

        Log::info('generate_token: ', ['token' => $token]);

        $request->merge(['token' => $token]);
        return $next($request);
    }


    /**
     * @param Request $request
     * @param Token $token
     * @return Request
     * @throws Exception
     */
    private function addUserDataInRequest(Request $request, Token $token)
    {
        return $request->merge([
            'user' => new User([
                'id_user_pk' => $token->getClaim('uid'),
                'id_company_fk' => (int)$token->getClaim('jti'),
            ])
        ]);
    }
}
