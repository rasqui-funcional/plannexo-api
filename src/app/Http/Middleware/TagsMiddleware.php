<?php

namespace App\Http\Middleware;

use App\Model\Tag;
use Closure;

class TagsMiddleware
{

    public function __construct()
    {
        Tag::$company_id_fk = app()->request->user->getIdCompanyFk();
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */

    public function handle($request, Closure $next, $tag)
    {
        if (Tag::hasTag($tag)) {
            return $next($request);
        }

        return response()->json(['status' => 'not authorized'], 403);

    }

}
