<?php

namespace App\Http\Middleware;

use App\Domain\Auth\BioId\BioIdAuthenticate;
use App\Domain\User\Entity\User;
use App\Model\UserLoginModel;
use Closure;
use Exception;

class UserAuthMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     * @throws Exception
     */
    public function handle($request, Closure $next)
    {
        if (!$request->user_data) {
            return response()->json(['error' => ['message' => 'Missing Data']], 422);
        }

        if (!$request->user_data->authenticate()) {
            return response()->json(['error' => ['message' => 'not authorized', 'success' => false]], 200);
        }

        $request->merge(['user_login' => $request->user_data->getUserByEmail()]);

        return $next($request);
    }
}
