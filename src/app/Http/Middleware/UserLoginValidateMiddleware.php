<?php

namespace App\Http\Middleware;

use App\Domain\User\Entity\User;
use App\Model\UserLoginModel;
use Closure;
use Exception;

class UserLoginValidateMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     * @throws Exception
     */
    public function handle($request, Closure $next)
    {
        $auth = $request->post('auth');

        if (!$auth['login'] || !$auth['password']) {
            return response()->json(['error' => ['message' => 'Missing Data']], 422);
        }

        $user_data = new UserLoginModel((new User(['email' => $auth['login'], 'password' => $auth['password']])));

        $request->merge(['user_data' => $user_data]);

        return $next($request);
    }
}
