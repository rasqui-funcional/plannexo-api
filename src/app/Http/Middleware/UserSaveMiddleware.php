<?php

namespace App\Http\Middleware;

use App\Model\UserSessionModel;
use Closure;

class UserSaveMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $request->merge(['id_company_fk' => $request->user->getIdCompanyFk()]);
        $request->merge(['id_user_pk' => (int)$request->get('id')]);
        $request->merge(['ref_company_bioid' => (int)$request->user->getRefCompanyBioid()]);

        return $next($request);
    }
}
