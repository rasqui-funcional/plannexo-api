<?php

namespace App\Http\Middleware;

use App\Model\UserSessionModel;
use Closure;

class UserSessionMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->user_login) {
            return response()->json(['error' => ['message' => 'Missing Data']], 422);
        }

        $user_session = new UserSessionModel($request->user_login);
        $user_session->selectUser()
            ->setToken($request->token)
            ->prepareSessionData()
            ->saveSession();

        $request->merge(['user_session' => $user_session]);
        return $next($request);
    }
}
