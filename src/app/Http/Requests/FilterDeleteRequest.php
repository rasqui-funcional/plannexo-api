<?php
declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

/**
 * Class FollowUpStoreRequest
 * @package App\Http\Requests
 */
class FilterDeleteRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'filter_id' => 'required|numeric',
        ];
    }

    public function withValidator($validator)
    {
        if ($validator->fails()) {
            $validator->after(function ($validator) {
                $response = ['error' =>
                    [
                        "message" => 'Missing Data',
                        "errors" => $validator->errors(),
                    ]
                ];
                throw new HttpResponseException(response()->json($response, 422));
            });
        }
    }

    public function all($keys = null)
    {
        $data = parent::all();
        $data['filter_id'] = $this->route('filter_id');
        return $data;
    }
}
