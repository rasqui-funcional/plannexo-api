<?php
declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

/**
 * Class FilterMailManuallyRequest
 * @package App\Http\Requests
 */
class FilterMailManuallyRequest extends FormRequest
{
    const JSON_KEY = 'manual_email';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            self::JSON_KEY . '.subject' => 'required|string|nullable|max:150|min:5',
            self::JSON_KEY . '.body' => 'required|string',
            self::JSON_KEY . '.emails' => 'required|array|min:1',
            self::JSON_KEY . '.header' => 'required|array|min:8',
            self::JSON_KEY . '.items' => 'required|array|min:1',
            self::JSON_KEY . '.i18n' => 'required|array|min:4',
        ];

        foreach ($this->request->get( self::JSON_KEY )['emails'] as $key => $value) {
            $rules[self::JSON_KEY . ".emails.{$key}"] = 'email:rfc,dns';
        }

        return $rules;
    }

    public function withValidator($validator)
    {
        if ($validator->fails()) {
            $validator->after(function ($validator) {
                $response = ['error' =>
                    [
                        "message" => 'Missing Data',
                        "errors" => $validator->errors(),
                    ]
                ];
                throw new HttpResponseException(response()->json($response, 422));
            });
        }
    }
}
