<?php
declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

/**
 * Class FollowUpSettingsRequest
 * @package App\Http\Requests
 */
class FollowUpSettingsRequest extends FormRequest
{
    /**
     *
     */
    const JSON_KEY = 'settings';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        dd($this->request->get('user'));
        // foreach ($this->request->get('settings') as $key => $val) {
        // }
        $rules[self::JSON_KEY . ".analyzeItems.pendingPurchaseOrder"] = 'numeric|required';
        $rules[self::JSON_KEY . ".analyzeItems.delayedPurchaseOrder"] = 'numeric|required';
        $rules[self::JSON_KEY . ".automaticEmail.pendingItemsStatus"] = 'boolean|required';
        $rules[self::JSON_KEY . ".automaticEmail.pendingEmailModel"] = 'numeric|required';
        $rules[self::JSON_KEY . ".automaticEmail.pendingDaysAlert"] = 'numeric|required';
        $rules[self::JSON_KEY . ".automaticEmail.delayedItemsStatus"] = 'boolean|required';
        $rules[self::JSON_KEY . ".automaticEmail.delayedEmailModel"] = 'numeric|required';
        $rules[self::JSON_KEY . ".automaticEmail.delayedDaysAlert"] = 'numeric|required';

        return $rules;
    }

    public function withValidator($validator)
    {

        if ($validator->fails()) {
            $validator->after(function ($validator) {
                $response = ['error' =>
                    [
                        "message" => 'Missing Data',
                        "errors" => $validator->errors(),
                    ]
                ];
                throw new HttpResponseException(response()->json($response, 422));
            });
        }
    }
}
