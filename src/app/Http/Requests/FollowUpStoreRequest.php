<?php
declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

/**
 * Class FollowUpStoreRequest
 * @package App\Http\Requests
 */
class FollowUpStoreRequest extends FormRequest
{
    /**
     *
     */
    const JSON_KEY = 'fup';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        foreach ($this->request->get('fup') as $key => $val) {
            $rules[self::JSON_KEY . ".{$key}.note"] = 'nullable|max:300';
            $rules[self::JSON_KEY . ".{$key}.orderNumber"] = 'numeric|required';
            $rules[self::JSON_KEY . ".{$key}.skuCode"] = 'numeric|required';
            $rules[self::JSON_KEY . ".{$key}.status"] = 'nullable|numeric';
        }
        return $rules;
    }

    public function withValidator($validator)
    {

        if ($validator->fails()) {
            $validator->after(function ($validator) {
                $response = ['error' =>
                    [
                        "message" => 'Missing Data',
                        "errors" => $validator->errors(),
                    ]
                ];
                throw new HttpResponseException(response()->json($response, 422));
            });
        }
    }
}
