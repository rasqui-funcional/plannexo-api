<?php
declare(strict_types=1);

namespace App\Http\Services\AF;

use App\Domain\AF\CompanySuppliersInterface;
use App\Http\Client\HttpServiceInterface;
use Illuminate\Support\Facades\Log;

class CompanySuppliersStoreCacheService implements CompanySuppliersInterface
{

    private $service;

    function __construct($client)
    {
        $this->service = $client;
    }

    public function createCompanySuppliers(int $id_company_fk = 0)
    {
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->buildEndpoint("/create-supplier/{$id_company_fk}"));
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_exec($ch);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    /**
     * @param string $endpoint
     * @return string
     */
    public function buildEndpoint(string $endpoint): string
    {
        return sprintf('%s%s', env('WORKER_URI', "http://{$_SERVER['REMOTE_ADDR']}:8000"), $endpoint);
    }
}
