<?php
declare(strict_types=1);

namespace App\Http\Services;


use App\Domain\Email\EmailInterface;
use App\Domain\Email\Services\EmailService;
use App\Http\Client\HttpServiceInterface;
use Exception;

class EmailSendingService extends EmailService implements EmailInterface
{
    /**
     * EmailSendingService constructor.
     * @param HttpServiceInterface $client
     */
    public function __construct(HttpServiceInterface $client)
    {
        $this->service = $client;
    }

    public function sendMailings()
    {
        try {
            $response = $this->service->method('POST')
                ->setData($this->getDataReceive())
                ->setPath($this->buildEndpoint('api/v1/Email'))
                ->setHeaders($this->getHeaders())
                ->send();
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }

        return $response->getContents()->decode();
    }

    /**
     * @inheritDoc
     */
    public function buildEndpoint(string $endpoint): string
    {
        return sprintf('%s/%s', env('API_EMAIL_URI'), $endpoint);
    }

    /**
     * @inheritDoc
     */
    public function getHeaders(): array
    {
        return [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'AwsSecretAccessKey' => env('AwsSecretAccessKey', 'AwsSecretAccessKey'),
            'AwsAccessKeyId' => env('AwsAccessKeyId', 'AwsAccessKeyId'),
        ];
    }

    /**
     * @inheritDoc
     */
    public function getDataReceive()
    {
        return [
            'senderAddress' => $this->getSender(),
            'receiversAddresses' => $this->getReceivers(),
            'ccAddresses' => $this->getCc(),
            'bccAddresses' => $this->getBcc(),
            'subject' => $this->getSubject(),
            'htmlBody' => $this->getHtmlBody(),
            'textBody' => $this->getTextBody(),
        ];
    }
}
