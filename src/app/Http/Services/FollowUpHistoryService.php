<?php

declare(strict_types=1);

namespace App\Http\Services;

use App\Domain\FollowUp\FollowUpHistoryInterface;
use App\Domain\FollowUp\Service\FollowUpService;
use App\Http\Client\HttpServiceInterface;
use App\Helpers\DateHelper;

/**
 * Class FollowUpHistoryService
 * @package App\Http\Services
 */
class FollowUpHistoryService extends FollowUpService implements FollowUpHistoryInterface
{

    /**
     * @var
     */
    private $data;

    /**
     * FollowUpHistoryService constructor.
     * @param HttpServiceInterface $client
     */
    function __construct(HttpServiceInterface $client)
    {
        $this->service = $client;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function getHistory(): array
    {
        try {

            $data = $this->service->method('GET')
                ->setPath($this->buildEndpoint("/api/v1/followup/{$this->id_supplier_fk}"))
                ->setHeaders($this->getHeaders())
                ->send()
                ->getContents()
                ->decode();

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }

        return $data;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function postHistory(): array
    {

        try {

            $data = $this->service->method('POST')
                ->setPath($this->buildEndpoint("/api/v1/followup/{$this->id_supplier_fk}"))
                ->setHeaders($this->getHeaders())
                ->setData($this->data)
                ->send()
                ->getContents()
                ->decode();

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }

        return $data;
    }

    /**
     * @return string
     */
    private function withStartOfTheDay($start): string
    {
        return $start .= date(' H:i:s ', strtotime("today"));
    }

    /**
     * @return string
     */
    private function withEndOfTheDay($end): string
    {
        return $end .= (date(' H:i:s', strtotime("today") + (60 * 1440) - 1));
    }

    /**
     * @return string
     */
    public function dateFormat($date) : string {
        return DateHelper::isoFormat($date);
    }

    /**
     * @param string $date
     */
    public function setData(array $date): self
    {
        $this->data['period']['start'] = $this->dateFormat($this->withStartOfTheDay($date['start']));
        $this->data['period']['end'] = $this->dateFormat($this->withEndOfTheDay($date['end']));
        return $this;
    }
}
