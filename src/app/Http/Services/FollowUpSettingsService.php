<?php
declare(strict_types=1);

namespace App\Http\Services;

use App\Domain\FollowUp\FollowUpSettingsInterface;
use App\Domain\FollowUp\Service\FollowUpService;
use App\Http\Client\HttpServiceInterface;

/**
 * Class FollowUpSettingsService
 * @package App\Http\Services
 */
class FollowUpSettingsService extends FollowUpService implements FollowUpSettingsInterface
{

    /**
     * @var array
     */
    private array $data = [];

    /**
     * FollowUpSettingsService constructor.
     * @param HttpServiceInterface $client
     */
    function __construct(HttpServiceInterface $client)
    {
        $this->service = $client;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function updateSettings(): array
    {
        try {
            $data = $this->followUpSettingsUpdate();
            $data['statusUpdated'] = $this->followUpStatusUpdate();
            $data['statusCreated'] = $this->followUpStatusCreate();

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }

        return $data;
    }

    private function followUpSettingsUpdate()
    {
        return $this->service->method('PUT')
            ->setPath($this->buildEndpoint('/api/v1/settings'))
            ->setHeaders($this->getHeaders())
            ->setData($this->data)
            ->send()
            ->getContents()
            ->decode();
    }

    /**
     * @inheritDoc
     * @throws \Exception
    */
    private function followUpStatusUpdate(): array
    {
        $status = [];

        if(!$this->data["statusRegistration"]["editStatus"]) {
            return $status;
        }

        foreach ($this->data["statusRegistration"]["editStatus"] as $key => $value) {

            $post = $this->service->method('PUT')
            ->setPath($this->buildEndpoint('/api/v1/status'))
            ->setHeaders($this->getHeaders())
            ->setData($value)
            ->send()
            ->getContents()
            ->decode();
            array_push($status, $post);
        }
        return $post;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function followUpStatusCreate(): array
    {
        $status = [];

        if(!$this->data["statusRegistration"]["newStatus"]) {
            return $status;
        }

        foreach ($this->data["statusRegistration"]["newStatus"] as $key => $value) {
            $post = $this->service->method('POST')
            ->setPath($this->buildEndpoint('/api/v1/status'))
            ->setHeaders($this->getHeaders())
            ->setData($value)
            ->send()
            ->getContents()
            ->decode();
            array_push($status, $post);
        }
        return $post;
    }

    /**
     * @param array $data
     * @return FollowUpSettingsInterface
     */
    public function setData($data): FollowUpSettingsInterface
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function getSettings(): array
    {
        try {

            $data = $this->service->method('GET')
                ->setPath($this->buildEndpoint('/api/v1/settings'))
                ->setHeaders($this->getHeaders())
                ->send()
                ->getContents()
                ->decode();

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }

        return $data;
    }
}
