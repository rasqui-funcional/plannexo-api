<?php

declare(strict_types=1);

namespace App\Http\Services;

use App\Domain\FollowUp\FollowUpSkuInterface;
use App\Domain\FollowUp\Service\FollowUpService;
use App\Http\Client\HttpServiceInterface;

/**
 * Class FollowUpSkuService
 * @package App\Http\Services
 */
class FollowUpSkuService extends FollowUpService implements FollowUpSkuInterface
{

    /**
     * @var
     */
    private $data;

    /**
     * FollowUpSkuService constructor.
     * @param HttpServiceInterface $client
     */
    function __construct(HttpServiceInterface $client)
    {
        $this->service = $client;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function getPendingSkus(): array
    {
        try {

            $data = $this->service->method('GET')
                ->setPath($this->buildEndpoint("/api/v1/itens/{$this->id_supplier_fk}"))
                ->setHeaders($this->getHeaders())
                ->send()
                ->getContents()
                ->decode();

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }
        //remove this code when API is done
        return $data;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function getPendingSkusFiltered(): array
    {
        try {

            $data = $this->service->method('POST')
                ->setPath($this->buildEndpoint("/api/v1/itens/{$this->id_supplier_fk}"))
                ->setHeaders($this->getHeaders())
                ->setData($this->data)
                ->send()
                ->getContents()
                ->decode();

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }

        return $data;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function setData(array $data)
    {
        $this->data = $data;
        return $this;

    }
}
