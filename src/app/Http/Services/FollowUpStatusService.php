<?php
declare(strict_types=1);

namespace App\Http\Services;

use App\Domain\FollowUp\FollowUpStatusInterface;
use App\Domain\FollowUp\Service\FollowUpService;
use App\Http\Client\HttpServiceInterface;

/**
 * Class FollowUpStatusService
 * @package App\Http\Services
 */
class FollowUpStatusService  extends FollowUpService implements FollowUpStatusInterface
{

    /**
     * FollowUpStatusService constructor.
     * @param HttpServiceInterface $client
     */
    function __construct(HttpServiceInterface $client)
    {
        $this->service = $client;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function getFollowUpStatus(): array
    {

        try {

            $data = $this->service->method('GET')
                ->setPath($this->buildEndpoint('/api/v1/status'))
                ->setHeaders($this->getHeaders())
                ->send()
                ->getContents()
                ->decode();

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }

        return $data;
    }

}
