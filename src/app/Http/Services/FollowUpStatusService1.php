<?php

declare(strict_types=1);

namespace App\Http\Services;

use App\Domain\FollowUp\FollowUpStatusInterface;
use App\Http\Client\HttpServiceInterface;

class FollowUpStatusService1 implements FollowUpStatusInterface
{

    private $client;

    function __construct(HttpServiceInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @inheritDoc
     */
    public function getFollowUpStatus(): array
    {
        return [
            ['id' => 1, 'name' => 'Em falta no mercado'],
            ['id' => 2, 'name' => 'Compra Cancelada'],
            ['id' => 3, 'name' => 'Pedido Enviado'],
            ['id' => 4, 'name' => 'Aguardando Pagamento'],
        ];
    }
}
