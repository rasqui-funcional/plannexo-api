<?php
declare(strict_types=1);

namespace App\Http\Services;

use App\Domain\FollowUp\FollowUpStoreInterface;
use App\Domain\FollowUp\Service\FollowUpService;
use App\Http\Client\HttpServiceInterface;

/**
 * Class FollowUpStoreService
 * @package App\Http\Services
 */
class FollowUpStoreService extends FollowUpService implements FollowUpStoreInterface
{

    /**
     * @var array
     */
    private array $data = [];
    private int $userId;

    /**
     * FollowUpStoreService constructor.
     * @param HttpServiceInterface $client
     */
    function __construct(HttpServiceInterface $client)
    {
        $this->service = $client;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function saveFollowUp(): bool
    {
        try {

            foreach ($this->data as $data) {

                $data['createdAt'] = (new \DateTime())->format('Y-m-d H:i:s');
                $data['user'] = $this->userId;

                $this->service->method('POST')
                    ->setPath($this->buildEndpoint('/api/v1/analysis'))
                    ->setHeaders($this->getHeaders())
                    ->setData($data)
                    ->send()
                    ->getContents()
                    ->decode();
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }

        return true;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function setUserId(int $userId): self {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @param array $data
     * @return FollowUpStoreInterface
     */
    public function setData($data): FollowUpStoreInterface
    {
        $this->data = $data;

        return $this;
    }
}
