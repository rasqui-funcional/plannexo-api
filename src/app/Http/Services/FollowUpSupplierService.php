<?php
declare(strict_types=1);

namespace App\Http\Services;

use App\Domain\FollowUp\FollowUpSupplierInterface;
use App\Domain\FollowUp\Service\FollowUpService;
use App\Http\Client\HttpServiceInterface;
use Exception;

/**
 * Class FollowUpSummaryService
 * @package App\Http\Services
 */
class FollowUpSupplierService extends FollowUpService implements FollowUpSupplierInterface
{

    private $data;
    /**
     * FollowUpSummaryService constructor
     * @param HttpServiceInterface $client
     */
    function __construct(HttpServiceInterface $client)
    {
        $this->service = $client;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function getSummary(): array
    {
        try {
            $data = $this->service->method('GET')
                ->setPath($this->buildEndpoint('/api/v1/suppliers'))
                ->setHeaders($this->getHeaders())
                ->send()
                ->getContents()
                ->decode();

        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }

        return $data;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function getInfo(): array
    {
        try {
            $data = $this->service->method('GET')
                ->setPath($this->buildEndpoint("/api/v1/suppliers/{$this->id_supplier_fk}"))
                ->setHeaders($this->getHeaders())
                ->send()
                ->getContents()
                ->decode();

        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }

        return $data;
    }


    /**
     * @return array
     * @throws Exception
     */
    public function getQuerySummary(): array
    {
        try {
            $data = $this->service->method('POST')
                ->setPath($this->buildEndpoint('/api/v1/suppliers/filtered'))
                ->setHeaders($this->getHeaders())
                ->setData($this->data)
                ->send()
                ->getContents()
                ->decode();

        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }

        return $data;
    }

    public function setData(array $data)
    {
        $this->data = $data;
        return $this;

    }
}
