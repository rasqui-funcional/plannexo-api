<?php
declare(strict_types=1);

namespace App\Http\Services;

use App\Http\Client\HttpServiceInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;

class HttpService implements HttpServiceInterface
{

    protected $client;

    private string $path = "";

    private string $method = "";

    private array $headers = [];

    private array $data = [];

    private Response $contents;

    private string $response;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function setPath(string $path = ""): HttpService
    {
        $this->path = $path;
        return $this;
    }

    public function method(string $method = ""): HttpService
    {
        $this->method = $method;
        return $this;
    }

    public function setHeaders(array $headers = []): HttpService
    {
        $this->headers = $headers;
        return $this;
    }

    public function setData(array $data = []): HttpService
    {
        $this->data = $data;
        return $this;
    }

    public function send(): HttpService
    {
        $this->contents = $this->client->request("{$this->method}", "{$this->path}", $this->getOptions());
        return $this;
    }

    public function getStatusCode()
    {
        return $this->contents->getStatusCode();
    }

    public function getReason()
    {
        return $this->contents->getReasonPhrase();
    }

    public function getContents(): HttpService
    {
        $this->response = $this->contents->getBody()->getContents();
        return $this;
    }

    public function decode()
    {
        return json_decode($this->response, true);
    }

    private function getOptions(bool $debug = false): array
    {
        return [
            'debug' => $debug,
            'headers' => $this->headers,
            'body' => json_encode($this->data),
        ];
    }
}
