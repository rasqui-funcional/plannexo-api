<?php


namespace App\Http\Services;


use App\Domain\Auth\AuthToken;
use App\Domain\Auth\BioAssigner\BioAssignerAuthToken;
use App\Domain\User\Entity\UserProfile;
use App\Domain\User\UserProfileInterface;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class UserProfileService extends HttpService implements UserProfileInterface
{
    /**
     * @var UserProfile
     */
    private UserProfile $entity;

    /**
     * @var AuthToken
     */
    private AuthToken $bio_assigner;

    /**
     * @var string
     */
    private string $authorization;

    public function __construct(UserProfile $userProfile)
    {
        parent::__construct(new Client);
        $this->entity = $userProfile;
        $this->bio_assigner = new BioAssignerAuthToken();
    }

    /**
     * @inheritDoc
     */
    public function getUserProfiles()
    {
        // TODO: Implement getUserProfiles() method.
    }

    /**
     * @inheritDoc
     */
    public function getUserProfile(int $id)
    {
        // TODO: Implement getUserProfile() method.
    }

    /**
     * @inheritDoc
     */
    public function createUserProfile()
    {
        $url = sprintf('%s%s', env('BIOID_API_URI') , '/api/v1/personas');
        $this->createToken($url, 'POST');

        $postsfields = [
            'persona' => [
                'user_id' => (int) $this->entity->getIdUserFk(),
                'company_id' => (int) $this->entity->getIdCompanyFk(),
            ],
            'acl_profile_uid' => 'plannexo'
        ];

        Log::info(__CLASS__ , ['payload' => $postsfields]);

        return $this->method('POST')
            ->setHeaders($this->headers())
            ->setPath($url)
            ->setData($postsfields)
            ->send()
            ->getContents()
            ->decode();
    }

    /**
     * @inheritDoc
     */
    public function updateUserProfile(int $id)
    {
        // TODO: Implement updateUserProfile() method.
    }

    /**
     * @inheritDoc
     */
    public function deleteUserProfile(int $id)
    {
        // TODO: Implement deleteUserProfile() method.
    }

    /**
     * @inheritDoc
     */
    public function getUserProfileByUserId()
    {
        // TODO: Implement getUserProfileByUserId() method.
    }

    /**
     * @return UserProfile
     */
    public function getEntity(): UserProfile
    {
        return $this->entity;
    }

    private function headers()
    {
        return [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => "{$this->authorization}"
        ];
    }

    private function createToken($url, $method)
    {
        $this->bio_assigner->setMethod($method);
        $this->bio_assigner->setUrl($url);

        $this->authorization = $this->bio_assigner->generateToken()->token();
    }
}
