<?php


namespace App\Http\Services;


use App\Domain\Auth\AuthToken;
use App\Domain\Auth\BioAssigner\BioAssignerAuthToken;
use App\Domain\User\Entity\User;
use App\Domain\User\Entity\UserProfile;
use App\Domain\User\UserInterface;
use GuzzleHttp\Client;

class UserService extends HttpService implements UserInterface
{
    /**
     * @var User
     */
    private User $entity;

    /**
     * @var AuthToken
     */
    private AuthToken $bio_assigner;

    /**
     * @var string
     */
    private string $authorization;

    /**
     * @var int
     */
    private int $references_user;

    /**
     * @inheritDoc
     */
    public function __construct(User $user)
    {
        parent::__construct(new Client());

        $this->entity = $user;
        $this->bio_assigner = new BioAssignerAuthToken();
    }

    /**
     * @inheritDoc
     */
    public function getUsers()
    {
        // TODO: Implement getUsers() method.
    }

    /**
     * @inheritDoc
     */
    public function getUser(int $id)
    {
        // TODO: Implement getUser() method.
    }

    /**
     * @inheritDoc
     */
    public function createUser()
    {
        $url = sprintf('%s%s', env('BIOID_API_URI') , '/api/v1/users');
        $this->createToken($url, 'POST');

        $postsfields = [
            'user' => [
                'first_name' => $this->entity->getName(),
                'last_name' => $this->entity->getLogin(),
                'email' => $this->entity->getMailLogin(),
            ],
        ];

        $response = $this->method('POST')
            ->setHeaders($this->headers())
            ->setPath($url)
            ->setData($postsfields)
            ->send()
            ->getContents()
            ->decode();

        $this->references_user = (int) $response['id'];
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function updateUser(int $id)
    {
        // TODO: Implement updateUser() method.
    }

    /**
     * @inheritDoc
     */
    public function deleteUser(int $id)
    {
        // TODO: Implement deleteUser() method.
    }

    /**
     * @return User
     */
    public function getEntity(): User
    {
        return $this->entity;
    }

    public function createProfile()
    {
        $userProfile = new UserProfile([
            'id_company_fk' => $this->entity->getRefCompanyBioid(),
            'id_profile_fk' => $this->entity->getProfile()->getIdProfilePk(),
            'id_user_fk' => $this->references_user
        ]);

        $up_service = (new UserProfileService($userProfile))->createUserProfile();

        $this->entity->setBioidReference($up_service['id']);

        return $this;
    }

    private function headers()
    {
        return [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => "{$this->authorization}"
        ];
    }

    private function createToken($url, $method)
    {
        $this->bio_assigner->setMethod($method);
        $this->bio_assigner->setUrl($url);

        $this->authorization = $this->bio_assigner->generateToken()->token();
    }
}
