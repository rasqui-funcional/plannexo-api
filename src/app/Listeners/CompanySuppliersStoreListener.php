<?php

namespace App\Listeners;

use App\Events\CompanySuppliersStoreEvent;

class CompanySuppliersStoreListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param CompanySuppliersStoreEvent $event
     * @return void
     */
    public function handle(CompanySuppliersStoreEvent $event)
    {
        $event->createCompanySuppliers();
    }
}
