<?php


namespace App\Listeners;


use App\Events\UserProfileEvent;

class UserProfileCreate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  UserProfileEvent  $event
     * @return void
     */
    public function handle(UserProfileEvent $event)
    {
        $event->createUserProfile();
    }
}
