<?php


namespace App\Listeners;


use App\Events\UserProfileEvent;

class UserProfileDelete
{
    /**
     * Handle the event.
     *
     * @param  UserProfileEvent  $event
     * @return void
     */
    public function handle(UserProfileEvent $event)
    {
        $event->deleteUserProfile();
    }
}
