<?php
declare(strict_types=1);

namespace App\Model\Company;

use App\Domain\Company\CompanyGroupsInterface;
use App\Domain\Company\Entity\Company;
use Illuminate\Support\Facades\DB;

/**
 * Class CompanyGroupsModel
 * @package App\Model\Company
 */
class CompanyGroupsModel implements CompanyGroupsInterface
{

    /**
     * @inheritDoc
     */

    private Company $company;

    /**
     * CompanyGroupsModel constructor.
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
    }


    /**
     * @return array
     * @throws \Exception
     */
    public function getCompanyGroups(): array
    {
        try {
            $data = DB::table('scm_group_name')
                ->where("scm_group_name.id_company_fk", '=', $this->company->getIdCompanyPk())
                ->where("scm_group_name.sit_group", '=', '1')
                ->orderBy("scm_group_name.num_group_pk")
                ->get([
                    "num_group_pk as id",
                    "desc_group as title",
                    DB::raw("CONCAT('cod_group', CONCAT(num_group_pk, '_fk')) AS key")
                ])
                ->toArray();

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }

        return $data;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function getCompanyGroup(string $id): array
    {
        try {
            $data = DB::table('scm_group_name')
                ->where("scm_group_name.id_company_fk", '=', $this->company->getIdCompanyPk())
                ->where("scm_group_name.sit_group", '=', '1')
                ->where("scm_group_name.num_group_pk", '=', $id)
                ->orderBy("scm_group_name.num_group_pk")
                ->get([
                    "num_group_pk as id",
                    "desc_group as title",
                    DB::raw("CONCAT('cod_group', CONCAT(num_group_pk, '_fk')) AS key")
                ])
                ->toArray();

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }

        return $data;
    }

    /**
     * @param string $group_id
     * @return array
     * @throws \Exception
     */
    public function getCompanyGroupData(string $group_id): array
    {
        try {

            $data = DB::table('scm_group_cont')
                ->where("scm_group_cont.id_company_fk", '=', $this->company->getIdCompanyPk())
                ->where("scm_group_cont.num_group_fk", '=', $group_id)
                ->orderBy("scm_group_cont.desc_group_cont")
                ->get([
                    DB::raw("DISTINCT cod_group_pk as id"),
                    "desc_group_cont as name"
                ])
                ->toArray();

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }

        return $data;
    }
}
