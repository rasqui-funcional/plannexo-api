<?php
declare(strict_types=1);

namespace App\Model\Company;

use App\Domain\Company\CompanyPoliciesInterface;
use App\Domain\Company\Entity\Company;
use Illuminate\Support\Facades\DB;

/**
 * Class CompanyPoliciesModel
 * @package App\Model\Company
 */
class CompanyPoliciesModel implements CompanyPoliciesInterface
{
    /**
     * @inheritDoc
     */

    private Company $company;

    /**
     * CompanyGroupsModel constructor.
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getCompanyPolicies(): array
    {
        try {
            $data = DB::table('scm_profile')
                ->where("scm_profile.id_company_fk", '=', $this->company->getIdCompanyPk())
                ->orderBy("scm_profile.cod_profile", 'ASC')
                ->get(["id_profile_pk as id", DB::raw("id_profile_pk || '-' || cod_profile AS name")])
                ->toArray();

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }

        return $data;
    }
}
