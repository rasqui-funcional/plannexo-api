<?php

namespace App\Model;

use App\Domain\Company\CompanyInterface;
use App\Domain\Company\Entity\Company;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CompanyModel extends Model implements CompanyInterface
{
    protected $table = 'adm_company';

    private Company $entity;

    public function __construct(Company $company)
    {
        $this->entity = $company;
    }

    /**
     * @inheritDoc
     */
    public function getCompanies()
    {
        return DB::table($this->table)
            ->orderBy('id_company_pk', 'ASC')
            ->get($this->getColumns())
            ->toArray();
    }

    /**
     * @inheritDoc
     */
    public function getCompany(int $id)
    {
        return DB::table($this->table)
            ->select($this->getColumns())
            ->where('id_company_pk', '=', $id)
            ->first();
    }

    public function getCompanyByBioId(int $bio_id = 0)
    {
        return DB::table($this->table)
            ->select($this->getColumns())
            ->where('bioid_reference', '=', $this->entity->getBioidReference())
            ->first();
    }

    /**
     * @inheritDoc
     */
    public function createCompany()
    {
        // TODO: Implement createCompany() method.
    }

    /**
     * @inheritDoc
     */
    public function updateCompany(int $id)
    {
        // TODO: Implement updateCompany() method.
    }

    /**
     * @inheritDoc
     */
    public function deleteCompany(int $id)
    {
        // TODO: Implement deleteCompany() method.
    }

    public function setReferenceBioId($row)
    {
        $data['bioid_reference'] = $row->reference;

        return DB::table($this->table)
            ->where('id_company_pk', $row->id)
            ->update($data);
    }

    private function getColumns()
    {
        return [
            'id_company_pk as id',
            'company_name as name',
            'company_reg as register',
            'bioid_reference as reference'
        ];
    }
}
