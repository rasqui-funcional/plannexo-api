<?php

namespace App\Model\Email;

use App\Domain\Email\EmailModelInterface;
use App\Domain\Email\Entity\EmailModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EmailModelModel extends Model implements EmailModelInterface
{
    protected $table = 'EMAIL_MODEL';

    private EmailModel $entity;

    public function __construct(EmailModel $email_model)
    {
        $this->entity = $email_model;
    }

    /**
     * @inheritDoc
     */
    public function getEmailModels()
    {
        return DB::table($this->table)
            ->where("{$this->table}.ID_COMPANY_FK", '=', $this->entity->getIdCompanyFk())
            ->orderBy('id', 'DESC')
            ->get($this->getColumnsList())
            ->toArray();
    }

    /**
     * @inheritDoc
     */
    public function getEmailModelsWithBody()
    {
        return DB::table($this->table)
            ->where("{$this->table}.ID_COMPANY_FK", '=', $this->entity->getIdCompanyFk())
            ->orderBy('id', 'DESC')
            ->get($this->getColumns())
            ->toArray();
    }

    /**
     * @param int $eid
     * @return mixed
     */
    public function getEmailModel(int $eid)
    {
        return DB::table($this->table)
            ->where("{$this->table}.ID_COMPANY_FK", '=', $this->entity->getIdCompanyFk())
            ->where("{$this->table}.ID", '=', $eid)
            ->orderBy('id', 'DESC')
            ->get($this->getColumns())
            ->toArray();
    }

    /**
     * @return mixed
     */
    public function createEmailModel() 
    {
        return DB::table($this->table)
            ->insert($this->entity->exchangeArray());
    }

    /**
     * @param int $eid
     * @return mixed
     */
    public function updateEmailModel(int $eid)
    {
        return DB::table($this->table)
            ->where('id', $eid)
            ->update($this->entity->exchangeArray());
    }

    /**
     * @param int $eid
     * @return mixed
     */
    public function deleteEmailModel(int $eid)
    {
        return DB::table($this->table)
            ->where('id', $eid)
            ->delete();
    }

    private function getColumnsList()
    {
        $columns = $this->getColumns();
        $del_val = 'DESCRIPTION as body';
        return array_filter($columns, function($e) use ($del_val) {
            return ($e !== $del_val);
        });
    }

    private function getColumns()
    {
        return [
            'ID as id',
            'NAME as title',
            'SUBJECT',
            'DESCRIPTION as body',
            'CREATED_AT',
            'UPDATE_AT',
            DB::raw('CONCAT(CONCAT(\'[\',(
                SELECT
                    LISTAGG(\'{"module_name":"\' || EMAIL_PRODUCT_MODULE.MODULE || \'","label":"\' || EMAIL_PRODUCT_MODULE.LABEL || \'", "type":"\' || EMAIL_TYPE.LABEL || \'"}\', \', \') WITHIN GROUP (
                    ORDER BY LIG_EMAIL_MODEL_PRODUCT.ID DESC) "used_in"
                FROM
                    LIG_EMAIL_MODEL_PRODUCT 
                LEFT JOIN EMAIL_PRODUCT_MODULE ON
                    (LIG_EMAIL_MODEL_PRODUCT.ID_MODULE_FK = EMAIL_PRODUCT_MODULE.ID )
                LEFT JOIN EMAIL_REGISTERED  ON 
                    (LIG_EMAIL_MODEL_PRODUCT.ID_MODEL_FK = EMAIL_REGISTERED.ID_MODEL_FK
                    AND LIG_EMAIL_MODEL_PRODUCT.ID_MODULE_FK = EMAIL_REGISTERED.ID_MODULE_FK)
                LEFT JOIN EMAIL_TYPE ON
                    (EMAIL_REGISTERED.ID_TYPE_FK = EMAIL_TYPE.ID)
                WHERE 
                    LIG_EMAIL_MODEL_PRODUCT.ID_MODEL_FK = EMAIL_MODEL.ID )), \']\') "USED_IN"
            ')
        ];
    }
}    