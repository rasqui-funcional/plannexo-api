<?php

namespace App\Model\Email;

use App\Domain\Email\EmailProductInterface;
use App\Domain\Email\Entity\EmailProduct;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EmailProductModel extends Model implements EmailProductInterface
{
    protected $table = 'EMAIL_PRODUCT_MODULE';

    private EmailProduct $entity;

    public function __construct(EmailProduct $email_product)
    {
        $this->entity = $email_product;
    }

    /**
     * @param string $product
     * @return mixed
     */
    public function getEmailProducts(string $product = 'plannexo')
    {
        return DB::table($this->table)
            ->where("{$this->table}.PRODUCT", '=', $product)
            ->orderBy('id', 'DESC')
            ->get($this->getColumns())
            ->toArray();
    }

    /**
     * @param string $module
     * @param string $product
     * @return mixed
     */
    public function getEmailProduct(string $product = 'plannexo')
    {
        return DB::table($this->table)
            ->where('MODULE', $this->entity->getModule())
            ->get($this->getColumns())
            ->first();
    }

    private function getColumns()
    {
        return [
            "ID",
            "MODULE",
            "LABEL",
        ];
    }
}    