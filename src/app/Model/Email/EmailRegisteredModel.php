<?php

namespace App\Model\Email;

use App\Domain\Email\EmailRegisteredInterface;
use App\Domain\Email\Entity\EmailRegistered;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EmailRegisteredModel extends Model implements EmailRegisteredInterface
{
    protected $table = 'EMAIL_REGISTERED';
    protected $table_type = 'EMAIL_TYPE';
    protected $table_module = 'EMAIL_PRODUCT_MODULE';
    protected $table_model = 'EMAIL_MODEL';

    private EmailRegistered $entity;

    public function __construct(EmailRegistered $email_registered)
    {
        $this->entity = $email_registered;
    }

    /**
     * @param int $id_module
     * @return mixed
     */
    public function getEmailRegistered()
    {
        return DB::table($this->table)
            ->leftJoin("{$this->table_type}", "{$this->table}.ID_TYPE_FK", "=", "{$this->table_type}.ID")
            ->leftJoin("{$this->table_module}", "{$this->table}.ID_MODULE_FK", "=", "{$this->table_module}.ID")
            ->where("{$this->table_module}.ID_MODULE_FK", '=', $this->entity->getIdModule())
            ->orderBy('id', 'DESC')
            ->get($this->getColumns())
            ->toArray();
    }

    /**
     * @return mixed
     */
    public function createEmailRegistered() 
    {
        return DB::table($this->table)
            ->insert($this->entity->exchangeArray());
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function updateEmailRegistered(int $id)
    {
        return DB::table($this->table)
            ->where('ID', $id)
            ->update($this->entity->exchangeArray());
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function deleteEmailRegistered(int $id)
    {
        return DB::table($this->table)
            ->where('id', $id)
            ->delete();
    }

    /**
     * @param int $id_type
     * @param int $id_model
     * @return mixed
     */
    public function deleteEmailRegisteredByIdModuleAndIdType(int $id_type, int $id_model)
    {
        return DB::table($this->table)
            ->where('ID_MODEL_FK', $id_model)
            ->where('ID_TYPE_FK', $id_type)
            ->delete();
    }

    private function getColumns()
    {
        return [
            "{$this->table}.ID AS ID", 
            "{$this->table}.ID_MODEL_FK AS ID_MODEL_FK",
            "{$this->table}.ID_MODULE_FK AS ID_MODULE_FK",
            "{$this->table_type}.LABEL AS LABEL",
            "{$this->table_type}.TYPE AS TYPE",
            "{$this->table_module}.LABEL AS LABEL_MODULE"
        ];
    }

    /**
     * @param int $id_company
     * @return mixed
     */
    public function insertOrUpdate(int $id_company)
    {
        $registered = DB::table($this->table_model)
            ->leftJoin("{$this->table}", "{$this->table_model}.ID", "=", "{$this->table}.ID_MODEL_FK")
            ->where("ID_COMPANY_FK", $id_company)
            ->where("ID_TYPE_FK", $this->entity->getIdType())
            ->get("{$this->table_model}.ID")
            ->first();

        if(!isset($registered->id) || is_null($registered->id)) {
            return $this->createEmailRegistered();
        }

        return $this->updateEmailRegistered($registered->id);
    }
}    