<?php

namespace App\Model\Email;

use App\Domain\Email\EmailTypeInterface;
use App\Domain\Email\Entity\EmailType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EmailTypeModel extends Model implements EmailTypeInterface
{
    protected $table = 'EMAIL_TYPE';

    private EmailType $entity;

    public function __construct(EmailType $email_type)
    {
        $this->entity = $email_type;
    }

    /**
     * @param string $type
     * @return mixed
     */
    public function getEmailTypes(string $type = 'plannexo')
    {
        return DB::table($this->table)
            ->orderBy('id', 'DESC')
            ->get($this->getColumns())
            ->toArray();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getEmailType(int $id)
    {
        return DB::table($this->table)
            ->where('ID', $id)
            ->get($this->getColumns())
            ->first();
    }


    /**
     * @return mixed
     */
    public function getEmailTypeByModuleId()
    {
        return DB::table($this->table)
            ->where('ID_MODULE_FK', $this->entity->getIdModule())
            ->get($this->getColumns())
            ->toArray();
    }

    private function getColumns()
    {
        return [
            "ID",
            "ID_MODULE_FK",
            "TYPE",
            "LABEL",
        ];
    }
}    