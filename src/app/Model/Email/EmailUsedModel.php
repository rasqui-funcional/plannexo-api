<?php

namespace App\Model\Email;

use App\Domain\Email\EmailUsedInterface;
use App\Domain\Email\Entity\EmailUsed;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EmailUsedModel extends Model implements EmailUsedInterface
{
    protected $table = 'LIG_EMAIL_MODEL_PRODUCT';
    protected $table_module = 'EMAIL_PRODUCT_MODULE';
    protected $table_model = 'EMAIL_MODEL';

    private EmailUsed $entity;

    public function __construct(EmailUsed $email_model)
    {
        $this->entity = $email_model;
    }

    /**
     * @param int $id_module
     * @return mixed
     */
    public function getEmailUsed()
    {
        return DB::table($this->table)
            ->leftJoin("{$this->table_module}", "{$this->table}.ID_MODULE_FK", "=", "{$this->table_module}.ID")
            ->where("{$this->table_module}.ID_MODULE_FK", '=', $this->entity->getIdModule())
            ->orderBy('id', 'DESC')
            ->get($this->getColumns())
            ->toArray();
    }

    /**
     * @return mixed
     */
    public function createEmailUsed() 
    {
        return DB::table($this->table)
            ->insert($this->entity->exchangeArray());
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function updateEmailUsed(int $id)
    {
        return DB::table($this->table)
            ->where('id', $id)
            ->update($this->entity->exchangeArray());
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function deleteEmailUsed(int $id)
    {
        return DB::table($this->table)
            ->where('id', $id)
            ->delete();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function deleteEmailUsedByModelAndModule(int $id_model, int $id_module)
    {
        return DB::table($this->table)
            ->where('ID_MODEL_FK', $id_model)
            ->where('ID_MODULE_FK', $id_module)
            ->delete();
    }

    private function getColumns()
    {
        return [
            "ID",
            "ID_MODEL_FK",
            "ID_MODULE_FK",
            "LABEL",
            "PRODUCT",
            "{$this->table}.CREATED_AT AS CREATED_AT",
            "{$this->table}.UPDATE_AT AS UPDATE_AT"
        ];
    }

    /**
     * @return mixed
     */
    public function noExistsInsert()
    {
        $used = DB::table($this->table)
            ->where("ID_MODULE_FK", '=', $this->entity->getIdModule())
            ->get("ID")
            ->first();

        if (!isset($used->id) || is_null($used->id)) {
            return $this->createEmailUsed();
        }

        return true;
    }
}