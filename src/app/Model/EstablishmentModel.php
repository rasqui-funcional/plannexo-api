<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EstablishmentModel extends Model
{
    protected $table = 'scm_estab';

    /**
     * @inheritDoc
     */
    public function getEstablishmentsByCompanyId(int $id_company)
    {
        return DB::table($this->table)
            ->orderBy('cod_estab_pk', 'DESC')
            ->where('id_company_fk', '=', $id_company)
            ->get($this->getColunms())
            ->toArray();
    }

    /**
     * @inheritDoc
     */
    public function getEstablishmentByCompanyIdAndCode($id_company, $code)
    {
        return DB::table($this->table)
            ->select($this->getColunms())
            ->where('id_company_fk', '=', $id_company)
            ->where('cod_estab_pk', '=', $code)
            ->first();
    }

    private function getColunms()
    {
        return [
            'cod_estab_pk as id',
            'desc_estab_short as short_name',
            'desc_estab as name'
        ];
    }
}
