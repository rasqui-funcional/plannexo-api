<?php
declare(strict_types=1);

namespace App\Model\FollowUp;

use App\Domain\Filters\FilterDeleteInterface;
use App\Domain\User\Entity\User;
use Illuminate\Support\Facades\DB;

/**
 * Class FollowUpFilterDelete
 * @package App\Model\FollowUp
 */
class FollowUpUserFilterDeleteModel implements FilterDeleteInterface
{

    /**
     * @var
     */
    private $data;
    /**
     * @var string
     */
    protected $table = 'uac_filters';

    /**
     * @var User
     */
    private User $user;

    /**
     * FollowUpUserFilterDeleteModel constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param int $id
     * @return array
     * @throws \Exception
     */
    public function deleteFilter(int $id): bool
    {

        try {
            DB::table($this->table)
                ->where('filter_id', '=', $id)
                ->where('id_user_fk', '=', $this->user->getIdUserPk())
                ->where('id_company_fk', '=', $this->user->getIdCompanyFk())
                ->delete();

        } catch (\Exception $e) {

            throw  new \Exception($e->getMessage(), $e->getCode());
        }

        return true;

    }

    /**
     * @param array $data
     * @return $this
     */
    public function setData(array $data): self
    {
        $this->data = $data;
        return $this;
    }
}
