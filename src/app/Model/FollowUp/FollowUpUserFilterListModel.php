<?php
declare(strict_types=1);

namespace App\Model\FollowUp;


use App\Domain\Filters\FilterListInterface;
use App\Domain\User\Entity\User;
use Illuminate\Support\Facades\DB;

/**
 * Class FollowUpFilterList
 * @package App\Model\FollowUp
 */
class FollowUpUserFilterListModel implements FilterListInterface
{

    /**
     * @var string
     */
    protected $table = 'uac_filters';

    /**
     * @var User
     */
    private User $user;

    /**
     * FollowUpFilterListModel constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function getFilters(): array
    {

        try {
            return DB::table($this->table)
                ->where("{$this->table}.id_user_fk", '=', $this->user->getIdUserPk())
                ->where("{$this->table}.id_company_fk", '=', $this->user->getIdCompanyFk())
                ->where("{$this->table}.filter_tela_id", '=', 'FUP')
                ->orderBy("{$this->table}.filter_nome")
                ->get($this->getColumns())
                ->toArray();
        } catch (\Exception $e) {

            throw  new \Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function getFilter(int $id): array
    {

        try {
            return DB::table($this->table)
                ->where("{$this->table}.filter_id", '=', $id)
                ->where("{$this->table}.id_user_fk", '=', $this->user->getIdUserPk())
                ->where("{$this->table}.id_company_fk", '=', $this->user->getIdCompanyFk())
                ->where("{$this->table}.filter_tela_id", '=', 'FUP')
                ->orderBy("{$this->table}.filter_nome")
                ->get($this->getColumns())
                ->toArray();

        } catch (\Exception $e) {
            throw  new \Exception($e->getMessage(), $e->getCode());

        }
    }

    private function getColumns()
    {
        return ['filter_id', 'filter_nome', 'filter_data'];
    }
}
