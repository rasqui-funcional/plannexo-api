<?php


namespace App\Model\FollowUp;


use App\Domain\Filters\FilterStoreInterface;
use App\Domain\User\Entity\User;
use Illuminate\Support\Facades\DB;

/**
 * Class FollowUpFilterStore
 * @package App\Model\FollowUp
 */
class FollowUpUserFilterStoreModel implements FilterStoreInterface
{


    /**
     * @var
     */
    private $data;
    /**
     * @var string
     */
    protected $table = 'uac_filters';

    /**
     * @var User
     */
    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @inheritDoc
     * @throws \Exception
     */
    public function saveFilter(): array
    {

        try {
            $data = $this->exchangeArray();
            DB::table($this->table)
                ->insert($data);
        } catch (\Exception $e) {

            throw  new \Exception($e->getMessage(), $e->getCode());
        }

        return $data;

    }

    /**
     * @param array $data
     * @return FilterStoreInterface
     */
    public function setData(array $data): self
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return array
     */
    public function exchangeArray()
    {
        return array_merge(
            $this->data,
            [
                'filter_id' => $this->nextUserId(),
                'id_company_fk' => $this->user->getIdCompanyFk(),
                'id_user_fk' => $this->user->getIdUserPk(),
                'filter_perfil' => 'Publico',
                'filter_tela_id' => 'FUP',
            ]
        );
    }

    private function nextUserId()
    {
        $nextId = DB::table($this->table)
            ->selectRaw('MAX(FILTER_ID) AS maxid')
            ->first();

        return (int)$nextId->maxid + 1;
    }

}
