<?php
declare(strict_types=1);

namespace App\Model\FollowUp;

use App\Domain\Filters\FilterUpdateInterface;
use App\Domain\User\Entity\User;
use Illuminate\Support\Facades\DB;

/**
 * Class FollowUpFilterUpdate
 * @package App\Model\FollowUp
 */
class FollowUpUserFilterUpdateModel implements FilterUpdateInterface
{


    /**
     * @var
     */
    private $data;
    /**
     * @var string
     */
    protected $table = 'uac_filters';

    /**
     * @var User
     */
    private User $user;

    /**
     * FollowUpUserFilterUpdateModel constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }


    /**
     * @param int $id
     * @return array
     * @throws \Exception
     */
    public function updateFilter(int $id): array
    {

        try {
            $data = $this->exchangeArray();
            DB::table($this->table)
                ->where('filter_id', '=', $id)
                ->where('id_user_fk', '=', $this->user->getIdUserPk())
                ->where('id_company_fk', '=', $this->user->getIdCompanyFk())
                ->update($data);
        } catch (\Exception $e) {

            throw  new \Exception($e->getMessage(), $e->getCode());
        }

        return $data;

    }

    /**
     * @param array $data
     * @return $this
     */
    public function setData(array $data): self
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return array
     */
    public function exchangeArray()
    {
        return array_merge(
            $this->data,
            [
                'id_company_fk' => $this->user->getIdCompanyFk(),
                'id_user_fk' => $this->user->getIdUserPk(),
                'filter_perfil' => 'Publico',
                'filter_tela_id' => 'FUP',
            ]
        );
    }
}
