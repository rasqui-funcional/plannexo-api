<?php

namespace App\Model;

use App\Domain\User\Entity\Profile;
use App\Domain\User\ProfileInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProfileModel extends Model implements ProfileInterface
{
    protected $table = 'uac_profile';

    private Profile $entity;

    public function __construct(Profile $profile)
    {
        $this->entity = $profile;
    }

    public function getProfiles()
    {
        return DB::table($this->table)
            ->where('id_company_fk', '=', $this->entity->getIdCompanyFk())
            ->orderBy('id_profile_pk', 'DESC')
            ->get($this->getColumns())
            ->toArray();
    }

    public function getProfile(int $id)
    {
        return DB::table($this->table)
            ->select($this->getColumns())
            ->where('id_company_fk', '=', $this->entity->getIdCompanyFk())
            ->where('id_profile_pk', '=', $id)
            ->first();
    }

    public function createProfile()
    {
        // TODO: Implement createProfile() method.
    }

    public function updateProfile(int $id)
    {
        // TODO: Implement updateProfile() method.
    }

    public function deleteProfile(int $id)
    {
        // TODO: Implement deleteProfile() method.
    }

    private function getColumns()
    {
        return [
            'id_profile_pk AS id',
            'desc_group AS name',
            DB::raw(
                '(SELECT COUNT(ID_USER_FK) FROM UAC_PROFILE_USER
                WHERE ID_PROFILE_FK = ID_PROFILE_PK) AS "amountUsers"'
            )
        ];
    }
}
