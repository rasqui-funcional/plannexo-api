<?php
declare(strict_types=1);

namespace App\Model;

use App\Domain\Tag\TagInterface;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Tag
 * @package App\Model
 */
class Tag extends Model implements TagInterface
{

    /**
     * @var int
     */
    public static int $company_id_fk = 0;


    /**
     * @inheritDoc
     * @throws Exception
     */
    public static function hasTag(string $tag = ''): bool
    {
        try {
            return DB::table('uac_tag')
                ->where('tag', '=', $tag)
                ->where('id_company_fk', '=', self::$company_id_fk)
                ->exists();

        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param string $tag
     * @throws Exception
     */
    public static function applyTag(string $tag)
    {
        try {

            DB::table('uac_tag')->insert([
                'id_company_fk' => self::$company_id_fk,
                'tag' => $tag
            ]);

        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param string $tag
     * @throws Exception
     */
    public static function removeTag(string $tag)
    {
        try {

            DB::table('uac_tag')
                ->where('tag', $tag)
                ->where('id_company_fk', self::$company_id_fk)
                ->delete();

        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }
}
