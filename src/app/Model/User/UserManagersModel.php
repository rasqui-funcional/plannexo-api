<?php
declare(strict_types=1);

namespace App\Model\User;


use App\Domain\User\Entity\User;
use App\Domain\User\Traits\UserManagersTrait;
use App\Domain\User\UserManagersInterface;
use Illuminate\Support\Facades\DB;

/**
 * Class UserManagers
 * @package App\Model\User
 */
class UserManagersModel implements UserManagersInterface
{

    /**
     * @var User
     */
    private $user;

    /**
     * UserManagersModel constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getUserManagers(): array
    {

        try {
            //TODO When all user have been relationed in table UAC_USER_ESTAB need apply to return only user on same estbs of him
            //Estabs relation was removed in PNR https://bionexo.atlassian.net/browse/PNR-1114 of PlannexoWeb
            // PR https://bitbucket.org/bionexo/plannexoweb/pull-requests/1058/bugfix-filtro-gestor/diff

            $data = DB::table('uac_user')
                ->where("uac_user.id_company_fk", '=', $this->user->getIdCompanyFk())
                ->where('active', '=', '1')
                ->orderBy("uac_user.login", 'ASC')
                ->get(['id_user_pk as id', 'login as name'])
                ->toArray();

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }

        return $data;
    }
}
