<?php
declare(strict_types=1);

namespace App\Model\User;


use App\Domain\User\Entity\User;
use App\Domain\User\Traits\UserStandardDepositTrait;
use App\Domain\User\UserStandardDepositInterface;
use Illuminate\Support\Facades\DB;

/**
 * Class UserStandardDeposit
 * @package App\Model\User
 */
class UserStandardDepositModel implements UserStandardDepositInterface
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var
     */
    private $estabs;

    /**
     * UserManagersModel constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return array
     * @throws \Exception
     */

    public function getUserStandardDeposit(): array
    {

        try {
            $data = DB::table('erp_local')
                ->where("erp_local.id_company_fk", '=', $this->user->getIdCompanyFk())
                ->whereIn('cod_estab_pk', $this->getEstabs())
                ->orderBy("erp_local.desc_local", 'ASC')
                ->get(['cod_local_pk as id', DB::raw("cod_local_pk || ' - ' || desc_local as name")])
                ->toArray();

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }

        return $data;
    }

    /**
     * @return mixed
     */
    private function getEstabs()
    {
        return explode(', ', str_replace("'", '', $this->estabs));
    }

    /**
     * @param mixed $estabs
     */
    public function setEstabs($estabs): self
    {
        $this->estabs = $estabs;

        return $this;
    }
}
