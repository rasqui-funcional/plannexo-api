<?php

namespace App\Model;

use App\Domain\User\Entity\User;
use App\Domain\User\Entity\UserEstablishments;
use App\Helpers\Mask;
use App\Domain\User\UserEstablishmentsInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

/**
 * Class UserEstablishmentsModel
 * @package App\Model
 */
class UserEstablishmentsModel extends Model implements UserEstablishmentsInterface
{
    /**
     * @var UserEstablishments
     */
    private UserEstablishments $entity;

    /**
     * @var string
     */
    private $table_user_estabs = 'UAC_USER_ESTAB';

    private $table_estabs = 'SCM_ESTAB';

    /**
     * @param array
     */
    private $user_estabs = [];

    public function __construct(UserEstablishments $UserEstablishments = null)
    {
        $this->entity = $UserEstablishments ?? new UserEstablishments();
    }

    /**
     * @return UserEstablishments
     */
    public function getEntity(): UserEstablishments
    {
        return $this->entity;
    }

    /**
     * @throws \Exception
     */
    private function selectEstablishmentsByUser()
    {
        try {

            $sel_estabs = DB::table($this->table_user_estabs)
                ->join($this->table_estabs, function($join)
                {
                    $join->on("{$this->table_estabs}.ID_COMPANY_FK", '=', "{$this->table_user_estabs}.ID_COMPANY_PK");
                    $join->on("{$this->table_estabs}.COD_ESTAB_PK", '=', "{$this->table_user_estabs}.COD_ESTAB_PK");
                })
                ->where("{$this->table_user_estabs}.ID_COMPANY_PK", '=', $this->entity->getIdCompanyFk())
                ->where("{$this->table_user_estabs}.ID_USER_PK", '=', $this->entity->getIdUserFk())
                ->where("{$this->table_user_estabs}.COD_ESTAB_PK", 'NOT LIKE', 'INTERDEPENDENCIA-%')
                ->get(["{$this->table_user_estabs}.COD_ESTAB_PK AS id", DB::raw("{$this->table_user_estabs}.cod_estab_pk || '-' || {$this->table_estabs}.desc_estab_short AS name")])
                ->toArray();

            if (empty($sel_estabs)) {
                //GET ALL ESTABS BY COMPANY
                $sel_estabs = DB::table($this->table_estabs)
                    ->where("ID_COMPANY_FK", '=', $this->entity->getIdCompanyFk())
                    ->where("COD_ESTAB_PK", 'NOT LIKE', 'INTERDEPENDENCIA-%')
                    ->get(['COD_ESTAB_PK AS id', DB::raw("cod_estab_pk || '-' || desc_estab_short AS name")])
                    ->toArray();
            }

            if ($sel_estabs) {
                foreach ($sel_estabs as $estab) {
                    array_push($this->user_estabs, $estab);
                }
            }

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }

        return $this;
    }

    private function selectEstablishmentsByUserIdAndCompanyId(int $user_id, int $company_id)
    {
        $sel_estabs = DB::table($this->table_user_estabs)
            ->where("{$this->table_user_estabs}.ID_COMPANY_PK", '=', $company_id)
            ->where("{$this->table_user_estabs}.ID_USER_PK", '=', $user_id)
            ->get(['COD_ESTAB_PK AS id'])
            ->toArray();

        if (!empty($sel_estabs)) {
            foreach ($sel_estabs as $estab) {
                array_push($this->user_estabs, $estab->id);
            }
        }

        return $this;
    }

    /*
    * @return array
    */
    public function getEstablishments()
    {
        if (empty($this->user_estabs)) {
            $this->selectEstablishmentsByUser();
        }

        return $this->user_estabs;
    }

    /*
    * @return string
    */
    public function getEstablishmentsCodes()
    {
        if (empty($this->user_estabs)) {
            $this->selectEstablishmentsByUser();
        }

        if (empty($this->user_estabs)) {
            return '';
        }

        return "'" . implode("', '", array_map(function ($estab) {
                return $estab->id;
            }, $this->user_estabs)) . "'";
    }

    /*
    * @return array
    */
    public function getEstablishmentsByUserIdAndCompanyId($user_id, $company_id)
    {
        $this->selectEstablishmentsByUserIdAndCompanyId($user_id, $company_id);
        return $this->user_estabs;
    }

    public function createUserEstablishments()
    {
        if (empty($this->entity->getCodeEstablishments())) {
            return false;
        }

        DB::table($this->table_user_estabs)
            ->insert($this->entity->exchangeArray());
    }

    public function deleteUserEstablishments()
    {
        DB::table($this->table_user_estabs)
            ->where('ID_USER_PK', '=', $this->entity->getIdUserFk())
            ->where('ID_COMPANY_PK', '=', $this->entity->getIdCompanyFk())
            ->delete();
    }
}
