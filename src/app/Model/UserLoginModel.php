<?php
declare(strict_types=1);

namespace App\Model;

use App\Domain\Auth\Authenticate;
use App\Domain\User\Entity\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use DateTime;
use Illuminate\Support\Facades\Log;

/**
 * Class UserLoginModel
 * @package App\Model
 */
class UserLoginModel extends Model implements Authenticate
{
    /**
     * @var string
     */
    protected $table = 'uac_user';

    protected $table_company = 'adm_company';

    /**
     * @var User|array
     */
    private $entity;

    /**
     * @inheritDoc
     */
    public function __construct(User $user)
    {
        $this->entity = $user;

        parent::__construct();
    }

    /**
     * @return bool
     */
    public function authenticate(): bool
    {
        return DB::table($this->table)
            ->where('mail_login', '=', $this->entity->getMailLogin())
            ->where('password', '=', $this->entity->getPassword())
            //->where('id_company_fk', '=', $this->entity->getIdCompanyFk())
            ->where('active', '=', '1')
            ->exists();
    }

    /**
     * @throws \Exception
     */
    public function updateUserLasAccess()
    {
        DB::table($this->table)
            ->where('id_user_pk', $this->entity->getIdUserPk())
            ->where('id_company_fk', $this->entity->getIdCompanyFk())
            ->increment('num_access');

        DB::table($this->table)
            ->where('id_user_pk', $this->entity->getIdUserPk())
            ->where('id_company_fk', $this->entity->getIdCompanyFk())
            ->update(['date_last_access' => (new DateTime())->format('Y-m-d H:i:s')]);
    }


    /**
     * @return User|Model|\Illuminate\Database\Query\Builder|object|null
     * @throws \Exception
     */
    public function getUserByEmail()
    {
        try {
            $data = DB::table($this->table)
                ->select([
                    "{$this->table}.name",
                    "{$this->table}.mail_login AS email",
                    "{$this->table}.id_company_fk",
                    "{$this->table}.id_user_pk",
                    "{$this->table_company}.company_name",
                    "{$this->table_company}.id_company_pk",
                ])
                ->join($this->table_company, "{$this->table_company}.id_company_pk", "=", "{$this->table}.id_company_fk")
                ->where("{$this->table}.mail_login", '=', $this->entity->getMailLogin())
                ->where("{$this->table}.active", '=', '1')
                ->first();

            $data = new User((array)$data);

        } catch (\Exception $e) {

            throw new \Exception($e->getMessage(), $e->getCode());
        }

        return $data;
    }


}
