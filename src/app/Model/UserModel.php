<?php
declare(strict_types = 1);

namespace App\Model;

use App\Domain\User\Entity\User;
use App\Domain\User\Entity\UserProfile;
use App\Domain\User\UserInterface;
use App\Events\UserProfileEvent;
use App\Events\UserEstablishmentsEvent;
use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;

class UserModel extends Model implements UserInterface
{
    /**
     * @var string
     */
    protected $table = 'uac_user';

    private User $entity;

    public function __construct(User $user)
    {
        $this->entity = $user;
    }

    public function getUsers()
    {
        // ->join('uac_profile_user', 'uac_profile_user.id_user_fk', '=', $this->table . '.id_user_pk')
        return DB::table("{$this->table}")
            ->where("{$this->table}.id_company_fk", '=', $this->entity->getIdCompanyFk())
//            ->whereNotIn("{$this->table}.id_user_pk", [$this->entity->getIdUserPk()]) // TODO: Retirar o id do usuário logado da lista
            ->orderBy("{$this->table}.id_user_pk", 'DESC')
            ->get($this->getColumns())
            ->toArray();
    }

    public function getUser(int $id)
    {
        $columns = $this->getColumns();
        // array_push($columns, DB::raw("(SELECT LISTAGG(ID_PROFILE_FK, ', ') WITHIN GROUP (ORDER BY ID_PROFILE_FK) AS profile FROM (SELECT DISTINCT ID_PROFILE_FK FROM UAC_PROFILE_USER WHERE UAC_PROFILE_USER.ID_COMPANY_FK = {$this->entity->getIdCompanyFk()} AND UAC_PROFILE_USER.ID_USER_FK = {$id})) AS PROFILE"));

        // DB::enableQueryLog();
        $query = DB::table($this->table)
            ->select($columns)
            ->where("{$this->table}.id_company_fk", '=', $this->entity->getIdCompanyFk())
            ->where("{$this->table}.id_user_pk", '=', $id)
            ->first();
        // dd($query);
        // DB::getQueryLog();

        if (!$query) {
            throw new \Exception("A user with the specified ID[{$id}] was not found", 404);
        }
        return $query;
    }

    public function getUserByMail(string $mail)
    {
        //DB::enableQueryLog();
        $query = DB::table($this->table)
            ->select($this->getColumns())
            ->where("mail_login", '=', $mail)
            ->where("active", '=', 1)
            ->first();
        //dd($query);
        //DB::getQueryLog();

        if (!$query) {
            throw new \Exception("User not found for {$mail}", 404);
        }

        return $query;
    }

    public function getPartialUsers()
    {
        return DB::table($this->table)
            ->where('id_company_fk', '=', $this->entity->getIdCompanyFk())
            ->orderBy('id_user_pk', 'DESC')
            ->get(['id_user_pk AS id', 'name'])
            ->toArray();
    }

    public function getMailNewUser(string $mail)
    {
        return DB::table($this->table)
            ->select('mail_login')
            ->where("{$this->table}.mail_login", '=', $mail)
            ->where("{$this->table}.id_company_fk", '=', $this->entity->getIdCompanyFk())
            ->exists();
    }

    public function getMailUpdateUser(string $mail, string $id)
    {
        $sameMail = DB::table($this->table)
            ->select('mail_login')
            ->where("{$this->table}.mail_login", '=', $mail)
            ->where("{$this->table}.id_company_fk", '=', $this->entity->getIdCompanyFk())
            ->where("{$this->table}.id_user_pk", '=', $id)
            ->exists();

        if($sameMail) {
            return false;
        }

        return $this->getMailNewUser($mail);
    }

    /**
     * Description: Metodo criado para ser usado na rotina de criação de usuarios no BIOID
     *
     * @param int $companyId
     * @return array
     */
    public function getUsersByCompanyId($companyId)
    {
        $stmt = DB::table($this->table)
            ->join('adm_company as ac', "{$this->table}.id_company_fk", '=', 'ac.id_company_pk');

        if ($companyId !== false) {
            $stmt->where("{$this->table}.id_company_fk", '=', (int) $companyId)
//                        ->where("{$this->table}.id_user_pk", '=', 1);
            ;
        }

        return $stmt->orderBy("{$this->table}.id_user_pk", 'ASC')
//            ->limit(1)
            ->get([
                "{$this->table}.id_user_pk as id",
                "{$this->table}.name",
                "{$this->table}.mail_login as email",
                "{$this->table}.login",
                "{$this->table}.pass",
                "{$this->table}.id_company_fk",
                'ac.bioid_reference as company_bioid_reference'
            ])
            ->toArray();
    }

    public function createUser()
    {
        $this->entity->setIdUserPk($this->nextUserId());

        $data = $this->entity->exchangeArray();
        unset($data['password']);

        DB::table($this->table)->insert($data);

        $this->execInsertEventProfile();
        $this->execInsertEventEstablishments();

        return $this->entity->toArray();
    }

    public function updateUser(int $id)
    {
        $user = $this->getUser($this->entity->getIdUserPk());

        $this->entity->setDateAdded(new DateTime($user->createat));
        $this->entity->setDateUpdated(new DateTime());
        if ($user->lastactivity)
            $this->entity->setDateLastAccess(new DateTime($user->lastactivity));

        $data = $this->entity->exchangeArray();
        // TODO: revar esse passo aqui para evitar de usar unset
        unset($data['id_company_fk'], $data['password'], $data['bioid_reference']
            , $data['pass'], $data['mail']);

        DB::table($this->table)
            ->where('id_user_pk', $this->entity->getIdUserPk())
            ->where('id_company_fk', $this->entity->getIdCompanyFk())
            ->update($data);

        // $this->execDeleteEventProfile();
        // $this->execInsertEventProfile();
        $this->execDeleteEventEstablishments();
        $this->execInsertEventEstablishments();

        return $this->entity->toArray();
    }

    public function updateBioIdReference(array $data, $user)
    {
        $update = [
            'date_updated' => (new DateTime())->format('Y-m-d H:i:s'),
            'bioid_reference' => $data['id']
        ];

        return DB::table($this->table)
            ->where('id_user_pk', (int) $user->id)
            ->where('id_company_fk', (int) $user->id_company_fk)
            ->update($update);
    }

    public function deleteUser(int $id)
    {
        // TODO: Implement deleteUser() method.
    }

    public function statusUser(int $id)
    {
        $user = $this->getUser($id);
        if (!$user) throw new Exception(sprintf('User not found for id %d', $id), 404);

        $data['active'] = $this->entity->exchangeArray()['active'];
        $data['date_updated'] = (new DateTime())->format('Y-m-d H:i:s');

//        DB::enableQueryLog();
        DB::table($this->table)
            ->where('id_user_pk', $user->id)
            ->where('id_company_fk', $user->id_company)
            ->update($data);
//        dd(DB::getQueryLog());

        return $this->getUser($id);
    }

    public function changeUser()
    {
        $userExists = $this->getUser($this->entity->getIdUserPk());
        if (!$userExists) throw new Exception(sprintf('User not found for id %d', $this->entity->getIdUserPk()), 404);

        $data['password'] = $this->entity->exchangeArray()['password'];
        $data['date_updated'] = (new DateTime())->format('Y-m-d H:i:s');

        DB::table($this->table)
            ->where('id_user_pk', $userExists->id)
            ->where('id_company_fk', $userExists->id_company)
            ->update($data);

        return $this->getUser($this->entity->getIdUserPk());
     }

    /**
     * @return UserProfile
     */
    public function getUserProfile(): UserProfile
    {
        return $this->entity->getUserProfile();
    }

    public function getUserByBioId()
    {
        return DB::table($this->table)
            ->select(['id_user_pk'])
            ->where("{$this->table}.id_company_fk", '=', $this->entity->getIdCompanyFk())
            ->where("{$this->table}.bioid_reference", '=', $this->entity->getBioidReference())
            ->first();
    }

    /**
     * @return User
     */
    public function getEntity(): User
    {
        return $this->entity;
    }

    private function getColumns()
    {
        return [
            "{$this->table}.id_user_pk AS id",
            "{$this->table}.id_company_fk AS id_company",
            "{$this->table}.login",
            "{$this->table}.name",
            "{$this->table}.type",
            "{$this->table}.mail_login AS email",
            "{$this->table}.cod_user_erp AS erpCode",
            "{$this->table}.ddd",
            "{$this->table}.tel AS phone",
            "{$this->table}.active AS status",
            "{$this->table}.date_added AS createAt",
            "{$this->table}.date_updated AS updateAt",
            "{$this->table}.date_last_access AS lastActivity",
            "{$this->table}.bioid_reference"
        ];
    }

    private function nextUserId()
    {
        $nextId = DB::table($this->table)
            ->selectRaw('MAX(ID_USER_PK) AS maxid')
            ->first();
        return (int) $nextId->maxid + 1;
    }

    private function execInsertEventProfile()
    {
        $userProfile = new UserProfile([
            'id_company_fk' => $this->entity->getIdCompanyFk(),
            'id_profile_fk' => $this->entity->getProfile()->getIdProfilePk(),
            'id_user_fk' => $this->entity->getIdUserPk()
        ]);
        event( (new UserProfileEvent($userProfile))->createUserProfile() );
    }

    private function execDeleteEventProfile()
    {
        $userProfile = new UserProfile([
            'id_company_fk' => $this->entity->getIdCompanyFk(),
            'id_profile_fk' => $this->entity->getProfile()->getIdProfilePk(),
            'id_user_fk' => $this->entity->getIdUserPk()
        ]);
        event( (new UserProfileEvent($userProfile))->deleteUserProfile() );
    }

    private function execInsertEventEstablishments()
    {
        $this->entity->getEstablishments()->setIdUserFk($this->entity->getIdUserPk());
        event( (new UserEstablishmentsEvent($this->entity->getEstablishments()))->createUserEstablishments() );
    }

    private function execDeleteEventEstablishments()
    {
        event( (new UserEstablishmentsModel($this->entity->getEstablishments()))->deleteUserEstablishments() );
    }
}
