<?php

namespace App\Model;

use App\Domain\User\Entity\UserProfile;
use App\Domain\User\UserProfileInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserProfileModel extends Model implements UserProfileInterface
{
    /**
     * @var string
     */
    protected $table = 'uac_profile_user';

    /**
     * @var UserProfile
     */
    private UserProfile $entity;

    /**
     * @param array
     */
    private $user_profiles = [];

    public function __construct(UserProfile $userProfile)
    {
        $this->entity = $userProfile;
        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function getUserProfiles()
    {
        // TODO: Implement getUserProfiles() method.
    }

    /**
     * @inheritDoc
     */
    public function getUserProfile(int $id)
    {
        // TODO: Implement getUserProfile() method.
    }

    /**
     * @inheritDoc
     */
    public function createUserProfile()
    {
        DB::table($this->table)
            ->insert($this->entity->exchangeArray());
    }

    /**
     * @inheritDoc
     */
    public function updateUserProfile(int $id)
    {
        // TODO: Implement updateUserProfile() method.
    }

    /**
     * @inheritDoc
     */
    public function deleteUserProfile(int $id)
    {
        DB::table($this->table)->where('id_user_fk', $id)->delete();
    }

    /**
     * @return UserProfile
     */
    public function getEntity(): UserProfile
    {
        return $this->entity;
    }

    public function getUserProfileByUserId()
    {
        return DB::table($this->table)
            ->where('id_company_fk', '=', $this->entity->getIdCompanyFk())
            ->where('id_user_fk', '=', $this->entity->getIdUserFk())
            ->first()
        ;
    }

    public function getUserProfilesIdByUserId() 
    {
        $sel_profiles = DB::table($this->table)
            ->where('id_company_fk', '=', $this->entity->getIdCompanyFk())
            ->where('id_user_fk', '=', $this->entity->getIdUserFk())
            ->get(['ID_PROFILE_FK AS id'])
            ->toArray();
        
        if ($sel_profiles) {
            foreach ($sel_profiles as $profile) {
                array_push($this->user_profiles, $profile->id);
            }
        }
        
        return $this;
    }
    
    public function getUserProfilesId()
    {
        return $this->user_profiles;
    }
}
