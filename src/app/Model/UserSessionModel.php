<?php
declare(strict_types=1);

namespace App\Model;

use App\Domain\User\Entity\User;
use App\Domain\User\Entity\UserEstablishments;
use App\Helpers\MaskHelper;
use App\Model\UserEstablishmentsModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

/**
 * Class UserSessionModel
 * @package App\Model
 */
class UserSessionModel extends Model
{
    /**
     * @var string
     */
    protected $table = 'uac_user';

    /**
     * @var string
     */
    protected $table_profile_user = 'uac_profile_user';

    /**
     * @var string
     */
    protected $table_profile = 'uac_profile';

    /**
     * @var string
     */
    protected $table_company = 'adm_company';

    /**
     * @var string
     */
    protected $table_language = 'adm_language';

    /**
     * @var string
     */
    protected $session_id;

    /**
     * @var User
     */
    private User $user;

    /**
     * @param array
     */
    private $user_profiles = [];

    /**
     * @param array
     */
    private $user_data = [];

    /**
     * @var string
     */
    private string $token;

    /*
     * @throws Exception
     */
    public function __construct(User $user = null)
    {
        $this->user = $user ?? new User();
        $this->createSessionId();
    }

    public function selectUser()
    {
        //DB::enableQueryLog();
        $this->user_profiles = DB::table($this->table)
        ->leftJoin($this->table_profile_user, function($join) {
            $join->on("{$this->table}.id_company_fk", "=", "{$this->table_profile_user}.id_company_fk");
            $join->on("{$this->table_profile_user}.id_user_fk", "=", "{$this->table}.id_user_pk");
        })
        ->leftJoin($this->table_profile, function($join) {
            $join->on("{$this->table_profile_user}.id_company_fk", "=", "{$this->table_profile}.id_company_fk");
            $join->on("{$this->table_profile}.id_profile_pk", "=", "{$this->table_profile_user}.id_profile_fk");
        })
        ->join($this->table_company, "{$this->table_company}.id_company_pk", "=", "{$this->table}.id_company_fk")
        ->leftJoin($this->table_language, "{$this->table_language}.id_language_pk", "=", "{$this->table_company}.id_language_pk")
        ->where("{$this->table}.mail_login", '=', $this->user->getMailLogin())
        ->where("{$this->table}.id_user_pk", '=', $this->user->getIdUserPk())
        ->where("{$this->table}.id_company_fk", '=', $this->user->getIdCompanyFk())
        ->get($this->getColumns())
        ->first();
        //dd(DB::getQueryLog());

        return $this;
    }

    /*
     * @return array
     */
    private function getColumns()
    {
        return [
            "{$this->table}.mail_login AS mail_login",
            "{$this->table}.mail AS email",
            "{$this->table}.name AS name",
            "{$this->table}.login AS login",
            "{$this->table}.id_user_pk AS id_user_pk",
            "{$this->table}.date_added AS created_at",
            "{$this->table}.id_company_fk AS id_company_fk",
            "{$this->table}.type AS type",
            "{$this->table}.bioid_reference AS user_refs_bioid",
            "{$this->table_profile}.desc_group AS profile",
            "{$this->table_company}.company_name AS company_name",
            "{$this->table_company}.company_reg AS company_reg",
            "{$this->table_company}.bioid_reference AS ref_company_bioid",
            DB::raw("(CASE {$this->table}.type
                WHEN 0 THEN 'buyer'
                WHEN 1 THEN 'buyer'
                WHEN 2 THEN 'supplier'
                WHEN 3 THEN 'sysadmin'
                WHEN 410 THEN 'spdmSysadmin'
                WHEN 411 THEN 'spdmPharmaceutical'
                WHEN 412 THEN 'spdmPlanner'
                WHEN 413 THEN 'spdmBuyer'
            END) AS user_type"),
            DB::raw("(CASE {$this->table_language}.lang_value
                WHEN 'es;es_pe' THEN 'pe'
                WHEN 'es;es_ar' THEN 'ar'
                WHEN 'es;es_co' THEN 'co'
            ELSE 'br' END) AS country_lang"),
            DB::raw("(CASE {$this->table_language}.lang_value
                WHEN 'es;es_pe' THEN 'PEN'
                WHEN 'es;es_ar' THEN 'ARS'
                WHEN 'es;es_co' THEN 'COP'
            ELSE 'BRL' END) AS country_currency")
        ];
    }

    private function createSessionId()
    {
        $this->session_id = session_id();
        if ($this->session_id == '') {
            $this->session_id = uniqid();
        }
    }

    public function getSessionId()
    {
        return $this->session_id;
    }

    public function saveSession()
    {
        if (!$this->user_profiles) {
            return false;
        }

        try {
            Redis::hmset("{$this->session_id}:session", $this->user_data);
            return true;
        } catch (Exception $exception) {
            return false;
        }
    }

    public function getSession()
    {
        return Redis::hgetall("{$this->session_id}:session");
    }

    public function setToken(string $token)
    {
        $this->token = $token;
        return $this;
    }

    public function prepareSessionData()
    {
        $this->user->setName($this->user_profiles->name);

        $userEstablishments = new UserEstablishments([
            'id_company_fk' => (int)$this->user_profiles->id_company_fk,
            'id_user_fk' => (int)$this->user_profiles->id_user_pk
        ]);

        $this->user_data = [
            'lang' => $this->user_profiles->country_lang,
            'currency' => $this->user_profiles->country_currency,
            'id_company_pk' => $this->user_profiles->id_company_fk,
            'company_name' => $this->user_profiles->company_name,
            'user_id' => $this->user_profiles->id_user_pk,
            'name' => $this->user_profiles->name,
            'login' => $this->user_profiles->login,
            'initials' => $this->user->findInitialsName(),
            'email' => $this->user_profiles->mail_login,
            'profile' => $this->user_profiles->user_type,
            'register' => MaskHelper::cnpjCpf($this->user_profiles->company_reg),
            'user_profiles' => $this->profilesToString($this->user_profiles->profile),
            'created_at' => $this->user_profiles->created_at,
            'type' => $this->user_profiles->type,
            'session_id' => $this->session_id,
            'ip' => $_SERVER['REMOTE_ADDR'] ??= '',
            'estab' => (new UserEstablishmentsModel($userEstablishments))->getEstablishmentsCodes(),
            'ref_company_bioid' => $this->user_profiles->ref_company_bioid,
            'user_refs_bioid' => $this->user_profiles->user_refs_bioid,
            'generate_id'=> $this->user->getGeneratedIdTagManager(),
            'token'=> $this->token,
        ];

        return $this;
    }

    public function getByParams(string $param)
    {
        return ($this->user_data[$param] ??= null);
    }

    /**
     * @return array
     */
    public function getUserData(): array
    {
        return $this->user_data;
    }

    private function profilesToString($profiles)
    {
        if (empty($profiles)) {
            return '';
        }

        return is_array($profiles) ? implode(', ', $profiles) : $profiles;
    }
}
