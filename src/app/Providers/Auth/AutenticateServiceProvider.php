<?php

namespace App\Providers\Auth;

use App\Domain\Auth\JWT\JwtAuthenticate;
use Illuminate\Support\ServiceProvider;

/**
 * Class AutenticateServiceProvider
 * @package App\Providers
 */
class AutenticateServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */


    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }


    public function register()
    {

        $this->app->bind('App\Domain\Auth\Authenticate', function ($app) {
            $token = $app->request->bearerToken() ?? $app->request->token ?? '';
            return new JwtAuthenticate($token);
        });

    }
}
