<?php
declare(strict_types=1);

namespace App\Providers\Company;

use App\Domain\Company\CompanyGroupingInterface;
use App\Domain\Company\CompanyGroupsInterface;
use App\Domain\Company\Entity\Company;
use App\Domain\Company\Groups\CompanyGrouping;
use Illuminate\Support\ServiceProvider;

/**
 * Class CompanyGroupsModelProvider
 * @package App\Providers
 */
class CompanyGroupingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */

    /**
     * Bootstrap services.
     *
     * @return void
     */

    public function boot(Company $company)
    {
        $this->app->bind(CompanyGroupingInterface::class, function () use ($company) {
            return new CompanyGrouping(resolve(CompanyGroupsInterface::class));
        });

    }
}
