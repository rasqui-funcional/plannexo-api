<?php
declare(strict_types=1);

namespace App\Providers\Company;

use App\Domain\Company\CompanyGroupsInterface;
use App\Domain\Company\Entity\Company;
use App\Model\Company\CompanyGroupsModel;
use Illuminate\Support\ServiceProvider;

/**
 * Class CompanyGroupsModelProvider
 * @package App\Providers
 */
class CompanyGroupsModelProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */

    /**
     * Bootstrap services.
     *
     * @return void
     */

    public function boot(Company $company)
    {
        $this->app->bind(CompanyGroupsInterface::class, function () use ($company) {
            return new CompanyGroupsModel($company);
        });

    }
}
