<?php
declare(strict_types=1);

namespace App\Providers\Company;

use App\Domain\Company\CompanyPoliciesInterface;
use App\Domain\Company\Entity\Company;
use App\Model\Company\CompanyPoliciesModel;
use Illuminate\Support\ServiceProvider;

/**
 * Class CompanyPoliciesModelProvider
 * @package App\Providers
 */
class CompanyPoliciesModelProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */

    /**
     * Bootstrap services.
     *
     * @return void
     */

    public function boot(Company $company)
    {
        $this->app->bind(CompanyPoliciesInterface::class, function () use ($company) {
            return new CompanyPoliciesModel($company);
        });

    }
}
