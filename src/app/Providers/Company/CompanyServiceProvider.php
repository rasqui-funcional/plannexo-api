<?php
declare(strict_types=1);

namespace App\Providers\Company;

use App\Domain\Auth\Authenticate;
use App\Domain\Company\Entity\Company;
use Illuminate\Support\ServiceProvider;

/**
 * Class CompanyServiceProvider
 * @package App\Providers
 */
class CompanyServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Authenticate $auth)
    {
        $this->app->bind(Company::class, function () use ($auth) {
            return $auth->getUser()->getCompany();
        });
    }
}
