<?php
declare(strict_types=1);

namespace App\Providers\Email;

use App\Domain\Email\EmailInterface;
use App\Http\Services\EmailSendingService;
use App\Http\Services\HttpService;
use Illuminate\Support\ServiceProvider;

/**
 * Class EmailServiceProvider
 * @package App\Providers
 */
class EmailSendingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EmailInterface::class, function ($app) {
            return new EmailSendingService(resolve(HttpService::class));
        });
    }
}
