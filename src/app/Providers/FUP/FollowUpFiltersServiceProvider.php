<?php
declare(strict_types=1);

namespace App\Providers\FUP;

use App\Domain\Company\CompanyGroupingInterface;
use App\Domain\Company\CompanyPoliciesInterface;
use App\Domain\FollowUp\FollowUpFiltersInterface;
use App\Domain\User\UserEstablishmentsInterface;
use App\Domain\User\UserManagersInterface;
use App\Domain\User\UserStandardDepositInterface;
use App\Domain\FollowUp\Filters\FollowUpFilters;
use Illuminate\Support\ServiceProvider;

/**
 * Class FollowUpFiltersServiceProvider
 * @package App\Providers
 */
class FollowUpFiltersServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */

    /**
     * Bootstrap services.
     *
     * @return void
     */

    public function boot(UserEstablishmentsInterface $user_establishment,
                         UserManagersInterface $user_managers,
                         UserStandardDepositInterface $user_standard_deposit,
                         CompanyPoliciesInterface $policies,
                         CompanyGroupingInterface $company_grouping

    )
    {
        $this->app->bind(FollowUpFiltersInterface::class, function ()
        use (
            $user_establishment,
            $user_managers,
            $user_standard_deposit,
            $policies,
            $company_grouping
        ) {
            return new FollowUpFilters(
                $user_establishment,
                $user_managers,
                $user_standard_deposit,
                $policies,
                $company_grouping
            );
        });

    }
}
