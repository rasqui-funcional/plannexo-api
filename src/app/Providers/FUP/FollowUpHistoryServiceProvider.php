<?php
declare(strict_types=1);

namespace App\Providers\FUP;

use App\Domain\FollowUp\FollowUpHistoryInterface;
use App\Http\Services\FollowUpHistoryService;
use App\Http\Services\HttpService;
use Illuminate\Support\ServiceProvider;

/**
 * Class FollowUpHistoryServiceProvider
 * @package App\Providers
 */
class FollowUpHistoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }

    public function register()
    {
        $this->app->bind(FollowUpHistoryInterface::class, function ($app) {
            return new FollowUpHistoryService(resolve(HttpService::class));
        });

    }
}
