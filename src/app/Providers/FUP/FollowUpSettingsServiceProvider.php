<?php
declare(strict_types=1);

namespace App\Providers\FUP;

use App\Domain\FollowUp\FollowUpSettingsInterface;
use App\Http\Services\FollowUpSettingsService;
use App\Http\Services\HttpService;
use Illuminate\Support\ServiceProvider;

/**
 * Class FollowUpSettingsServiceProvider
 * @package App\Providers
 */
class FollowUpSettingsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }

    public function register()
    {
        $this->app->bind(FollowUpSettingsInterface::class, function ($app) {
            return new FollowUpSettingsService(resolve(HttpService::class));
        });

    }
}
