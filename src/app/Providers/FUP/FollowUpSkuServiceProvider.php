<?php
declare(strict_types=1);

namespace App\Providers\FUP;

use App\Domain\FollowUp\FollowUpSkuInterface;
use App\Http\Services\FollowUpSkuService;
use App\Http\Services\HttpService;
use Illuminate\Support\ServiceProvider;

/**
 * Class FollowUpSkuServiceProvider
 * @package App\Providers
 */
class FollowUpSkuServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }

    public function register()
    {
        $this->app->bind(FollowUpSkuInterface::class, function ($app) {
            return new FollowUpSkuService(resolve(HttpService::class));
        });

    }
}
