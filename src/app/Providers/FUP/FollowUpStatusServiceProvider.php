<?php
declare(strict_types=1);

namespace App\Providers\FUP;

use App\Domain\FollowUp\FollowUpStatusInterface;
use App\Http\Services\FollowUpStatusService;
use App\Http\Services\HttpService;
use Illuminate\Support\ServiceProvider;

/**
 * Class FollowUpStatusServiceProvider
 * @package App\Providers
 */
class FollowUpStatusServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }

    public function register()
    {
        $this->app->bind(FollowUpStatusInterface::class, function ($app) {
            return new FollowUpStatusService(resolve(HttpService::class));
        });

    }
}
