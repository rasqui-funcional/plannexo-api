<?php
declare(strict_types=1);

namespace App\Providers\FUP;

use App\Http\Services\FollowUpStatusService;
use Illuminate\Support\ServiceProvider;

/**
 * Class AutenticateServiceProvider
 * @package App\Providers
 */
class FollowUpStatusServiceProvider1 extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }

    public function register()
    {
        $this->app->bind('App\Domain\FollowUp\FollowUpStatusInterface', function ($app) {
            return new FollowUpStatusService(resolve(\GuzzleHttp\ClientInterface::class));
        });

    }
}
