<?php
declare(strict_types=1);

namespace App\Providers\FUP;

use App\Domain\FollowUp\FollowUpStoreInterface;
use App\Http\Services\FollowUpStoreService;
use App\Http\Services\HttpService;
use Illuminate\Support\ServiceProvider;

/**
 * Class FollowUpStoreServiceProvider
 * @package App\Providers
 */
class FollowUpStoreServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }

    public function register()
    {
        $this->app->bind(FollowUpStoreInterface::class, function ($app) {
            return new FollowUpStoreService(resolve(HttpService::class));
        });
    }
}
