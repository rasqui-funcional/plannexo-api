<?php
declare(strict_types=1);

namespace App\Providers\FUP;

use App\Domain\FollowUp\FollowUpSupplierInterface;
use App\Http\Services\FollowUpSupplierService;
use App\Http\Services\HttpService;
use Illuminate\Support\ServiceProvider;

/**
 * Class FollowUpSummaryServiceProvider
 * @package App\Providers
 */
class FollowUpSummaryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }

    public function register()
    {
        $this->app->bind(FollowUpSupplierInterface::class, function ($app) {
            return new FollowUpSupplierService(resolve(HttpService::class));
        });
    }
}
