<?php
declare(strict_types=1);

namespace App\Providers\Http;

use App\Http\Services\HttpService;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\ServiceProvider;

/**
 * Class AutenticateServiceProvider
 * @package App\Providers
 */
class HttpServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */


    /**
     * Bootstrap services.
     *
     * @return
     */
    public function boot()
    {
    }

    public function register()
    {
        $this->app->bind(ClientInterface::class, function () {
            return new Client;
        });
    }
}
