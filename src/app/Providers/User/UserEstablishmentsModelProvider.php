<?php
declare(strict_types=1);

namespace App\Providers\User;

use App\Domain\User\Entity\User;
use App\Domain\User\UserEstablishmentsInterface;
use App\Model\UserEstablishmentsModel;
use Illuminate\Support\ServiceProvider;

/**
 * Class UserEstablishmentsModelProvider
 * @package App\Providers
 */
class UserEstablishmentsModelProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */

    /**
     * Bootstrap services.
     *
     * @return void
     */

    public function boot(User $user)
    {
        $this->app->bind(UserEstablishmentsInterface::class, function () use ($user) {
            return new UserEstablishmentsModel($user->getEstablishments());
        });

    }
}
