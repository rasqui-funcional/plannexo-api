<?php
declare(strict_types=1);

namespace App\Providers\User;

use App\Domain\User\Entity\User;
use App\Domain\User\UserManagersInterface;
use App\Model\User\UserManagersModel;
use Illuminate\Support\ServiceProvider;

/**
 * Class UserManagersModelProvider
 * @package App\Providers
 */
class UserManagersModelProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */

    /**
     * Bootstrap services.
     *
     * @return void
     */

    public function boot(User $user)
    {
        $this->app->bind(UserManagersInterface::class, function () use ($user) {
            return new UserManagersModel($user);
        });

    }
}
