<?php
declare(strict_types=1);

namespace App\Providers\User;

use App\Domain\Auth\Authenticate;
use App\Domain\Company\Entity\Company;
use App\Domain\User\Entity\User;
use Illuminate\Support\ServiceProvider;

/**
 * Class UserServiceProvider
 * @package App\Providers
 */
class UserServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Authenticate $auth)
    {
        $this->app->bind(User::class, function () use ($auth) {

            return $auth->getUser();
        });
    }
}
