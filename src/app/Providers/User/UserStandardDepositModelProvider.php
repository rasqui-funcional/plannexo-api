<?php
declare(strict_types=1);

namespace App\Providers\User;

use App\Domain\User\Entity\User;
use App\Domain\User\UserStandardDepositInterface;
use App\Model\User\UserManagersModel;
use App\Model\User\UserStandardDepositModel;
use Illuminate\Support\ServiceProvider;

/**
 * Class UserStandardDepositModelProvider
 * @package App\Providers
 */
class UserStandardDepositModelProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */

    /**
     * Bootstrap services.
     *
     * @return void
     */

    public function boot(User $user)
    {
        $this->app->bind(UserStandardDepositInterface::class, function () use ($user) {
            return new UserStandardDepositModel($user);
        });

    }
}
