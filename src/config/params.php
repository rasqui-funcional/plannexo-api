<?php
return [
    'app_url' => env('APP_URL', 'https://api.planning.bio'),
    'main_domain' => env('MAIN_DOMAIN', 'https://planning.bio')
];
