import yaml

class Config:

    def __init__(self):
        return None

    def load(self):
        config_file = self.load_config('/var/www/api/crypto/config.yaml')
        return config_file

    def load_config(self, path):
        try:
            with open(path, 'rt') as config_file:
                conf = yaml.load(config_file.read(), Loader=yaml.FullLoader)
        except IOError as err:
            print("Could not get conf file [%s]" % err)
            raise SystemExit(1)
        return conf
