import sys
import urllib
import sha
import base64
import random
import datetime

from urlparse import urlparse
from pytz import timezone
from Crypto.Cipher import AES
from config import Config

class BioCryptor(object):

    def __init__(self, secret):
        self.secret_key = sha.new(secret).hexdigest()[:32]


    def encrypt(self, string, iv = None):

        string = urllib.quote(string)
        if (iv == None):
            iv = ''.join(chr(random.randint(0, 0xFF)) for i in range(16))

        encryptor = AES.new(self.secret_key, AES.MODE_CBC, iv)

        #PKCS#7 padding
        length = 16 - (len(string) % 16)
        string += chr(length)*length

        encrypted = encryptor.encrypt(string)

        return base64.b64encode(iv + encrypted)


class BioAssigner(object):

    def __init__(self, secret, timezone):
        self.conf_timezone = timezone
        self.cryptor = BioCryptor(secret)


    def assign(self, http_verb, path, expire):
        tz = timezone(self.conf_timezone)
        time = datetime.datetime.now(tz) + datetime.timedelta(milliseconds=expire)
        string = "%s|%s|%s" % (http_verb, time.isoformat(), self._normalize_path(path))
        crypted = self.cryptor.encrypt(string)

        return crypted.replace("\n", "##")


    def _normalize_path(self, path):
        return path if path[-1:] == "/" else path+"/"


param = sys.argv

url = param[1]
method = param[2]
environment = param[3]

config = Config().load()

client_id = config[environment]['client']['id']
client_secret = config[environment]['client']['secret']
valid_time = config[environment]['client']['valid_time']

parts = urlparse(url)
assigner = BioAssigner(client_secret, "America/Sao_Paulo")
signature = "%s %s:%s" % ("BIONEXO", client_id, assigner.assign(method, parts.path, valid_time))
print(signature)
