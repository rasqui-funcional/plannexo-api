#!/bin/bash

if [ ! -e "../public/swagger" ]; then
    mkdir ../public/swagger
fi

php ../vendor/bin/openapi --format json --bootstrap ./swagger-constants.php --output ../public/swagger/swagger.json ./swagger-v1.php ../app/Http/Controllers/
