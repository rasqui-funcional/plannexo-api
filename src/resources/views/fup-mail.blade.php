<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <table cellspacing="0" cellpadding="20" border="0" width="100%" align="center" bgcolor="#f2f4f8">
        <tr>
            <td>
                <table cellspacing="0" cellpadding="10" border="0" width="90%" align="center" bgcolor="#ffffff" style="min-width: 750px;">
                    <!-- logo -->
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="10" border="0" align="center" width="650">
                                <tr>
                                    <td align="center">
                                        <a href="{!! env('PLX_WEB_V2') !!}">
                                            <img src="https://planning.bio/resources/img/plannexo-logo.png" alt="plannexo" width="120" style="margin: 20px 0 0 0">
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" >
                                        <h3 style="color: #585858">NOTIFICAÇÃO DE {!! strtoupper($company_name) !!}</h3>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- descrição -->
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="10" border="0"  align="center" width="90%" style="min-width: 650px;">
                                <tr>
                                    <td align="left">
                                        <font size="2" face="arial, sans-serif" color="#000000">
                                            {!! $description !!}
                                        </font>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <!-- texto para o fornecedor -->
                    <!-- <tr>
                        <td>
                            <table cellspacing="0" cellpadding="10" border="0" align="center" width="90%" style="min-width: 650px;">
                                <tr>
                                    <td>
                                        <font size="2" face="arial, sans-serif" color="#000000">
                                            <p style="text-align: center;"><b>Olá {--!! $fornecedor !!},</b></p>
                                            <p style="text-align: center;"><b>Vocẽ pode acessar o sistema e alterar as datas de chegada das ordens de compra em atraso.</b></p>
                                            <p style="text-align: center;"><b>Clique no link abaixo para preencher as informações:</b></p>
                                        </font>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table bgcolor="#e66136" align="center" style="padding: 8px 20px; border-radius: 5px; box-shadow: 0px 2px 4px 0px #FCBEAA;">
                                            <tr>
                                                <td>
                                                    <a href="" style="background:#e66136; text-decoration: none;">
                                                        <font size="2" face="arial, sans-serif" color="#ffffff"><b>Atualizar informações de entrega</b></font>
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr> -->

                    <!-- lista de itens -->
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="10" border="0" width="90%" style="min-width: 650px;" align="center">
                                <tr>
                                    <th align="left" colspan="10">
                                        <font size="3" face="arial, sans-serif" color="#000000">{!! $i18n[0] !!}</font>
                                    </th>
                                </tr>

                                <tr>
                                    @foreach ($th_headers as $header)
                                        <th align="left">
                                            <font size="1" face="arial, sans-serif" color="#000000">{!! @strtoupper($header) !!}</font>
                                        </th>
                                    @endforeach
                                </tr>
                                <tr><td colspan="10" style="padding: 0"><hr style="margin: 0"></td></tr>

                                @foreach($items as $item)
                                    <tr>
                                        @foreach($item as $key => $value)
                                            @switch($key)
                                                @case('delayed_days')
                                                    <td align="left">
                                                        <font size="1" face="arial, sans-serif" color=@if($item->delayed_days > 0) "#FF0000" @else "#000000" @endif>{!! $value !!}</font>
                                                    </td>
                                                    @break

                                                @case('order_date')
                                                    <td align="left">
                                                        <font size="1" face="arial, sans-serif" color=@if($item->delayed_days > 0) "#FF0000" @else "#000000" @endif>{!! $value !!}</font>
                                                    </td>
                                                    @break

                                                @case('expected_date')
                                                    <td align="left">
                                                        <font size="1" face="arial, sans-serif" color=@if($item->delayed_days > 0) "#FF0000" @else "#000000" @endif>{!! $value !!}</font>
                                                    </td>
                                                    @break

                                                @default
                                                    <td align="left">
                                                        <font size="1" face="arial, sans-serif" color=#000000">{!! $value !!}</font>
                                                    </td>
                                            @endswitch
                                        @endforeach
                                    </tr>
                                @endforeach
                                <tr><td colspan="10" style="padding: 0"><hr style="margin: 0"></td></tr>
                            </table>
                        </td>
                    </tr>

                    <!-- footer  -->
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0" border="0" width="90%" style="min-width: 650px; padding: 30px 0 0 0" align="center">
                                <tr>
                                    <td align="center">
                                        <font size="1" face="arial, sans-serif" color="#000000">
                                            <p>{!! $i18n[1] !!}</p>
                                            <p>{!! $i18n[2] !!}</p>
                                        </font>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0" border="0" width="90%" style="min-width: 650px;" align="center">
                                <tr>
                                    <td align="center">
                                        <font size="1" face="arial, sans-serif" color="#585858">
                                            <p>Bionexo {!! $year !!} - {!! $i18n[3] !!}</p>
                                        </font>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0" border="0" width="90%" style="min-width: 650px; padding: 20px 0 30px 0;" align="center">
                                <tr>
                                    <td align="left">
                                        <a href="https://bionexo.com/" style="text-decoration: none;">
                                            <img src="https://planning.bio/resources/img/bionexo-logo.png" alt="bionexo">
                                        </a>
                                    </td>
                                    <td align="right">
                                        <a href="https://www.youtube.com/channel/UChSA67k3sR9amcXGF8QqdPw" style="padding: 0 0 0 5px; text-decoration: none;">
                                            <img src="https://planning.bio/resources/img/favicon-youtube.png" alt="youtube bionexo">
                                        </a>
                                        <a href="https://www.linkedin.com/company/bionexo" style="padding: 0 0 0 5px; text-decoration: none;">
                                            <img src="https://planning.bio/resources/img/favicon-linkedin.png" alt="linkedin bionexo">
                                        </a>
                                        <a href="https://www.facebook.com/bionexo/" style="padding: 0 0 0 5px; text-decoration: none;">
                                            <img src="https://planning.bio/resources/img/favicon-facebook.png" alt="facebook bionexo">
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</html>
