<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8"/>
        <title>{!! $title !!}</title>
        <style>
        </style>
    </head>
    <body style="background-color: #F2F4F8; padding: 35px;">
        <table border="0" width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse">
            <tbody>
                <tr>
                    <td width="100%" align="center">
                        <table style="background-color: #fff; width: 600px; max-width: 600px; margin: auto; font-family: helvetica, arial, sans serif;">
                            <tbody>
                                <tr>
                                    <td align="center" style="text-align: center; padding-left: 30px; padding-right: 30px; padding-top: 30px; padding-bottom: 25px;">
                                        <img src="{!! env('PLX_RESOURCES_URL') . 'img/plannexo-logo.png' !!}" alt="Plannexo" title="Plannexo" height="32" width="auto">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 30px; padding-right: 30px;">
                                        <h2 style="margin-bottom: 35px; color: #333333;">Cadastro de usuário</h2>
                                        <p style="margin-bottom: 25px; color: #333333; font-size: 14px;">Olá {!! $title !!},</p>
                                        <p style="margin-bottom: 25px; color: #333333; font-size: 14px;">Você foi convidado a ter acesso à plataforma Plannexo.</p>
                                        <p style="margin-bottom: 25px; color: #333333; font-size: 14px;">Entre no link abaixo para finalizar o cadastro.</p>
                                        <p style="margin-bottom: 25px; color: #333333; font-size: 14px;">Grato,</p>
                                        <p style="margin-bottom: 25px; color: #333333; font-size: 14px;">Equipe Bionexo</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="padding-left: 30px; padding-right: 30px; margin-top: 50px; text-align: center;">
                                        <a href="{!! env('PLX_WEB_V2') !!}/register-password?token={!! $token !!}" target="_blank" style="display: block; width: 190px; margin-bottom: 15px; font-size: 16px; text-decoration: none; font-weight: bold; margin: auto; background-color: #F96737; border: none; border-radius: 4px; border: none; color: #fff; padding-top: 15px; padding-bottom: 15px;">
                                            <img src="{!! env('PLX_RESOURCES_URL') . 'img/keyicon.png' !!}" alt="Bionexo" title="Bionexo" height="12" width="auto" style="margin-right: 5px;">
                                            Cadastrar senha
                                        </a>
                                        <p style="font-size: 12px; margin-bottom: 0px; color: #333333;">Ou cole este link no seu navegador</p>
                                        <a style="font-size: 12px; color: #00A2AD;" href="{!! env('PLX_WEB_V2') !!}/register-password?token={!! $token !!}">
                                            {!! env('PLX_WEB_V2') !!}/register-password?token={!! $token !!}
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="padding-left: 30px; padding-right: 30px;  padding-top: 40px; text-align: center;">
                                        <p style="font-size: 12px; margin-bottom: 0px; color: #333333;">Este é um e-mail automático, por favor, não responda.</p>
                                        <p style="font-size: 12px; color: #333333; margin-top: 5px;">Caso você não tenha solicitado o cadastro de uma nova senha, desconsidere este e-mail.</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="padding-left: 30px; padding-right: 30px; padding-bottom: 40px; text-align: center;">
                                        <p style="font-size: 12px; color: #888888;">Bionexo {!! $year !!} - todos os direitos reservados</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <table width="100%">
                                            <tr width="100%">
                                                <td align="left" width="50%" style="padding-left: 30px; padding-bottom: 30px;">
                                                    <a href="https://bionexo.com/" target="_blank">
                                                        <img src="{!! env('PLX_RESOURCES_URL') . 'img/bionexo-logo.png' !!}" alt="Bionexo" title="Bionexo" height="32" width="auto">
                                                    </a>
                                                </td>
                                                <td align="right" width="50%" style="padding-right: 30px; padding-bottom: 30px;">
                                                    <a href="https://www.facebook.com/bionexo/" target="_blank" style="float: right;">
                                                        <img src="{!! env('PLX_RESOURCES_URL') . 'img/favicon-facebook.png' !!}" alt="Bionexo YouTube" title="Bionexo Facebook" height="32" width="auto">
                                                    </a>
                                                    <a href="https://www.linkedin.com/company/bionexo/" target="_blank" style="float: right; margin-right: 10px;">
                                                        <img src="{!! env('PLX_RESOURCES_URL') . 'img/favicon-linkedin.png' !!}" alt="Bionexo YouTube" title="Bionexo Linkedin" height="32" width="auto">
                                                    </a>
                                                    <a href="https://www.youtube.com/channel/UChSA67k3sR9amcXGF8QqdPw" target="_blank" style="float: right; margin-right: 10px;">
                                                        <img src="{!! env('PLX_RESOURCES_URL') . 'img/favicon-youtube.png' !!}" alt="Bionexo YouTube" title="Bionexo YouTube" height="32" width="auto">
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
