<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::get('/auth/mail-validation/{email}', 'Auth\AuthController@mailValidation')->middleware(['cors', 'web',]);

    Route::group([
        'middleware' => [
            'cors',
            'user.login.validate',
            'user.auth',
            'jwt.token',
            'user.plannexoweb.session'
        ]
    ], function () {
        Route::post('auth/login', 'Auth\AuthController@callback');
    });

    Route::group([
        'middleware' => [
            'cors',
            'bioid.token',
            'bioid.auth',
            'user.auth',
            'jwt.token',
            'user.plannexoweb.session'
        ]
    ], function () {
        Route::get('auth/callback', 'Auth\AuthController@callback');
    });

    Route::group([
        'prefix' => 'integration',
    ],
        function () {
            Route::get('', 'TesteController@open');
        }
    );

    Route::group([
        'prefix' => 'auth',
        'namespace' => 'User',
    ],
        function () {
            Route::get('/forgot-password/{mail}', 'UserController@forgotPassword');
            Route::patch('/change-data/{uid?}', 'UserController@changeData');
        }
    );

    Route::group(['middleware' => ['cors', 'jwt.verify', 'user.save']], function () {
        Route::get('closed', 'TesteController@closed'); // TODO: retirar no fim do desenvolvimento

        Route::group([
            'prefix' => 'users',
            'namespace' => 'User',
        ],
            function () {
                Route::get('', 'UserController@getList');
                Route::get('establishments', 'UserEstablishmentsController@getList');
                Route::get('managers', 'UserManagersController@getList');
                Route::get('standard-deposit', 'UserStandardDepositController@getList');
                Route::get('/partial-get-users', 'UserController@getPartialList');
                Route::get('{uid}', 'UserController@get');
                Route::get('/mail-validation/{mail}/{id}', 'UserController@mailValidation');
                Route::post('', 'UserController@create');
                Route::put('{uid}', 'UserController@update');
                Route::patch('{uid}', 'UserController@statusUser');
                Route::delete('{uid}', 'UserController@delete');
            }
        );

        Route::group([
            'prefix' => 'profiles',
            'namespace' => 'Profile',
        ],
            function () {
                Route::get('', 'ProfileController@getList');
                Route::get('{pid}', 'ProfileController@get');
                Route::post('', 'ProfileController@create');
                Route::put('{pid}', 'ProfileController@update');
                Route::delete('{pid}', 'ProfileController@delete');
            }
        );

        Route::group([
            'prefix' => 'companies',
            'namespace' => 'Company',
        ],
            function () {
                Route::get('', 'CompanyController@getList');
                Route::get('establishments', 'CompanyController@getListEstablishment');
                Route::get('policies', 'CompanyPoliciesController@getList');
                Route::get('groups', 'CompanyGroupsController@getList');
                Route::get('groups/data/{id}', 'CompanyGroupsController@getGroupData');
                Route::get('groups/{id}', 'CompanyGroupsController@getGroup');
                Route::get('grouping', 'CompanyGroupingController@getList');
                Route::get('grouping/{id}', 'CompanyGroupingController@getGroup');
                Route::get('{cid}', 'CompanyController@get');
                Route::post('', 'CompanyController@create');
                Route::put('{cid}', 'CompanyController@update');
                Route::delete('{cid}', 'CompanyController@delete');
            }
        );

        Route::group([
            'prefix' => 'permissions',
            'namespace' => 'Permission',
        ],
            function () {
                Route::post('apply', 'PermissionController@apply');
                Route::delete('remove', 'PermissionController@remove');
                Route::get('{tag}', 'PermissionController@hasTag');
                Route::get('', 'PermissionController@getList');
            }
        );

        Route::group([
            'middleware' => ['tags:fup'],
            'prefix' => 'fup',
            'namespace' => 'FUP',
        ],
            function () {
                Route::post('store', 'FollowUpStoreController@store');
                Route::get('settings', 'FollowUpSettingsController@getSettings');
                Route::post('settings', 'FollowUpSettingsController@updateSettings');
                Route::get('filters', 'FollowUpFiltersController@getList');
                Route::post('user/filters', 'FollowUpFiltersStoreController@store');
                Route::put('user/filters/{filter_id}', 'FollowUpFiltersUpdateController@update');
                Route::get('user/filters/{filter_id}', 'FollowUpUserFiltersController@getFilter');
                Route::delete('user/filters/{filter_id}', 'FollowUpFiltersDeleteController@delete');
                Route::get('user/filters', 'FollowUpUserFiltersController@getList');
                Route::get('status/list', 'FollowUpStatusController@getList');
                Route::get('{supplierId}', 'FollowUpPendingSkuController@getList')->middleware(['supplier.info']);
                Route::post('{supplierId}', 'FollowUpPendingSkuController@postList')->middleware(['supplier.info']);
                Route::get('', 'FollowUpSummaryController@getList');
                Route::post('', 'FollowUpSummaryController@getQueryList');
                Route::post('email/send', 'FollowUpMailManually@sendEmail');
                Route::get('history/{supplierId}', 'FollowUpHistoryController@get')->middleware(['supplier.info']);
                Route::post('history/{supplierId}', 'FollowUpHistoryController@post')->middleware(['supplier.info']);
            }
        );

        Route::group([
            'prefix' => 'email-models',
            'namespace' => 'Email',
        ],
            function () {
                Route::post('store', 'EmailModelController@store');
                Route::get('all', 'EmailModelController@getListWithBody');
                Route::get('', 'EmailModelController@getList');
                Route::get('{eid}', 'EmailModelController@get');
                Route::put('{eid}', 'EmailModelController@update');
                Route::delete('{eid}', 'EmailModelController@delete');
            }
        );
    });
});

Route::get('open', 'TesteController@open'); // TODO: retirar no fim do desenvolvimento
