<?php

namespace Tests;

use App\Http\Services\HttpService;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

trait MockClientResponse
{

    public function createHttpClientMock($response_data, $status_code)
    {
        $headers =
        ['Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'companyId' => 2,
        ];
        $body = json_encode($response_data);

        $response = new Response($status_code, $headers, $body);

        $mock = new MockHandler([
            $response
        ]);

        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);


        return new HttpService($client);
    }

    public function getMock($file_name, $assoc = true)
    {
        return $this->getJson(__DIR__ . "/mocks/{$file_name}", $assoc);
    }

    public function getJson($file_path, $assoc = true)
    {
        $json_content = file_get_contents("{$file_path}");
        return json_decode($json_content, $assoc);
    }

}
