<?php
declare(strict_types=1);

namespace Tests\Feature\Auth;
use Tests\TestCase;
use App\Domain\Auth;

/**
 * Class AuthenticateTest
 * @package Tests\Feature\Auth
 */
class BioIdTokenTest extends TestCase
{


    /**
     * @var Auth\JWT\JwtToken
     */
    private $bioIdMock;
    private $auth;

    public function setUp(): void
    {

        $this->bioIdMock = $stub = $this->createMock(Auth\BioId\BioId::class);
        $this->auth = new Auth\BioId\BioIdAuthToken($this->bioIdMock);
    }

    public function testObjectType()
    {
        $this->assertInstanceOf("app\Domain\Auth\AuthToken", $this->auth);
    }

    public function testGenerateToken()
    {

        $this->bioIdMock->method('getAccessToken')->willReturn('123');

        $token = $this->auth->setCode('456')->generateToken();
        $this->assertInstanceOf("app\Domain\Auth\AuthToken", $token);
        $this->assertTrue(method_exists($token, 'token'));
        $this->assertNotNull($token->token());
        $this->assertIsString("string", $token->token());
        $this->assertEquals('123', $token->token());
    }
}
