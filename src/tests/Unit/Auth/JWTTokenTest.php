<?php
declare(strict_types=1);
namespace Tests\Feature\Auth;

use App\Domain\User\Entity\User;
use Tests\TestCase;
use App\Domain\Auth;

/**
 * Class AuthenticateTest
 * @package Tests\Feature\Auth
 */
class JWTTokenTest extends TestCase
{
    /**
     * @var Auth\JWT\JwtToken
     */
    private $auth;

    public function setUp(): void
    {

        $this->auth = new Auth\JWT\JwtToken((new User(['id_user_pk'=> 2, 'id_company_fk' => 2])));
    }

    public function testObjectType()
    {

        $this->assertInstanceOf("app\Domain\Auth\AuthToken", $this->auth);
    }

    public function testGenerateToken()
    {
//        $stub = $this->createMock(Auth\BioId\BioId::class);
//        $stub->method('getAccessToken')->willReturn('123');
//        $bioId = new Auth\BioId\BioIdAuthToken( '123' );
//        echo $bioId->generateToken()->token();exit;
        $token = $this->auth->generateToken();
        $this->assertInstanceOf("app\Domain\Auth\JWT\JwtToken", $token);
        $this->assertTrue(method_exists($token, 'token'));
        $this->assertIsString("string", $token->token());
        $this->assertNotNull($token->token());
        $verify_token = explode('.', $token->token());
        $this->assertEquals(3, count($verify_token));
    }
}
