<?php
declare(strict_types=1);

namespace Tests\Feature\Auth;

use App\Domain\Auth\JWT\JwtAuthenticate;
use App\Domain\Auth\JWT\JwtToken;
use App\Domain\User\Entity\User;
use Tests\TestCase;

/**
 * Class AuthenticateTest
 * @package Tests\Feature\Auth
 */
class JwtAutenticateTest extends TestCase
{

    public function testValidTokenReturnTrue()
    {

        $token = $this->geToken();
        $this->assertIsString($token);
        $autenticate = new JwtAuthenticate($token);
        $this->assertTrue($autenticate->authenticate());
        $this->assertEquals(2, $autenticate->getCompany()->getIdCompanyPk());
        $this->assertEquals('Hospital Bionexo', $autenticate->getUser()->getCompany()->getCompanyName());
        $this->assertEquals(2, $autenticate->getUser()->getCompany()->getIdCompanyPk());
        $this->assertEquals(2, $autenticate->getUser()->getIdUserPk());
    }

    public function testInValidTokenReturnFalse()
    {
        $token = $this->geToken();
        $token = "{$token}plannexo";
        $autenticate = new JwtAuthenticate($token);
        $this->assertFalse($autenticate->authenticate());
    }

    public function testInTokenWithoutRequiredClaim()
    {
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImp0aSI6IjRmMWcyM2ExMmFhIn0.eyJpc3MiOiJodHRwczpcL1wvYXBpLnBsYW5uaW
        5nLmJpbyIsImF1ZCI6Imh0dHBzOlwvXC9wbGFubmluZy5iaW8iLCJqdGkiOiI0ZjFnMjNhMTJhYSIsImlhdCI6MTU3NTkwMjE2OSwibmJmIjoxNT
        c1OTAyMTY5LCJleHAiOjE1NzU5MDU3Njl9.MGK-Ea5CLkeO-eRvWyegTq2LA_fccUicSeHniNw8Glc';

        $this->expectException(\OutOfBoundsException::class);
        $this->expectExceptionMessage("Requested claim is not configured");
        $autenticate = new JwtAuthenticate($token);
        $this->assertFalse($autenticate->authenticate());
    }

    public function testEmptyTokenException()
    {
        $autenticate = new JwtAuthenticate('');
        $this->assertEquals(0, $autenticate->getCompany()->getIdCompanyPk());
        $this->assertEquals(0, $autenticate->getUser()->getIdUserPk());

    }

    public function testTokenNotProvided()
    {

        $this->expectException(\ArgumentCountError::class);
        (new JwtAuthenticate());
    }

    public function testTokenIntToken()
    {

        $this->expectException(\TypeError::class);
        (new JwtAuthenticate(1));
    }

    /**
     * @return string
     */
    private function geToken()
    {
        return (new JwtToken((new User(['id_user_pk' => 2, 'id_company_fk' => 2, 'company_name' => 'Hospital Bionexo']))))->generateToken()->token();
    }

}
