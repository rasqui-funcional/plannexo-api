<?php
declare(strict_types=1);

namespace Tests\Unit\User;

use App\Domain\Auth\Authenticate;
use App\Domain\User\Entity\User;
use App\Model\UserLoginModel;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

/**
 * Class UserLoginModelTest
 * @package Tests\Feature\User
 */
class UserLoginModelTest extends TestCase
{

    private $user_login;
    private $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = new User(['id_user_pk' => 2, 'id_company_fk' => 2, 'mail_login' => 'qa@bionexo.com']);

        $this->user_login = new UserLoginModel($this->user);

    }

    /**
     * @testdox is instance of User Object
     */
    public function testObjectType()
    {
        $this->assertInstanceOf(Authenticate::class, $this->user_login);
    }


    public function testHasMethods()
    {
        $this->assertTrue(method_exists($this->user_login, 'authenticate'));
    }

    public function testGetUserByEmail()
    {

        $mock_response = $this->getMock('auth_mail_validation.json');

        DB::shouldReceive('table')->andReturnSelf()
            ->shouldReceive('select')->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('first')->andReturn($mock_response);

        $result = $this->user_login->getUserByEmail();
        $this->assertInstanceOf(User::class, $result);
    }

    public function testUserLoginModelModelParamType()
    {
        $this->expectException(\TypeError::class);
        $this->user_login = new UserLoginModel();
    }

    public function testException()
    {

        DB::shouldReceive('table')->andReturnSelf()
            ->shouldReceive('select')->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('first')->andThrow(\Exception::class);

        $this->expectException(\Exception::class);
        $this->user_login->getUserByEmail();
    }

    public function testGetUserLoginModelAuthenticate()
    {

        $mock_response = true;

        DB::shouldReceive('table')->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('exists')->andReturn(true);

        $result = $this->user_login->authenticate();

        $this->assertSame($result, $mock_response);
    }


    public function testExceptionAuthenticate()
    {

        DB::shouldReceive('table')->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('exists')->andThrow(\Exception::class);

        $this->expectException(\Exception::class);
        $this->user_login->authenticate();
    }

}
