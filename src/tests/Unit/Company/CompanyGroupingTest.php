<?php
declare(strict_types=1);

namespace Tests\Unit\User;

use App\Domain\Company\CompanyGroupsInterface;
use App\Domain\Company\Entity\Company;
use App\Domain\Company\Groups\CompanyGrouping;
use App\Domain\User\Entity\User;
use Tests\TestCase;

/**
 * Class CompanyGroupingTest
 * @package Tests\Unit\FollowUp
 */
class CompanyGroupingTest extends TestCase
{

    private $company_groups;
    private $user;
    private $company;
    private $company_grouping;

    public function setUp(): void
    {

        parent::setUp();
        $this->user = new User(['id_company_fk' => 2, 'id_user_pk' => 1]);
        $this->company = new Company(['id_company_fk' => 2]);

        $this->company_groups = $this->getMockBuilder(CompanyGroupsInterface::class)
            ->setConstructorArgs([$this->company])
            ->getMock();


        $this->company_grouping = new CompanyGrouping(
            $this->company_groups
        );
    }

    public function testHasMethodGetFollowUpFilters()
    {
        $this->assertTrue(method_exists($this->company_grouping, 'getCompanyGrouping'));
        $this->assertTrue(method_exists($this->company_grouping, 'getCompanyGroupingById'));
    }

    /**
     * @testdox is instance of User Object
     */
    public function testGetFollowUpFilters()
    {
        $groups = $this->getMock('company_groups_fk_1.json', false);
        $group_data = $this->getMock('company_group_data_fk_1.json');
        $company_grouping_data_fk_1 = $this->getMock('company_grouping_fk_1.json');
        $group = $this->getMock('company_group.json', false);

        $this->company_groups->method('getCompanyGroups')
            ->willReturn($groups);

        $this->company_groups->method('getCompanyGroupData')
            ->with('1')
            ->willReturn($group_data);

        $this->company_groups->method('getCompanyGroup')
            ->with('1')
            ->willReturn($group);

        $this->assertEquals($groups, $this->company_grouping->getCompanyGroups());
        $this->assertEquals($group_data, $this->company_grouping->getCompanyGroupData('1'));
        $this->assertEquals($group, $this->company_grouping->getCompanyGroup('1'));
        $this->assertEquals($company_grouping_data_fk_1, $this->company_grouping->getCompanyGrouping());
        $this->assertEquals($company_grouping_data_fk_1, $this->company_grouping->getCompanyGroupingById('1'));
    }

    public function testGetFollowUpFiltersExceptions()
    {

        $this->company_groups->method('getCompanyGroups')
            ->will($this->throwException(new \Exception));

        $this->company_groups->method('getCompanyGroupData')
            ->with(1)
            ->will($this->throwException(new \Exception));

        $this->company_groups->method('getCompanyGroup')
            ->with(1)
            ->will($this->throwException(new \Exception));

        $this->expectException(\Exception::class);
        $this->company_grouping->getCompanyGrouping();

        $this->expectException(\TypeError::class);
        $this->company_grouping->getCompanyGroupData(1);

        $this->expectException(\Exception::class);
        $this->company_grouping->getCompanyGroups();

        $this->expectException(\TypeError::class);
        $this->company_grouping->getCompanyGroupingById(1);

    }

    public function testGetCompanyGroupingByIdExceptions()
    {

        $this->company_groups->method('getCompanyGroupData')
            ->with(1)
            ->will($this->throwException(new \Exception));

        $this->company_groups->method('getCompanyGroup')
            ->with(1)
            ->will($this->throwException(new \Exception));

        $this->expectException(\Exception::class);
        $this->company_grouping->getCompanyGroupingById('1');

        $this->expectException(\TypeError::class);
        $this->company_grouping->getCompanyGroupingById(1);

    }
}
