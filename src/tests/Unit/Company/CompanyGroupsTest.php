<?php
declare(strict_types=1);

namespace Tests\Unit\Company;

use App\Domain\Company\CompanyGroupsInterface;
use App\Domain\Company\Entity\Company;
use App\Domain\Company\Traits\CompanyGroupsTrait;
use App\Model\Company\CompanyGroupsModel;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

/**
 * Class CompanyGroupsTest
 * @package Tests\Feature\User
 */
class CompanyGroupsTest extends TestCase
{

    private $company_groups;
    private $mock;
    private $company;

    public function setUp(): void
    {
        parent::setUp();
        $this->company = new Company(['id_company_pk' => 2]);
        $this->company_groups = new CompanyGroupsModel($this->company);

    }

    /**
     * @testdox is instance of User Object
     */
    public function testObjectType()
    {
        $this->assertInstanceOf(CompanyGroupsInterface::class, $this->company_groups);
    }

    public function testHasMethodGetCompanyGroups()
    {
        $this->assertTrue(method_exists($this->company_groups, 'getCompanyGroups'));
        $this->assertTrue(method_exists($this->company_groups, 'getCompanyGroup'));
        $this->assertTrue(method_exists($this->company_groups, 'getCompanyGroupData'));
    }

    public function testGetCompanyGroups()
    {

        $this->mock = $this->getMock('company_groups.json');

        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('raw')->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('orderBy')->andReturnSelf()
            ->shouldReceive('get')->andReturnSelf()
            ->shouldReceive('toArray')
            ->andReturn($this->mock);

        $result = $this->company_groups->getCompanyGroups();
        $this->assertSame($this->mock, $result);
    }

    public function testGetCompanyGroupsException()
    {
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('raw')->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('orderBy')->andReturnSelf()
            ->shouldReceive('get')->andReturnSelf()
            ->shouldReceive('toArray')
            ->andThrow(\Exception::class);

        $this->expectException(\Exception::class);

        $this->company_groups->getCompanyGroups();
    }

    public function testGetCompanyGroup()
    {

        $this->mock = $this->getMock('company_group.json');
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('raw')->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('orderBy')->andReturnSelf()
            ->shouldReceive('get')->andReturnSelf()
            ->shouldReceive('toArray')
            ->andReturn($this->mock);

        $result = $this->company_groups->getCompanyGroup("1");
        $this->assertSame($this->mock, $result);
    }

    public function testGetCompanyGroupException()
    {
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('raw')->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('orderBy')->andReturnSelf()
            ->shouldReceive('get')->andReturnSelf()
            ->shouldReceive('toArray')
            ->andThrow(\Exception::class);

        $this->expectException(\Exception::class);

        $this->company_groups->getCompanyGroup("1");
    }

    public function testGetCompanyGroupTypeErrorException()
    {
        $this->expectException(\TypeError::class);

        $this->company_groups->getCompanyGroup(1);
    }

    public function testGetCompanyGroupData()
    {

        $this->mock = $this->getMock('company_group_data.json');
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('raw')->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('orderBy')->andReturnSelf()
            ->shouldReceive('get')->andReturnSelf()
            ->shouldReceive('toArray')
            ->andReturn($this->mock);

        $result = $this->company_groups->getCompanyGroupData("1");
        $this->assertSame($this->mock, $result);
    }

    public function testGetCompanyGroupDataException()
    {
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('raw')->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('orderBy')->andReturnSelf()
            ->shouldReceive('get')->andReturnSelf()
            ->shouldReceive('toArray')
            ->andThrow(\Exception::class);

        $this->expectException(\Exception::class);

        $this->company_groups->getCompanyGroupData("1");
    }

    public function testGetCompanyGroupDataTypeErrorException()
    {
        $this->expectException(\TypeError::class);

        $this->company_groups->getCompanyGroupData(1);
    }


    public function testCompanyGroupsModelParamType()
    {
        $this->expectException(\TypeError::class);
        $this->company_groups = new CompanyGroupsModel();
    }

    public function testCompanyGroupsTrait()
    {
        $reflection = new \ReflectionClass(CompanyGroupsTrait::class);
        $this->assertTrue($reflection->isTrait());
        $this->assertTrue($reflection->hasMethod('getCompanyGroups'));
        $this->assertTrue($reflection->hasMethod('getCompanyGroup'));
        $this->assertTrue($reflection->hasMethod('getCompanyGroupData'));

        $mock = $this->getMockBuilder(CompanyGroupsTrait::class)
            ->setConstructorArgs([$this->company_groups])
            ->getMockForTrait();

        $groups = $this->getMock('company_groups.json', false);
        $group = $this->getMock('company_group.json', false);
        $group_data = $this->getMock('company_group_data.json', false);
        $this->assertEquals($groups, $mock->getCompanyGroups());
        $this->assertEquals($group, $mock->getCompanyGroup('1'));
        $this->assertEquals($group_data, $mock->getCompanyGroupData('1'));

        $this->expectException(\TypeError::class);
        $mock->getCompanyGroup(1);
        $mock->getCompanyGroupData(1);
    }
}
