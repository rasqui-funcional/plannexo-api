<?php
declare(strict_types=1);

namespace Tests\Unit\Company;

use App\Domain\Company\CompanyPoliciesInterface;
use App\Domain\Company\Entity\Company;
use App\Domain\Company\Traits\CompanyPoliciesTrait;
use App\Model\Company\CompanyPoliciesModel;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

/**
 * Class CompanyPoliciesTest
 * @package Tests\Feature\User
 */
class CompanyPoliciesTest extends TestCase
{

    private $company_policies;
    private $mock;
    private $company;

    public function setUp(): void
    {
        parent::setUp();
        $this->company = new Company(['id_companY_pk' => 2]);
        $this->mock = $this->getMock('company_policies.json');
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('raw')->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('orderBy')->andReturnSelf()
            ->shouldReceive('get')->andReturnSelf()
            ->shouldReceive('toArray')
            ->andReturn($this->mock);

        $this->company_policies = new CompanyPoliciesModel($this->company);

    }

    /**
     * @testdox is instance of User Object
     */
    public function testObjectType()
    {
        $this->assertInstanceOf(CompanyPoliciesInterface::class, $this->company_policies);
    }

    public function testHasMethodGetCompanyPolicies()
    {
        $this->assertTrue(method_exists($this->company_policies, 'getCompanyPolicies'));
    }

    public function testGetCompanyPolicies()
    {

        $result = $this->company_policies->getCompanyPolicies();
        $this->assertSame($this->mock, $result);
    }

    public function testCompanyPoliciesModelParamType()
    {
        $this->expectException(\TypeError::class);
        $this->company_policies = new CompanyPoliciesModel();
    }

    public function testIsTrait()
    {
        $reflection = new \ReflectionClass(CompanyPoliciesTrait::class);
        $this->assertTrue($reflection->isTrait());
        $this->assertTrue($reflection->hasMethod('getCompanyPolicies'));

        $mock = $this->getMockBuilder(CompanyPoliciesTrait::class)
            ->setConstructorArgs([$this->company_policies])
            ->getMockForTrait();

        $this->assertEquals($this->mock, $mock->getCompanyPolicies());

    }
}
