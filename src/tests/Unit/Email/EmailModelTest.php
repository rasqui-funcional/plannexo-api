<?php
declare(strict_types=1);

namespace Tests\Feature\Email;

use App\Domain\Email\EmailModelInterface;
use App\Domain\Email\Entity\EmailUsed;
use App\Domain\Email\Entity\EmailModel;
use App\Domain\Email\Entity\EmailProduct;
use App\Domain\Email\Entity\EmailRegistered;
use App\Domain\Email\Entity\EmailType;
use App\Model\Email\EmailModelModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;

/**
 * Class EmailModelTest
 * @package Tests\Feature\Email
 */
class EmailModelTest extends TestCase
{
    /**
     * @var Email\Entity\EmailModel
     */
    private $email_model;

    /**
     * @var Email\Entity\EmailUsed
     */
    private $email_used;

    /**
     * @var Email\Entity\EmailProduct
     */
    private $email_product;

    /**
     * @var Email\Entity\EmailRegistered
     */
    private $email_registered;

    /**
     * @var Email\Entity\EmailType
     */
    private $email_type;

    /**
     * @var Email\Entity\EmailModel
     */
    private $emailmodel_model;

    public function setUp(): void
    {
        parent::setUp();
        new EmailModel([]);
        $this->mock = $this->getMock('email_model.json');
        $this->email_model = new EmailModel($this->mock['email_models'][0]);
        $this->email_used = new EmailUsed($this->getMock('email_used.json'));
        $this->email_registered = new EmailRegistered($this->getMock('email_registered.json'));
        $this->email_type = new EmailType($this->getMock('email_type.json'));

        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('raw')->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('orderBy')->andReturnSelf()
            ->shouldReceive('get')->andReturnSelf()
            ->shouldReceive('toArray')
            ->andReturn($this->mock);

        $this->emailmodel_model = new EmailModelModel($this->email_model);
    }

    /**
     * @testdox is instance of EmailModel Object
     */
    public function testObjectType()
    {
        $this->assertInstanceOf(EmailModelInterface::class, $this->emailmodel_model);
    }

    /**
     * @testdox getEmailModels
     */
    public function testHasMethodGetEmailModels()
    {
        $this->assertTrue(method_exists($this->emailmodel_model, 'getEmailModels'));
    }

    public function testGetEmailModels()
    {
        $result = $this->emailmodel_model->getEmailModels();
        $this->assertSame($this->mock, $result);
    }


    /**
     * @testdox getEmailModelsWithBody
     */
    public function testHasMethodGetEmailModelsWithBody()
    {
        $this->assertTrue(method_exists($this->emailmodel_model, 'getEmailModelsWithBody'));
    }

    public function testGetEmailModelsWithBody()
    {
        $result = $this->emailmodel_model->getEmailModelsWithBody();
        $this->assertSame($this->mock, $result);
    }

    /**
     * @testdox getEmailModel
     */
    public function testHasMethodGetEmailModel()
    {
        $this->assertTrue(method_exists($this->emailmodel_model, 'getEmailModel'));
    }

    public function testGetEmailModel()
    {
        $result = $this->emailmodel_model->getEmailModel(1);
        $this->assertSame($this->mock, $result);
    }
    
    /**
     * @testdox updateEmailModel
     */
    public function testHasMethodUpdateEmailModel()
    {
        $this->assertTrue(method_exists($this->emailmodel_model, 'updateEmailModel'));
    }

    public function testUpdateEmailModel()
    {
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('update')->andReturnSelf()
            ->once()->andReturn(true);

        $result = $this->emailmodel_model->updateEmailModel(2);
        $this->assertTrue($result);
    }

    /**
     * @testdox deleteEmailModel
     */
    public function testHasMethodDeleteEmailModel()
    {
        $this->assertTrue(method_exists($this->emailmodel_model, 'deleteEmailModel'));
    }

    public function testDeleteEmailModel()
    {
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('delete')->andReturnSelf()
            ->once()->andReturn(true);

        $result = $this->emailmodel_model->deleteEmailModel(1);
        $this->assertTrue($result);
    }

    /**
     * @testdox insertEmailModel
     */
    public function testHasMethodInsertEmailModel()
    {
        $this->assertTrue(method_exists($this->emailmodel_model, 'createEmailModel'));
    }

    public function testInsertEmailModel()
    {
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('insert')->andReturnSelf()
            ->once()->andReturn(true);

        $result = $this->emailmodel_model->createEmailModel();
        $this->assertTrue($result);
    }

    /**
     * @testdox getIdEmailModelPk
     */
    public function testHasMethodGetIdEmailModelPk()
    {
        $this->assertTrue(method_exists($this->email_model, 'getIdEmailModelPk'));
        $this->assertEquals(1, $this->email_model->getIdEmailModelPk());
    }

    /**
     * @testdox getTitle
     */
    public function testHasMethodGetTitle()
    {
        $this->assertTrue(method_exists($this->email_model, 'getTitle'));
        $this->assertEquals("Email de teste 2", $this->email_model->getTitle());
    }

    /**
     * @testdox getSubject
     */
    public function testHasMethodGetSubject()
    {
        $this->assertTrue(method_exists($this->email_model, 'getSubject'));
        $this->assertEquals("Assunto de email", $this->email_model->getSubject());
    }

    /**
     * @testdox getBody
     */
    public function testHasMethodGetBody()
    {
        $this->assertTrue(method_exists($this->email_model, 'getBody'));
        $this->assertEquals("", $this->email_model->getBody());
    }

    /**
     * @testdox getCreateAt
     */
    public function testHasMethodGetCreateAt()
    {
        $this->assertTrue(method_exists($this->email_model, 'getCreateAt'));
        $this->assertIsString($this->email_model->getCreateAt());
    }

    /**
     * @testdox getUpdateAt
     */
    public function testHasMethodGetUpdateAt()
    {
        $this->assertTrue(method_exists($this->email_model, 'getUpdateAt'));
        $this->assertIsString($this->email_model->getUpdateAt());
    }

    /**
     * @testdox getEmailUsed
     */
    public function testHasMethodGetEmailUsed()
    {
        $this->assertTrue(method_exists($this->email_model, 'getEmailUsed'));
        $this->assertIsArray($this->email_model->getEmailUsed());
    }

    /**
     * @testdox EmailUsedGetIdModel
     */
    public function testHasMethodEmailUsedGetIdModel()
    {
        $this->assertTrue(method_exists($this->email_used, 'getIdModel'));
        $this->assertIsInt($this->email_used->getIdModel());
    }

    /**
     * @testdox EmailUsedGetIdModule
     */
    public function testHasMethodEmailUsedGetIdModule()
    {
        $this->assertTrue(method_exists($this->email_used, 'getIdModule'));
        $this->assertIsInt($this->email_used->getIdModule());
    }

    /**
     * @testdox EmailUsedGetModuleName
     */
    public function testHasMethodEmailUsedGetModuleName()
    {
        $this->assertTrue(method_exists($this->email_used, 'getModuleName'));
        $this->assertIsString($this->email_used->getModuleName());
    }

    /**
     * @testdox EmailUsedGetLabel
     */
    public function testHasMethodEmailUsedGetLabel()
    {
        $this->assertTrue(method_exists($this->email_used, 'getLabel'));
        $this->assertIsString($this->email_used->getLabel());
    }

    /**
     * @testdox EmailUsedexchangeArray
     */
    public function testHasMethodEmailUsedexchangeArray()
    {
        $this->assertTrue(method_exists($this->email_used, 'exchangeArray'));
        $this->assertIsArray($this->email_used->exchangeArray());
    }

}