<?php
declare(strict_types=1);

namespace Tests\Feature\Email;

use App\Domain\Email\EmailProductInterface;
use App\Domain\Email\Entity\EmailProduct;
use App\Model\Email\EmailProductModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;

/**
 * Class EmailModelTest
 * @package Tests\Feature\Email
 */
class EmailProductTest extends TestCase
{
    /**
     * @var Email\Entity\EmailProduct
     */
    private $email_product;

    /**
     * @var App\Model\Email\EmailProductModel
     */
    private $email_product_model;

    public function setUp(): void
    {
        parent::setUp();
        $this->email_product = new EmailProduct($this->getMock('email_product.json'));

        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('raw')->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('orderBy')->andReturnSelf()
            ->shouldReceive('get')->andReturnSelf()
            ->shouldReceive('toArray')
            ->andReturn($this->email_product);

        $this->email_product_model = new EmailProductModel($this->email_product);
    }

    /**
     * @testdox is instance of EmailProductModel Object
     */
    public function testObjectType()
    {
        $this->assertInstanceOf(EmailProductInterface::class, $this->email_product_model);
    }

    /**
     * @testdox getEmailProducts
     */
    public function testHasMethodGetEmailProducts()
    {
        $this->assertTrue(method_exists($this->email_product_model, 'getEmailProducts'));
    }

    public function testGetEmailProducts()
    {
        $result = $this->email_product_model->getEmailProducts();
        $this->assertSame($this->email_product, $result);
    }

    /**
     * @testdox getEmailProduct
     */
    public function testHasMethodGetEmailProduct()
    {
        $this->assertTrue(method_exists($this->email_product_model, 'getEmailProduct'));
    }

    public function testGetEmailProduct()
    {
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('get')->andReturnSelf()
            ->shouldReceive('first')
            ->once()->andReturn(true);

        $result = $this->email_product_model->getEmailProduct();
        $this->assertTrue($result);
    }

    /**
     * @testdox EmailProductGetLabel
     */
    public function testHasMethodEmailProductGetLabel()
    {
        $this->assertTrue(method_exists($this->email_product, 'getLabel'));
        $this->assertIsString($this->email_product->getLabel());
    }

    /**
     * @testdox EmailProductGetModule
     */
    public function testHasMethodEmailProductGetModule()
    {
        $this->assertTrue(method_exists($this->email_product, 'getModule'));
        $this->assertIsString($this->email_product->getModule());
    }

    /**
     * @testdox EmailProductGetProduct
     */
    public function testHasMethodEmailProductGetProduct()
    {
        $this->assertTrue(method_exists($this->email_product, 'getProduct'));
        $this->assertIsString($this->email_product->getProduct());
    }

    /**
     * @testdox EmailProductexchangeArray
     */
    public function testHasMethodEmailProductExchangeArray()
    {
        $this->assertTrue(method_exists($this->email_product, 'exchangeArray'));
        $this->assertIsArray($this->email_product->exchangeArray());
    }

}