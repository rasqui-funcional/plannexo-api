<?php
declare(strict_types=1);

namespace Tests\Feature\Email;

use App\Domain\Email\EmailRegisteredInterface;
use App\Domain\Email\Entity\EmailRegistered;
use App\Model\Email\EmailRegisteredModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;

/**
 * Class EmailModelTest
 * @package Tests\Feature\Email
 */
class EmailRegisteredTest extends TestCase
{
    /**
     * @var Email\Entity\EmailRegistered
     */
    private $email_registered;

    /**
     * @var App\Model\Email\EmailRegisteredModel
     */
    private $email_registered_model;

    public function setUp(): void
    {
        parent::setUp();
        $this->email_registered = new EmailRegistered($this->getMock('email_registered.json'));

        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('raw')->andReturnSelf()
            ->shouldReceive('leftJoin')->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('orderBy')->andReturnSelf()
            ->shouldReceive('get')->andReturnSelf()
            ->shouldReceive('toArray')
            ->andReturn($this->email_registered);

        $this->email_registered_model = new EmailRegisteredModel($this->email_registered);
    }

    /**
     * @testdox is instance of EmailRegisteredModel Object
     */
    public function testObjectType()
    {
        $this->assertInstanceOf(EmailRegisteredInterface::class, $this->email_registered_model);
    }

    /**
     * @testdox getEmailRegistered
     */
    public function testHasMethodGetEmailRegistered()
    {
        $this->assertTrue(method_exists($this->email_registered_model, 'getEmailRegistered'));
    }

    public function testGetEmailRegistered()
    {
        $result = $this->email_registered_model->getEmailRegistered();
        $this->assertSame($this->email_registered, $result);
    }

    /**
     * @testdox createEmailRegistered
     */
    public function testHasMethodCreateEmailRegistered()
    {
        $this->assertTrue(method_exists($this->email_registered_model, 'createEmailRegistered'));
    }

    public function testCreateEmailRegistered()
    {
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('insert')->andReturnSelf()
            ->once()->andReturn(true);

        $result = $this->email_registered_model->createEmailRegistered(2);
        $this->assertTrue($result);
    }

    /**
     * @testdox updateEmailRegistered
     */
    public function testHasMethodUpdateEmailRegistered()
    {
        $this->assertTrue(method_exists($this->email_registered_model, 'updateEmailRegistered'));
    }

    public function testUpdateEmailRegistered()
    {
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('update')->andReturnSelf()
            ->once()->andReturn(true);

        $result = $this->email_registered_model->updateEmailRegistered(2);
        $this->assertTrue($result);
    }


    /**
     * @testdox deleteEmailRegistered
     */
    public function testHasMethodDeleteEmailRegistered()
    {
        $this->assertTrue(method_exists($this->email_registered_model, 'deleteEmailRegistered'));
    }

    public function testdeleteEmailRegistered()
    {
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('delete')->andReturnSelf()
            ->once()->andReturn(true);

        $result = $this->email_registered_model->deleteEmailRegistered(2);
        $this->assertTrue($result);
    }

    /**
     * @testdox deleteEmailRegisteredByIdModuleAndIdType
     */
    public function testHasMethodDeleteEmailRegisteredByIdModuleAndIdType()
    {
        $this->assertTrue(method_exists($this->email_registered_model, 'deleteEmailRegisteredByIdModuleAndIdType'));
    }

    public function testdeleteEmailRegisteredByIdModuleAndIdType()
    {
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('delete')->andReturnSelf()
            ->once()->andReturn(true);

        $result = $this->email_registered_model->deleteEmailRegisteredByIdModuleAndIdType(1, 1);
        $this->assertTrue($result);
    }

    /**
     * @testdox insertOrUpdate
     */
    public function testHasMethodInsertOrUpdate()
    {
        $this->assertTrue(method_exists($this->email_registered_model, 'insertOrUpdate'));
    }

    public function testInsertOrUpdate()
    {
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('leftJoin')->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('get')->andReturnSelf()
            ->shouldReceive('first')->andReturnSelf()
            ->shouldReceive('insert')->andReturnSelf()
            ->once()->andReturn(true);

        $result = $this->email_registered_model->insertOrUpdate(10);
        $this->assertTrue($result);
    }

    /**
     * @testdox EmailRegisteredGetIdModel
     */
    public function testHasMethodEmailRegisteredGetIdModel()
    {
        $this->assertTrue(method_exists($this->email_registered, 'getIdModel'));
        $this->assertIsInt($this->email_registered->getIdModel());
    }


    /**
     * @testdox EmailRegisteredGetIdModule
     */
    public function testHasMethodEmailRegisteredGetIdModule()
    {
        $this->assertTrue(method_exists($this->email_registered, 'getIdModule'));
        $this->assertIsInt($this->email_registered->getIdModule());
    }

    /**
     * @testdox EmailRegisteredGetIdType
     */
    public function testHasMethodEmailRegisteredGetIdType()
    {
        $this->assertTrue(method_exists($this->email_registered, 'getIdType'));
        $this->assertIsInt($this->email_registered->getIdType());
    }

    /**
     * @testdox EmailRegisteredGetModuleName
     */
    public function testHasMethodEmailRegisteredGetModuleName()
    {
        $this->assertTrue(method_exists($this->email_registered, 'getModuleName'));
        $this->assertIsString($this->email_registered->getModuleName());
    }

    /**
     * @testdox EmailRegisteredGetLabel
     */
    public function testHasMethodEmailRegisteredGetLabel()
    {
        $this->assertTrue(method_exists($this->email_registered, 'getLabel'));
        $this->assertIsString($this->email_registered->getLabel());
    }

    /**
     * @testdox EmailRegisteredGetType
     */
    public function testHasMethodEmailRegisteredGetType()
    {
        $this->assertTrue(method_exists($this->email_registered, 'getType'));
        $this->assertIsString($this->email_registered->getType());
    }

    /**
     * @testdox EmailRegisteredexchangeArray
     */
    public function testHasMethodEmailRegisteredExchangeArray()
    {
        $this->assertTrue(method_exists($this->email_registered, 'exchangeArray'));
        $this->assertIsArray($this->email_registered->exchangeArray());
    }
}