<?php
declare(strict_types=1);

namespace Tests\Feature\Email;

use App\Domain\Email\EmailTypeInterface;
use App\Domain\Email\Entity\EmailType;
use App\Model\Email\EmailTypeModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;

/**
 * Class EmailModelTest
 * @package Tests\Feature\Email
 */
class EmailTypeTest extends TestCase
{
    /**
     * @var Email\Entity\EmailType
     */
    private $email_type;

    /**
     * @var App\Model\Email\EmailTypeModel
     */
    private $email_type_model;

    public function setUp(): void
    {
        parent::setUp();
        $this->email_type = new EmailType($this->getMock('email_type.json'));

        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('raw')->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('orderBy')->andReturnSelf()
            ->shouldReceive('get')->andReturnSelf()
            ->shouldReceive('toArray')
            ->andReturn($this->email_type);

        $this->email_type_model = new EmailTypeModel($this->email_type);
    }

    /**
     * @testdox is instance of EmailTypeModel Object
     */
    public function testObjectType()
    {
        $this->assertInstanceOf(EmailTypeInterface::class, $this->email_type_model);
    }

    /**
     * @testdox getEmailTypes
     */
    public function testHasMethodGetEmailTypes()
    {
        $this->assertTrue(method_exists($this->email_type_model, 'getEmailTypes'));
    }

    public function testGetEmailTypes()
    {
        $result = $this->email_type_model->getEmailTypes();
        $this->assertSame($this->email_type, $result);
    }

    /**
     * @testdox getEmailType
     */
    public function testHasMethodGetEmailType()
    {
        $this->assertTrue(method_exists($this->email_type_model, 'getEmailType'));
    }

    public function testGetEmailType()
    {
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('raw')->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('orderBy')->andReturnSelf()
            ->shouldReceive('get')->andReturnSelf()
            ->shouldReceive('first')
            ->once()->andReturn(true);

        $result = $this->email_type_model->getEmailType(1);
        $this->assertTrue($result);
    }

    /**
     * @testdox getEmailTypeByModuleId
     */
    public function testHasMethodGetEmailTypeByModuleId()
    {
        $this->assertTrue(method_exists($this->email_type_model, 'getEmailTypeByModuleId'));
    }

    public function testGetEmailTypeByModuleId()
    {
        $result = $this->email_type_model->getEmailTypeByModuleId(1);
        $this->assertSame($this->email_type, $result);
    }

   /**
     * @testdox EmailTypeGetIdModule
     */
    public function testHasMethodEmailTypeGetIdModule()
    {
        $this->assertTrue(method_exists($this->email_type, 'getIdModule'));
        $this->assertIsInt($this->email_type->getIdModule());
    }

    /**
     * @testdox EmailTypeGetLabel
     */
    public function testHasMethodEmailTypeGetLabel()
    {
        $this->assertTrue(method_exists($this->email_type, 'getLabel'));
        $this->assertIsString($this->email_type->getLabel());
    }

    /**
     * @testdox EmailTypeGetType
     */
    public function testHasMethodEmailTypeGetType()
    {
        $this->assertTrue(method_exists($this->email_type, 'getType'));
        $this->assertIsString($this->email_type->getType());
    }

    /**
     * @testdox EmailTypeExchangeArray
     */
    public function testHasMethodEmailTypeExchangeArray()
    {
        $this->assertTrue(method_exists($this->email_type, 'exchangeArray'));
        $this->assertIsArray($this->email_type->exchangeArray());
    }
}