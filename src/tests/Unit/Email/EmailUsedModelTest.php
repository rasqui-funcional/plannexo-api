<?php
declare(strict_types=1);

namespace Tests\Feature\Email;

use App\Domain\Email\EmailUsedInterface;
use App\Domain\Email\Entity\EmailUsed;
use App\Model\Email\EmailUsedModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;

/**
 * Class EmailUsedModelTest
 * @package Tests\Feature\Email
 */
class EmailUsedModelTest extends TestCase
{
    /**
     * @var Email\Entity\EmailUsed
     */
    private $email_used;

    /**
     * @var App\Model\Email\EmailUsedModel
     */
    private $email_used_model;

    public function setUp(): void
    {
        parent::setUp();
        $this->email_used = new EmailUsed($this->getMock('email_used.json'));

        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('raw')->andReturnSelf()
            ->shouldReceive('leftJoin')->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('orderBy')->andReturnSelf()
            ->shouldReceive('get')->andReturnSelf()
            ->shouldReceive('toArray')
            ->andReturn($this->email_used);

        $this->email_used_model = new EmailUsedModel($this->email_used);
    }

    /**
     * @testdox is instance of EmailUsedModel Object
     */
    public function testObjectType()
    {
        $this->assertInstanceOf(EmailUsedInterface::class, $this->email_used_model);
    }

    /**
     * @testdox getEmailUsed
     */
    public function testHasMethodGetEmailUsed()
    {
        $this->assertTrue(method_exists($this->email_used_model, 'getEmailUsed'));
    }

    public function testGetEmailUsed()
    {
        $result = $this->email_used_model->getEmailUsed();
        $this->assertSame($this->email_used, $result);
    }

    /**
     * @testdox createEmailUsed
     */
    public function testHasMethodCreateEmailUsed()
    {
        $this->assertTrue(method_exists($this->email_used_model, 'createEmailUsed'));
    }

    public function testCreateEmailUsed()
    {
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('insert')->andReturnSelf()
            ->once()->andReturn(true);

        $result = $this->email_used_model->createEmailUsed(2);
        $this->assertTrue($result);
    }

    /**
     * @testdox updateEmailUsed
     */
    public function testHasMethodUpdateEmailUsed()
    {
        $this->assertTrue(method_exists($this->email_used_model, 'updateEmailUsed'));
    }

    public function testUpdateEmailUsed()
    {
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('update')->andReturnSelf()
            ->once()->andReturn(true);

        $result = $this->email_used_model->updateEmailUsed(2);
        $this->assertTrue($result);
    }

    /**
     * @testdox deleteEmailUsed
     */
    public function testHasMethodDeleteEmailUsed()
    {
        $this->assertTrue(method_exists($this->email_used_model, 'deleteEmailUsed'));
    }

    public function testDeleteEmailUsed()
    {
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('delete')->andReturnSelf()
            ->once()->andReturn(true);

        $result = $this->email_used_model->deleteEmailUsed(2);
        $this->assertTrue($result);
    }

    /**
     * @testdox deleteEmailUsedByModelAndModule
     */
    public function testHasMethodDeleteEmailUsedByModelAndModule()
    {
        $this->assertTrue(method_exists($this->email_used_model, 'deleteEmailUsedByModelAndModule'));
    }

    public function testDeleteEmailUsedByModelAndModule()
    {
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('delete')->andReturnSelf()
            ->once()->andReturn(true);

        $result = $this->email_used_model->deleteEmailUsedByModelAndModule(2, 1);
        $this->assertTrue($result);
    }

    /**
     * @testdox noExistsInsert
     */
    public function testHasMethodNoExistsInsert()
    {
        $this->assertTrue(method_exists($this->email_used_model, 'noExistsInsert'));
    }

    public function testNoExistsInsert()
    {
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('get')->andReturnSelf()
            ->shouldReceive('first')->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('insert')->andReturnSelf()
            ->once()->andReturn(true);

        $result = $this->email_used_model->noExistsInsert(2, 1);
        $this->assertTrue($result);
    }
}
