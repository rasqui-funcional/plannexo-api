<?php
declare(strict_types=1);

namespace Tests\Unit\User;

use App\Domain\Filters\FilterDeleteInterface;
use App\Domain\User\Entity\User;
use App\Model\FollowUp\FollowUpUserFilterDeleteModel;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

/**
 * Class FollowUpFilterDeleteTest
 * @package Tests\Feature\User
 */
class FollowUpFilterDeleteTest extends TestCase
{
    private $fup_filter_store;
    private $mock_data;
    private $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = new User(['id_user_pk' => 2, 'id_company_fk' => 2]);
        $this->mock_data = $this->getMock('follow_up_filter_store.json');
        $this->fup_filter_store = new FollowUpUserFilterDeleteModel($this->user);
    }

    /**
     * @testdox is instance of User Object
     */
    public function testObjectType()
    {
        $this->assertInstanceOf(FilterDeleteInterface::class, $this->fup_filter_store);
    }

    public function testHasMethods()
    {
        $this->assertTrue(method_exists($this->fup_filter_store, 'deleteFilter'));
        $this->assertTrue(method_exists($this->fup_filter_store, 'setData'));
    }

    public function testGetFollowUpFilterDeletes()
    {
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('delete')->andReturnTrue();

        $result = $this->fup_filter_store
            ->setData($this->mock_data['fupFilters'])
            ->deleteFilter(736);

        $this->assertSame($result, true);
    }

    public function testFollowUpFilterDeletesModelArgumentError()
    {
        $this->expectException(\ArgumentCountError::class);
        $result = $this->fup_filter_store->deleteFilter();

        $this->expectException(\TypeError::class);
        $result = $this->fup_filter_store->deleteFilter('string');
    }

    public function testFollowUpFilterDeletesModelParamType()
    {
        $this->expectException(\TypeError::class);
        $this->fup_filter_store = new FollowUpUserFilterDeleteModel();
    }

    public function testFollowException()
    {
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('delete')->andThrow(\Exception::class);

        $this->expectException(\Exception::class);
        $this->fup_filter_store
            ->setData($this->mock_data['fupFilters'])
            ->deleteFilter(736);
    }
}
