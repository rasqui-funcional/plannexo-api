<?php
declare(strict_types=1);

namespace Tests\Unit\User;
use App\Domain\FollowUp\Filters\FollowUpFiltersFiltered;
use Tests\TestCase;

/**
 * Class FollowUpFilterFilteredTest
 * @package Tests\Unit\FollowUp
 */
class FollowUpFilterFilteredTest extends TestCase
{

    private $follow_up_filter;

    public function setUp(): void
    {
        $this->follow_up_filter = $this->getMock('filter_supplier.json');

        $this->follow_up_filter = new FollowUpFiltersFiltered($this->follow_up_filter['filters']);

    }

    /**
     * @testdox is instance of User Object
     */
    public function testFormatedDates()
    {
        $this->assertEquals('2015-08-05 00:00:00', $this->follow_up_filter->getFilters()['specialFilters']['dateRequestOrder']['start']);
        $this->assertEquals('2015-08-15 23:59:59', $this->follow_up_filter->getFilters()['specialFilters']['dateRequestOrder']['end']);

        $this->assertEquals('2015-08-05 00:00:00', $this->follow_up_filter->getFilters()['specialFilters']['datePurchaseOrder']['start']);
        $this->assertEquals('2015-08-15 23:59:59', $this->follow_up_filter->getFilters()['specialFilters']['datePurchaseOrder']['end']);

        $this->assertEquals('2015-08-05 00:00:00', $this->follow_up_filter->getFilters()['specialFilters']['dateExpected']['start']);
        $this->assertEquals('2015-08-15 23:59:59', $this->follow_up_filter->getFilters()['specialFilters']['dateExpected']['end']);
    }

    public function testSkuData()
    {
        $filters = $this->follow_up_filter->getFilters();
        $this->assertIsArray($filters['sku']['suppliersId']);
        $this->assertTrue(in_array("123", $filters['sku']['suppliersId']));
        $this->assertTrue(in_array("456", $filters['sku']['suppliersId']));

        $this->assertIsArray($filters['sku']['itemCode']);
        $this->assertTrue(in_array("567", $filters['sku']['itemCode']));
        $this->assertTrue(in_array("890", $filters['sku']['itemCode']));

        $this->assertIsArray($filters['sku']['requestOrderIds']);
        $this->assertTrue(in_array("567", $filters['sku']['requestOrderIds']));

        $this->assertIsArray($filters['sku']['purchaseOrderIds']);
        $this->assertTrue(empty($filters['sku']['purchaseOrderIds']));
        $this->assertTrue(count($filters['sku']['purchaseOrderIds']) == 0);

    }

}
