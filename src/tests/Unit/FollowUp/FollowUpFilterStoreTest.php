<?php
declare(strict_types=1);

namespace Tests\Unit\User;

use App\Domain\Filters\FilterStoreInterface;
use App\Domain\User\Entity\User;
use App\Model\FollowUp\FollowUpUserFilterStoreModel;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

/**
 * Class FollowUpFilterStoreTest
 * @package Tests\Feature\User
 */
class FollowUpFilterStoreTest extends TestCase
{

    private $fup_filter_store;
    private $mock_data;
    private $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = new User(['id_user_pk' => 2, 'id_company_fk' => 2]);
        $this->mock_data = $this->getMock('follow_up_filter_store.json');


        $this->fup_filter_store = new FollowUpUserFilterStoreModel($this->user);

    }

    /**
     * @testdox is instance of User Object
     */
    public function testObjectType()
    {
        $this->assertInstanceOf(FilterStoreInterface::class, $this->fup_filter_store);
    }


    public function testHasMethods()
    {
        $this->assertTrue(method_exists($this->fup_filter_store, 'saveFilter'));
        $this->assertTrue(method_exists($this->fup_filter_store, 'setData'));
    }

    public function testGetFollowUpFilterStores()
    {

        $next_id = $this->getMock('follow_up_filter_store_next_id.json');
        $mock_response = $this->getMock('follow_up_filter_store_response.json');
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('selectRaw')->andReturnSelf()
            ->shouldReceive('first')->andReturn((object)$next_id);

        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('insert')->andReturnTrue();

        $result = $this->fup_filter_store
            ->setData($this->mock_data['fupFilters'])
            ->saveFilter();

        $this->assertSame($result, $mock_response);
    }

    public function testFollowUpFilterStoresModelParamType()
    {
        $this->expectException(\TypeError::class);
        $this->fup_filter_store = new FollowUpUserFilterStoreModel();
    }

    public function testFollowException()
    {
        $next_id = $this->getMock('follow_up_filter_store_next_id.json');
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('selectRaw')->andReturnSelf()
            ->shouldReceive('first')->andReturn((object)$next_id);

        DB::shouldReceive('table')->andReturn(\Exception::class);

        $this->expectException(\Exception::class);
        $this->fup_filter_store
            ->setData($this->mock_data['fupFilters'])
            ->saveFilter();
    }

}
