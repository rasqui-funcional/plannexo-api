<?php
declare(strict_types=1);

namespace Tests\Unit\User;

use App\Domain\Company\CompanyGroupingInterface;
use App\Domain\Company\CompanyPoliciesInterface;
use App\Domain\Company\Entity\Company;
use App\Domain\FollowUp\Filters\FollowUpFilters;
use App\Domain\User\Entity\User;
use App\Domain\User\UserEstablishmentsInterface;
use App\Domain\User\UserManagersInterface;
use App\Domain\User\UserStandardDepositInterface;
use Tests\TestCase;

/**
 * Class FollowUpFilterTest
 * @package Tests\Unit\FollowUp
 */
class FollowUpFilterTest extends TestCase
{

    private $user_establishment;
    private $user_managers;
    private $user_standard_deposit;
    private $policies;
    private $company_grouping;
    private $user;
    private $company;
    private $follow_up_filters;

    public function setUp(): void
    {

        parent::setUp();
        $this->user = new User(['id_company_fk' => 2, 'id_user_pk' => 1]);
        $this->company = new Company(['id_company_fk' => 2]);

        $this->user_establishment = $this->getMockBuilder(UserEstablishmentsInterface::class)
            ->setConstructorArgs([$this->user->getEstablishments()])
            ->getMock();

        $this->user_managers = $this->getMockBuilder(UserManagersInterface::class)
            ->setConstructorArgs([$this->user])
            ->getMock();

        $this->user_standard_deposit = $this->getMockBuilder(UserStandardDepositInterface::class)
            ->setConstructorArgs([$this->user])
            ->getMock();

        $this->policies = $this->getMockBuilder(CompanyPoliciesInterface::class)
            ->setConstructorArgs([$this->company])
            ->getMock();

         $this->company_grouping = $this->getMockBuilder(CompanyGroupingInterface::class)
             ->getMock();

         $this->follow_up_filters = new FollowUpFilters(
             $this->user_establishment,
             $this->user_managers,
             $this->user_standard_deposit,
             $this->policies,
             $this->company_grouping
         );
    }

    public function testHasMethodGetFollowUpFilters()
    {
        $this->assertTrue(method_exists($this->follow_up_filters, 'getFollowUpFilters'));
        $this->assertTrue(method_exists($this->follow_up_filters, 'getEstablishments'));
        $this->assertTrue(method_exists($this->follow_up_filters, 'getUserManagers'));
        $this->assertTrue(method_exists($this->follow_up_filters, 'getCompanyPolicies'));
        $this->assertTrue(method_exists($this->follow_up_filters, 'getStandardDeposit'));
        $this->assertTrue(method_exists($this->follow_up_filters, 'getCompanyGrouping'));
    }

    /**
     * @testdox is instance of User Object
     */
    public function testGetFollowUpFilters()
    {
        $policies = $this->getMock('company_policies.json');
        $managers = $this->getMock('user_managers.json');
        $grouping = $this->getMock('company_grouping.json');
        $deposits = $this->getMock('user_standard_deposit.json');
        $estabs = $this->getMock('user_estabs.json');
        $filters = $this->getMock('follow_up_filters.json');

        $this->user_establishment->method('getEstablishments')
            ->willReturn($estabs);

        $this->user_standard_deposit->method('getUserStandardDeposit')
            ->willReturn($deposits);

        $this->policies->method('getCompanyPolicies')
            ->willReturn($policies);

        $this->user_managers->method('getUserManagers')
            ->willReturn($managers);

        $this->company_grouping->method('getCompanyGrouping')
            ->willReturn($grouping);

        $this->assertEquals($estabs, $this->follow_up_filters->getEstablishments());
        $this->assertEquals($deposits, $this->follow_up_filters->getStandardDeposit());
        $this->assertEquals($policies, $this->follow_up_filters->getCompanyPolicies());
        $this->assertEquals($managers, $this->follow_up_filters->getUserManagers());
        $this->assertEquals($grouping, $this->follow_up_filters->getCompanyGrouping());

        $this->assertEquals($filters, $this->follow_up_filters->getFollowUpFilters());
    }
}
