<?php
declare(strict_types=1);

namespace Tests\Unit\User;

use App\Domain\Filters\FilterUpdateInterface;
use App\Domain\User\Entity\User;
use App\Model\FollowUp\FollowUpUserFilterUpdateModel;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

/**
 * Class FollowUpFilterUpdateTest
 * @package Tests\Feature\User
 */
class FollowUpFilterUpdateTest extends TestCase
{

    private $fup_filter_store;
    private $mock_data;
    private $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = new User(['id_user_pk' => 2, 'id_company_fk' => 2]);
        $this->mock_data = $this->getMock('follow_up_filter_store.json');


        $this->fup_filter_store = new FollowUpUserFilterUpdateModel($this->user);

    }

    /**
     * @testdox is instance of User Object
     */
    public function testObjectType()
    {
        $this->assertInstanceOf(FilterUpdateInterface::class, $this->fup_filter_store);
    }


    public function testHasMethods()
    {
        $this->assertTrue(method_exists($this->fup_filter_store, 'updateFilter'));
        $this->assertTrue(method_exists($this->fup_filter_store, 'setData'));
    }

    public function testGetFollowUpFilterUpdates()
    {

        $mock_response = $this->getMock('follow_up_filter_update_response.json');

        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('update')->andReturnTrue();

        $result = $this->fup_filter_store
            ->setData($this->mock_data['fupFilters'])
            ->updateFilter(736);

        $this->assertSame($result, $mock_response);
    }

    public function testFollowUpFilterUpdatesModelArgumentError()
    {
        $this->expectException(\ArgumentCountError::class);
        $result = $this->fup_filter_store
            ->setData($this->mock_data['fupFilters'])
            ->updateFilter();

    }
    public function testFollowUpFilterUpdatesModelParamType()
    {
        $this->expectException(\TypeError::class);
        $this->fup_filter_store = new FollowUpUserFilterUpdateModel();
    }

    public function testFollowException()
    {
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('update')->andThrow(\Exception::class);

        $this->expectException(\Exception::class);
        $this->fup_filter_store
            ->setData($this->mock_data['fupFilters'])
            ->updateFilter(736);
    }

}
