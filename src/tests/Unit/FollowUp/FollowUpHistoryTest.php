<?php
declare(strict_types=1);

namespace Tests\Unit\FollowUp;

use App\Http\Services\FollowUpHistoryService;
use Tests\TestCase;

class FollowUpHistoryTest extends TestCase
{
    private $follow_up_history;
    private $mock_data;

    public function setUp(): void
    {

        $this->mock_data = $this->getMock('follow_up_pending_items.json');
        $status_code = 200;
        $client_mock = $this->createHttpClientMock($this->mock_data, $status_code);

        $this->follow_up_history = new FollowUpHistoryService($client_mock);
    }


    public function testObjectType()
    {
        $this->assertInstanceOf("App\Domain\FollowUp\FollowUpHistoryInterface", $this->follow_up_history);
    }

    public function testClientType()
    {
        $this->assertInstanceOf("App\Http\Client\HttpServiceInterface", $this->follow_up_history->getClient());
    }

    public function testGetHistory()
    {
        $response = $this->follow_up_history
            ->setCompanyId(2)
            ->setSupplierId("123")
            ->getHistory();
        $this->assertSame($this->mock_data, $response);
    }

    public function testCompanyParamType()
    {

        $this->expectException(\TypeError::class);
        $this->follow_up_history->setCompanyId("2");
    }

    public function testSupplierParamType()
    {

        $this->expectException(\TypeError::class);
        $this->follow_up_history->setSupplierId(2);
    }

    public function testCompanySupplierReturnType()
    {
        $company = $this->follow_up_history->setCompanyId(2);
        $supplier = $this->follow_up_history->setSupplierId("2");
        $this->assertInstanceOf("App\Http\Services\FollowUpHistoryService", $company);
        $this->assertInstanceOf("App\Http\Services\FollowUpHistoryService", $supplier);

    }


    public function testInterfaceContract()
    {
        $this->assertTrue(
            method_exists($this->follow_up_history->getClient(), 'setPath'),
            'Class does not implemented the method setPath on interface contract'
        );
        $this->assertTrue(
            method_exists($this->follow_up_history->getClient(), 'method'),
            'Class does not implemented the method method on interface contract'
        );
        $this->assertTrue(
            method_exists($this->follow_up_history->getClient(), 'setHeaders'),
            'Class does not implemented the method setHeaders on interface contract'
        );
        $this->assertTrue(
            method_exists($this->follow_up_history->getClient(), 'setData'),
            'Class does not implemented the method getStatusCode on interface contract'
        );
        $this->assertTrue(
            method_exists($this->follow_up_history->getClient(), 'send'),
            'Class does not implemented the method send on interface contract'
        );
        $this->assertTrue(
            method_exists($this->follow_up_history->getClient(), 'getStatusCode'),
            'Class does not implemented the method getStatusCode on interface contract'
        );
        $this->assertTrue(
            method_exists($this->follow_up_history->getClient(), 'getContents'),
            'Class does not implemented the method getContents on interface contract'
        );

        $this->assertTrue(
            method_exists($this->follow_up_history->getClient(), 'decode'),
            'Class does not implemented the method decode on interface contract'
        );
    }

    public function testConstructError()
    {
        $this->expectException(\ArgumentCountError::class);
        $this->follow_up_history = new FollowUpHistoryService();
    }

    public function testTypeErrorParamException()
    {
        $this->expectException(\TypeError::class);
        $this->follow_up_history = new FollowUpHistoryService(10);
    }

    public function testErrorGetHistory500Exception()
    {
        $status_code = 500;
        $client_mock = $this->createHttpClientMock([], $status_code);

        $this->follow_up_history = new FollowUpHistoryService($client_mock);
        $this->expectException(\Exception::class);

        $this->follow_up_history
            ->setCompanyId(2)
            ->setSupplierId("123")
            ->getHistory();
    }

}
