<?php
declare(strict_types=1);

namespace Tests\Unit\FollowUp;

use App\Http\Services\FollowUpSettingsService;
use Tests\TestCase;

class FollowUpSettingsTest extends TestCase
{
    private $follow_up_sku;
    private $mock_data;

    public function setUp(): void
    {
        $this->mock_data = $this->getMock('follow_up_settings.json');
        $status_code = 200;
        $client_mock = $this->createHttpClientMock($this->mock_data, $status_code);

        $this->follow_up_sku = new FollowUpSettingsService($client_mock);

    }


    public function testObjectType()
    {
        $this->assertInstanceOf("App\Domain\FollowUp\FollowUpSettingsInterface", $this->follow_up_sku);
    }

    public function testClientType()
    {
        $this->assertInstanceOf("App\Http\Client\HttpServiceInterface", $this->follow_up_sku->getClient());
    }

    public function testUpdateSettings()
    {

        $this->mock_data = $this->getMock('follow_up_settings.json');
        $status_code = 200;
        $client_mock = $this->createHttpClientMock($this->mock_data, $status_code);

        $this->follow_up_sku = new FollowUpSettingsService($client_mock);

        $response = $this->follow_up_sku
            ->setCompanyId(2)
            ->setData($this->mock_data['settings'])
            ->updateSettings();

        $this->assertSame($response, $this->mock_data);
    }

    public function testGetSettings()
    {
        $this->mock_data = $this->getMock('follow_up_settings.json');
        $status_code = 200;
        $client_mock = $this->createHttpClientMock($this->mock_data, $status_code);

        $this->follow_up_sku = new FollowUpSettingsService($client_mock);

        $response = $this->follow_up_sku
            ->setCompanyId(2)
            ->getSettings();
        $this->assertSame($this->mock_data, $response);
    }

    public function testCompanyParamType()
    {

        $this->expectException(\TypeError::class);
        $this->follow_up_sku->setCompanyId("2");
    }

    public function testCompanySupplierReturnType()
    {
        $company = $this->follow_up_sku->setCompanyId(2);

        $this->assertInstanceOf("App\Http\Services\FollowUpSettingsService", $company);
    }


    public function testInterfaceContract()
    {
        $this->assertTrue(
            method_exists($this->follow_up_sku->getClient(), 'setPath'),
            'Class does not implemented the method setPath on interface contract'
        );
        $this->assertTrue(
            method_exists($this->follow_up_sku->getClient(), 'method'),
            'Class does not implemented the method method on interface contract'
        );
        $this->assertTrue(
            method_exists($this->follow_up_sku->getClient(), 'setHeaders'),
            'Class does not implemented the method setHeaders on interface contract'
        );
        $this->assertTrue(
            method_exists($this->follow_up_sku->getClient(), 'setData'),
            'Class does not implemented the method getStatusCode on interface contract'
        );
        $this->assertTrue(
            method_exists($this->follow_up_sku->getClient(), 'send'),
            'Class does not implemented the method send on interface contract'
        );
        $this->assertTrue(
            method_exists($this->follow_up_sku->getClient(), 'getStatusCode'),
            'Class does not implemented the method getStatusCode on interface contract'
        );
        $this->assertTrue(
            method_exists($this->follow_up_sku->getClient(), 'getContents'),
            'Class does not implemented the method getContents on interface contract'
        );

        $this->assertTrue(
            method_exists($this->follow_up_sku->getClient(), 'decode'),
            'Class does not implemented the method decode on interface contract'
        );
    }

    public function testConstructError()
    {
        $this->expectException(\ArgumentCountError::class);
        $this->follow_up_sku = new FollowUpSettingsService();
    }

    public function testTypeErrorParamException()
    {
        $this->expectException(\TypeError::class);
        $this->follow_up_sku = new FollowUpSettingsService(10);
    }

    public function testErrorGetPendingSkus500Exception()
    {
        $status_code = 500;
        $client_mock = $this->createHttpClientMock([], $status_code);

        $this->follow_up_sku = new FollowUpSettingsService($client_mock);
        $this->expectException(\Exception::class);

        $this->follow_up_sku
            ->setCompanyId(2)
            ->setData($this->mock_data)
            ->updateSettings();
    }
}
