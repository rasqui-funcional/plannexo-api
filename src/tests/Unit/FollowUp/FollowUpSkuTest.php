<?php
declare(strict_types=1);

namespace Tests\Unit\FollowUp;

use App\Http\Services\FollowUpSkuService;
use Tests\TestCase;

class FollowUpSkuTest extends TestCase
{
    private $follow_up_sku;
    private $mock_data;

    public function setUp(): void
    {

        $this->mock_data = $this->getMock('follow_up_pending_items.json');
        $status_code = 200;
        $client_mock = $this->createHttpClientMock($this->mock_data, $status_code);

        $this->follow_up_sku = new FollowUpSkuService($client_mock);
    }


    public function testObjectType()
    {
        $this->assertInstanceOf("App\Domain\FollowUp\FollowUpSkuInterface", $this->follow_up_sku);
    }

    public function testClientType()
    {
        $this->assertInstanceOf("App\Http\Client\HttpServiceInterface", $this->follow_up_sku->getClient());
    }

    public function testGetPendingSkus()
    {
        $response = $this->follow_up_sku
            ->setCompanyId(2)
            ->setSupplierId("123")
            ->getPendingSkus();
        $this->assertSame($this->mock_data, $response);
    }

    public function testCompanyParamType()
    {

        $this->expectException(\TypeError::class);
        $this->follow_up_sku->setCompanyId("2");
    }

    public function testSupplierParamType()
    {

        $this->expectException(\TypeError::class);
        $this->follow_up_sku->setSupplierId(2);
    }

    public function testCompanySupplierReturnType()
    {
        $company = $this->follow_up_sku->setCompanyId(2);
        $supplier = $this->follow_up_sku->setSupplierId("2");
        $this->assertInstanceOf("App\Http\Services\FollowUpSkuService", $company);
        $this->assertInstanceOf("App\Http\Services\FollowUpSkuService", $supplier);

    }


    public function testInterfaceContract()
    {
        $this->assertTrue(
            method_exists($this->follow_up_sku->getClient(), 'setPath'),
            'Class does not implemented the method setPath on interface contract'
        );
        $this->assertTrue(
            method_exists($this->follow_up_sku->getClient(), 'method'),
            'Class does not implemented the method method on interface contract'
        );
        $this->assertTrue(
            method_exists($this->follow_up_sku->getClient(), 'setHeaders'),
            'Class does not implemented the method setHeaders on interface contract'
        );
        $this->assertTrue(
            method_exists($this->follow_up_sku->getClient(), 'setData'),
            'Class does not implemented the method getStatusCode on interface contract'
        );
        $this->assertTrue(
            method_exists($this->follow_up_sku->getClient(), 'send'),
            'Class does not implemented the method send on interface contract'
        );
        $this->assertTrue(
            method_exists($this->follow_up_sku->getClient(), 'getStatusCode'),
            'Class does not implemented the method getStatusCode on interface contract'
        );
        $this->assertTrue(
            method_exists($this->follow_up_sku->getClient(), 'getContents'),
            'Class does not implemented the method getContents on interface contract'
        );

        $this->assertTrue(
            method_exists($this->follow_up_sku->getClient(), 'decode'),
            'Class does not implemented the method decode on interface contract'
        );
    }

    public function testConstructError()
    {
        $this->expectException(\ArgumentCountError::class);
        $this->follow_up_sku = new FollowUpSkuService();
    }

    public function testTypeErrorParamException()
    {
        $this->expectException(\TypeError::class);
        $this->follow_up_sku = new FollowUpSkuService(10);
    }

    public function testErrorGetPendingSkus500Exception()
    {
        $status_code = 500;
        $client_mock = $this->createHttpClientMock([], $status_code);

        $this->follow_up_sku = new FollowUpSkuService($client_mock);
        $this->expectException(\Exception::class);

        $this->follow_up_sku
            ->setCompanyId(2)
            ->setSupplierId("123")
            ->getPendingSkus();
    }

    public function testGetPendingSkusFilteredParamType()
    {

        $this->expectException(\ArgumentCountError::class);
        $this->follow_up_sku->setCompanyId(2)
            ->setData()
            ->getPendingSkusFiltered();

        $this->expectException(\TypeError::class);
        $this->follow_up_sku->setCompanyId(2)
            ->setData('2')
            ->getPendingSkusFiltered();

        $this->expectException(\TypeError::class);
        $this->follow_up_sku->setCompanyId(2)
            ->setData(2)
            ->getPendingSkusFiltered();
    }

    public function testGetPendingSkusFiltered500Exception()
    {
        $status_code = 500;
        $client_mock = $this->createHttpClientMock([], $status_code);

        $this->follow_up_sku = new FollowUpSkuService($client_mock);
        $this->expectException(\Exception::class);

        $this->follow_up_sku
            ->setCompanyId(2)
            ->setSupplierId("123")
            ->setData([])
            ->getPendingSkusFiltered();
    }

    public function testGetPendingSkusFiltered()
    {
        $response = $this->follow_up_sku
            ->setCompanyId(2)
            ->setSupplierId("123")
            ->setData([])
            ->getPendingSkusFiltered();
        $this->assertSame($this->mock_data, $response);
    }
}
