<?php
declare(strict_types=1);

namespace Tests\Unit\FollowUp;

use App\Http\Services\FollowUpStatusService;
use Tests\TestCase;

class FollowUpStatusTest extends TestCase
{
    private $mock_data;
    private $followup_status;

    public function setUp(): void
    {

        $this->mock_data = $this->getMock('follow_up_status.json');
        $status_code = 200;
        $client_mock = $this->createHttpClientMock($this->mock_data, $status_code);

        $this->followup_status = new FollowUpStatusService($client_mock);

    }


    public function testObjectType()
    {
        $this->assertInstanceOf("App\Domain\FollowUp\FollowUpStatusInterface", $this->followup_status);
    }

    public function testClientType()
    {
        $this->assertInstanceOf("App\Http\Client\HttpServiceInterface", $this->followup_status->getClient());
    }

    public function testgetFollowUpStatus()
    {
        $data = $this->followup_status->setCompanyId(2)->getFollowUpStatus();
        $this->assertArrayHasKey('statuses', $data);
        $this->assertIsArray($data);
    }

    public function testInterfaceContract()
    {
        $this->assertTrue(
            method_exists($this->followup_status->getClient(), 'setPath'),
            'Class does not implemented the method setPath on interface contract'
        );
        $this->assertTrue(
            method_exists($this->followup_status->getClient(), 'method'),
            'Class does not implemented the method method on interface contract'
        );
        $this->assertTrue(
            method_exists($this->followup_status->getClient(), 'setHeaders'),
            'Class does not implemented the method setHeaders on interface contract'
        );
        $this->assertTrue(
            method_exists($this->followup_status->getClient(), 'setData'),
            'Class does not implemented the method getStatusCode on interface contract'
        );
        $this->assertTrue(
            method_exists($this->followup_status->getClient(), 'send'),
            'Class does not implemented the method send on interface contract'
        );
        $this->assertTrue(
            method_exists($this->followup_status->getClient(), 'getStatusCode'),
            'Class does not implemented the method getStatusCode on interface contract'
        );
        $this->assertTrue(
            method_exists($this->followup_status->getClient(), 'getContents'),
            'Class does not implemented the method getContents on interface contract'
        );

        $this->assertTrue(
            method_exists($this->followup_status->getClient(), 'decode'),
            'Class does not implemented the method decode on interface contract'
        );
    }

    public function testCompanyParamType()
    {

        $this->expectException(\TypeError::class);
        $this->followup_status->setCompanyId("2");
    }

    public function testCompanyReturnType()
    {
        $response = $this->followup_status->setCompanyId(2);
        $this->assertInstanceOf("App\Http\Services\FollowUpStatusService", $response);
    }

    public function testConstructError()
    {
        $this->expectException(\ArgumentCountError::class);
        $this->followup_status = new FollowUpStatusService();
    }

    public function testTypeErrorParamException()
    {
        $this->expectException(\TypeError::class);
        $this->followup_status = new FollowUpStatusService(10);
    }

    public function testErrorGetSummary500Exception()
    {
        $status_code = 500;
        $client_mock = $this->createHttpClientMock([], $status_code);

        $this->followup_status = new FollowUpStatusService($client_mock);
        $this->expectException(\Exception::class);

        $this->followup_status->setCompanyId(2)->getFollowUpStatus();
    }

    public function testGetInfoError500Exception()
    {
        $status_code = 500;
        $client_mock = $this->createHttpClientMock([], $status_code);

        $this->followup_status = new FollowUpStatusService($client_mock);
        $this->expectException(\Exception::class);

        $this->followup_status->setCompanyId(2)->getFollowUpStatus();
    }
}
