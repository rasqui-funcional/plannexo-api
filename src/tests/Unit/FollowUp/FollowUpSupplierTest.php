<?php
declare(strict_types=1);

namespace Tests\Unit\FollowUp;

use App\Http\Services\FollowUpSupplierService;
use Tests\TestCase;

class FollowUpSupplierTest extends TestCase
{
    private FollowUpSupplierService $follow_up;
    private $mock_data;

    public function setUp(): void
    {
        $this->mock_data = $this->getMock('follow_up.json');
        $status_code = 200;
        $client_mock = $this->createHttpClientMock($this->mock_data, $status_code);

        $this->follow_up = new FollowUpSupplierService($client_mock);
    }


    public function testObjectType()
    {
        $this->assertInstanceOf("App\Domain\FollowUp\FollowUpSupplierInterface", $this->follow_up);
    }

    public function testClientType()
    {
        $this->assertInstanceOf("App\Http\Client\HttpServiceInterface", $this->follow_up->getClient());
    }

    public function testGetFollowUpSummary()
    {
        $this->follow_up->setCompanyId(2);
        $this->assertSame($this->mock_data, $this->follow_up->getSummary());
    }

    public function testGetFollowUpSupplierInfo()
    {
        $this->mock_data = $this->getMock('follow_up_supplier_info.json');
        $status_code = 200;
        $client_mock = $this->createHttpClientMock($this->mock_data, $status_code);

        $this->follow_up = new FollowUpSupplierService($client_mock);
        $this->follow_up
            ->setSupplierId("1")
            ->setCompanyId(2);

        $this->assertSame($this->mock_data, $this->follow_up->getInfo());
    }

    public function testSupplierParamType()
    {

        $this->expectException(\TypeError::class);
        $this->follow_up->setSupplierId(2);
    }

    public function testSupplierReturnType()
    {
        $response = $this->follow_up->setCompanyId(2);
        $this->assertInstanceOf("App\Http\Services\FollowUpSupplierService", $response);
    }

    public function testCompanyParamType()
    {

        $this->expectException(\TypeError::class);
        $this->follow_up->setCompanyId("2");
    }

    public function testCompanyReturnType()
    {
        $response = $this->follow_up->setCompanyId(2);
        $this->assertInstanceOf("App\Http\Services\FollowUpSupplierService", $response);
    }

    public function testInterfaceContract()
    {
        $this->assertTrue(
            method_exists($this->follow_up->getClient(), 'setPath'),
            'Class does not implemented the method setPath on interface contract'
        );
        $this->assertTrue(
            method_exists($this->follow_up->getClient(), 'method'),
            'Class does not implemented the method method on interface contract'
        );
        $this->assertTrue(
            method_exists($this->follow_up->getClient(), 'setHeaders'),
            'Class does not implemented the method setHeaders on interface contract'
        );
        $this->assertTrue(
            method_exists($this->follow_up->getClient(), 'setData'),
            'Class does not implemented the method getStatusCode on interface contract'
        );
        $this->assertTrue(
            method_exists($this->follow_up->getClient(), 'send'),
            'Class does not implemented the method send on interface contract'
        );
        $this->assertTrue(
            method_exists($this->follow_up->getClient(), 'getStatusCode'),
            'Class does not implemented the method getStatusCode on interface contract'
        );
        $this->assertTrue(
            method_exists($this->follow_up->getClient(), 'getContents'),
            'Class does not implemented the method getContents on interface contract'
        );

        $this->assertTrue(
            method_exists($this->follow_up->getClient(), 'decode'),
            'Class does not implemented the method decode on interface contract'
        );
    }

    public function testConstructError()
    {
        $this->expectException(\ArgumentCountError::class);
        $this->follow_up = new FollowUpSupplierService();
    }

    public function testTypeErrorParamException()
    {
        $this->expectException(\TypeError::class);
        $this->follow_up = new FollowUpSupplierService(10);
    }

    public function testErrorGetSummary500Exception()
    {
        $status_code = 500;
        $client_mock = $this->createHttpClientMock([], $status_code);

        $this->follow_up = new FollowUpSupplierService($client_mock);
        $this->expectException(\Exception::class);

        $this->follow_up
            ->setSupplierId("1")
            ->setCompanyId(2)
            ->getSummary();
    }

    public function testGetInfoError500Exception()
    {
        $status_code = 500;
        $client_mock = $this->createHttpClientMock([], $status_code);

        $this->follow_up = new FollowUpSupplierService($client_mock);
        $this->expectException(\Exception::class);

        $this->follow_up
            ->setSupplierId("1")
            ->setCompanyId(2)
            ->getInfo();
    }

    public function testGetQueryStringFollowUpSummary()
    {
        $filters = $this->getMock('filter_supplier.json');

        $this->mock_data = $this->getMock('follow_up.json');
        $status_code = 200;
        $client_mock = $this->createHttpClientMock($this->mock_data, $status_code);

        $this->follow_up = new FollowUpSupplierService($client_mock);
        $this->follow_up->setCompanyId(2);
        $this->assertSame($this->mock_data, $this->follow_up->setData($filters)->getQuerySummary());
    }

    public function testGetQuerySummaryError500Exception()
    {
        $filters = $this->getMock('filter_supplier.json');

        $status_code = 500;
        $client_mock = $this->createHttpClientMock([], $status_code);

        $this->follow_up = new FollowUpSupplierService($client_mock);

        $this->expectException(\Exception::class);

        $this->follow_up->setCompanyId(2);
        $this->follow_up->setData($filters)->getQuerySummary();
    }
}
