<?php
declare(strict_types=1);

namespace Tests\Unit\User;

use App\Domain\Filters\FilterListInterface;
use App\Domain\User\Entity\User;
use App\Model\FollowUp\FollowUpUserFilterListModel;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

/**
 * Class FollowUpUserFilterTest
 * @package Tests\Feature\User
 */
class FollowUpUserFilterTest extends TestCase
{

    private $fup_user_filter;
    private $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = new User(['id_user_pk' => 2, 'id_company_fk' => 2]);

        $this->fup_user_filter = new FollowUpUserFilterListModel($this->user);

    }

    /**
     * @testdox is instance of User Object
     */
    public function testObjectType()
    {
        $this->assertInstanceOf(FilterListInterface::class, $this->fup_user_filter);
    }


    public function testHasMethods()
    {
        $this->assertTrue(method_exists($this->fup_user_filter, 'getFilters'));
        $this->assertTrue(method_exists($this->fup_user_filter, 'getFilter'));
    }

    public function testGetFollowUpGetUserFilters()
    {

        $mock_response = $this->getMock('follow_up_user_filters.json');
        DB::shouldReceive('table')->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('orderBy')->andReturnSelf()
            ->shouldReceive('get')->andReturnSelf()
            ->shouldReceive('toArray')->andReturn($mock_response);

        $result = $this->fup_user_filter->getFilters();

        $this->assertSame($result, $mock_response);
    }

    public function testGetFollowUpGetUserFiltersException()
    {

        DB::shouldReceive('table')->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('orderBy')->andReturnSelf()
            ->shouldReceive('get')->andReturnSelf()
            ->shouldReceive('toArray')->andThrow(\Exception::class);

        $this->expectException(\Exception::class);

        $this->fup_user_filter->getFilters();
    }

    public function testFollowUpUserFiltersModelParamType()
    {
        $this->expectException(\TypeError::class);
        $this->fup_user_filter = new FollowUpUserFilterListModel();
    }

    public function testGetFollowUpGetUserFilter()
    {

        $mock_response = $this->getMock('follow_up_user_filters.json');
        DB::shouldReceive('table')->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('orderBy')->andReturnSelf()
            ->shouldReceive('get')->andReturnSelf()
            ->shouldReceive('toArray')->andReturn($mock_response);

        $result = $this->fup_user_filter->getFilter(1);

        $this->assertSame($result, $mock_response);
    }

    public function testGetFollowUpGetUserFilterException()
    {

        DB::shouldReceive('table')->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('orderBy')->andReturnSelf()
            ->shouldReceive('get')->andReturnSelf()
            ->shouldReceive('toArray')->andThrow(\Exception::class);

        $this->expectException(\Exception::class);

        $this->fup_user_filter->getFilter(1);
    }

    public function testGetFollowUpGetUserFilterTypeErrorException()
    {

        $this->expectException(\TypeError::class);

        $this->fup_user_filter->getFilter();
        $this->fup_user_filter->getFilter('7');
    }

}
