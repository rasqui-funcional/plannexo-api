<?php
declare(strict_types=1);
namespace Tests\Feature\Helpers;

use App\Helpers\MaskHelper;
use Tests\TestCase;

class MaskHelperTest extends TestCase
{
    private $helper;

    protected function setUp(): void
    {
        $this->helper = new MaskHelper();    
    }
    
    /** 
     * @testdox add mask to cnpj or cpf
    */
    public function testCnpjCpf()
    {
        $this->assertEquals("123.465.789-10", $this->helper->cnpjCpf("12346578910"));
    }

    public function testCnpjCpfEmpty()
    {
        $this->assertEquals("", $this->helper->cnpjCpf(""));
    }
}