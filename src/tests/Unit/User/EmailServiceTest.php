<?php
declare(strict_types=1);

namespace Tests\Feature\User;


use App\Domain\Email\Services\EmailService;
use App\Http\Services\EmailSendingService;
use Tests\TestCase;

class EmailServiceTest extends TestCase
{
    /**
    /**
     * @var EmailService
     */
    private EmailService $service;

    /**
     * @var array;
     */
    private $mock_data;

    public function setUp(): void
    {
        $status_code = 200;
        $this->mock_data = $this->getMock('email.json');
        $client_mock = $this->createHttpClientMock($this->mock_data, $status_code);

        $this->service = new EmailSendingService($client_mock);
        $this->service->exchangeArray($this->mock_data);
    }

    public function testObjectType()
    {
        $this->assertInstanceOf('app\Domain\Email\Services\EmailService', $this->service);
    }

    public function testSetSenderEmptyOrNull()
    {
        $str = '';

        $this->service->setSender($str);
        $this->assertEmpty($this->service->getSender());

        $this->service->setSender(null);
        $this->assertEquals(null, $this->service->getSender());
    }

    public function testGetSenderNotEmpty()
    {
        $this->service->setSender('email@dominio.com');
        $this->assertNotEmpty($this->service->getSender());
        $this->assertEquals('email@dominio.com', $this->service->getSender());
        $this->assertIsString($this->service->getSender());
    }

    public function testSetReceiversArrayOrNull()
    {
        $arr = [];

        $this->service->setReceivers($arr);
        $this->assertEmpty($this->service->getReceivers());

        $this->service->setReceivers(null);
        $this->assertEquals(null, $this->service->getReceivers());

        return $arr;
    }

    /**
     * @depends testSetReceiversArrayOrNull
     * @param array $arr
     */
    public function testGetReceiversNotEmpty(array $arr)
    {
        array_push($arr, 'email@dominio.com');

        $this->service->setReceivers($arr);
        $this->assertEquals($arr[count($arr) - 1],
            $this->service->getReceivers()[count($this->service->getReceivers()) - 1]);
        $this->assertNotEmpty($arr);
        $this->assertIsString($this->service->getReceivers()[count($this->service->getReceivers()) - 1]);
    }

    public function testSetCcArrayOrNull()
    {
        $arr = [];

        $this->service->setCc($arr);
        $this->assertEmpty($this->service->getCc());

        $this->service->setCc(null);
        $this->assertEquals(null, $this->service->getCc());

        return $arr;
    }

    /**
     * @depends testSetCcArrayOrNull
     * @param array $arr
     */
    public function testGetCcNotEmpty(array $arr)
    {
        array_push($arr, 'email@dominio.com');
        $this->service->setCc($arr);
        $this->assertNotEmpty($this->service->getCc());
        $this->assertEquals($arr[count($arr) - 1],
            $this->service->getCc()[count($this->service->getCc()) - 1]);
        $this->assertIsString($this->service->getCc()[count($this->service->getCc()) - 1]);
    }

    public function testSetBccArrayOrNull()
    {
        $arr = [];

        $this->service->setBcc($arr);
        $this->assertEmpty($this->service->getBcc());

        $this->service->setBcc(null);
        $this->assertEquals(null, $this->service->getBcc());

        return $arr;
    }

    /**
     * @depends testSetBccArrayOrNull
     * @param array $arr
     */
    public function testGetBccNotEmpty(array $arr)
    {
        array_push($arr, 'email@dominio.com');
        $this->service->setBcc($arr);

        $this->assertNotEmpty($this->service->getBcc());
        $this->assertEquals($arr[count($arr) - 1],
            $this->service->getBcc()[count($this->service->getBcc()) - 1]);
        $this->assertIsString($this->service->getBcc()[count($this->service->getBcc()) - 1]);
    }

    public function testSetSubjectEmptyOrNUll()
    {
        $this->service->setSubject('');
        $this->assertEmpty($this->service->getSubject());

        $this->service->setSubject(null);
        $this->assertEquals(null, $this->service->getSubject());
    }

    public function testGetSubjectNotEmpty()
    {
        $this->service->setSubject('Aqui vem o assunto');
        $this->assertNotEmpty($this->service->getSubject());
        $this->assertIsString($this->service->getSubject());
    }

    public function testSetHtmlBodyEmptyOrNull()
    {
        $this->service->setHtmlBody('');
        $this->assertEmpty($this->service->getHtmlBody());

        $this->service->setHtmlBody(null);
        $this->assertEquals(null, $this->service->getHtmlBody());
    }

    public function testGetHtmlBodyNotEmpty()
    {
        $html = '<html><head><title>titulo</title></head><body>body</body></html>';

        $this->service->setHtmlBody($html);
        $this->assertNotEmpty($this->service->getHtmlBody());
        $this->assertIsString($this->service->getHtmlBody());
    }

    public function testSetTextBodyEmptyOrNull()
    {
        $this->service->setTextBody('');
        $this->assertEmpty($this->service->getTextBody());

        $this->service->setTextBody(null);
        $this->assertEquals(null, $this->service->getTextBody());
    }

    public function testGetTextBodyNotEmpty()
    {
        $this->service->setTextBody('Lorem Ipsum dest ai ipsum');
        $this->assertNotEmpty($this->service->getTextBody());
        $this->assertIsString($this->service->getTextBody());
    }

    public function testSetEncodingEmptyOrNull()
    {
        $this->service->setEncoding('');
        $this->assertEmpty($this->service->getEncoding());

        $this->service->setEncoding(null);
        $this->assertEquals(null, $this->service->getEncoding());
    }

    public function testGetEncodingNotEmpty()
    {
        $this->service->setEncoding('UTF-8');
        $this->assertNotEmpty($this->service->getEncoding());
        $this->assertIsString($this->service->getEncoding());
    }
}
