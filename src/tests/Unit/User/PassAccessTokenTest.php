<?php
declare(strict_types=1);

namespace Tests\Feature\User;


use App\Domain\User\Token\PassAccessToken;
use Tests\TestCase;

class PassAccessTokenTest extends TestCase
{
    /**
     * @var
     */
    private $token;

    public function setUp(): void
    {

        $this->token = new PassAccessToken();
    }

    public function testObjectType()
    {
        $this->assertInstanceOf("app\Domain\User\Token\PassAccessToken", $this->token);
    }

    public function testSetUuidNotEmpty()
    {
        $this->assertTrue(method_exists($this->token, 'setUuid'));
    }

    public function testSetCcidNotEmpty()
    {
        $this->assertTrue(method_exists($this->token, 'setCcid'));
    }

    public function testGenerateToken()
    {
        $token = $this->token->setUuid(2)->setCcid(10)->generateToken();

        $this->assertInstanceOf("app\Domain\Auth\AuthToken", $token);
        $this->assertTrue(method_exists($token, 'token'));
        $this->assertIsString("string", $token->token());
        $this->assertNotNull($token->token());
    }
}
