<?php
declare(strict_types=1);
namespace Tests\Feature\User;

use App\Domain\User\Entity\User;
use App\Domain\User\Entity\Profile;
use Tests\TestCase;

class ProfileTest extends TestCase
{
    private $mock_user;
    private $user;
    private $profile;

    public function setUp(): void
    {
        parent::setUp();
        $this->mock_user = $this->getMock("user.json");
        $this->mock_user["date_added"] = new \DateTime($this->mock_user["date_added"]);
        $this->mock_user["date_updated"] = new \DateTime($this->mock_user["date_updated"]);
        $this->user = new User($this->mock_user);
        $this->profile = new Profile(['id_profile_pk' => 2]);
    }

    /** 
     * @testdox is instance of Profile Object
    */
    public function testObjectType()
    {
        $this->assertInstanceOf("App\Domain\User\Entity\Profile", $this->profile);
    }

    /** 
     * @testdox id company values
    */
    public function testSetIdCompanyEmpty()
    {
        $this->profile->setIdCompanyFk(0);
        $this->assertEquals(0, $this->profile->getIdCompanyFk());
    }

    public function testSetIdCompany()
    {
        $this->profile->setIdCompanyFk(2);
        $this->assertIsInt($this->profile->getIdCompanyFk());
        $this->assertEquals(2, $this->profile->getIdCompanyFk());
    }

    public function testGetIdCompany()
    {
        $this->assertIsInt($this->profile->getIdCompanyFk());
    }

    public function testIdCompanyParamType()
    {
        $this->expectException(\TypeError::class);
        $this->profile->setIdCompanyFk("2");
    }

    /** 
     * @testdox id profile values
    */
    public function testSetIdProfileEmpty()
    {
        $this->profile->setIdProfilePk(0);
        $this->assertEquals(0, $this->profile->getIdProfilePk());
    }

    public function testSetIdProfile()
    {
        $this->profile->setIdProfilePk(2);
        $this->assertIsInt($this->profile->getIdProfilePk());
        $this->assertEquals(2, $this->profile->getIdProfilePk());
    }

    public function testGetIdProfile()
    {
        $this->assertIsInt($this->profile->getIdProfilePk());
    }

    public function testIdProfileParamType()
    {
        $this->expectException(\TypeError::class);
        $this->profile->setIdProfilePk("2");
    }

    /** 
     * @testdox profile desc group values
    */
    public function testSetNameEmpty()
    {
        $this->profile->setDescGroup('');
        $this->assertEquals('', $this->profile->getDescGroup());
    }

    public function testSetDescGroup()
    {
        $this->profile->setDescGroup('MVP Consulta Plannexo');
        $this->assertIsString($this->profile->getDescGroup());
        $this->assertEquals('MVP Consulta Plannexo', $this->profile->getDescGroup());
    }

    public function testGetDescGroup()
    {
        $this->profile->setDescGroup('MVP Consulta Plannexo');
        $this->assertIsString($this->profile->getDescGroup());
    }

    public function testDescGroupParamType()
    {
        $this->expectException(\TypeError::class);
        $this->profile->setDescGroup(2);
    }
}