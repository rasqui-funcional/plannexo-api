<?php
declare(strict_types=1);
namespace Tests\Feature\User;

use App\Domain\User\Entity\UserEstablishments;
use Tests\TestCase;

/**
 * Class UserEstablishmentsTest
 * @package Tests\Feature\User
 */
class UserEstablishmentsTest extends TestCase
{
    /**
     * @var User\Entity\UserEstablishments
     */
    private $establishments;

    public function setUp(): void
    {
        $this->establishments = new UserEstablishments([
            'establishments' => [2],
            'id_company_fk' => 2,
            'id_user_fk' => 2,
        ]);
    }

    /** 
     * @testdox is instance of User Object
    */
    public function testObjectType()
    {
        $this->assertInstanceOf("app\Domain\User\Entity\UserEstablishments", $this->establishments);
    }

    /** 
     * @testdox id company values
    */
    public function testSetIdCompanyFkEmpty()
    {
        $this->establishments->setIdCompanyFk(0);
        $this->assertEquals(0, $this->establishments->getIdCompanyFk());
    }

    public function testSetIdCompanyFk()
    {
        $this->establishments->setIdCompanyFk(2);
        $this->assertIsInt($this->establishments->getIdCompanyFk());
        $this->assertEquals(2, $this->establishments->getIdCompanyFk());
    }

    public function testGetIdCompanyFk()
    {
        $this->assertIsInt($this->establishments->getIdCompanyFk());
    }

    public function testIdCompanyFkParamType()
    {
        $this->expectException(\TypeError::class);
        $this->establishments->setIdCompanyFk("2");
    }

    /** 
     * @testdox code establishments values
    */
    public function testSetCodeEstablishmentsEmpty()
    {
        $this->establishments->setCodeEstablishments([]);
        $this->assertEquals([], $this->establishments->getCodeEstablishments());
    }

    public function testSetCodeEstablishments()
    {
        $this->establishments->setCodeEstablishments(['01', 'A2']);
        $this->assertIsArray($this->establishments->getCodeEstablishments());
        $this->assertEquals(['01', 'A2'], $this->establishments->getCodeEstablishments());
    }

    public function testGetCodeEstablishments()
    {
        $this->establishments->setCodeEstablishments(['01', 'A2']);
        $this->assertIsArray($this->establishments->getCodeEstablishments());
    }

    public function testCodeEstablishmentsParamType()
    {
        $this->expectException(\TypeError::class);
        $this->establishments->setCodeEstablishments(2);
    }

    /** 
     * @testdox id user values
    */
    public function testSetIdUserFkEmpty()
    {
        $this->establishments->setIdUserFk(0);
        $this->assertEquals(0, $this->establishments->getIdUserFk());
    }

    public function testSetIdUserFk()
    {
        $this->establishments->setIdUserFk(2);
        $this->assertIsInt($this->establishments->getIdUserFk());
        $this->assertEquals(2, $this->establishments->getIdUserFk());
    }

    public function testGetIdUserFk()
    {
        $this->assertIsInt($this->establishments->getIdUserFk());
    }

    public function testIdUserFkParamType()
    {
        $this->expectException(\TypeError::class);
        $this->establishments->setIdUserFk("2");
    }

    /** 
     * @testdox is array
    */
    public function testExchangeArray()
    {
        $this->assertIsArray($this->establishments->exchangeArray());
    }

}