<?php
declare(strict_types=1);

namespace Tests\Unit\User;

use App\Domain\User\Entity\User;
use App\Domain\User\Traits\UserManagersTrait;
use App\Domain\User\UserManagersInterface;
use App\Model\User\UserManagersModel;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

/**
 * Class UserManagerTest
 * @package Tests\Feature\User
 */
class UserManagerTest extends TestCase
{

    private $user_manager;
    private $mock;
    private $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = new User(['id_user_pk' => 2, 'id_company_fk' => 2]);
        $this->mock = $this->getMock('user_managers.json');
        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('orderBy')->andReturnSelf()
            ->shouldReceive('get')->andReturnSelf()
            ->shouldReceive('toArray')
            ->andReturn($this->mock);

        $this->user_manager = new UserManagersModel($this->user);

    }

    /**
     * @testdox is instance of User Object
     */
    public function testObjectType()
    {
        $this->assertInstanceOf(UserManagersInterface::class, $this->user_manager);
    }


    public function testHasMethodGetUserManagers()
    {
        $this->assertTrue(method_exists($this->user_manager, 'getUserManagers'));
    }

    public function testGetUserManagers()
    {

        $result = $this->user_manager->getUserManagers();
        $this->assertSame($this->mock, $result);
    }

    public function testUserManagersModelParamType()
    {
        $this->expectException(\TypeError::class);
        $this->user_manager = new UserManagersModel();
    }

    public function testIsTrait()
    {
        $reflection = new \ReflectionClass(UserManagersTrait::class);
        $this->assertTrue($reflection->isTrait());

        $this->assertTrue($reflection->hasMethod('getUserManagers'));

        $mock = $this->getMockBuilder(UserManagersTrait::class)
            ->setConstructorArgs([$this->user_manager])
            ->getMockForTrait();

        $this->assertEquals($this->mock, $mock->getUserManagers());
    }
}
