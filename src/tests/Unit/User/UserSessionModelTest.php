<?php
declare(strict_types=1);
namespace Tests\Feature\User;

use App\Domain\User\Entity\User;
use App\Model\UserSessionModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Tests\TestCase;

/**
 * Class UserSessionModelTest
 * @package Tests\Feature\User
 */
class UserSessionModelTest extends TestCase
{
    /**
     * @var User\Entity\User
     */
    private $user;

    /**
     * @var App\Model\UserSessionModel
     */
    private $session_model;

    private $instance;
    private $mock_user;
    private $reflection;
    private $method;

    public function setUp(): void
    {
        parent::setUp();
        $this->mock_user = $this->getMock("user.json");
        $this->mock_user["date_added"] = new \DateTime($this->mock_user["date_added"]);
        $this->mock_user["date_updated"] = new \DateTime($this->mock_user["date_updated"]);
        $this->user = new User($this->mock_user);

        $this->session_model = new UserSessionModel($this->user);
        $this->reflection = new \ReflectionClass(UserSessionModel::class);
        $this->instance = $this->reflection->newInstanceArgs([$this->user]);
    }

    private function invoke()
    {
        $method = $this->reflection->getMethod($this->method);
        $method->setAccessible(true);
        return $method->invoke($this->session_model);
    }

    /**
     * @testdox is instance of UserSessionModel Object
    */
    public function testObjectType()
    {
        $this->assertInstanceOf("App\Model\UserSessionModel", $this->session_model);
    }

    /**
     * @testdox session code values
    */
    public function testGetSessionId()
    {
        $this->assertIsString($this->session_model->getSessionId());
    }

    /**
     * @testdox session code values
    */
    public function testSelectUser()
    {
        $this->method = 'selectUser';
        $this->assertEquals($this->session_model, $this->invoke());
    }

    /**
     * @testdox return user columns
    */
    public function testGetColumns()
    {
        $this->method = 'getColumns';
        $this->assertIsArray($this->invoke());
    }

    /**
     * @testdox save session id
    */
    public function testSaveSession()
    {
        Redis::shouldReceive('hmset')->andReturnTrue();
        $this->assertEquals(false, $this->instance->saveSession());
        $this->instance->selectUser();
        $this->instance->prepareSessionData();
        $this->assertEquals(true, $this->instance->saveSession());
    }

    /**
     * @testdox get session id
    */
    public function testGetSession()
    {
        Redis::shouldReceive('hmset')->andReturnTrue();
        Redis::shouldReceive('hgetall')->andReturn([]);
        $this->instance->selectUser();
        $this->instance->prepareSessionData();
        $this->instance->saveSession();

        $this->assertIsArray($this->instance->getSession());
    }

    /**
     * @testdox get user data by parameters id
    */
    public function testGetByParams()
    {
        Redis::shouldReceive('hmset')->andReturnTrue();
        $this->instance->selectUser();
        $this->instance->prepareSessionData();
        $this->instance->saveSession();
        $this->assertEquals('qa@bionexo.com', $this->instance->getByParams('email'));
    }

    /**
     * @testdox get user data
    */
    public function testGetUserData()
    {
        Redis::shouldReceive('hmset')->andReturnTrue();
        $this->instance->selectUser();
        $this->instance->prepareSessionData();
        $this->instance->saveSession();
        $this->assertIsArray($this->instance->getUserData());
    }

    /**
     * @testdox user profile to string
    */
    public function testProfilesToString()
    {
        $reflectionMethod = new \ReflectionMethod($this->session_model, 'profilesToString');
        $reflectionMethod->setAccessible(true);
        $this->assertEquals('', $reflectionMethod->invokeArgs($this->instance->selectUser(), ['']));
    }
}
