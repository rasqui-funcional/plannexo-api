<?php
declare(strict_types=1);

namespace Tests\Unit\User;

use App\Domain\User\Entity\User;
use App\Domain\User\Traits\UserStandardDepositTrait;
use App\Domain\User\UserStandardDepositInterface;
use App\Model\User\UserStandardDepositModel;
use App\Model\UserEstablishmentsModel;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

/**
 * Class UserStandardDepositTest
 * @package Tests\Feature\User
 */
class UserStandardDepositTest extends TestCase
{

    private $user_standard_deposit;
    private $mock;
    private $user;
    private $estabs;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = new User(['id_user_pk' => 2, 'id_company_fk' => 2]);
        $this->mock = $this->getMock('user_standard_deposit.json');
        //TODO Need improve User to retrieve user_estabs to not be necessay invoke class UserEstablishmentsModel here
        $this->estabs = (new UserEstablishmentsModel($this->user->getEstablishments()))->getEstablishmentsCodes();

        DB::shouldReceive('table')
            ->andReturnSelf()
            ->shouldReceive('raw')->andReturnSelf()
            ->shouldReceive('join')->andReturnSelf()
            ->shouldReceive('where')->andReturnSelf()
            ->shouldReceive('whereIn')->andReturnSelf()
            ->shouldReceive('orderBy')->andReturnSelf()
            ->shouldReceive('get')->andReturnSelf()
            ->shouldReceive('toArray')
            ->andReturn($this->mock);

        $this->user_standard_deposit = (new UserStandardDepositModel($this->user))->setEstabs( $this->estabs);

    }

    /**
     * @testdox is instance of User Object
     */
    public function testObjectType()
    {
        $this->assertInstanceOf(UserStandardDepositInterface::class, $this->user_standard_deposit);
    }


    public function testHasMethodGetUserStandardDeposit()
    {
        $this->assertTrue(method_exists($this->user_standard_deposit, 'getUserStandardDeposit'));
    }

    public function testGetUserStandardDeposit()
    {

        $result = $this->user_standard_deposit->getUserStandardDeposit();
        $this->assertSame($this->mock, $result);
    }

    public function testUserStandardModelParamType()
    {
        $this->expectException(\TypeError::class);
        $this->user_standard_deposit = new UserStandardDepositModel();
    }

    public function testIsTrait()
    {
        $reflection = new \ReflectionClass(UserStandardDepositTrait::class);
        $this->assertTrue($reflection->isTrait());

        $this->assertTrue($reflection->hasMethod('getUserStandardDeposit'));

        $mock = $this->getMockBuilder(UserStandardDepositTrait::class)
            ->setConstructorArgs([$this->user_standard_deposit])
            ->getMockForTrait();

        $this->assertEquals($this->mock, $mock->setEstabs($this->estabs)->getUserStandardDeposit());
    }
}
