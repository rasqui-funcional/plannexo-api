<?php
declare(strict_types=1);

namespace Tests\Feature\User;

use App\Domain\User\Entity\User;
use App\Domain\User\Entity\Profile;
use App\Domain\User\Entity\UserEstablishments;
use Tests\TestCase;

/**
 * Class UserTest
 * @package Tests\Feature\User
 */
class UserTest extends TestCase
{
    /**
     * @var User\Entity\User
     */
    private $user;

    /**
     * @var User\Entity\Profile
     */
    private $profile;

    /**
     * @var User\Entity\UserEstablishments
     */
    private $user_establishments;

    /**
     * @var DateTime;
     */
    private $datetime;

    /**
     * @var array;
     */
    private $mock_data;


    public function setUp(): void
    {
        $this->mock_data = $this->getMock("user.json");
        $this->mock_data["date_added"] = new \DateTime($this->mock_data["date_added"]);
        $this->mock_data["date_updated"] = new \DateTime($this->mock_data["date_updated"]);

        $this->user = new User($this->mock_data);

        $this->profile = new Profile(['id_profile_pk' => 2]);

        $this->user_establishments = new UserEstablishments([
            'establishments' => [2],
            'id_company_fk' => 2,
            'id_user_fk' => 2,
        ]);

        $this->datetime = $this->mock_data["date_added"];
    }

    /**
     * @testdox is instance of User Object
    */
    public function testObjectType()
    {
        $this->assertInstanceOf("app\Domain\User\Entity\User", $this->user);
    }

    /**
     * @testdox id user values
    */
    public function testSetIdUserEmpty()
    {
        $this->user->setIdUserPk(null);
        $this->assertEquals(null, $this->user->getIdUserPk());
    }

    public function testSetIdUser()
    {
        $this->user->setIdUserPk(2);
        $this->assertIsInt($this->user->getIdUserPk());
        $this->assertEquals(2, $this->user->getIdUserPk());
    }

    public function testGetIdUser()
    {
        $this->assertIsInt($this->user->getIdUserPk());
    }

    public function testIdUserParamType()
    {
        $this->expectException(\TypeError::class);
        $this->user->setIdUserPk("2");
    }

    /**
     * @testdox id company values
    */
    public function testSetIdCompanyEmpty()
    {
        $this->user->setIdCompanyFk(0);
        $this->assertIsInt($this->user->getIdCompanyFk());
        $this->assertEquals(0, $this->user->getIdCompanyFk());
    }

    public function testSetIdCompany()
    {
        $this->user->setIdCompanyFk(2);
        $this->assertIsInt($this->user->getIdCompanyFk());
        $this->assertEquals(2, $this->user->getIdCompanyFk());
    }

    public function testGetIdCompany()
    {
        $this->assertIsInt($this->user->getIdCompanyFk());
    }

    public function testIdCompanyParamType()
    {
        $this->expectException(\TypeError::class);
        $this->user->setIdCompanyFk("2");
    }

    /**
     * @testdox user name values
    */
    public function testSetNameEmpty()
    {
        $this->user->setName(null);
        $this->assertEquals(null, $this->user->getName());
    }

    public function testSetName()
    {
        $this->user->setName('QA');
        $this->assertIsString($this->user->getName());
        $this->assertEquals('QA', $this->user->getName());
    }

    public function testGetName()
    {
        $this->user->setName('QA');
        $this->assertIsString($this->user->getName());
    }

    public function testNameParamType()
    {
        $this->expectException(\TypeError::class);
        $this->user->setName(2);
    }

    /**
     * @testdox user login values
    */
    public function testSetLoginEmpty()
    {
        $this->user->setLogin(null);
        $this->assertEquals(null, $this->user->getLogin());
    }

    public function testSetLogin()
    {
        $this->user->setLogin('sysadmin');
        $this->assertIsString($this->user->getLogin());
        $this->assertEquals('sysadmin', $this->user->getLogin());
    }

    public function testGetLogin()
    {
        $this->user->setLogin('sysadmin');
        $this->assertIsString($this->user->getLogin());
    }

    public function testLoginParamType()
    {
        $this->expectException(\TypeError::class);
        $this->user->setLogin(2);
    }

    /**
     * @testdox user type values
    */
    public function testSetTypeEmpty()
    {
        $this->user->setType(0);
        $this->assertEquals(0, $this->user->getType());
    }

    public function testSetType()
    {
        $this->user->setType(1);
        $this->assertIsInt($this->user->getType());
        $this->assertEquals(1, $this->user->getType());
    }

    public function testGetType()
    {
        $this->user->setType(1);
        $this->assertIsInt($this->user->getType());
    }

    public function testTypeParamType()
    {
        $this->expectException(\TypeError::class);
        $this->user->setType("2");
    }

    /**
     * @testdox user email values
    */
    public function testSetMailLoginEmpty()
    {
        $this->user->setMailLogin('');
        $this->assertEquals('', $this->user->getMailLogin());
    }

    public function testSetMailLogin()
    {
        $this->user->setMailLogin('qa@bionexo.com');
        $this->assertIsString($this->user->getMailLogin());
        $this->assertEquals('qa@bionexo.com', $this->user->getMailLogin());
    }

    public function testGetMailLogin()
    {
        $this->user->setMailLogin('qa@bionexo.com');
        $this->assertIsString($this->user->getMailLogin());
    }

    public function testMailLoginParamType()
    {
        $this->expectException(\TypeError::class);
        $this->user->setMailLogin(1);
    }

    /**
     * @testdox user erp code values
    */
    public function testSetCodUserErpEmpty()
    {
        $this->user->setCodUserErp(null);
        $this->assertEquals(null, $this->user->getCodUserErp());
    }

    public function testSetCodUserErp()
    {
        $this->user->setCodUserErp('ABC01');
        $this->assertIsString($this->user->getCodUserErp());
        $this->assertEquals('ABC01', $this->user->getCodUserErp());
    }

    public function testGetCodUserErp()
    {
        $this->user->setCodUserErp('ABC01');
        $this->assertIsString($this->user->getCodUserErp());
    }

    public function testCodUserErpParamType()
    {
        $this->expectException(\TypeError::class);
        $this->user->setCodUserErp(2);
    }

    /**
     * @testdox ddd values
    */
    public function testSetDddEmpty()
    {
        $this->user->setDdd(null);
        $this->assertEquals(null, $this->user->getDdd());
    }

    public function testSetDdd()
    {
        $this->user->setDdd('011');
        $this->assertIsString($this->user->getDdd());
        $this->assertEquals('011', $this->user->getDdd());
    }

    public function testGetDdd()
    {
        $this->user->setDdd('011');
        $this->assertIsString($this->user->getDdd());
    }

    public function testDddParamType()
    {
        $this->expectException(\TypeError::class);
        $this->user->setDdd(2);
    }

    /**
     * @testdox tel values
    */
    public function testSetTelEmpty()
    {
        $this->user->setTel(null);
        $this->assertEquals(null, $this->user->getTel());
    }

    public function testSetTel()
    {
        $this->user->setTel('99999-9999');
        $this->assertIsString($this->user->getTel());
        $this->assertEquals('99999-9999', $this->user->getTel());
    }

    public function testGetTel()
    {
        $this->user->setTel('99999-9999');
        $this->assertIsString($this->user->getTel());
    }

    public function testTelParamType()
    {
        $this->expectException(\TypeError::class);
        $this->user->setTel(999999999);
    }

    /**
     * @testdox user active values
    */
    public function testSetActiveEmpty()
    {
        $this->user->setActive(0);
        $this->assertEquals(0, $this->user->getActive());
    }

    public function testSetActive()
    {
        $this->user->setActive(1);
        $this->assertIsInt($this->user->getActive());
        $this->assertEquals(1, $this->user->getActive());
    }

    public function testGetActive()
    {
        $this->user->setActive(1);
        $this->assertIsInt($this->user->getActive());
    }

    public function testActiveParamType()
    {
        $this->expectException(\TypeError::class);
        $this->user->setActive('active');
    }

    /**
     * @testdox password values
    */
    public function testSetPasswordEmpty()
    {
        $this->user->setPassword('');
        $this->assertEquals(hash('sha256',''), $this->user->getPassword());
    }

    public function testSetPassword()
    {
        $this->user->setPassword('123456');
        $this->assertIsString($this->user->getPassword());
        $this->assertEquals(hash('sha256','123456'), $this->user->getPassword());
    }

    public function testGetPassword()
    {
        $this->user->setPassword('123456');
        $this->assertIsString($this->user->getPassword());
    }

    public function testPasswordParamType()
    {
        $this->expectException(\TypeError::class);
        $this->user->setPassword(123456);
    }

    /**
     * @testdox user bio id reference values
    */
    public function testSetBioidReferenceEmpty()
    {
        $this->user->setBioidReference(0);
        $this->assertEquals(0, $this->user->getBioidReference());
    }

    public function testSetBioidReference()
    {
        $this->user->setBioidReference(123456);
        $this->assertIsInt($this->user->getBioidReference());
        $this->assertEquals(123456, $this->user->getBioidReference());
    }

    public function testGetBioidReference()
    {
        $this->user->setBioidReference(1);
        $this->assertIsInt($this->user->getBioidReference());
    }

    public function testBioidReferenceParamType()
    {
        $this->expectException(\TypeError::class);
        $this->user->setBioidReference("1");
    }

    /**
     * @testdox user bio id reference company value
    */
    public function testSetRefCompanyBioidEmpty()
    {
        $this->user->setRefCompanyBioid(0);
        $this->assertEquals(0, $this->user->getRefCompanyBioid());
    }

    public function testSetRefCompanyBioid()
    {
        $this->user->setRefCompanyBioid(1);
        $this->assertIsInt($this->user->getRefCompanyBioid());
        $this->assertEquals(1, $this->user->getRefCompanyBioid());
    }

    public function testGetRefCompanyBioid()
    {
        $this->user->setRefCompanyBioid(1);
        $this->assertIsInt($this->user->getRefCompanyBioid());
    }

    public function testRefCompanyBioidParamType()
    {
        $this->expectException(\TypeError::class);
        $this->user->setRefCompanyBioid("1");
    }

    /**
     * @testdox user companies values
    */
    public function testSetCompaniesEmpty()
    {
        $this->user->setCompanies([]);
        $this->assertEquals([], $this->user->getCompanies());
    }

    public function testSetCompanies()
    {
        $this->user->setCompanies([1, 2, 3]);
        $this->assertIsArray($this->user->getCompanies());
        $this->assertEquals([1, 2, 3], $this->user->getCompanies());
    }

    public function testGetCompanies()
    {
        $this->user->setCompanies([1, 2, 3]);
        $this->assertIsArray($this->user->getCompanies());
    }

    public function testCompaniesParamType()
    {
        $this->expectException(\TypeError::class);
        $this->user->setCompanies('1');
        $this->user->setCompanies(1);
        $this->user->setCompanies('');
    }

    /**
     * @testdox user company id value
    */
    public function testSetIdCompanyFkEmpty()
    {
        $this->user->setIdCompanyFk(0);
        $this->assertEquals(0, $this->user->getIdCompanyFk());
    }

    public function testSetIdCompanyFk()
    {
        $this->user->setIdCompanyFk(1);
        $this->assertIsInt($this->user->getIdCompanyFk());
        $this->assertEquals(1, $this->user->getIdCompanyFk());
    }

    public function testGetIdCompanyFk()
    {
        $this->user->setIdCompanyFk(1);
        $this->assertIsInt($this->user->getIdCompanyFk());
    }

    public function testIdCompanyFkParamType()
    {
        $this->expectException(\TypeError::class);
        $this->user->setIdCompanyFk("1");
    }

    /**
     * @testdox user date added value
    */
    public function testSetDateAddedEmpty()
    {
        $this->user->setDateAdded($this->datetime);
        $this->assertEquals($this->datetime, $this->user->getDateAdded());
    }

    public function testSetAndGetDateAdded()
    {
        $this->user->setDateAdded($this->datetime);
        $this->assertIsObject($this->user->getDateAdded());
        $this->assertEquals($this->datetime, $this->user->getDateAdded());
    }

    public function testDateAddedParamType()
    {
        $this->expectException(\TypeError::class);
        $this->user->setDateAdded('1');
    }

    /**
     * @testdox user date updated value
    */
    public function testSetDateUpdatedEmpty()
    {
        $this->user->setDateUpdated($this->datetime);
        $this->assertEquals($this->datetime, $this->user->getDateUpdated());
    }

    public function testSetAndGetDateUpdated()
    {
        $this->user->setDateUpdated($this->datetime);
        $this->assertIsObject($this->user->getDateUpdated());
        $this->assertEquals($this->datetime, $this->user->getDateUpdated());
    }

    public function testDateUpdatedParamType()
    {
        $this->expectException(\TypeError::class);
        $this->user->setDateUpdated('1');
    }

    /**
     * @testdox user date last access value
    */
    public function testSetDateLastAccessEmpty()
    {
        $this->user->setDateLastAccess($this->datetime);
        $this->assertEquals($this->datetime, $this->user->getDateLastAccess());
    }

    public function testSetAndGetDateLastAccess()
    {
        $this->user->setDateLastAccess($this->datetime);
        $this->assertIsObject($this->user->getDateLastAccess());
        $this->assertEquals($this->datetime, $this->user->getDateLastAccess());
    }

    public function testDateLastAccessParamType()
    {
        $this->expectException(\TypeError::class);
        $this->user->setDateLastAccess('1');
    }

    /**
     * @testdox is instance of UserProfile Object
    */
    public function testObjectTypeProfile()
    {
        $this->user->setProfile($this->profile);
        $this->assertInstanceOf("app\Domain\User\Entity\Profile", $this->user->getProfile());
    }

    /**
     * @testdox is instance of UserEstablishments Object
    */
    public function testObjectTypeUserEstablishments()
    {
        $this->user->setEstablishments($this->user_establishments);
        $this->assertInstanceOf("app\Domain\User\Entity\UserEstablishments", $this->user->getEstablishments());
    }

    public function testInitialName()
    {
        $this->user->setName('QU AN');
        $this->assertEquals('QA', $this->user->findInitialsName(2));

        $this->user->setName('U');
        $this->assertEquals('U', $this->user->findInitialsName(2));
    }

    /**
     * @testdox is array
    */
    public function testExchangeArray()
    {
        $this->assertIsArray($this->user->exchangeArray());
    }

    /**
     * @testdox is array
    */
    public function testToArray()
    {
        $this->assertIsArray($this->user->toArray());
    }
}
